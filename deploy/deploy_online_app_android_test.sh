#!/usr/bin/env bash
export SENTRY_PROPERTIES=./android/sentry.properties;
ENV=Development
VERSION=1.4
BUILD_GRADLE=./android/app/build.gradle

[[ ! -d ./build ]] && mkdir -p ./build

yarn install && \
appcenter codepush release-react --app luult-giigaa.com/android_online_app -d ${ENV} -t ${VERSION} --output-dir ./build && \
sentry-cli react-native appcenter --bundle-id com.giigaa.online_app --deployment ${ENV} luult-giigaa.com/android_online_app android ./build/CodePush/