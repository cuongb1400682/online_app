#!/bin/bash

export SENTRY_PROPERTIES=./ios/sentry.properties
ENV=Development
VERSION=1.4
PLIST_FILE=./ios/online_app/Info.plist

[[ ! -d ./build ]] && mkdir -p ./build

yarn install && \
appcenter codepush release-react --app luult-giigaa.com/test-ci -d ${ENV} -t ${VERSION} --output-dir ./build && \
sentry-cli react-native appcenter --bundle-id com.giigaa.online-app --deployment ${ENV} luult-giigaa.com/test-ci ios ./build/CodePush/
