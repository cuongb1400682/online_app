#import "DebugUtils.h"
#import <Foundation/Foundation.h>

@implementation DebugUtils

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(logString:(NSString *) string) {
  NSLog(@"online_app_log: %@", string);
}

- (NSDictionary *)constantsToExport
{
  NSString * currentDeploymentKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CodePushDeploymentKey"];
  NSString * facebookAppId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"];
  BOOL isDebug = NO;
  
  NSURL * googleServiceInfoURL = [[NSBundle mainBundle] URLForResource:@"GoogleService-Info" withExtension:@"plist"];
  NSDictionary * googleServiceInfo = [NSDictionary dictionaryWithContentsOfURL:googleServiceInfoURL];
  NSString * googleClientId = [googleServiceInfo valueForKey:@"CLIENT_ID"];
  
#ifdef DEBUG
  isDebug = YES;
#endif
  
  return @{
           @"currentDeploymentKey": currentDeploymentKey,
           @"facebookAppId": facebookAppId,
           @"googleClientId":googleClientId,
           @"isDebug": @(isDebug)
           };
}

@end
