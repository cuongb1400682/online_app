import {AsyncStorage} from "react-native";

import registerScreens from "src/navigation/registerScreens";
import startTabBasedApp from "src/navigation/startTabBasedApp";
import {setupLocalization} from "src/localization/localization";
import {initializeSessionKey} from "src/utils/userAuth/session";
import {syncBundle} from "src/utils/codePush/codePush";
import {setupGoogleAnalytics} from "src/utils/firebase/analytics";
import overrideFormStyleSheet from "src/styles/overrideFormStyleSheet";
import setupSentry from "src/utils/sentry/setupSentry";
import {DEPLOYMENT_KEY} from "src/utils/codePush/deploymentChanging";
import deploymentKeys from "src/constants/deploymentKeys";
import {checkPermissionPushNotify} from "src/utils/firebase/notifications";
import {setupAppFont} from "src/utils/reactNative/fonts";
import {setupNetworkConnectivityListener} from "src/utils/misc/networkConnectivity";

const asyncInitialize = async () => {
    const deploymentKey = await AsyncStorage.getItem(DEPLOYMENT_KEY) || deploymentKeys.production;
    await Promise.all([
        checkPermissionPushNotify(),
        syncBundle(deploymentKey),
        initializeSessionKey(),
        setupSentry(deploymentKey)
    ]);
};

const startApp = async () => {
    overrideFormStyleSheet();
    setupLocalization();
    setupGoogleAnalytics();
    setupAppFont();
    registerScreens();
    setupNetworkConnectivityListener(asyncInitialize);
    await startTabBasedApp();
};

export {
    startApp
};
