import {Navigation} from "react-native-navigation";

import screenIds from "src/constants/screenIds";
import {tr} from "src/localization/localization";
import colors from "src/constants/colors";
import * as tabBarIcons from "src/assets/static/icons/tabBarIcons/tabBarIcons";
import {systemFontName} from "src/styles/systemFont";

const tabBarStyles = {
    tabBarButtonColor: "#666",
    tabBarSelectedButtonColor: colors.primaryColor,
    tabBarBackgroundColor: "#fff",
    orientation: "portrait"
};

const startTabBasedApp = async () => await Navigation.startTabBasedApp({
    tabs: [
        {
            label: tr("main_tab_bar_home_title"),
            screen: screenIds.HOME,
            icon: tabBarIcons.home,
            selectedIcon: tabBarIcons.homeFilled
        },
        {
            label: tr("main_tab_bar_catalog_title"),
            screen: screenIds.CATEGORIES,
            icon: tabBarIcons.category,
            selectedIcon: tabBarIcons.categoryFilled
        },
        {
            label: tr("main_tab_bar_notification_title"),
            screen: screenIds.NOTIFICATION,
            icon: tabBarIcons.notify,
            selectedIcon: tabBarIcons.notifyFilled
        },
        {
            label: tr("main_tab_bar_account_title"),
            screen: screenIds.ACCOUNT,
            icon: tabBarIcons.user,
            selectedIcon: tabBarIcons.userFilled
        },
    ],
    tabsStyle: {
        initialTabIndex: 0,
        ...tabBarStyles
    },
    appStyle: {
        tabFontSize: 10,
        selectedTabFontSize: 10,
        tabFontFamily: systemFontName,
        forceTitlesDisplay: true,
        ...tabBarStyles,
    }
});

export default startTabBasedApp;
