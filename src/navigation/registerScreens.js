import {Navigation} from "react-native-navigation";
import {Provider} from "react-redux";

import store from "src/redux/store";
import screenIds from "src/constants/screenIds";
import Home from "src/pages/Home/Home";
import CatalogByCategory from "src/pages/Catalog/CatalogByCategory/CatalogByCategory";
import Account from "src/pages/UserAuth/Account/Account";
import ProductDetail from "src/pages/ProductDetail/ProductDetail";
import Cart from "src/pages/Cart/Cart";
import PaymentStep from "src/pages/Checkout/PaymentStep/PaymentStep";
import ShippingStep from "src/pages/Checkout/ShippingStep/ShippingStep";
import OrderSuccess from "src/pages/Checkout/OrderSuccess/OrderSuccess";
import SearchResult from "src/pages/SearchResult/SearchResult";
import Search from "src/pages/Search/Search";
import Login from "src/pages/UserAuth/Login/Login";
import ProfileManager from "src/pages/ProfileManager/ProfileManager/ProfileManager";
import UnderDevelopment from "src/pages/UnderDevelopment/UnderDevelopment";
import Categories from "src/pages/Categories/Categories";
import BestSaleProduct from "src/pages/BestSaleProduct/BestSaleProduct";
import NewProduct from "src/pages/NewProduct/NewProduct";
import PromotedProduct from "src/pages/PromotedProduct/PromotedProduct";
import LoginWithGiigaaID from "src/pages/UserAuth/LoginWithGiigaaID/LoginWithGiigaaID";
import MyAccount from "src/pages/ProfileManager/MyAccount/MyAccount";
import Notification from "src/pages/Notification/Notification";
import OrderManager from "src/pages/ProfileManager/OrderManager/OrderManager";
import OrderDetail from "src/pages/ProfileManager/OrderDetail/OrderDetail";
import AddressDetail from "src/pages/Address/AddressDetail/AddressDetail";
import ApplyCoupon from "src/pages/Checkout/ApplyCoupon/ApplyCoupon";
import CouponManager from "src/pages/ProfileManager/CouponManager/CouponManager";
import DealList from "src/pages/DealList/DealList";
import LandingPage from "src/pages/LandingPage/LandingPage";
import UserRegistration from "src/pages/UserAuth/UserRegistration/UserRegistration";
import UserForgotPassword from "src/pages/UserAuth/UserForgotPassword/UserForgotPassword";
import ViewedProducts from "src/pages/ProfileManager/ViewedProducts/ViewedProducts";

const registerReduxComponent = ([screenId, Component]) => Navigation.registerComponent(
    screenId, () => Component, store, Provider
);

const registerScreens = () => Object.entries({
    // Main Tabs
    [screenIds.HOME]: Home,
    [screenIds.CATEGORIES]: Categories,
    [screenIds.NOTIFICATION]: Notification,
    [screenIds.ACCOUNT]: Account,
    // Cart
    [screenIds.CART]: Cart,
    // Checkout
    [screenIds.CHECKOUT_PAYMENT_STEP]: PaymentStep,
    [screenIds.CHECKOUT_SHIPPING_STEP]: ShippingStep,
    [screenIds.CHECKOUT_SUCCESS_STEP]: OrderSuccess,
    // Product Detail
    [screenIds.PRODUCT_DETAIL]: ProductDetail,
    // Search and Listing
    [screenIds.CATALOG_BY_CATEGORY]: CatalogByCategory,
    [screenIds.SEARCH_RESULT]: SearchResult,
    [screenIds.SEARCH]: Search,
    // User Authentication
    [screenIds.LOGIN]: Login,
    [screenIds.LOGIN_WITH_GIIGAA_ID]: LoginWithGiigaaID,
    [screenIds.USER_REGISTRATION]: UserRegistration,
    // Profile Manager
    [screenIds.PROFILE_MANAGER]: ProfileManager,
    [screenIds.ORDER_MANAGER]: OrderManager,
    [screenIds.ORDER_DETAIL]: OrderDetail,
    [screenIds.MY_ACCOUNT]: MyAccount,
    [screenIds.USER_FORGOT_PASSWORD]: UserForgotPassword,
    [screenIds.VIEWED_PRODUCTS]: ViewedProducts,
    // Under Development
    [screenIds.UNDER_DEVELOPMENT]: UnderDevelopment,
    // Best Sale
    [screenIds.BEST_SALE_PRODUCT]: BestSaleProduct,
    [screenIds.NEW_PRODUCT]: NewProduct,
    [screenIds.PROMOTED_PRODUCT]: PromotedProduct,
    // Address
    [screenIds.ADDRESS_DETAIL]: AddressDetail,
    // Coupon
    [screenIds.COUPON_MANAGER]: CouponManager,
    [screenIds.APPLY_COUPON]: ApplyCoupon,
    // Deal
    [screenIds.DEAL_LIST]: DealList,
    // Landing Page
    [screenIds.LANDING_PAGE]: LandingPage
}).forEach(registerReduxComponent);

export default registerScreens;
