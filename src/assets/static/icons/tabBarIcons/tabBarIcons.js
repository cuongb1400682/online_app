import home from "./home.png";
import category from "./category.png";
import notify from "./notify.png";
import user from "./user.png";
import homeFilled from "./home-filled.png";
import categoryFilled from "./category-filled.png";
import notifyFilled from "./notify-filled.png";
import userFilled from "./user-filled.png";

export {
    home,
    category,
    notify,
    user,
    homeFilled,
    categoryFilled,
    notifyFilled,
    userFilled,
};
