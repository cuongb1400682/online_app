import RNLanguages from "react-native-languages";

import store from "src/redux/store"
import {changeSystemLanguage} from "src/redux/common/localizationActions";
import viDictionary from "./strings/vi";
import enDictionary from "./strings/en";
import {formatString} from "src/utils/extensions/strings";

const DEFAULT_LANGUAGE = "vi";

const dictionaries = ({
    "en": enDictionary,
    "vi": viDictionary
});

const languageChangeEventHandler = (language) => {
    store.dispatch(changeSystemLanguage(language));
};

const getSystemLanguage = () => {
    // TODO: for debugging
    return DEFAULT_LANGUAGE // (RNLanguages.language || DEFAULT_LANGUAGE).substr(0, 2);
};

const getStoredLanguage = () => {
    const language = store.getState().localization.current_language || DEFAULT_LANGUAGE;

    return language.substr(0, 2);
};

const getSuitableDictionary = () => {
    const language = getStoredLanguage();

    if (!dictionaries[language]) {
        console.warn(`"${language}.json" not found. Use the default ${DEFAULT_LANGUAGE}.json`);
        return dictionaries[DEFAULT_LANGUAGE];
    } else {
        return dictionaries[language];
    }
};

const setupLocalization = () => {
    RNLanguages.addEventListener("change", languageChangeEventHandler);
};

const tr = (string = "", ...args) => {
    const localizedString = string.toLowerCase();
    const dictionary = getSuitableDictionary();

    if (!dictionary[localizedString]) {
        return localizedString;
    }

    return formatString(dictionary[localizedString], ...args);
};

export {
    getSystemLanguage,
    setupLocalization,
    tr
};
