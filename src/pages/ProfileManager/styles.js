import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    orderDetailSection: {
        flexWrap: "wrap",
        paddingHorizontal: 8,
        paddingVertical: 16
    },
    hasBottomBorder: {
        borderStyle: "solid",
        borderColor: "#ddd",
        borderBottomWidth: 0.5
    },
    commonText: {
        fontSize: 12
    }
});

export default styles;
