import React, {Component} from "react";
import {View} from "react-native";
import {TabView} from "react-native-tab-view";

import commonStyles from "src/styles/commonStyles";
import NavBar from "src/containers/NavBar/NavBar";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import {tr} from "src/localization/localization";
import TabBar from "src/components/base/TabBar/TabBar";
import UserInfo from "src/pages/ProfileManager/containers/UserInfo/UserInfo";
import ChangePassword from "src/pages/ProfileManager/containers/ChangePassword/ChangePassword";

const routeKeys = {
    USER_INFO: "user-info",
    CHANGE_PASSWORD: "change-password"
};

const routes = [
    {
        key: routeKeys.USER_INFO,
        title: tr("my_account_user_info_tab_title")
    },
    // {
    //     key: routeKeys.CHANGE_PASSWORD,
    //     title: tr("my_account_change_password_tab_title")
    // }
];

class MyAccount extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        tabBarHidden: true
    };

    constructor(props) {
        super(props);

        this.state = {
            tabIndex: 0
        };
    }

    changeTabIndex = index => this.setState({
        tabIndex: index
    });

    renderNavBar = () => {
        const {navigator} = this.props;

        return (
            <NavBar
                navigator={navigator}
                title={tr("my_account_nav_bar_title")}
                right={
                    <NavBarShortcuts
                        showsCart
                        showsSearch
                        showsVerticalEllipsis
                        navigator={navigator}
                    />
                }
            />
        );
    };

    renderScene = ({route = {}}) => {
        return ({
            [routeKeys.USER_INFO]: <UserInfo/>,
            [routeKeys.CHANGE_PASSWORD]: <ChangePassword/>
        })[route.key] || <View/>;
    };

    render() {
        const {tabIndex} = this.state;

        return (
            <View style={commonStyles.matchParent}>
                {this.renderNavBar()}
                <TabView
                    style={commonStyles.matchParent}
                    navigationState={{
                        index: tabIndex,
                        routes: routes
                    }}
                    onIndexChange={this.changeTabIndex}
                    renderScene={this.renderScene}
                    renderTabBar={props => <TabBar {...props}/>}
                    animationEnabled
                    swipeEnabled
                    keyboardDismissMode="on-drag"
                    useNativeDriver
                    tabBarPosition="top"
                />
            </View>
        );
    }
}

export default MyAccount;
