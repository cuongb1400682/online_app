import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    separator: {
        height: 5
    },
    emptyListContainer: {
        height: 64,
        width: "100%",
        ...commonStyles.centerChildren
    },
    emptyListText: {
        fontSize: 12,
        color: "#222"
    },
    flatList: {
        marginHorizontal: 6,
        marginTop: 6,
    },
    couponItemContainer: {
        width: "100%",
        ...commonStyles.centerChildren
    }
});

export default styles;
