import React, {Component} from "react";
import {FlatList, RefreshControl, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import commonStyles from "src/styles/commonStyles";
import {tr} from "src/localization/localization";
import {fetchMyCoupons} from "src/pages/ProfileManager/profileManagerActions";
import CouponTicket from "src/components/base/CouponTicket/CouponTicket";
import LazyTabView from "src/components/base/LazyTabView/LazyTabView";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";
import Text from "src/components/platformSpecific/Text/Text";

const routeKeys = {
    ACTIVE: "ACTIVE",
    USED: "USED",
    EXPIRED: "EXPIRED"
};

const routes = [
    {
        key: routeKeys.ACTIVE,
        title: tr("coupon_manager_active")
    },
    {
        key: routeKeys.USED,
        title: tr("coupon_manager_used")
    },
    {
        key: routeKeys.EXPIRED,
        title: tr("coupon_manager_expired")
    },
];

class CouponManager extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        tabBarHidden: true
    };

    static propTypes = {
        coupons: PropTypes.array,
        fetching: PropTypes.bool,
        fetchMyCoupons: PropTypes.func,
    };

    static defaultProps = {
        coupons: [],
        fetching: false,
        fetchMyCoupons: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            tabIndex: 0
        };
    }

    componentDidMount() {
        this.fetchMyCoupons();
    }

    fetchMyCoupons = () => {
        const {tabIndex} = this.state;
        const {key} = routes[tabIndex];

        this.props.fetchMyCoupons({status: key});
    };

    changeTabIndex = index => {
        this.setState({
            tabIndex: index
        }, this.fetchMyCoupons);
    };

    renderRefreshControl = () => {
        const {fetching} = this.props;

        return (
            <RefreshControl
                refreshing={fetching}
                onRefresh={this.fetchMyCoupons}
            />
        );
    };

    renderCouponItem = ({item: coupon = {}}) => (
        <CouponTicket coupon={coupon}/>
    );

    renderScene = () => {
        const {coupons} = this.props;

        return (
            <FlatList
                style={styles.flatList}
                renderItem={this.renderCouponItem}
                data={coupons}
                keyExtractor={(_, index) => `${index}`}
                refreshControl={this.renderRefreshControl()}
                ListEmptyComponent={
                    <View style={styles.emptyListContainer}>
                        <Text style={styles.emptyListText}>{tr("coupon_manager_empty_list")}</Text>
                    </View>
                }
                ItemSeparatorComponent={() => <View style={styles.separator}/>}
            />
        );
    };

    render() {
        const {tabIndex} = this.state;
        const {navigator} = this.props;

        return (
            <View style={commonStyles.matchParent}>
                <NavBar title={tr("coupon_manager_nav_bar_title")} navigator={navigator}/>
                <LazyTabView
                    renderScene={this.renderScene}
                    routes={routes}
                    currentRouteIndex={tabIndex}
                    onChangeTab={this.changeTabIndex}
                />
                <FacebookMessenger floating/>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    coupons: state.profile_manager.coupon_manager.coupons,
    fetching: state.profile_manager.coupon_manager.fetching
});

export default connect(mapStateToProps, {
    fetchMyCoupons,
})(CouponManager);
