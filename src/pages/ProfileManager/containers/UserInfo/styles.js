import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    userInfo: {
        flex: 1,
        flexDirection: "column",
        paddingHorizontal: 10,
        paddingVertical: 16,
        backgroundColor: "#fff"
    },
    avatarContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 14
    },
    changeAvatarLink: {
        color: colors.primaryColor,
        fontSize: 12,
        marginLeft: 10
    },
    updateButton: {
        width: 125,
        height: 36,
        alignSelf: "center",
        marginTop: 10,
        marginBottom: 100
    },
    form: {}
});

export default styles;
