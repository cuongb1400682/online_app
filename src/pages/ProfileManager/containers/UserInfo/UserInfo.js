import React, {Component} from "react";
import {View} from "react-native";
import * as tComb from "tcomb-form-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import Avatar from "src/components/base/svgIcons/Avatar/Avatar";
import LinkButton from "src/components/base/LinkButton/LinkButton";
import {tr} from "src/localization/localization";
import colors from "src/constants/colors";
import GenderSelect from "src/pages/ProfileManager/components/GenderSelect/GenderSelect";
import DatePicker from "src/pages/ProfileManager/components/DatePicker/DatePicker";
import {clearProfileUserInfo, fetchUserInformation} from "src/pages/ProfileManager/profileManagerActions";
import KeyboardAwareView from "src/components/platformSpecific/KeyboardAwareView/KeyboardAwareView";

const formStructure = tComb.struct({
    fullName: tComb.String,
    phoneNumber: tComb.String,
    email: tComb.String,
    gender: tComb.String,
    dateOfBirth: tComb.Date
});

const commonTextInputOptions = {
    selectionColor: colors.primaryColor,
    autoCapitalize: "none",
    allowFontScaling: false
};

const formOptions = {
    fields: {
        fullName: {
            label: tr("my_account_user_info_form_full_name_label"),
            placeholder: tr("my_account_user_info_form_full_name_placeholder"),
            ...commonTextInputOptions
        },
        phoneNumber: {
            label: tr("my_account_user_info_form_phone_number_label"),
            placeholder: tr("my_account_user_info_form_phone_number_placeholder"),
            ...commonTextInputOptions
        },
        email: {
            label: tr("my_account_user_info_form_email_label"),
            placeholder: tr("my_account_user_info_form_email_placeholder"),
            ...commonTextInputOptions
        },
        gender: {
            label: tr("my_account_user_info_form_gender_label"),
            template: locals => <GenderSelect locals={locals}/>
        },
        dateOfBirth: {
            label: tr("my_account_user_info_form_date_of_birth_label"),
            template: locals => <DatePicker locals={locals}/>
        }
    }
};

class UserInfo extends Component {
    static propTypes = {
        fetchingUserInfo: PropTypes.bool,
        userInfo: PropTypes.any,
        fetchUserInformation: PropTypes.func,
        clearProfileUserInfo: PropTypes.func,
    };

    static defaultProps = {
        fetchingUserInfo: false,
        userInfo: {},
        fetchUserInformation: () => {
        },
        clearProfileUserInfo: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            formValues: {}
        };
    }

    componentDidMount() {
        const {fetchUserInformation} = this.props;

        fetchUserInformation();
    }

    componentWillUnmount() {
        const {clearProfileUserInfo} = this.props;

        clearProfileUserInfo();
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const {userInfo} = nextProps;

        this.setState({
            formValues: {
                fullName: userInfo.name,
                phoneNumber: userInfo.phone,
                email: userInfo.email,
                gender: userInfo.gender,
                dateOfBirth: userInfo.date_of_birth,
            }
        });
    }

    updateUserInfo = () => {
        // TODO: invoke API to update user info
    };

    renderAvatar = () => (
        <View style={styles.avatarContainer}>
            <Avatar size={50}/>
            <LinkButton
                title={tr("my_account_user_info_change_avatar_link_button")}
                textStyle={styles.changeAvatarLink}
            />
        </View>
    );

    render() {
        return (
            <KeyboardAwareView style={styles.userInfo}>
                {this.renderAvatar()}
                <tComb.form.Form
                    style={styles.form}
                    type={formStructure}
                    options={formOptions}
                    onChange={formValues => this.setState({formValues})}
                    value={this.state.formValues}
                />
                {/*<Button*/}
                {/*type="fully-colored"*/}
                {/*style={styles.updateButton}*/}
                {/*title={tr("my_account_user_info_form_update_button")}*/}
                {/*onPress={this.updateUserInfo}*/}
                {/*disabled*/}
                {/*/>*/}
            </KeyboardAwareView>
        );
    }
}

const mapStateToProps = (state) => ({
    userInfo: state.profile_manager.user_info,
    fetchingUserInfo: state.profile_manager.user_info.fetching
});

export default connect(mapStateToProps, {
    fetchUserInformation,
    clearProfileUserInfo
})(UserInfo);
