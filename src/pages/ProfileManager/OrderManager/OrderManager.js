import React, {Component} from "react";
import {FlatList, View, RefreshControl} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import orderListItemStyles from "src/pages/ProfileManager/components/OrderListItem/styles"
import NavBar from "src/containers/NavBar/NavBar";
import commonStyles from "src/styles/commonStyles";
import {tr} from "src/localization/localization";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import {clearOrdersList, fetchOrdersList} from "src/pages/ProfileManager/profileManagerActions";
import OrderListItem from "src/pages/ProfileManager/components/OrderListItem/OrderListItem";
import Button from "src/components/base/Button/Button";
import defaultOnPressHandler from "src/utils/reactNative/defaultOnPressHandler";
import screenIds from "src/constants/screenIds";
import Text from "src/components/platformSpecific/Text/Text";

const DEFAULT_ORDERS_LIST_PAGE_SIZE = 10;
const LOAD_MORE_BUTTON = "LOAD_MORE_BUTTON";
const ORDER_LIST_ITEM_HEIGHT = (
    orderListItemStyles.orderListItem.height +
    orderListItemStyles.orderListItem.margin * 2 +
    styles.separator.height
);

class OrderManager extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        tabBarHidden: true
    };

    static propTypes = {
        orders: PropTypes.array,
        isFetchingOrders: PropTypes.bool,
        fetchOrdersList: PropTypes.func,
        clearOrdersList: PropTypes.func,
    };

    static defaultProps = {
        orders: [],
        isFetchingOrders: false,
        fetchOrdersList: () => {
        },
        clearOrdersList: () => {
        }
    };

    componentDidMount() {
        const {fetchOrdersList} = this.props;

        fetchOrdersList(1);
    }

    componentWillUnmount() {
        const {clearOrdersList} = this.props;

        clearOrdersList();
    }

    getCurrentPageIndex = () => {
        const {orders} = this.props;

        return Math.ceil(orders.length / DEFAULT_ORDERS_LIST_PAGE_SIZE);
    };

    getItemLayout = (date, index) => ({
        length: ORDER_LIST_ITEM_HEIGHT,
        offset: index * ORDER_LIST_ITEM_HEIGHT,
        index
    });

    loadNextPage = () => {
        const {fetchOrdersList} = this.props;
        const nextPageIndex = this.getCurrentPageIndex() + 1;

        fetchOrdersList(nextPageIndex);
    };

    reloadList = () => {
        const {fetchOrdersList, clearOrdersList} = this.props;

        clearOrdersList();
        fetchOrdersList(1);
    };

    renderNavBar = () => (
        <NavBar
            title={tr("order_manager_nav_bar_title")}
            navigator={this.props.navigator}
            right={
                <NavBarShortcuts
                    showsCart
                    showsSearch
                    showsVerticalEllipsis
                    navigator={this.props.navigator}
                />
            }
        />
    );

    renderLoadMoreButton = () => {
        const {isFetchingOrders} = this.props;

        return (
            <Button
                title={tr("order_manager_load_more_button")}
                style={styles.loadMoreButton}
                loading={isFetchingOrders}
                onPress={this.loadNextPage}
            />
        );
    };

    renderItem = ({item = {}}) => {
        const {viewType = "", id: orderId} = item;
        const {navigator} = this.props;

        if (viewType === LOAD_MORE_BUTTON) {
            return this.renderLoadMoreButton();
        }

        return (
            <OrderListItem
                order={item}
                onPress={
                    defaultOnPressHandler({
                        navigator,
                        screenId: screenIds.ORDER_DETAIL,
                        passProps: {
                            orderId
                        }
                    })
                }
            />
        )
    };

    renderEmptyList = () => (
        <View style={styles.emptyListTextContainer}>
            <Text style={styles.emptyListText}>{tr("order_manager_empty_list")}</Text>
        </View>
    );

    render() {
        const {orders, isFetchingOrders} = this.props;

        return (
            <View style={commonStyles.matchParent}>
                {this.renderNavBar()}
                {/*<DateFilter/>*/}
                <FlatList
                    style={styles.flatList}
                    renderItem={this.renderItem}
                    getItemLayout={this.getItemLayout}
                    data={[
                        ...orders,
                        {viewType: LOAD_MORE_BUTTON}
                    ]}
                    keyExtractor={(_, index) => `${index}`}
                    ListEmptyComponent={this.renderEmptyList()}
                    ItemSeparatorComponent={() => <View style={styles.separator}/>}
                    refreshControl={
                        <RefreshControl
                            refreshing={isFetchingOrders}
                            onRefresh={this.reloadList}
                        />
                    }
                />
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    orders: state.profile_manager.orders_lists.data,
    isFetchingOrders: state.profile_manager.orders_lists.fetching
});

const mapDispatchToProps = (dispatch) => ({
    clearOrdersList: () => dispatch(clearOrdersList()),
    fetchOrdersList: pageIndex => dispatch(fetchOrdersList(pageIndex))
});

const wrappedComponent = connect(mapStateToProps, mapDispatchToProps)(OrderManager);

export {
    wrappedComponent as default,
    DEFAULT_ORDERS_LIST_PAGE_SIZE
};
