import {StyleSheet} from "react-native";

import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    flatList: {
        flex: 1,
    },
    emptyListText: {
        color: "#3c3c3c"
    },
    emptyListTextContainer: {
        width: "100%",
        ...commonStyles.centerChildren,
        marginTop: 24,
    },
    separator: {
        marginHorizontal: 16,
        height: 0.5,
        backgroundColor: "#eee"
    },
    loadMoreButton: {
        marginVertical: 16,
        height: 24,
        alignSelf: "center"
    }
});

export default styles;
