import profileManagerActionTypes from "src/redux/actionTypes/profileManagerActionTypes";
import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import {addOrderStepToOrdersList, convertToOrderManagerStep, reduceStatusHistory} from "src/utils/misc/orderStep";

const defaultOrdersList =  {
    data: [],
    fetching: false,
    error: false
};

const defaultUserInfo = {
    fetching: false
};

const defaultViewedProducts = {
    products: [],
    fetching: false
};

const initialState = {
    user_info: {
        ...defaultUserInfo
    },
    orders_lists: {
        ...defaultOrdersList
    },
    order_detail: {
        data: {},
        fetching: false,
        error: false
    },
    coupon_manager: {
        coupons: [],
        fetching: false
    },
    viewed_products: {
        ...defaultViewedProducts
    }
};

const profileManagerReducers = (state = initialState, {type, payload}) => {
    switch (type) {
        case toRequest(profileManagerActionTypes.FETCH_VIEWED_PRODUCTS):
            return {
                ...state,
                viewed_products: {
                    products: [],
                    fetching: true
                }
            };
        case toSuccess(profileManagerActionTypes.FETCH_VIEWED_PRODUCTS):
            return {
                ...state,
                viewed_products: {
                    products: [...payload.reply],
                    fetching: false
                }
            };
        case toFailure(profileManagerActionTypes.FETCH_VIEWED_PRODUCTS):
            return {
                ...state,
                viewed_products: {
                    fetching: false
                }
            };
        case profileManagerActionTypes.CLEAR_ORDERS_LIST:
            return {
                ...state,
                orders_lists: {
                    ...defaultOrdersList
                }
            };
        case profileManagerActionTypes.CLEAR_USER_INFO:
            return {
                ...state,
                user_info: {
                    ...defaultUserInfo
                }
            };
        case toFailure(profileManagerActionTypes.FETCH_USER_INFORMATION):
            return {
                ...state,
                user_info: {
                    ...state.user_info,
                    fetching: false
                }
            };
        case toRequest(profileManagerActionTypes.FETCH_USER_INFORMATION):
            return {
                ...state,
                user_info: {
                    ...state.user_info,
                    fetching: true
                }
            };
        case toSuccess(profileManagerActionTypes.FETCH_USER_INFORMATION):
            return {
                ...state,
                user_info: {
                    ...payload.reply,
                    fetching: false
                }
            };
        case toRequest(profileManagerActionTypes.FETCH_ORDER_DETAIL):
            return {
                ...state,
                order_detail: {
                    ...state.order_detail,
                    fetching: true,
                    error: false,
                    data: {}
                }
            };
        case toFailure(profileManagerActionTypes.FETCH_ORDER_DETAIL):
            return {
                ...state,
                order_detail: {
                    ...state.order_detail,
                    fetching: false,
                    error: true,
                }
            };
        case toSuccess(profileManagerActionTypes.FETCH_ORDER_DETAIL):
            return {
                ...state,
                order_detail: {
                    ...state.order_detail,
                    fetching: false,
                    error: false,
                    data: {
                        ...payload.reply,
                        order_step: convertToOrderManagerStep(payload.reply || {}),
                        status_history: reduceStatusHistory(payload.reply || {})
                    }
                }
            };
        case toRequest(profileManagerActionTypes.FETCH_ORDERS_LIST):
            return {
                ...state,
                orders_lists: {
                    ...state.orders_lists,
                    fetching: true,
                    error: false
                }
            };
        case toFailure(profileManagerActionTypes.FETCH_ORDERS_LIST):
            return {
                ...state,
                orders_lists: {
                    ...state.orders_lists,
                    fetching: false,
                    error: true
                }
            };
        case toSuccess(profileManagerActionTypes.FETCH_ORDERS_LIST):
            return {
                ...state,
                orders_lists: {
                    ...state.orders_lists,
                    data: [
                        ...state.orders_lists.data,
                        ...addOrderStepToOrdersList(payload.reply),
                    ],
                    fetching: false,
                    error: false
                }
            };
        case toRequest(profileManagerActionTypes.FETCH_MY_COUPONS):
            return {
                ...state,
                coupon_manager: {
                    ...state.coupon_manager,
                    coupons: [],
                    fetching: true
                }
            };
        case toSuccess(profileManagerActionTypes.FETCH_MY_COUPONS):
            return {
                ...state,
                coupon_manager: {
                    ...state.coupon_manager,
                    coupons: [...payload.reply],
                    fetching: false
                }
            };
        case toFailure(profileManagerActionTypes.FETCH_MY_COUPONS):
            return {
                ...state,
                coupon_manager: {
                    ...state.coupon_manager,
                    fetching: false
                }
            };
        default:
            return state;
    }
};

export default profileManagerReducers;
