import {getUserInfo} from "src/utils/userAuth/userAuth";
import profileManagerActionTypes from "src/redux/actionTypes/profileManagerActionTypes";
import {createAction, createAsyncAction} from "src/utils/api/createAction";
import {popToRoot} from "src/utils/misc/navigator";
import {clearUserInfo} from "src/redux/common/sessionActions";
import {DEFAULT_ORDERS_LIST_PAGE_SIZE} from "src/pages/ProfileManager/OrderManager/OrderManager";

const clearOrdersList = () => createAction({
    type: profileManagerActionTypes.CLEAR_ORDERS_LIST,
    payload: {}
});

const clearProfileUserInfo = () => createAction({
    type: profileManagerActionTypes.CLEAR_USER_INFO,
    payload: {}
});

const fetchViewedProducts = () => createAsyncAction({
    type: profileManagerActionTypes.FETCH_VIEWED_PRODUCTS,
    payload: {
        request: "/viewed_product/get_product",
        params: {}
    }
});

const fetchOrderDetail = (order_id) => createAsyncAction({
    type: profileManagerActionTypes.FETCH_ORDER_DETAIL,
    payload: {
        request: "/sales_order/get_detail",
        params: {
            id: order_id
        }
    }
});

const fetchOrdersList = (page_index) => {
    return createAsyncAction({
        type: profileManagerActionTypes.FETCH_ORDERS_LIST,
        payload: {
            request: "/sales_order/get_by_user",
            params: {
                page_size: DEFAULT_ORDERS_LIST_PAGE_SIZE,
                page_index: page_index - 1
            }
        }
    });
};

const fetchUserInformation = () => createAsyncAction({
    type: profileManagerActionTypes.FETCH_USER_INFORMATION,
    payload: {
        request: "/profile/account/get"
    }
});

const serverSideLogOut = () => {
    const {access_token} = getUserInfo();

    return createAsyncAction({
        type: profileManagerActionTypes.LOGOUT,
        payload: {
            method: "POST",
            request: "/api/auth/sign_out",
            headers: {
                "x-giigaa-access-token": access_token
            },
            options: {
                isAuth: true
            }
        }
    });
};

const logout = (navigator) => (dispatch) => {
    dispatch(serverSideLogOut());
    dispatch(clearUserInfo());
    popToRoot(navigator);
};

const fetchMyCoupons = ({status}) => createAsyncAction({
    type: profileManagerActionTypes.FETCH_MY_COUPONS,
    payload: {
        request: "/coupon/list",
        params: {
            status
        }
    }
});

export {
    clearProfileUserInfo,
    fetchMyCoupons,
    clearOrdersList,
    fetchOrderDetail,
    fetchOrdersList,
    fetchUserInformation,
    fetchViewedProducts,
    logout
};
