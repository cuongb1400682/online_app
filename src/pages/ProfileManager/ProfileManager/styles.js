import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    logoutButton: {
        marginTop: 24,
        height: 40,
        width: 135,
        alignSelf: "center"
    },
    profileManagerContent: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "#eee",
    },
    userInfo: {
        width: "100%",
        height: 64,
        backgroundColor: "#fff",
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10
    },
    userFullName: {
        fontSize: 13,
    },
    userEmail: {
        fontSize: 13,
        color: "#757575"
    },
    avatar: {
        marginRight: 12
    }
});

export default styles;
