import React, {Component} from "react";
import {ScrollView, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import Button from "src/components/base/Button/Button";
import {tr} from "src/localization/localization";
import commonStyles from "src/styles/commonStyles";
import NavBar from "src/containers/NavBar/NavBar";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import {logout} from "src/pages/ProfileManager/profileManagerActions";
import {isUserLoggedIn} from "src/utils/userAuth/userAuth";
import {popToRoot} from "src/utils/misc/navigator";
import Avatar from "src/components/base/svgIcons/Avatar/Avatar";
import MenuItem from "src/pages/ProfileManager/components/MenuItem/MenuItem";
import screenIds from "src/constants/screenIds";
import Text from "src/components/platformSpecific/Text/Text";

class ProfileManager extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        tabBarHidden: true
    };

    static propTypes = {
        userInfo: PropTypes.any,
        logout: PropTypes.func
    };

    static defaultProps = {
        userInfo: {},
        logout: () => {
        }
    };

    componentWillMount() {
        const {navigator} = this.props;

        if (!isUserLoggedIn()) {
            popToRoot(navigator);
        }
    }

    logout = () => {
        const {logout, navigator} = this.props;

        logout(navigator);
    };

    renderNavBar = () => {
        const {navigator} = this.props;

        return (
            <NavBar
                navigator={navigator}
                title={tr("profile_manager_navbar_title")}
                right={
                    <NavBarShortcuts
                        navigator={navigator}
                        showsCart
                        showsSearch
                        showsVerticalEllipsis
                    />
                }
            />
        );
    };

    renderUserInfo = () => {
        const {userInfo} = this.props;

        return (
            <View style={styles.userInfo}>
                <Avatar size={48} style={styles.avatar}/>
                <View>
                    <Text style={styles.userFullName}>{userInfo.full_name}</Text>
                    <Text style={styles.userEmail}>{userInfo.email}</Text>
                </View>
            </View>
        );
    };

    renderLogoutButton = () => (
        <Button
            style={styles.logoutButton}
            type="fully-colored"
            title={tr("profile_manager_logout_button")}
            onPress={this.logout}
        />
    );

    renderMenu = () => [
        {
            title: tr("profile_manager_account_info"),
            screenId: screenIds.MY_ACCOUNT,
            iconName: "user"
        }
    ].map((props, index) => <MenuItem key={index} {...props} navigator={this.props.navigator}/>);

    render() {
        return (
            <View style={commonStyles.matchParent}>
                {this.renderNavBar()}
                <ScrollView style={styles.profileManagerContent}>
                    {this.renderUserInfo()}
                    {this.renderMenu()}
                    {this.renderLogoutButton()}
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    userInfo: state.session.user_info
});

export default connect(mapStateToProps, {
    logout
})(ProfileManager);
