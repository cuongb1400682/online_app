import React, {Component} from "react";
import {ScrollView, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import commonStyles from "src/styles/commonStyles";
import {fetchOrderDetail} from "src/pages/ProfileManager/profileManagerActions";
import NavBar from "src/containers/NavBar/NavBar";
import {tr} from "src/localization/localization";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import OrderGeneralInfo from "src/pages/ProfileManager/components/OrderGeneralInfo/OrderGeneralInfo";
import OrderSteps from "src/pages/ProfileManager/components/OrderSteps/OrderSteps";
import ShippingStatus from "src/pages/ProfileManager/components/ShippingStatus/ShippingStatus";
import BillingInfo from "src/pages/ProfileManager/components/BillingInfo/BillingInfo";
import OrderStepHistory from "src/pages/ProfileManager/components/OrderStepHistory/OrderStepHistory";
import PaymentStatus from "src/pages/ProfileManager/components/PaymentStatus/PaymentStatus";
import AllProducts from "src/pages/ProfileManager/components/AllProducts/AllProducts";

class OrderDetail extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        tabBarHidden: true
    };

    static propTypes = {
        orderId: PropTypes.number,
        orderDetail: PropTypes.any,
        fetchOrderDetail: PropTypes.func
    };

    static defaultProps = {
        orderId: -1,
        orderDetail: {},
        fetchOrderDetail: () => {
        }
    };

    componentDidMount() {
        const {fetchOrderDetail, orderId} = this.props;

        fetchOrderDetail(orderId);
    }

    renderNavBar = () => (
        <NavBar
            title={tr("order_detail_nav_bar_title")}
            navigator={this.props.navigator}
            right={
                <NavBarShortcuts
                    showsCart
                    showsSearch
                    showsVerticalEllipsis
                    navigator={this.props.navigator}
                />
            }
        />
    );

    render() {
        const {orderDetail, navigator} = this.props;

        return (
            <View style={commonStyles.matchParent}>
                {this.renderNavBar()}
                <ScrollView style={commonStyles.matchParent}>
                    {[
                        OrderGeneralInfo,
                        OrderSteps,
                        ShippingStatus,
                        BillingInfo,
                        OrderStepHistory,
                        PaymentStatus,
                        AllProducts
                    ].map((SubView, index) =>
                        <SubView key={index} orderDetail={orderDetail} navigator={navigator}/>
                    )}
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    orderDetail: state.profile_manager.order_detail.data
});

export default connect(mapStateToProps, {
    fetchOrderDetail
})(OrderDetail);
