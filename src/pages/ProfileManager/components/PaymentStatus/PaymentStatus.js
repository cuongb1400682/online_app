import React from "react";
import {View} from "react-native";

import styles from "./styles";
import {tr} from "src/localization/localization";
import {SOStatus} from "src/constants/enums";
import {capitalizeString} from "src/utils/extensions/strings";
import Text from "src/components/platformSpecific/Text/Text";

const InfoLine = ({label = "", value = ""}) => (
    <Text>
        <Text style={styles.infoLineLabel}>{label}</Text>
        <Text style={styles.infoLineValue}>{capitalizeString(value)}</Text>
    </Text>
);

const PaymentStatus = ({orderDetail = {}}) => {
    const {
        payment_method: paymentMethod = "",
        status = SOStatus.COMPLETED
    } = orderDetail;

    const paymentStatus = (
        status === SOStatus.COMPLETED
            ? "completed"
            : "uncompleted"
    );

    return (
        <View style={styles.paymentStatus}>
            <InfoLine
                label={tr("payment_status_method")}
                value={tr(paymentMethod)}
            />
            <View style={styles.placeholder}/>
            <InfoLine
                label={tr("payment_status_status")}
                value={tr(`payment_status_${paymentStatus}`)}
            />
        </View>
    )
};

export default PaymentStatus;
