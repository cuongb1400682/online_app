import {StyleSheet} from "react-native";

import profileManagerStyles from "src/pages/ProfileManager/styles";

const styles = StyleSheet.create({
    paymentStatus: {
        ...profileManagerStyles.orderDetailSection,
        ...profileManagerStyles.hasBottomBorder,
    },
    infoLineLabel: {
        color: "#757575",
        ...profileManagerStyles.commonText
    },
    infoLineValue: {
        ...profileManagerStyles.commonText
    },
    placeholder: {
        height: 6
    }
});

export default styles;
