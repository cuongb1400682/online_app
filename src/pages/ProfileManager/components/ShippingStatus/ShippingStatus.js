import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import OrderStepBadge from "src/pages/ProfileManager/components/OrderStepBadge/OrderStepBadge";
import profileManagerStyles from "src/pages/ProfileManager/styles";
import Text from "src/components/platformSpecific/Text/Text";

class ShippingStatus extends Component {
    static propTypes = {
        orderDetail: PropTypes.any
    };

    static defaultProps = {
        orderDetail: {}
    };

    render() {
        const {
            order_step: orderStep = "",
        } = this.props.orderDetail;

        return (
            <View style={styles.shippingStatus}>
                <Text style={[styles.shippingStatusSectionTitle, profileManagerStyles.commonText]}>
                    {tr("shipping_status_section_title")}
                </Text>
                <Text style={[styles.shippingStatusLabel, profileManagerStyles.commonText]}>
                    {tr("shipping_status_shipping_brand")}
                    <Text style={styles.shippingStatusShippingBrandName}>
                        {tr("shipping_status_default_shipping_brand")}
                    </Text>
                </Text>
                <View style={styles.shippingStatusLabelContainer}>
                    <Text style={[styles.shippingStatusLabel, profileManagerStyles.commonText]}>
                        {tr("shipping_status_status")}
                    </Text>
                    <OrderStepBadge step={orderStep}/>
                </View>
            </View>
        );
    }
}

export default ShippingStatus;
