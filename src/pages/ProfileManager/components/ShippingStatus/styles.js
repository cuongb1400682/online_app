import {StyleSheet} from "react-native";

import profileManagerStyles from "src/pages/ProfileManager/styles";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    shippingStatus: {
        width: "100%",
        ...profileManagerStyles.orderDetailSection,
        ...profileManagerStyles.hasBottomBorder,
    },
    textCommon: {
        fontSize: 12,
    },
    shippingStatusSectionTitle: {
        fontWeight: "bold"
    },
    shippingStatusShippingBrandName: {
        fontWeight: "bold"
    },
    shippingStatusLabel: {
        marginTop: 6,
    },
    shippingStatusLabelText: {
        marginTop: 5
    },
    shippingStatusLabelContainer: {
        flexDirection: "row",
        alignItems: "center"
    }
});

export default styles;
