import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {orderManagerSteps} from "src/constants/enums";
import OrderStatusIcon from "src/pages/ProfileManager/components/OrderStatusIcon/OrderStatusIcon";
import {tr} from "src/localization/localization";
import profileManagerStyles from "src/pages/ProfileManager/styles";
import Text from "src/components/platformSpecific/Text/Text";

const orderSteps = [
    orderManagerSteps.CREATION,
    orderManagerSteps.PICKING,
    orderManagerSteps.SHIPPING,
    orderManagerSteps.COMPLETED
];

class OrderSteps extends Component {
    static propTypes = {
        orderDetail: PropTypes.any
    };

    static defaultProps = {
        orderDetail: {}
    };

    isOrderCancelled() {
        const {order_step = ""} = this.props.orderDetail;

        return order_step === orderManagerSteps.CANCEL;
    }

    isStepFinished(step) {
        const {order_step = ""} = this.props.orderDetail;

        const stepIndex = orderSteps.indexOf(step);
        const orderStatusIndex = orderSteps.indexOf(order_step);

        return stepIndex <= orderStatusIndex;
    }

    renderOrderStep = (currentStep, index) => {
        const isStepActive = !this.isOrderCancelled() && this.isStepFinished(currentStep);

        return (
            <React.Fragment key={index}>
                {index > 0 && <View style={styles.lineInBetween}/>}
                <View style={styles.orderStepIconContainer}>
                    <OrderStatusIcon
                        type={currentStep}
                        active={isStepActive}
                    />
                    <Text style={styles.orderStepBadge}>{tr(`shipping_steps_${currentStep}`)}</Text>
                </View>
            </React.Fragment>
        );
    };

    render() {
        return (
            <View>
                <View style={styles.orderSteps}>
                    {orderSteps.map(this.renderOrderStep)}
                </View>
                <View style={profileManagerStyles.hasBottomBorder}/>
            </View>
        );
    }
}

export default OrderSteps;
