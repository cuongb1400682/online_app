import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    orderSteps: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "center",
        marginBottom: 16,
    },
    orderStepIconContainer: {
        flexDirection: "column",
        width: 55,
        height: 70,
        overflow: "visible",
        alignItems: "center"
    },
    orderStepBadge: {
        fontSize: 12,
        textAlign: "center",
        color: colors.textColor,
        marginTop: 8,
        width: 70
    },
    lineInBetween: {
        width: 28,
        height: 2,
        borderRadius: 2,
        marginTop: 20,
        backgroundColor: colors.orderStatus.inactive
    },
});

export default styles;
