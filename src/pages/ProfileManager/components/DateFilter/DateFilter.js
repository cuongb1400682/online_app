import React, {Component} from "react";
import * as tComb from "tcomb-form-native";
import {View} from "react-native";
import PropTypes from "prop-types"

import styles, {overrideStylesheet} from "./styles";
import DatePicker from "src/pages/ProfileManager/components/DatePicker/DatePicker";
import Button from "src/components/base/Button/Button";
import {tr} from "src/localization/localization";

const formStructure = tComb.struct({
    dateFrom: tComb.Date,
    dateTo: tComb.Date
});

const formOptions = {
    fields: {
        dateFrom: {
            template: locals => <DatePicker hideLabel
                                            locals={locals}
                                            placeholder={tr("date_filter_date_from_placeholder")}/>,
        },
        dateTo: {
            template: locals => <DatePicker hideLabel
                                            locals={locals}
                                            placeholder={tr("date_filter_date_to_placeholder")}/>,
        }
    },
    stylesheet: overrideStylesheet()
};

class DateFilter extends Component {
    static propTypes = {
        onSubmit: PropTypes.func
    };

    static defaultProps = {
        onSubmit: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            formValues: {
                dateFrom: null,
                dateTo: null,
            }
        };
    }

    handleSubmit = () => {
        const {onSubmit} = this.props;

        onSubmit(this.state.formValues);
    };

    render() {
        return (
            <View style={styles.dateFilter}>
                <tComb.form.Form
                    type={formStructure}
                    options={formOptions}
                    onChange={formValues => this.setState({formValues})}
                    value={this.state.formValues}
                />
                <Button
                    title={tr("data_filter_submit_button_text")}
                    type="fully-colored"
                    style={styles.submitButton}
                    onPress={this.handleSubmit}
                />
            </View>
        );
    }
}

export default DateFilter;
