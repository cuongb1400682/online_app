import {StyleSheet} from "react-native";
import * as tComb from "tcomb-form-native";

const styles = StyleSheet.create({
    dateFilter: {
        flexDirection: "row",
        width: "100%",
        borderStyle: "solid",
        borderBottomWidth: 2,
        borderColor: "#ddd",
        backgroundColor: "#f9f9f9",
        justifyContent: "center",
        alignItems: "center"
    },
    submitButton: {
        height: 32,
        marginRight: 10
    }
});

const overrideStylesheet = () => {
    const stylesheet = JSON.parse(JSON.stringify(tComb.form.Form.stylesheet));

    stylesheet.fieldset = {
        flex: 1,
        flexDirection: "row",
        paddingLeft: 10,
        paddingTop: 12,
        alignItems: "center",
    };
    stylesheet.formGroup.normal.flex = 1;
    stylesheet.formGroup.normal.marginRight = 10;
    stylesheet.formGroup.error.flex = 1;
    stylesheet.formGroup.error.flex = 1;

    return stylesheet;
};

export {
    styles as default,
    overrideStylesheet
};
