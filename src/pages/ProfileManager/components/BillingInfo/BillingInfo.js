import React, {Component} from "react";
import {View} from "react-native";

import styles from "./styles";
import PropTypes from "prop-types";
import {tr} from "src/localization/localization";
import {capitalizeString, makeFullAddress} from "src/utils/extensions/strings";
import Text from "src/components/platformSpecific/Text/Text";

class BillingInfo extends Component {
    static propTypes = {
        orderDetail: PropTypes.any
    };

    static defaultProps = {
        orderDetail: {}
    };

    render() {
        const {address = {}} = this.props.orderDetail;

        return (
            <View style={styles.billingInfo}>
                <Text style={styles.billingInfoSectionTitle}>
                    {tr("billing_info_section_title")}
                </Text>
                <Text style={styles.billingContactName}>
                    {capitalizeString(address.contact_name)}
                </Text>
                <Text style={styles.billingAddress}>
                    {tr("billing_info_address")}
                    {makeFullAddress(address)}
                </Text>
                <Text style={styles.billingContactPhone}>
                    {tr("billing_info_phone")}
                    {address.contact_phone}
                </Text>
            </View>
        );
    }
}

export default BillingInfo;
