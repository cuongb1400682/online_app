import {StyleSheet} from "react-native";

import profileManagerStyles from "src/pages/ProfileManager/styles";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    billingInfo: {
        ...profileManagerStyles.orderDetailSection,
        ...profileManagerStyles.hasBottomBorder,
    },
    billingInfoSectionTitle: {
        fontWeight: "bold",
        ...profileManagerStyles.commonText
    },
    billingContactName: {
        marginTop: 14,
        fontWeight: "bold",
        ...profileManagerStyles.commonText
    },
    billingAddress: {
        color: "#757575",
        marginVertical: 2,
        ...profileManagerStyles.commonText
    },
    billingContactPhone: {
        color: "#757575",
        ...profileManagerStyles.commonText
    },
});

export default styles;
