import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    datePickerBox: {
        width: "100%",
        height: 36,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        borderStyle: "solid",
        borderWidth: 1,
        borderRadius: 4,
        borderColor: "#ddd",
        backgroundColor: "#fff",
        padding: 7
    },
    calendarIcon: {
        color: "#757575",
        fontSize: 13
    },
    datePickerContainer: {
        backgroundColor: "#fff",
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderWidth: 1,
        borderColor: "#ccc",
        flexDirection: "column",
        justifyContent: "flex-end",
        shadowColor: "#000",
        shadowOpacity: 0.5,
        shadowOffset: {
            width: -3,
            height: 1
        },
        elevation: 1
    },
    datePickerTitleBar: {
        width: "100%",
        height: 36,
        paddingHorizontal: 16,
        marginTop: 8,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    linkButtonText: {
        color: colors.primaryColor,
        fontSize: 15
    },
    title: {
        fontSize: 15,
    },
    placeholder: {
        color: "#757575"
    }
});

export default styles;
