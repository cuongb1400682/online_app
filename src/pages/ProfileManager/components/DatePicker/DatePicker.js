import React, {Component} from "react";
import {DatePickerIOS, Modal, TouchableWithoutFeedback, View} from "react-native";
import PropTypes from "prop-types";
import moment from "moment";

import styles from "./styles";
import FontIcon from "src/components/base/FontIcon/FontIcon";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import LinkButton from "src/components/base/LinkButton/LinkButton";
import {tr} from "src/localization/localization";
import commonStyles from "src/styles/commonStyles";
import Text from "src/components/platformSpecific/Text/Text";

class DatePicker extends Component {
    static propTypes = {
        locals: PropTypes.any,
        leftText: PropTypes.string,
        rightText: PropTypes.string,
        title: PropTypes.string,
        hideLabel: PropTypes.bool,
        placeholder: PropTypes.string,
    };

    static defaultProps = {
        locals: {},
        leftText: tr("date_picker_left_button_default_text"),
        rightText: tr("date_picker_right_button_default_text"),
        title: tr("date_picker_title_default_text"),
        hideLabel: false,
        placeholder: "",
    };

    constructor(props) {
        super(props);

        this.state = {
            isDatePickerVisible: false
        };
    }

    onValueChanged = (value) => {
        const {onChange} = this.props.locals;

        onChange(value);
        this.setState({value});
    };

    onModalShow = () => {
        const {value} = this.props.locals;

        this.oldValue = value;
    };

    onModalDismiss = () => {
        const {value} = this.props.locals;

        this.oldValue = value;
    };

    toggleDatePicker = () => {
        const newVisibility = !this.state.isDatePickerVisible;

        this.setState({isDatePickerVisible: newVisibility});
    };

    revertToOldValue = () => {
        const {onChange} = this.props.locals;

        onChange(this.oldValue);
        this.toggleDatePicker();
    };

    acceptNewValue = () => {
        const {onChange, value} = this.props.locals;

        onChange(value || new Date());
        this.toggleDatePicker();
    };

    renderInputBox = () => {
        const {locals, placeholder} = this.props;
        const {value} = locals;

        return (
            <Touchable onPress={this.toggleDatePicker}>
                <View style={styles.datePickerBox}>
                    {!value
                        ? <Text style={styles.placeholder}>{placeholder}</Text>
                        : <Text>{value ? moment(value).format("DD/MM/YYYY") : ""}</Text>}
                    <FontIcon
                        style={styles.calendarIcon}
                        fontName="FontAwesome5"
                        name="calendar-alt"
                    />
                </View>
            </Touchable>
        );
    };

    renderModal = () => {
        const {leftText, title, rightText, locals} = this.props;
        const {isDatePickerVisible} = this.state;
        const {value} = locals;

        return (
            <Modal
                visible={isDatePickerVisible}
                hardwareAccelerated
                transparent
                animationType="slide"
                onShow={this.onModalShow}
                onDismiss={this.onModalDismiss}
            >
                <TouchableWithoutFeedback onPress={this.revertToOldValue}>
                    <View style={commonStyles.matchParent}/>
                </TouchableWithoutFeedback>
                <View style={styles.datePickerContainer}>
                    <View style={styles.datePickerTitleBar}>
                        <LinkButton
                            textStyle={styles.linkButtonText}
                            title={leftText}
                            onPress={this.revertToOldValue}
                        />
                        <Text style={styles.title}>{title}</Text>
                        <LinkButton
                            textStyle={styles.linkButtonText}
                            title={rightText}
                            onPress={this.acceptNewValue}
                        />
                    </View>
                    <DatePickerIOS
                        mode="date"
                        onDateChange={this.onValueChanged}
                        date={value || new Date()}
                        locale="vi"
                    />
                </View>
            </Modal>
        );
    };

    render() {
        const {locals, hideLabel} = this.props;
        const {stylesheet = {}} = locals;
        const controlLabelStyle = stylesheet.controlLabel.normal;
        const formGroupStyle = stylesheet.formGroup.normal;

        if (locals.hidden) {
            return null;
        }

        return (
            <View style={formGroupStyle}>
                {!hideLabel && <Text style={controlLabelStyle}>{locals.label}</Text>}
                {this.renderInputBox()}
                {this.renderModal()}
            </View>
        );
    }
}

export default DatePicker;
