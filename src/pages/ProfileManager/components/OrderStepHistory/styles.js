import {StyleSheet} from "react-native";

import profileManagerStyles from "src/pages/ProfileManager/styles";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    orderStepHistory: {
        ...profileManagerStyles.orderDetailSection,
        ...profileManagerStyles.hasBottomBorder
    },
    verticalBar: {
        width: 2,
        height: 32,
        backgroundColor: "#ccc",
        position: "absolute",
        left: 2,
        top: -24,
        zIndex: -1000
    },
    historyItem: {
        flexDirection: "row",
        alignItems: "center"
    },
    commonDot: {
        width: 6,
        height: 6,
        borderRadius: 6,
        position: "absolute",
        zIndex: 1000,
        backgroundColor: "green"
    },
    orderStepHistoryText: {
        fontWeight: "bold"
    },
    activeOrderStepHistoryText: {
        color: colors.primaryColor
    },
    inactiveDot: {
        backgroundColor: "#ccc"
    },
    activeDot: {
        backgroundColor: colors.primaryColor
    },
    date: {
        width: 104,
        marginLeft: 16,
    }
});

export default styles;
