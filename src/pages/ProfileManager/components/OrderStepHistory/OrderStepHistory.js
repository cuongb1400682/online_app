import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import moment from "moment";

import styles from "./styles";
import profileManagerStyles from "src/pages/ProfileManager/styles";
import {tr} from "src/localization/localization";
import Text from "src/components/platformSpecific/Text/Text";

class OrderStepHistory extends Component {
    static propTypes = {
        orderDetail: PropTypes.any
    };

    static defaultProps = {
        orderDetail: {}
    };

    getReducedStatusHistory = () => {
        const {
            status_history = [],
            order_step = ""
        } = this.props.orderDetail;

        const currentStepIndex = status_history.findIndex(status => status.step === order_step);

        const result = currentStepIndex > -1
            ? status_history.slice(0, currentStepIndex + 1)
            : status_history;

        return result.reverse();
    };

    renderHistoryItem = (historyItem = {}) => {
        const {
            index = 0,
            step = "",
            currentStep = "",
            created_time = ""
        } = historyItem;

        const isActiveStep = (currentStep === step);

        const historyItemStyles = [
            styles.historyItem,
            {
                marginTop: index > 0 ? 20 : 0
            }
        ];

        const dotStyles = [
            styles.commonDot,
            isActiveStep ? styles.activeDot : styles.inactiveDot
        ];

        const orderStepHistoryStyles = [
            styles.orderStepHistoryText,
            profileManagerStyles.commonText,
            isActiveStep ? styles.activeOrderStepHistoryText : {}
        ];

        const dateStyles = [
            styles.date,
            profileManagerStyles.commonText
        ];

        return (
            <View key={index} style={historyItemStyles}>
                <View style={dotStyles}/>
                <Text style={dateStyles}>{moment(created_time).format("HH:mm DD/MM/YYYY")}</Text>
                <Text style={orderStepHistoryStyles}>{tr(`order_step_history_${step}`)}</Text>
                {index > 0 && <View style={styles.verticalBar}/>}
            </View>
        );
    };

    render() {
        const {order_step = ""} = this.props.orderDetail;
        const reducedStatusHistory = this.getReducedStatusHistory();

        return (
            <View style={styles.orderStepHistory}>
                {reducedStatusHistory.map((status, index) =>
                    this.renderHistoryItem({
                        ...status,
                        index,
                        currentStep: order_step
                    })
                )}
            </View>
        );
    }
}

export default OrderStepHistory;
