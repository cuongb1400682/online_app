import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    radio: {
        marginTop: 0,
        marginRight: 8
    },
    radioText: {
    },
    radioWrapper: {
        flex: 1,
    },
    radioGroup: {
        height: 24,
        flexDirection: "row",
        alignItems: "center"
    }
});

export default styles;
