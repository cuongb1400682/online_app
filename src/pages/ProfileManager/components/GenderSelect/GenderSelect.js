import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import RadioGroup from "src/components/base/RadioGroup/RadioGroup";
import Radio from "src/components/base/Radio/Radio";
import {genders} from "src/constants/enums";
import {tr} from "src/localization/localization";
import Text from "src/components/platformSpecific/Text/Text";

const supportedGenders = Object.keys(genders);

class GenderSelect extends Component {
    static propTypes = {
        locals: PropTypes.any
    };

    static defaultProps = {
        locals: {}
    };

    selectValue = (currentValue = "") => (
        supportedGenders.findIndex(value => currentValue === value)
    );

    onValueChanged = (selectedIndex) => {
        const {onChange} = this.props.locals;

        if (typeof onChange === "function") {
            return onChange(supportedGenders[selectedIndex]);
        }
    };

    renderRadio = (gender = "", index) => (
        <Radio
            key={index}
            style={styles.radioWrapper}
            radioStyle={styles.radio}
            size={18}
        >
            <Text style={styles.radioText}>
                {tr(`gender_select_${gender}`)}
            </Text>
        </Radio>
    );

    render() {
        const {locals} = this.props;
        const {stylesheet = {}, value = ""} = locals;
        const controlLabelStyle = stylesheet.controlLabel.normal;
        const formGroupStyle = stylesheet.formGroup.normal;

        if (locals.hidden) {
            return null;
        }

        return (
            <View style={formGroupStyle}>
                <Text style={controlLabelStyle}>{locals.label}</Text>
                <RadioGroup
                    style={styles.radioGroup}
                    selectedIndex={this.selectValue(value)}
                    onChildrenPress={this.onValueChanged}
                >
                    {supportedGenders.map(this.renderRadio)}
                </RadioGroup>
            </View>
        );
    }
}

export default GenderSelect;
