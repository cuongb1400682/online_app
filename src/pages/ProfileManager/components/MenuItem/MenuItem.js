import React from "react";
import {View} from "react-native";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import FontIcon from "src/components/base/FontIcon/FontIcon";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import defaultOnPressHandler from "src/utils/reactNative/defaultOnPressHandler";
import Text from "src/components/platformSpecific/Text/Text";

const MenuItem = ({title = "", iconName = "", fontName = "fontawesome5", navigator, screenId}) => (
    <Touchable onPress={defaultOnPressHandler({navigator, screenId})}>
        <View style={styles.menuItem}>
            <View style={styles.menuIconContainer}>
                <FontIcon
                    name={iconName}
                    fontName={fontName}
                    style={styles.menuIcon}
                />
            </View>
            <Text style={styles.menuText}>{title}</Text>
            <FontAwesome style={styles.angleRight} name="chevron-right"/>
        </View>
    </Touchable>
);

export default MenuItem;
