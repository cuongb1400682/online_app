import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    menuItem: {
        width: "100%",
        height: 40,
        backgroundColor: "#fff",
        marginTop: 10,
        flexDirection: "row",
        alignItems: "center"
    },
    menuIcon: {
        fontSize: 15,
        color: "#999",
    },
    menuText: {
        fontSize: 13,
    },
    menuIconContainer: {
        width: 40,
        height: 40,
        ...commonStyles.centerChildren
    },
    angleRight: {
        fontSize: 10,
        color: "#1d1d1d",
        position: "absolute",
        right: 16
    }
});

export default styles;
