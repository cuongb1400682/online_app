import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    orderStepContainer: {
        marginTop: 6,
        height: 24,
        paddingHorizontal: 12,
        borderRadius: 4,
        flexDirection: "row",
        alignItems: "center",
        alignSelf: "flex-start"
    },
    orderStepBadge: {
        color: "#fff",
        fontSize: 13,
    }
});

export default styles;
