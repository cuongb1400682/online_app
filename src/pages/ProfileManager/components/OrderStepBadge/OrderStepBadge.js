import React from "react";
import {View} from "react-native";

import styles from "./styles";
import {tr} from "src/localization/localization";
import colors from "src/constants/colors";
import Text from "src/components/platformSpecific/Text/Text";

const OrderStepBadge = ({step = ""}) => {
    const orderStepContainerStyles = [
        styles.orderStepContainer,
        {backgroundColor: colors.orderStep[step.toLowerCase()]}
    ];
    return (
        <View style={orderStepContainerStyles}>
            <Text style={styles.orderStepBadge}>
                {tr(`order_list_item_step_${step}`)}
            </Text>
        </View>
    );
};

export default OrderStepBadge;
