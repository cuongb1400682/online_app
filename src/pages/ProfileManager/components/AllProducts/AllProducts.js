import React, {Component} from "react";
import {FlatList, View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import profileManagerStyles from "src/pages/ProfileManager/styles";
import commonStyles from "src/styles/commonStyles";
import FastImage from "src/components/base/FastImage/FastImage";
import {calculateDiscount, getSuffixDiscountLabel} from "src/utils/misc/businessLogics";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {makeCDNImageURL} from "src/utils/cdn/images";
import {redirectLinkTypes, imageTypes} from "src/constants/enums";
import {formatWithCurrency} from "src/utils/extensions/strings";
import openRedirectLink from "src/utils/reactNative/openRedirectLink";
import {tr} from "src/localization/localization";
import Text from "src/components/platformSpecific/Text/Text";

const BottomLine = ({left, right}) => (
    <View style={styles.bottomLine}>
        {typeof left === "string" ? <Text style={styles.bottomLineText}>{left}</Text> : left}
        <View style={commonStyles.matchParent}/>
        {typeof right === "string" ? <Text style={styles.bottomLineText}>{right}</Text> : right}
    </View>
);

class AllProducts extends Component {
    static propTypes = {
        orderDetail: PropTypes.any
    };

    static defaultProps = {
        orderDetail: {}
    };

    showProductDetail = (url) => () => openRedirectLink({
        navigator: this.props.navigator,
        link_type: redirectLinkTypes.PRODUCT,
        link: url
    });

    renderProduct = ({item = {}}) => {
        const {
            id: itemId,
            item_qty: itemQty,
            price: itemPrice,
            product = {},
            discount,
            discount_type: discountType,
            total_line: totalLine
        } = item;

        const {
            name: productName = "",
            original_thumbnail: thumbnail,
            url
        } = product;

        const discountAmount = calculateDiscount(discount, discountType, itemPrice);

        return (
            <View key={itemId} style={styles.product}>
                <Touchable
                    style={styles.productImageTouchable}
                    onPress={this.showProductDetail(url)}
                >
                    <FastImage
                        style={styles.productImage}
                        source={{uri: makeCDNImageURL(thumbnail, imageTypes.CART)}}
                    />
                </Touchable>
                <View style={styles.productNameContainer}>
                    <Text style={styles.productName}>{productName}</Text>
                    <Text style={styles.productPrice}>{formatWithCurrency(itemPrice - discountAmount)}</Text>
                </View>
                <View style={styles.productQuantityContainer}>
                    <Text style={styles.productQuantity}>x{itemQty}</Text>
                </View>
                <View style={styles.productTotalPriceContainer}>
                    <Text style={styles.productTotalPrice}>{formatWithCurrency(totalLine)}</Text>
                </View>
            </View>
        );
    };

    renderBottomLines = () => {
        const {so_lines = [], sub_total = 0, discount = 0, shipping_fee = 0, discount_type = ""} = this.props.orderDetail;
        const discountAmount = calculateDiscount(discount, discount_type, sub_total);
        const suffixDiscountLabel = getSuffixDiscountLabel(discount, discount_type);
        const hasDiscount = !!discountAmount;

        return (
            <View style={commonStyles.flexColumn}>
                <BottomLine
                    left={tr("all_product_temporary_summation")}
                    right={formatWithCurrency(sub_total)}
                />
                {hasDiscount && <BottomLine
                    left={tr("all_product_temporary_discount") + suffixDiscountLabel}
                    right={`-${formatWithCurrency(discountAmount)}`}
                />}
                <BottomLine
                    left={tr("all_product_temporary_shipping_fee")}
                    right={formatWithCurrency(shipping_fee)}
                />
                <BottomLine
                    left={tr("all_product_temporary_final_estimation", so_lines.length)}
                    right={
                        <View style={styles.finalEstimationContainer}>
                            <Text style={styles.finalEstimatedPrice}>
                                {formatWithCurrency(sub_total + shipping_fee - discountAmount)}
                            </Text>
                            <Text style={styles.vatIncluded}>
                                {tr("all_product_vat_included")}
                            </Text>
                        </View>
                    }
                />
            </View>
        );
    };

    render() {
        const {so_lines = []} = this.props.orderDetail;

        return (
            <View>
                <View style={styles.allProductTitleContainer}>
                    <Text style={profileManagerStyles.commonText}>{tr("all_product_section_title")}</Text>
                </View>
                <FlatList
                    style={styles.flatList}
                    renderItem={this.renderProduct}
                    data={so_lines}
                    keyExtractor={(_, index) => `${index}`}
                    ItemSeparatorComponent={() => <View style={styles.separator}/>}
                    ListEmptyComponent={<View/>}
                />
                {this.renderBottomLines()}
            </View>
        );
    }
}

export default AllProducts;
