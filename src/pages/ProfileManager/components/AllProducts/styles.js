import {StyleSheet} from "react-native";

import profileManagerStyles from "src/pages/ProfileManager/styles";
import commonStyles from "src/styles/commonStyles";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    flatList: {
        flex: 1,
        ...profileManagerStyles.hasBottomBorder
    },
    product: {
        width: "100%",
        height: 68,
        flexDirection: "row",
        alignItems: "center",
        paddingRight: 6
    },
    productImage: {
        width: 48,
        height: 48
    },
    productImageTouchable: {
        width: 68,
        height: 68,
        ...commonStyles.centerChildren
    },
    separator: {
        width: "100%",
        borderStyle: "dashed",
        borderWidth: 0.5,
        borderColor: "#ebebeb"
    },
    productNameContainer: {
        flex: 1,
        flexWrap: "wrap",
        flexDirection: "column",
        justifyContent: "center",
    },
    productName: {
        height: 30,
        ...profileManagerStyles.commonText
    },
    productPrice: {
        color: colors.primaryColor,
        fontWeight: "bold",
        ...profileManagerStyles.commonText
    },
    productQuantityContainer: {
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center",
        width: 50,
        height: "100%"
    },
    productQuantity: {
        ...profileManagerStyles.commonText
    },
    productTotalPriceContainer: {
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center",
        width: 90,
        height: "100%"
    },
    productTotalPrice: {
        ...profileManagerStyles.commonText
    },
    allProductTitleContainer: {
        width: "100%",
        height: 45,
        backgroundColor: "#FAFAFA",
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 8
    },
    bottomLine: {
        flexDirection: "row",
        paddingHorizontal: 8,
        marginVertical: 8
    },
    bottomLineText: {
        ...profileManagerStyles.commonText
    },
    finalEstimationContainer: {
        flexDirection: "column",
        alignItems: "flex-end",
        justifyContent: "flex-end"
    },
    finalEstimatedPrice: {
        fontSize: 16,
        color: colors.primaryColor
    },
    vatIncluded: {
        fontSize: 9,
        color: "#757575"
    }
});

export default styles;
