import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    iconContainer: {
        width: 40,
        height: 40,
        borderRadius: 40,
        borderWidth: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    activeIconContainer: {
        backgroundColor: colors.orderStatus.active,
        borderColor: colors.orderStatus.active
    },
    inactiveIconContainer: {
        backgroundColor: "#fff",
        borderColor: colors.orderStatus.inactive,
    }
});

export default styles;
