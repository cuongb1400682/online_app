import React from "react";
import {View} from "react-native";

import styles from "./styles";
import {orderManagerSteps} from "src/constants/enums";
import OrderStatusCompleted from "src/components/base/svgIcons/OrderStatusCompleted/OrderStatusCompleted";
import OrderStatusCreated from "src/components/base/svgIcons/OrderStatusCreated/OrderStatusCreated";
import OrderStatusPacking from "src/components/base/svgIcons/OrderStatusPacking/OrderStatusPacking";
import OrderStatusShipping from "src/components/base/svgIcons/OrderStatusShippng/OrderStatusShipping";
import colors from "src/constants/colors";

const OrderStatusIcon = ({active = false, type = orderManagerSteps.CREATION}) => {
    const Icon = ({
        [orderManagerSteps.CREATION]: OrderStatusCreated,
        [orderManagerSteps.PICKING]: OrderStatusPacking,
        [orderManagerSteps.SHIPPING]: OrderStatusShipping,
        [orderManagerSteps.COMPLETED]: OrderStatusCompleted,
    })[type] || OrderStatusCompleted;

    const iconContainerStyles = [
        styles.iconContainer,
        active
            ? styles.activeIconContainer
            : styles.inactiveIconContainer,
    ];

    return (
        <View style={iconContainerStyles}>
            <Icon color={active ? "#fff" : colors.orderStatus.inactive}/>
        </View>
    );
};

export default OrderStatusIcon;

