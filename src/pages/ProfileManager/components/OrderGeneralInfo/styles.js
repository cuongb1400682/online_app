import {StyleSheet} from "react-native";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    infoLine: {
        flexDirection: "row",
        marginBottom: 4,
        flexWrap: "wrap"
    },
    infoLineTitle: {
        color: "#222",
        fontSize: 12
    },
    infoLineInfo: {
        color: "#222",
        fontSize: 12,
        fontWeight: "bold"
    },
});

export default styles;
