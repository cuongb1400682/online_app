import React, {Component} from "react";
import {View} from "react-native";
import moment from "moment";

import styles from "./styles";
import PropTypes from "prop-types";
import {tr} from "src/localization/localization";
import {getEstimateShippingTime} from "src/utils/extensions/dateTime";
import profileManagerStyles from "src/pages/ProfileManager/styles";
import Text from "src/components/platformSpecific/Text/Text";

const InfoLine = ({title = "", info = ""}) => {
    return (
        <View style={styles.infoLine}>
            <Text style={styles.infoLineTitle}>{title}</Text>
            <Text style={styles.infoLineInfo}>{info}</Text>
        </View>
    )
};

class OrderGeneralInfo extends Component {
    static propTypes = {
        orderDetail: PropTypes.any
    };

    static defaultProps = {
        orderDetail: {}
    };

    render() {
        const {
            order_number = "",
            created_time = "",
            from_expected_shipping_time = "",
            to_expected_shipping_time = ""
        } = this.props.orderDetail;

        return (
            <View style={profileManagerStyles.orderDetailSection}>
                <InfoLine
                    title={tr("order_general_info_order_number")}
                    info={`#${order_number}`}
                />
                <InfoLine
                    title={tr("order_general_info_created_date")}
                    info={moment(created_time).format("DD/MM/YYYY")}
                />
                <InfoLine
                    title={tr("order_general_info_estimated_shipping_time")}
                    info={getEstimateShippingTime(from_expected_shipping_time, to_expected_shipping_time)}
                />
            </View>
        );
    }
}

export default OrderGeneralInfo;
