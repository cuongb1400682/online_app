import {StyleSheet} from "react-native";

import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    orderListItem: {
        height: 115,
        margin: 16,
        flexDirection: "column"
    },
    chevron: {
        fontSize: 12,
        position: "absolute",
        right: 10,
        top: 10,
        color: "#292929"
    },
    orderListItemText: {
        paddingTop: 6,
        fontSize: 13,
        color: "#757575",
    },
    orderNumber: {
        fontSize: 13,
        fontWeight: "bold",
        color: "#222"
    },
    moneyAmount: {
        fontSize: 13,
        color: colors.primaryColor
    },
    noPaddingTop: {
        paddingTop: 0
    }
});

export default styles;
