import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import moment from "moment";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {tr} from "src/localization/localization";
import {formatWithCurrency} from "src/utils/extensions/strings";
import FontIcon from "src/components/base/FontIcon/FontIcon";
import OrderStepBadge from "src/pages/ProfileManager/components/OrderStepBadge/OrderStepBadge";
import Text from "src/components/platformSpecific/Text/Text";

class OrderListItem extends Component {
    static propTypes = {
        order: PropTypes.any,
        onPress: PropTypes.func
    };

    static defaultProps = {
        order: {},
        onPress: () => {
        }
    };

    render() {
        const {onPress, order} = this.props;
        const {
            order_step: orderStep = "",
            order_number: orderNumber = "",
            created_time: createdTime = "",
            payment_method: paymentMethod = "",
            total = 0,
        } = order;

        return (
            <Touchable onPress={onPress}>
                <View style={styles.orderListItem}>
                    <Text style={[styles.orderListItemText, styles.noPaddingTop]}>
                        {tr("order_list_item_order_number")}
                        <Text style={styles.orderNumber}>#{orderNumber}</Text>
                    </Text>
                    <Text style={styles.orderListItemText}>
                        {tr("order_list_item_created_date")}
                        <Text>{moment(createdTime).format("DD/MM/YYYY")}</Text>
                    </Text>
                    <Text style={styles.orderListItemText}>
                        {tr("order_list_item_payment_method")}
                        {tr(paymentMethod)}
                    </Text>
                    <Text style={styles.orderListItemText}>
                        {tr("order_list_item_total")}
                        <Text style={styles.moneyAmount}>{formatWithCurrency(total)}</Text>
                    </Text>
                    <OrderStepBadge step={orderStep}/>
                    <FontIcon
                        fontName="FontAwesome"
                        name="chevron-right"
                        style={styles.chevron}
                    />
                </View>
            </Touchable>
        );
    }

}

export default OrderListItem;
