import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    emptyList: {
        width: "100%",
        marginTop: 16,
        height: 30,
        ...commonStyles.centerChildren
    }
});

export default styles;
