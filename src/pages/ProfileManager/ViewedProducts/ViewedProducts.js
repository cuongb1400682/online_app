import React, {Component} from "react";
import {FlatList, RefreshControl, View} from "react-native";
import {connect} from "react-redux"
import PropTypes from "prop-types";

import NavBar from "src/containers/NavBar/NavBar";
import {tr} from "src/localization/localization";
import commonStyles from "src/styles/commonStyles";
import {fetchViewedProducts} from "src/pages/ProfileManager/profileManagerActions";
import HorizontalProduct from "src/components/base/HorizontalProduct/HorizontalProduct";
import {pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";
import styles from "src/pages/ProfileManager/ViewedProducts/styles";
import Text from "src/components/platformSpecific/Text/Text";

class ViewedProducts extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        tabBarHidden: true
    };

    static propTypes = {
        products: PropTypes.array,
        fetchingViewedProducts: PropTypes.bool,
        fetchViewedProducts: PropTypes.func,
    };

    static defaultProps = {
        products: [],
        fetchingViewedProducts: false,
        fetchViewedProducts: () => {
        },
    };

    componentDidMount() {
        const {fetchViewedProducts} = this.props;

        fetchViewedProducts();
    }

    showProductDetail = (item = {}) => () => {
        const {navigator} = this.props;
        const {id: productId} = item;

        pushTo(navigator, {
            screen: screenIds.PRODUCT_DETAIL,
            passProps: {
                productId
            }
        });
    };

    renderItem = ({item = {}}) => (
        <HorizontalProduct
            source={item}
            onPress={this.showProductDetail(item)}
        />
    );

    render() {
        const {navigator, products, fetchingViewedProducts, fetchViewedProducts} = this.props;

        return (
            <View style={commonStyles.matchParent}>
                <NavBar
                    navigator={navigator}
                    title={tr("viewed_products_nav_bar_title")}
                />
                <FlatList
                    style={commonStyles.matchParent}
                    data={products}
                    keyExtractor={(_, index) => index.toString()}
                    renderItem={this.renderItem}
                    ListEmptyComponent={
                        <View style={styles.emptyList}>
                            <Text>{tr("viewed_products_empty_list")}</Text>
                        </View>
                    }
                    refreshControl={
                        <RefreshControl
                            refreshing={fetchingViewedProducts}
                            onRefresh={fetchViewedProducts}
                        />
                    }
                />
            </View>
        );
    }
}

const mapStateToProps = state => ({
    products: state.profile_manager.viewed_products.products,
    fetchingViewedProducts: state.profile_manager.viewed_products.fetching
});

export default connect(mapStateToProps, {
    fetchViewedProducts
})(ViewedProducts);
