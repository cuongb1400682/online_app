import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import searchResultActionTypes from "src/redux/actionTypes/searchResultActionTypes";
import {productViewModes} from "src/constants/enums";

const defaultSearchState = {
    products: [],
    number_items: 0,
    searching: false,
    params: {
        category_id: "-1",
        q: ""
    }
};

const initialState = {
    viewMode: productViewModes.GRID,
    ...defaultSearchState
};

const searchReducer = (state = initialState, {type, payload, meta}) => {
    switch (type) {
        case searchResultActionTypes.CHANGE_VIEW_MODE:
            return {
                ...state,
                viewMode: payload.mode
            };
        case toRequest(searchResultActionTypes.SEARCH_PRODUCT_BY_KEYWORD):
            if (state.params.q !== meta.q) {
                return {
                    ...state,
                    ...defaultSearchState,
                    params: {
                        ...meta
                    },
                    searching: true
                };
            }

            return {
                ...state,
                params: {
                    ...state.params,
                    ...meta
                },
                searching: true
            };
        case toSuccess(searchResultActionTypes.SEARCH_PRODUCT_BY_KEYWORD):
            const {products, ...others} = payload.reply;

            return {
                ...state,
                ...others,
                products: [...state.products, ...products],
                searching: false
            };
        case toFailure(searchResultActionTypes.SEARCH_PRODUCT_BY_KEYWORD):
            return {
                ...state,
                ...defaultSearchState
            };
        default:
            return state;
    }
};

export default searchReducer;
