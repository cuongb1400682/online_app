import React, {Component} from "react";
import {View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import {tr} from "src/localization/localization";
import {changeViewMode, searchProductByKeyword} from "src/pages/SearchResult/searchResultActions";
import {productViewModes} from "src/constants/enums";
import FilterBar from "src/components/base/FilterBar/FilterBar";
import ProductList from "src/components/base/ProductList/ProductList";
import LoadMore from "src/components/base/LoadMore/LoadMore";
import {popBack} from "src/utils/misc/navigator";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";

const DEFAULT_PAGE_SIZE = 50;

class SearchResult extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        searchParams: PropTypes.object,
        params: PropTypes.object,
        products: PropTypes.array,
        searching: PropTypes.bool,
        totalItems: PropTypes.number,
        viewMode: PropTypes.oneOf([
            productViewModes.LIST,
            productViewModes.GRID
        ]),
        changeViewMode: PropTypes.func,
        searchProductByKeyword: PropTypes.func,
    };

    static defaultProps = {
        searchParams: {},
        params: {},
        products: [],
        searching: false,
        totalItems: 0,
        viewMode: productViewModes.GRID,
        changeViewMode: () => {
        },
        searchProductByKeyword: () => {
        },
    };

    componentDidMount() {
        const {searchProductByKeyword, searchParams, params} = this.props;

        if (searchParams.q !== params.q) {
            searchProductByKeyword({
                ...searchParams,
                page_size: DEFAULT_PAGE_SIZE,
                page_index: 1
            });
        } else {
            this.forceUpdate();
        }
    }

    onFetchNextProduct = () => {
        const {products} = this.props;
        const nextPageIndex = Math.ceil(products.length / DEFAULT_PAGE_SIZE) + 1;

        this.props.searchProductByKeyword({
            ...this.props.params,
            page_size: DEFAULT_PAGE_SIZE,
            page_index: nextPageIndex
        });
    };

    renderFooter = () => {
        const {searching, totalItems, products} = this.props;

        return (
            <LoadMore
                onFetchNextProduct={this.onFetchNextProduct}
                searching={searching}
                maximumNumberOfItems={totalItems}
                currentNumberOfItems={products.length}
            />
        );
    };

    render() {
        const {navigator, products, viewMode, changeViewMode} = this.props;

        return (
            <View style={styles.search}>
                <NavBar
                    title={tr("search_nav_bar_title")}
                    right={
                        <NavBarShortcuts
                            navigator={navigator}
                            showsSearch
                            showsCart
                            showsVerticalEllipsis
                            onSearchPress={() => popBack(navigator)}
                        />
                    }
                    navigator={navigator}
                />
                <ProductList
                    headerHeight={54}
                    header={
                        <FilterBar
                            changeViewMode={changeViewMode}
                            viewMode={viewMode}
                        />
                    }
                    footerHeight={50}
                    footer={this.renderFooter()}
                    emptyListHeight={50}
                    emptyList={tr("search_result_empty_list")}
                    products={products}
                    navigator={navigator}
                    showsHorizontalProduct={viewMode === productViewModes.LIST}
                />
                <FacebookMessenger floating/>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    params: state.search_result.params,
    products: state.search_result.products,
    searching: state.search_result.searching,
    totalItems: state.search_result.number_items,
    viewMode: state.search_result.viewMode
});

export default connect(mapStateToProps, {
    changeViewMode,
    searchProductByKeyword
})(SearchResult);

