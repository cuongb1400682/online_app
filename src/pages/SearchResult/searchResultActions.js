import searchResultActionTypes from "src/redux/actionTypes/searchResultActionTypes";
import {createAction, createAsyncAction} from "src/utils/api/createAction";
import {decodeSearchKeyword} from "src/utils/api/queryString";

const changeViewMode = (mode) => createAction({
    type: searchResultActionTypes.CHANGE_VIEW_MODE,
    payload: {
        mode
    }
});

const searchProductByKeyword = (params, callback) => {
    const {page_size, page_index, sort_by, filters, q: keyword, category_id} = params;

    return createAsyncAction({
        type: searchResultActionTypes.SEARCH_PRODUCT_BY_KEYWORD,
        payload: {
            request: "/product/search",
            params: {
                key: decodeSearchKeyword(keyword),
                page_size,
                page_index: Math.max(page_index - 1, 0),
                sort_by: sort_by,
                filters: filters,
                category_id: category_id
            }
        },
        meta: {
            ...params
        },
        onSuccess: () => {
            if (typeof callback === "function") {
                callback();
            }
        }
    });
};

export {
    changeViewMode,
    searchProductByKeyword
};

