import React, {Component} from "react";
import {FlatList, View} from "react-native";
import PropTypes from "prop-types";
import styles from "./styles";

import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {makeCDNImageURL} from "src/utils/cdn/images";
import FastImage from "src/components/base/FastImage/FastImage";
import {tr} from "src/localization/localization";
import Text from "src/components/platformSpecific/Text/Text";

class SubCategoryBar extends Component {
    static HEIGHT = styles.subCategoryBar.height;
    static MARGIN_BUTTON = styles.subCategoryBar.marginBottom;

    static propTypes = {
        selectedIndex: PropTypes.number,
        data: PropTypes.array,
        onCategoryPress: PropTypes.func
    };

    static defaultProps = {
        selectedIndex: 0,
        data: [],
        onCategoryPress: () => {
        }
    };

    handleCategoryPress = (category = {}, index) => () => {
        const {onCategoryPress} = this.props;

        onCategoryPress(category, index);
    };

    renderCategory = ({item: category = {}, index}) => {
        const {selectedIndex} = this.props;

        const isSelected = (index === selectedIndex);

        const categoryNameStyles = [
            styles.categoryName,
            isSelected ? styles.selectedCategoryName : {}
        ];

        const categoryNameWrapperStyles = [
            styles.categoryNameWrapper,
            isSelected ? styles.selectedCategoryNameWrapper : {}
        ];

        return (
            <Touchable onPress={this.handleCategoryPress(category, index)}>
                <View style={styles.categoryItem}>
                    <FastImage
                        style={styles.categoryImage}
                        source={{uri: makeCDNImageURL(category.mobile_icon)}}
                    />
                    <View style={categoryNameWrapperStyles}>
                        <Text style={categoryNameStyles}>{category.name}</Text>
                    </View>
                </View>
            </Touchable>
        );
    };

    render() {
        const {data} = this.props;

        return (
            <View style={styles.subCategoryBar}>
                <View style={styles.subCategoryBarTitleContainer}>
                    <Text style={styles.subCategoryBarTitle}>{tr("sub_category_bar_title")}</Text>
                </View>
                <FlatList
                    style={styles.flatList}
                    data={data}
                    keyExtractor={(_, index) => `${index}`}
                    extraData={this.props.selectedIndex}
                    horizontal
                    renderItem={this.renderCategory}
                    ListEmptyComponent={<View/>}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        );
    }
}

export default SubCategoryBar;
