import {StyleSheet} from "react-native";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const CATEGORY_IMAGE_SIZE = 90;

const styles = StyleSheet.create({
    subCategoryBar: {
        width: "100%",
        backgroundColor: "#fff",
        marginBottom: 8,
        height: 190
    },
    subCategoryBarTitleContainer: {
        height: 40,
        paddingLeft: 16,
        flexDirection: "column",
        justifyContent: "center"
    },
    subCategoryBarTitle: {
        fontSize: 14,
        fontWeight: "bold",
    },
    categoryImage: {
        width: CATEGORY_IMAGE_SIZE,
        height: CATEGORY_IMAGE_SIZE,
    },
    selectedCategoryName: {
        color: colors.primaryColor
    },
    categoryName: {
        fontSize: 13,
        color: "#333",
        textAlign: "center"
    },
    categoryNameWrapper: {
        paddingVertical: 2,
        paddingHorizontal: 2,
    },
    selectedCategoryNameWrapper: {
        color: "#fff"
    },
    categoryItem: {
        width: 120,
        height: 150,
        marginHorizontal: 8,
        paddingVertical: 8,
        flexDirection: "column",
        alignItems: "center",
        flexWrap: "wrap",
    },
    flatList: {
        paddingBottom: 8
    }
});

export default styles;
