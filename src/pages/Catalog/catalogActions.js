import {createAction, createAsyncAction} from "src/utils/api/createAction";
import catalogActionTypes from "src/redux/actionTypes/catalogActionTypes";

const changeViewMode = (mode) => {
    return createAction({
        type: catalogActionTypes.CHANGE_VIEW_MODE,
        payload: {
            mode
        }
    });
};

const fetchProductsByCategory = ({pageSize, pageIndex, url, sortBy, filters}) => createAsyncAction({
    type: catalogActionTypes.FETCH_NEXT_PRODUCT_BY_CATEGORY,
    payload: {
        request: "/product/search",
        params: {
            url,
            page_size: pageSize,
            page_index: pageIndex - 1,
            sort_by: sortBy,
            filters: filters,
        }
    },
    meta: {
        url,
        page_index: pageIndex
    }
});

const clearProducts = () => createAction({
    type: catalogActionTypes.CLEAR_PRODUCTS,
    payload: {}
});

export {
    clearProducts,
    changeViewMode,
    fetchProductsByCategory,
};
