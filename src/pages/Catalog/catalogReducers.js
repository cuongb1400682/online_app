import catalogActionTypes from "src/redux/actionTypes/catalogActionTypes";
import {productViewModes} from "src/constants/enums";
import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";

const initialState = {
    viewMode: productViewModes.GRID,
    products: [],
    fetching: false,
    number_items: 0,
    meta: {
        page_index: 1
    }
};

 const catalogReducer = (state = initialState, {type, payload, meta = {}}) => {
    switch (type) {
        case catalogActionTypes.CLEAR_PRODUCTS:
            return {
                ...initialState,
                products: [],
                viewMode: state.viewMode || productViewModes.GRID
            };
        case catalogActionTypes.CHANGE_VIEW_MODE:
            return {
                ...state,
                viewMode: payload.mode
            };
        case toRequest(catalogActionTypes.FETCH_NEXT_PRODUCT_BY_CATEGORY):
            const keepProducts = state.meta.url === meta.url;

            return {
                ...state,
                products: keepProducts ? [...state.products] : [],
                meta: {...meta},
                fetching: true
            };
        case toSuccess(catalogActionTypes.FETCH_NEXT_PRODUCT_BY_CATEGORY):
            const {products, ...others} = payload.reply;

            return {
                ...state,
                ...others,
                products: [...state.products, ...products],
                fetching: false
            };
        case toFailure(catalogActionTypes.FETCH_NEXT_PRODUCT_BY_CATEGORY):
            return {
                ...state,
                fetching: false
            };
        default:
            return state;
    }
};

 export default catalogReducer;

