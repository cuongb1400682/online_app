import {StyleSheet} from "react-native";
import {catalogByCategoryBannerHeight} from "src/utils/layouts/catalogByCategoryLayout";

const styles = StyleSheet.create({
    catalogByCategory: {
        flex: 1,
        backgroundColor: "#eee"
    },
    banner: {
        width: "100%",
        height: catalogByCategoryBannerHeight()
    }
});

export default styles;
