import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import SubCategoryBar from "src/pages/Catalog/components/SubCategoryBar/SubCategoryBar";
import FilterBar from "src/components/base/FilterBar/FilterBar";
import {changeViewMode, clearProducts, fetchProductsByCategory} from "src/pages/Catalog/catalogActions";
import ProductList from "src/components/base/ProductList/ProductList";
import FastImage from "src/components/base/FastImage/FastImage";
import {productViewModes} from "src/constants/enums";
import commonStyles from "src/styles/commonStyles";
import LoadMore from "src/components/base/LoadMore/LoadMore";
import {makeCDNImageURL} from "src/utils/cdn/images";
import {catalogByCategoryBannerHeight} from "src/utils/layouts/catalogByCategoryLayout";
import {isEmptyString} from "src/utils/extensions/strings";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";
import {tr} from "src/localization/localization";

const DEFAULT_PAGE_SIZE = 50;

const HEADER_HEIGHT_WITHOUT_BANNER = (
    SubCategoryBar.HEIGHT +
    SubCategoryBar.MARGIN_BUTTON +
    FilterBar.HEIGHT +
    FilterBar.MARGIN_BOTTOM
);

const HEADER_HEIGHT = HEADER_HEIGHT_WITHOUT_BANNER + catalogByCategoryBannerHeight();

const FOOTER_HEIGHT = 50;

class CatalogByCategory extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        isFetchingProduct: PropTypes.bool,
        totalItems: PropTypes.number,
        products: PropTypes.array,
        viewMode: PropTypes.oneOf([
            productViewModes.LIST,
            productViewModes.GRID
        ]),
        meta: PropTypes.object,
        selectedCategory: PropTypes.object,
        subCategoryIndex: PropTypes.number,
        changeViewMode: PropTypes.func,
        fetchProductsByCategory: PropTypes.func,
        clearProducts: PropTypes.func,
    };

    static defaultProps = {
        isFetchingProduct: false,
        totalItems: 0,
        products: [],
        viewMode: productViewModes.LIST,
        meta: {},
        selectedCategory: {},
        subCategoryIndex: -1,
        changeViewMode: () => {
        },
        fetchProductsByCategory: () => {
        },
        clearProducts: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedSubCategoryIndex: props.subCategoryIndex,
            products: []
        };
    }

    componentDidMount() {
        const {subCategoryIndex} = this.props;

        this.setState({products: this.props.products})

        this.setState({selectedSubCategoryIndex: subCategoryIndex});
        this.fetchNextProductsByCategoryPage();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (this.props.products !== nextProps.products)
            return true;
        if (this.state !== nextState)
            return true;
        return false;
    }

    componentWillMount() {
        const {clearProducts} = this.props;

        clearProducts();
    }

    componentWillUnmount() {
        const {clearProducts} = this.props;

        clearProducts();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props !== prevProps) {
            this.setState({products: this.props.products});
        }
    }

    getCurrentSelectedSubCategory = () => {
        const {selectedCategory = {}} = this.props;
        const {selectedSubCategoryIndex} = this.state;
        const {sub_categories = []} = selectedCategory;
        const subCategory = sub_categories[selectedSubCategoryIndex];

        return subCategory || selectedCategory;
    };

    getCurrentPageIndex = () => {
        const {products} = this.state;

        return Math.ceil(products.length / DEFAULT_PAGE_SIZE);
    };

    fetchProductByCategory = () => {
        const subCategory = this.getCurrentSelectedSubCategory();

        this.props.clearProducts();
        this.props.fetchProductsByCategory({
            url: subCategory.url,
            pageIndex: 1,
            pageSize: DEFAULT_PAGE_SIZE,
        });
    };

    fetchNextProductsByCategoryPage = () => {
        const nextPageIndex = this.getCurrentPageIndex() + 1;
        const subCategory = this.getCurrentSelectedSubCategory();

        this.props.fetchProductsByCategory({
            url: subCategory.url,
            pageSize: DEFAULT_PAGE_SIZE,
            pageIndex: nextPageIndex
        });
    };

    getHeaderTitle = () => {
        const {selectedCategory} = this.props;

        if (!selectedCategory.sub_categories) {
            return selectedCategory.name;
        }

        const {sub_categories = []} = selectedCategory;
        const {selectedSubCategoryIndex} = this.state;
        const categoryToDisplay = sub_categories[selectedSubCategoryIndex] || selectedCategory

        return categoryToDisplay.name;
    };

    changeSubCategory = (_, index) => {
        this.setState({
            selectedSubCategoryIndex: index
        }, this.fetchProductByCategory);
    };

    renderNavBarShortcuts = () => (
        <NavBarShortcuts
            showsCart
            showsSearch
            showsVerticalEllipsis
            navigator={this.props.navigator}
        />
    );

    hasListingBanner = () => {
        const {mobile_listing_banner = ""} = this.props.selectedCategory || {};

        return !isEmptyString(mobile_listing_banner);
    };

    renderBanner = () => {
        const {mobile_listing_banner = ""} = this.props.selectedCategory || {};

        return (
            this.hasListingBanner() && <View style={styles.banner}>
                <FastImage
                    style={commonStyles.matchParent}
                    source={{uri: makeCDNImageURL(mobile_listing_banner)}}
                />
            </View>
        )
    };

    renderHeader = () => {
        const {selectedCategory = {}, viewMode, changeViewMode} = this.props;
        const {selectedSubCategoryIndex} = this.state;
        const {sub_categories = []} = selectedCategory;

        return (
            <React.Fragment>
                {this.renderBanner()}
                <SubCategoryBar
                    data={sub_categories}
                    onCategoryPress={this.changeSubCategory}
                    selectedIndex={selectedSubCategoryIndex}
                />
                <FilterBar viewMode={viewMode} changeViewMode={changeViewMode}/>
            </React.Fragment>
        );
    };

    renderFooter = () => {
        const {isFetchingProduct, totalItems, products} = this.props;

        return (
            <LoadMore
                onFetchNextProduct={this.fetchNextProductsByCategoryPage}
                searching={isFetchingProduct}
                maximumNumberOfItems={totalItems}
                currentNumberOfItems={products.length}
            />
        );
    };

    render() {
        const {navigator, products, viewMode} = this.props;

        return (
            <View style={styles.catalogByCategory}>
                <NavBar
                    right={this.renderNavBarShortcuts()}
                    title={this.getHeaderTitle()}
                    navigator={navigator}
                />
                <View style={commonStyles.matchParent}>
                    <ProductList
                        header={this.renderHeader()}
                        footer={this.renderFooter()}
                        headerHeight={
                            this.hasListingBanner()
                                ? HEADER_HEIGHT
                                : HEADER_HEIGHT_WITHOUT_BANNER
                        }
                        emptyListHeight={50}
                        emptyList={tr("catalog_by_category_empty_list")}
                        footerHeight={FOOTER_HEIGHT}
                        products={products}
                        navigator={navigator}
                        showsHorizontalProduct={viewMode === productViewModes.LIST}
                    />
                </View>
                <FacebookMessenger floating/>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    isFetchingProduct: state.catalog.fetching,
    totalItems: state.catalog.number_items,
    products: state.catalog.products,
    viewMode: state.catalog.viewMode,
    meta: state.catalog.meta,
});

export default connect(mapStateToProps, {
    clearProducts,
    changeViewMode,
    fetchProductsByCategory,
})(CatalogByCategory);

