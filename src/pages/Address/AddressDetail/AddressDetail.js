import React, {Component} from "react";
import {View, KeyboardAvoidingView} from "react-native";
import {connect} from "react-redux";
import * as tComb from "tcomb-form-native";
import PropTypes from "prop-types";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view"

import styles from "./styles";
import commonStyles from "src/styles/commonStyles";
import NavBar from "src/containers/NavBar/NavBar";
import {tr} from "src/localization/localization";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import colors from "src/constants/colors";
import AddressSelectionBox from "src/pages/Address/components/AddressSelectionBox/AddressSelectionBox";
import {
    addNewAddress,
    clearAddress,
    fetchAddressLocation,
    fetchCustomerAddressById,
    fetchUserInformation,
    updateCustomerAddress
} from "src/pages/Address/addressActions";
import Button from "src/components/base/Button/Button";
import {requireField} from "src/utils/misc/formValidators";
import {isEmptyArray} from "src/utils/extensions/arrays";
import {isEmptyString} from "src/utils/extensions/strings";
import {objectCompare} from "src/utils/misc/object";
import {addressDetailScreenTypes} from "src/constants/enums";
import KeyboardAwareView from "src/components/platformSpecific/KeyboardAwareView/KeyboardAwareView";

const localRequireField = (value) => {
    if (!value || parseInt(value, 10) === -1 || isEmptyString(value)) {
        return tr("required_field");
    }
};

const formStructure = tComb.struct({
    name: tComb.String,
    phone: tComb.String,
    provinceId: tComb.Number,
    districtId: tComb.Number,
    wardId: tComb.Number,
    address: tComb.String
});

const commonTextInputOptions = {
    selectionColor: colors.primaryColor,
    autoCapitalize: "none",
    allowFontScaling: false
};

class AddressDetail extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        tabBarHidden: true
    };

    static propTypes = {
        firstAddress: PropTypes.bool,
        addressId: PropTypes.any,
        address: PropTypes.object,
        provinces: PropTypes.array,
        districts: PropTypes.array,
        wards: PropTypes.array,
        fetchUserInformation: PropTypes.func,
        addNewAddress: PropTypes.func,
        updateCustomerAddress: PropTypes.func,
        fetchCustomerAddressById: PropTypes.func,
        fetchAddressLocation: PropTypes.func,
        clearAddress: PropTypes.func,
        onAddressDidSave: PropTypes.func
    };

    static defaultProps = {
        firstAddress: false,
        addressId: null,
        address: {},
        provinces: [],
        districts: [],
        wards: [],
        fetchUserInformation: () => {
        },
        addNewAddress: () => {
        },
        updateCustomerAddress: () => {
        },
        fetchCustomerAddressById: () => {
        },
        fetchAddressLocation: () => {
        },
        clearAddress: () => {
        },
        onAddressDidSave: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            formValues: {},
        };

        this.screenType = addressDetailScreenTypes.CREATE;
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const newFormValues = {
            name: nextProps.address.contact_name,
            phone: nextProps.address.contact_phone,
            provinceId: nextProps.address.province_id || 1, // TODO: Ho Chi Minh city is default
            districtId: nextProps.address.district_id,
            wardId: nextProps.address.ward_id,
            address: nextProps.address.address
        };

        if (!objectCompare(newFormValues, this.state.formValues)) {
            this.setState({formValues: newFormValues});
        }
    }

    componentDidMount() {
        const {
            clearAddress,
            fetchUserInformation,
            fetchCustomerAddressById,
            fetchAddressLocation,
            firstAddress,
            addressId
        } = this.props;

        if (addressId) {
            fetchCustomerAddressById(addressId);
            this.screenType = addressDetailScreenTypes.EDIT;
        } else {
            clearAddress();

            if (firstAddress) {
                fetchUserInformation();
            }
        }

        fetchAddressLocation();
    }

    componentWillUnmount() {
        const {clearAddress} = this.props;

        clearAddress();
    }

    onFormValuesChange = (formValues = {}) => {
        const {provinceId, districtId} = formValues;
        const {formValues: oldFormValues} = this.state;

        if (provinceId !== oldFormValues.provinceId) {
            formValues.districtId = undefined;
        }

        if (districtId !== oldFormValues.districtId) {
            formValues.wardId = undefined;
        }

        this.setState({formValues});
    };

    validateValues = () => {
        const {errors = []} = this.formRef.validate();

        return isEmptyArray(errors);
    };

    saveCustomerAddress = () => {
        const allFormValuesValid = this.validateValues();

        if (!allFormValuesValid) {
            return;
        }

        const {addressId, onAddressDidSave, updateCustomerAddress, addNewAddress} = this.props;
        const {name, phone, wardId, address} = this.state.formValues;

        if (this.screenType === addressDetailScreenTypes.EDIT) {
            updateCustomerAddress({addressId, name, phone, address, wardId}, onAddressDidSave);
        } else {
            addNewAddress({name, phone, address, wardId}, onAddressDidSave);
        }
    };

    getFormOptions = () => {
        const {provinces, districts, wards} = this.props;
        const {formValues = {}} = this.state;
        const selectedDistricts = districts.filter(district => district.province_id === formValues.provinceId);
        const selectedWards = wards.filter(ward => ward.district_id === formValues.districtId);

        return {
            fields: {
                name: {
                    ...commonTextInputOptions,
                    label: tr("address_detail_name_label"),
                    placeholder: tr("address_detail_name_placeholder"),
                    autoCapitalize: "words",
                    error: localRequireField,
                },
                phone: {
                    ...commonTextInputOptions,
                    label: tr("address_detail_phone_label"),
                    placeholder: tr("address_detail_phone_placeholder"),
                    keyboardType: "phone-pad",
                    error: localRequireField,
                },
                provinceId: {
                    label: tr("address_detail_province_label"),
                    placeholder: tr("address_detail_province_placeholder"),
                    error: localRequireField,
                    template: locals => <AddressSelectionBox locals={locals}
                                                             dataSource={provinces}/>
                },
                districtId: {
                    label: tr("address_detail_district_label"),
                    placeholder: tr("address_detail_district_placeholder"),
                    error: localRequireField,
                    template: locals => <AddressSelectionBox locals={locals}
                                                             dataSource={selectedDistricts}/>
                },
                wardId: {
                    label: tr("address_detail_ward_label"),
                    placeholder: tr("address_detail_ward_placeholder"),
                    error: localRequireField,
                    template: locals => <AddressSelectionBox locals={locals}
                                                             dataSource={selectedWards}/>
                },
                address: {
                    label: tr("address_detail_address_label"),
                    placeholder: tr("address_detail_address_placeholder"),
                    error: requireField,
                    ...commonTextInputOptions
                }
            }
        }
    };

    renderNavBar = () => {
        const {navigator} = this.props;

        return (
            <NavBar
                navigator={navigator}
                title={
                    this.screenType === addressDetailScreenTypes.EDIT
                        ? tr("address_detail_edit_nav_bar_title")
                        : tr("address_detail_add_nav_bar_title")
                }
                right={
                    <NavBarShortcuts
                        showsCart
                        showsSearch
                        showsVerticalEllipsis
                        navigator={navigator}
                    />
                }
            />
        );
    };

    render() {
        return (
            <View style={commonStyles.matchParent}>
                {this.renderNavBar()}
                <KeyboardAwareView style={styles.addressDetail}>
                    <tComb.form.Form
                        ref={ref => this.formRef = ref}
                        type={formStructure}
                        options={this.getFormOptions()}
                        value={this.state.formValues}
                        onChange={this.onFormValuesChange}
                    />
                </KeyboardAwareView>
                <View style={styles.submitButtonContainer}>
                    <Button
                        onPress={this.saveCustomerAddress}
                        style={styles.submitButton}
                        type="fully-colored"
                        title={
                            this.screenType === addressDetailScreenTypes.CREATE
                                ? tr("address_detail_add_address_button")
                                : tr("address_detail_edit_address_button")
                        }
                    />
                </View>
            </View>
        );
    }
}

const mapStateTopProps = (state) => ({
    address: state.address.detail,
    provinces: state.address.location.provinces,
    districts: state.address.location.districts,
    wards: state.address.location.wards
});

export default connect(mapStateTopProps, {
    fetchUserInformation,
    addNewAddress,
    updateCustomerAddress,
    fetchCustomerAddressById,
    fetchAddressLocation,
    clearAddress,
})(AddressDetail);
