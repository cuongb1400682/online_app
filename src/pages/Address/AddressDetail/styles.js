import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    addressDetail: {
        flex: 1,
        paddingHorizontal: 8,
        paddingVertical: 16
    },
    submitButtonContainer: {
        width: "100%",
        height: 52,
        position: "absolute",
        bottom: 0,
        paddingVertical: 8,
        paddingHorizontal: 8,
        backgroundColor: "#fff",
        ...commonStyles.hasShadow
    },
    submitButton: {
        flex: 1,
    }
});

export default styles;
