import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import addressActionTypes from "src/redux/actionTypes/addressActionTypes";

const initialState = {
    detail: {
        fetching: false
    },
    location: {
        provinces: [],
        districts: [],
        wards: []
    }
};

const addressReducers = (state = initialState, {type, payload}) => {
    switch (type) {
        case toRequest(addressActionTypes.FETCH_USER_INFORMATION):
        case toFailure(addressActionTypes.FETCH_USER_INFORMATION):
            return {
                ...state,
                detail: {
                    fetching: false
                }
            };
        case toSuccess(addressActionTypes.FETCH_USER_INFORMATION):
            return {
                ...state,
                detail: {
                    contact_phone: payload.reply.phone,
                    contact_name: payload.reply.name
                }
            };
        case toSuccess(addressActionTypes.FETCH_CUSTOMER_ADDRESS_BY_ID):
            return {
                ...state,
                detail: {
                    ...state.detail,
                    ...payload.reply,
                    fetching: false
                }
            };
        case toSuccess(addressActionTypes.FETCH_ADDRESS_LOCATION):
            return {
                ...state,
                location: {
                    ...state.location,
                    ...payload.reply
                }
            };
        case addressActionTypes.CLEAR_ADDRESS_DATA:
            return {
                ...state,
                detail: {
                    fetching: false
                }
            };
        default:
            return state;
    }
};

export default addressReducers;
