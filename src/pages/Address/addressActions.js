import {fetchCustomerAddress} from "src/redux/common/commonActions";
import {createAction, createAsyncAction} from "src/utils/api/createAction";
import addressActionTypes from "src/redux/actionTypes/addressActionTypes";
import {tr} from "src/localization/localization";
import Message from "src/components/platformSpecific/Message/Message";

const showErrorMessage = (dispatch, err) => {
    Message.show("", tr(err.result));
};

const fetchUserInformation = () => createAsyncAction({
    type: addressActionTypes.FETCH_USER_INFORMATION,
    payload: {
        request: "/profile/account/get"
    }
});

const addNewAddress = ({name, phone, address, wardId}, callBack) => createAsyncAction({
    type: addressActionTypes.ADD_NEW_CUSTOMER_ADDRESS,
    payload: {
        request: "/customer/address/add",
        method: "POST",
        body: {
            address,
            contact_name: name,
            contact_phone: phone,
            ward_id: wardId
        }
    },
    onSuccess: (dispatch) => {
        callBack();
        dispatch(fetchCustomerAddress());
    },
    onFailure: showErrorMessage
});

const updateCustomerAddress = ({addressId, address, name, phone, wardId}, callBack) => createAsyncAction({
    type: addressActionTypes.UPDATE_CUSTOMER_ADDRESS,
    payload: {
        request: "/customer/address/update",
        method: "POST",
        body: {
            address_id: addressId,
            address: address,
            contact_name: name,
            contact_phone: phone,
            ward_id: wardId
        }
    },
    onSuccess: (dispatch) => {
        callBack();
        dispatch(fetchCustomerAddress());
    },
    onFailure: showErrorMessage
});

const fetchCustomerAddressById = addressId => createAsyncAction({
    type: addressActionTypes.FETCH_CUSTOMER_ADDRESS_BY_ID,
    payload: {
        request: "/customer/address/get",
        params: {
            address_id: addressId
        }
    },
});

const fetchAddressLocation = () => createAsyncAction({
    type: addressActionTypes.FETCH_ADDRESS_LOCATION,
    payload: {
        request: "/location/list"
    }
});

const clearAddress = () => createAction({
    type: addressActionTypes.CLEAR_ADDRESS_DATA,
    payload: {}
});


export {
    fetchUserInformation,
    addNewAddress,
    updateCustomerAddress,
    fetchCustomerAddressById,
    fetchAddressLocation,
    clearAddress
};
