import React, {Component} from "react";
import {View, Platform} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import SelectionBox from "src/components/base/SelectionBox/SelectionBox";
import Text from "src/components/platformSpecific/Text/Text";

class AddressSelectionBox extends Component {
    static propTypes = {
        locals: PropTypes.object,
        dataSource: PropTypes.array,
    };

    static defaultProps = {
        locals: {},
        dataSource: [],
    };

    changeSelectedValue = (selectedValue = {}) => {
        const {onChange} = this.props.locals;

        if (typeof onChange === "function") {
            onChange(selectedValue.value);
        }
    };

    render() {
        const {locals, dataSource} = this.props;
        const {stylesheet = {}} = locals;
        let controlLabelStyle = stylesheet.controlLabel.normal;
        let formGroupStyle = stylesheet.formGroup.normal;
        let errorBlockStyle = stylesheet.errorBlock;
        let textboxViewStyle = {};

        if (locals.hasError) {
            formGroupStyle = stylesheet.formGroup.error;
            controlLabelStyle = stylesheet.controlLabel.error;
            textboxViewStyle = styles.textBoxError
        }

        if (locals.hidden) {
            return null;
        }

        if (Platform.OS === "android") {
            controlLabelStyle = {
                ...controlLabelStyle,
                fontSize: 14
            };
        }

        return (
            <View style={formGroupStyle}>
                <Text style={controlLabelStyle}>{locals.label}</Text>
                <SelectionBox
                    dataSource={dataSource.map(
                        item => ({
                            value: item.id,
                            displayValue: item.name
                        })
                    )}
                    value={parseInt(locals.value, 10)}
                    placeholder={locals.placeholder}
                    rightIconName="caret-down"
                    contentContainerStyle={[styles.selectionBox, textboxViewStyle]}
                    onSelect={this.changeSelectedValue}
                />
                {locals.hasError && locals.error
                    ? <Text accessibilityLiveRegion="polite"
                            style={errorBlockStyle}>{locals.error} </Text> : null}
            </View>
        );
    }
}

export default AddressSelectionBox;
