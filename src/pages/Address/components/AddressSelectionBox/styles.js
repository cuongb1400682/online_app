import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    selectionBox: {
        borderRadius: 4,
        borderColor: "#ccc",
        justifyContent: "flex-start",
        paddingHorizontal: 8,
        marginBottom: 4
    },
    textBoxError: {
        borderColor: "#a94442"
    }
});

export default styles;
