import React, {Component} from "react";
import {View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import {tr} from "src/localization/localization";
import {changeViewMode, clearBestSale, fetchBestSaleProduct} from "src/pages/BestSaleProduct/bestSaleProductActions";
import {productViewModes} from "src/constants/enums";
import FilterBar from "src/components/base/FilterBar/FilterBar";
import ProductList from "src/components/base/ProductList/ProductList";
import LoadMore from "src/components/base/LoadMore/LoadMore";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";

const DEFAULT_PAGE_SIZE = 50;

class BestSaleProduct extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        searchParams: PropTypes.object,
        params: PropTypes.object,
        products: PropTypes.array,
        fetching: PropTypes.bool,
        totalItems: PropTypes.number,
        viewMode: PropTypes.oneOf([
            productViewModes.LIST,
            productViewModes.GRID
        ]),
        changeViewMode: PropTypes.func,
        fetchBestSaleProduct: PropTypes.func,
        clearBestSale: PropTypes.func,
    };

    static defaultProps = {
        searchParams: {},
        params: {},
        products: [],
        fetching: false,
        totalItems: 0,
        viewMode: productViewModes.GRID,
        changeViewMode: () => {
        },
        fetchBestSaleProduct: () => {
        },
        clearBestSale: () => {
        },
    };

    constructor(props) {
        super(props);
        this.state = {
            currentPageIndex: 1
        }
    }

    componentDidMount() {
        const {fetchBestSaleProduct} = this.props;
        const {currentPageIndex} = this.state;
        fetchBestSaleProduct({
            page_index: currentPageIndex,
            page_size: DEFAULT_PAGE_SIZE
        });
    }

    componentWillUnmount() {
        const {clearBestSale} = this.props;

        clearBestSale();
    }

    onFetchNextProduct = () => {
        const nextCurrentPageIndex = this.state.currentPageIndex + 1;

        this.setState({
            pageIndex: nextCurrentPageIndex
        });

        this.props.fetchBestSaleProduct({
            page_index: nextCurrentPageIndex,
            page_size: DEFAULT_PAGE_SIZE
        });
    };

    onRefresh = () => {
        const {clearBestSale, fetchBestSaleProduct} = this.props;

        clearBestSale();
        fetchBestSaleProduct({
            page_index: 1,
            page_size: DEFAULT_PAGE_SIZE
        });
    };

    renderNavBarShortcuts = () => (
        <NavBarShortcuts
            navigator={this.props.navigator}
            showsSearch
            showsCart
            showsVerticalEllipsis
        />
    );

    renderFooter = () => {
        const {totalItems, products, fetching} = this.props;

        return (
            <LoadMore
                onFetchNextProduct={this.onFetchNextProduct}
                searching={fetching}
                maximumNumberOfItems={totalItems}
                currentNumberOfItems={products.length}
            />
        );
    };

    render() {
        const {navigator, products, viewMode, changeViewMode, fetching} = this.props;
        return (
            <View style={styles.search}>
                <NavBar
                    title={tr("best_sale")}
                    right={this.renderNavBarShortcuts()}
                    navigator={navigator}
                />
                <ProductList
                    headerHeight={54}
                    header={
                        <FilterBar
                            changeViewMode={changeViewMode}
                            viewMode={viewMode}
                        />
                    }
                    footerHeight={50}
                    footer={this.renderFooter()}
                    emptyListHeight={50}
                    emptyList={tr("best_sale_product_empty_list")}
                    isRefreshing={fetching}
                    onRefresh={this.onRefresh}
                    products={products}
                    navigator={navigator}
                    showsHorizontalProduct={viewMode === productViewModes.LIST}
                />
                <FacebookMessenger floating/>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    products: state.bestsales.products,
    fetching: state.bestsales.fetching,
    totalItems: state.bestsales.number_items,
    viewMode: state.bestsales.viewMode,
    pageSize: state.bestsales.page_size
});

export default connect(mapStateToProps, {
    clearBestSale,
    fetchBestSaleProduct,
    changeViewMode,
})(BestSaleProduct);
