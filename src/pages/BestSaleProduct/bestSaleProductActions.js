import {createAction, createAsyncAction} from "src/utils/api/createAction";
import bestSaleActionTypes from "src/redux/actionTypes/bestSaleProductActionTypes";

const clearBestSale = () => createAction({
    type: bestSaleActionTypes.CLEAR_BEST_SALE,
    payload: {}
});

const fetchBestSaleProduct = ({page_index, page_size}) => createAsyncAction({
    type: bestSaleActionTypes.FETCH_PRODUCT_BEST_SALE,
    payload: {
        request: "/product/best_sale/list",
        params: {
            page_index: Math.max(page_index - 1, 0),
            page_size: page_size
        }
    },
    meta: {
        page_index
    }
});

const changeViewMode = (mode) => createAction({
    type: bestSaleActionTypes.CHANGE_VIEW_MODE,
    payload: {
        mode
    }
});

export {
    clearBestSale,
    fetchBestSaleProduct,
    changeViewMode,
};
