import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    search: {
        flex: 1,
        backgroundColor: "#eee"
    },
});

export default styles;
