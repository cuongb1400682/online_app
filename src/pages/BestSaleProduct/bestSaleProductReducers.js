import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import bestSaleProductActionTypes from "src/redux/actionTypes/bestSaleProductActionTypes";
import {productViewModes} from "src/constants/enums";

const initialState = {
    products: [],
    number_items: 0,
    page_index: 1,
    page_size: 50,
    fetching: false,
    viewMode: productViewModes.GRID,
};

const bestSalesProductReducers = (state = initialState, {type, payload, meta}) => {
    switch (type) {
        case bestSaleProductActionTypes.CHANGE_VIEW_MODE:
            return {
                ...state,
                viewMode: payload.mode
            };
        case toRequest(bestSaleProductActionTypes.FETCH_PRODUCT_BEST_SALE):
            return {
                ...state,
                fetching: true
            };
        case toSuccess(bestSaleProductActionTypes.FETCH_PRODUCT_BEST_SALE):
            const {products, ...others} = payload.reply;
            return {
                ...state,
                ...others,
                products: [...state.products, ...products],
                page_index: meta.page_index,
                fetching: false
            };
        case toFailure(bestSaleProductActionTypes.FETCH_PRODUCT_BEST_SALE):
            return {
                ...state,
                fetching: false
            };
        case bestSaleProductActionTypes.CLEAR_BEST_SALE:
            return {
                ...initialState,
                viewMode: state.viewMode || productViewModes.GRID
            };
        default:
            return state;
    }
};

export default bestSalesProductReducers;
