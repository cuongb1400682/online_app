import React, {Component} from "react";
import {FlatList, Image, RefreshControl, View} from "react-native";
import PropTypes from "prop-types"

import styles from "./styles";
import commonStyles from "src/styles/commonStyles";
import connect from "react-redux/es/connect/connect";
import {addToCart, fetchCart, removeProductFromCart} from "src/redux/common/commonActions";
import CartItem from "src/pages/Cart/components/CartItem/CartItem";
import NavBar from "src/containers/NavBar/NavBar";
import {tr} from "src/localization/localization";
import emptyCart from "src/assets/static/images/emptyCart.png";
import Button from "src/components/base/Button/Button";
import screenIds from "src/constants/screenIds";
import PaymentButton from "src/containers/PaymentButton/PaymentButton";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import {popToRoot, pushTo, switchToTab} from "src/utils/misc/navigator";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";
import Text from "src/components/platformSpecific/Text/Text";

class Cart extends Component {
    static nextStep = screenIds.CHECKOUT_SHIPPING_STEP;

    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        navigator: PropTypes.any,
        isFetching: PropTypes.bool,
        items: PropTypes.array,
        numberOfCartItems: PropTypes.number,
        fetchCart: PropTypes.func,
        addToCart: PropTypes.func,
        removeProductFromCart: PropTypes.func,
    };

    static defaultProps = {
        navigator: null,
        isFetching: false,
        items: [],
        numberOfCartItems: 0,
        fetchCart: () => {
        },
        addToCart: () => {
        },
        removeProductFromCart: () => {
        },
    };

    componentDidMount() {
        const {fetchCart} = this.props;

        fetchCart();
    }

    onChangeCartItemQty = productId => quantity => {
        this.props.addToCart({productId, quantity});
    };

    onRemoveProductFromCart = productId => () => {
        this.props.removeProductFromCart({productId});
    };

    goToHome = () => {
        const {navigator} = this.props;

        popToRoot(navigator);
        switchToTab(navigator, {screen: screenIds.HOME});
    };

    goToCheckoutShippingStep = () => {
        const {navigator} = this.props;

        pushTo(navigator, {screen: Cart.nextStep});
    };

    renderEmptyCart = () => (
        <View style={styles.emptyCart}>
            <Image source={emptyCart}/>
            <View style={styles.emptyCartLabelContainer}>
                <Text style={styles.emptyCartLabel}>
                    {tr("checkout_cart_empty")}
                </Text>
            </View>
            <Button
                title={tr("checkout_cart_continue_shopping")}
                type="light"
                style={styles.continueShoppingButton}
                onPress={this.goToHome}
            />
        </View>
    );

    renderCartItem = ({item = {}}) => {
        const {
            product = {},
            qty = 0
        } = item;

        return (
            <CartItem
                product={product}
                quantity={qty}
                onChangeQuantity={this.onChangeCartItemQty(product.id)}
                onRemove={this.onRemoveProductFromCart(product.id)}
            />
        );
    };

    renderSeparator = () => <View style={styles.separator}/>;

    render() {
        const {navigator, items, numberOfCartItems, fetchCart} = this.props;

        return (
            <View style={commonStyles.matchParent}>
                <NavBar
                    navigator={navigator}
                    right={<NavBarShortcuts showsVerticalEllipsis/>}
                    title={tr("checkout_cart_title")}
                />
                <FlatList
                    style={styles.cartItemsList}
                    renderItem={this.renderCartItem}
                    data={items}
                    keyExtractor={(_, index) => `${index}`}
                    refreshControl={
                        <RefreshControl
                            refreshing={false}
                            onRefresh={fetchCart}
                        />
                    }
                    ItemSeparatorComponent={this.renderSeparator}
                    ListEmptyComponent={this.renderEmptyCart()}
                />
                {numberOfCartItems > 0 && <PaymentButton onButtonPress={this.goToCheckoutShippingStep}
                                                         navigator={navigator}
                                                         nextStep={Cart.nextStep}/>}
                <FacebookMessenger floating/>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    isFetching: state.common.cart.fetching,
    items: state.common.cart.items,
    numberOfCartItems: state.common.cart.number_items
});

export default connect(mapStateToProps, {
    fetchCart,
    addToCart,
    removeProductFromCart
})(Cart);
