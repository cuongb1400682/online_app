import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    cartItemsList: {
        flex: 1,
        backgroundColor: "#eee"
    },
    separator: {
        width: "100%",
        height: 0.5,
        backgroundColor: "#e2e2e2"
    },
    emptyCart: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 40
    },
    emptyCartLabel: {
        color: "#757575"
    },
    emptyCartLabelContainer: {
        marginTop: 20
    },
    continueShoppingButton: {
        marginTop: 20,
        height: 40
    }
});

export default styles;
