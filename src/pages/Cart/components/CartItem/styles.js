import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    cartItem: {
        width: "100%",
        height: 110,
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 16,
        backgroundColor: "#fff"
    },
    productOutOfStockText: {
        color: colors.primaryColor,
        fontSize: 12
    },
    productNameContainer: {
        flex: 1,
    },
    productName: {
        flexWrap:"wrap",
        fontSize: 12
    },
    productImage: {
        width: 62,
        height: 62,
        marginRight: 12
    },
    caretDown: {
        color: "#515356",
        fontSize: 12,
        position: "absolute",
        right: 5
    },
    quantitySelection: {
        width: 54,
        height: 30,
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#eee",
        marginLeft: 16,
    },
    quantityLabel: {
        position: "absolute",
        left: 10
    },
    pricesContainer: {
        flexDirection: "column",
        alignItems: "flex-end",
        width: 75
    },
    unitPrice: {
        fontSize: 14,
        color: "#333"
    },
    marketPrice: {
        fontSize: 12,
        color: "#a2a2a2",
        textDecorationStyle: "solid",
        textDecorationLine: "line-through"
    },
    removeButtonTouchable: {
        width: 36,
        height: 36,
        position: "absolute",
        right: 0,
        top: 0
    },
    numberQuantityPicker: {
        height: "50%",
        width: "100%"
    }
});

export default styles;
