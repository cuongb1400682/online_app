import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

import styles from "./styles";
import {makeCDNImageURL} from "src/utils/cdn/images";
import FastImage from "src/components/base/FastImage/FastImage";
import {imageTypes} from "src/constants/enums";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {formatWithCurrency} from "src/utils/extensions/strings";
import commonStyles from "src/styles/commonStyles";
import Cross from "src/components/base/svgIcons/Cross/Cross";
import {getOutOfStockText} from "src/utils/misc/businessLogics";
import {isEmptyArray, makeIntRange} from "src/utils/extensions/arrays";
import {tr} from "src/localization/localization";
import Text from "src/components/platformSpecific/Text/Text";
import Picker from "src/components/platformSpecific/Picker/Picker";

class CartItem extends Component {
    static propTypes = {
        product: PropTypes.object.isRequired,
        quantity: PropTypes.number,
        onChangeQuantity: PropTypes.func,
        onRemove: PropTypes.func
    };

    static defaultProps = {
        quantity: 1,
        onChangeQuantity: () => {
        },
        onRemove: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            value: props.quantity
        };
    }

    componentWillUnmount() {
        this.hidePicker();
    }

    showPicker = () => {
        if (this.pickerRef) {
            this.pickerRef.show();
        }
    };

    hidePicker = () => {
        if (this.pickerRef) {
            this.pickerRef.hide();
        }
    };

    getProductQty = () => {
        const {qty = 0} = this.props.product;

        return qty <= 0 ? 0 : qty; // Sometime backend response negative integer
    };

    isMarketPriceVisible = () => {
        const {
            market_price = 0,
            unit_price = 0
        } = this.props.product;

        return market_price && market_price > unit_price;
    };

    isOutOfStock = () => {
        const {value: currentSelectedQty} = this.state;

        return this.getProductQty() === 0 || this.getProductQty() < currentSelectedQty;
    };

    onChangeQuantity = (qty) => {
        if (isEmptyArray(qty)) {
            return;
        }

        this.setState({value: qty});
        this.props.onChangeQuantity(qty);
    };

    removeItem = () => {
        this.props.onRemove();
    };

    renderQuantitySelection = () => (
        <Touchable onPress={this.showPicker}>
            <View style={styles.quantitySelection}>
                <Text style={styles.quantityLabel}>{this.state.value}</Text>
                <FontAwesome5 style={styles.caretDown} name="caret-down"/>
            </View>
        </Touchable>
    );

    renderPrices = () => {
        const {
            unit_price = 0,
            market_price = 0
        } = this.props.product;

        return (
            <View style={styles.pricesContainer}>
                <Text style={styles.unitPrice}>{formatWithCurrency(unit_price)}</Text>
                {this.isMarketPriceVisible() &&
                <Text style={styles.marketPrice}>{formatWithCurrency(market_price)}</Text>}
            </View>
        );
    };

    renderRemoveButton = () => (
        <Touchable
            style={[styles.removeButtonTouchable, commonStyles.centerChildren]}
            onPress={this.removeItem}
        >
            <Cross width={14} height={14} fill="#a2a2a2"/>
        </Touchable>
    );

    renderProductImage = () => (
        <FastImage
            source={{uri: makeCDNImageURL(this.props.product.original_thumbnail, imageTypes.CART)}}
            style={styles.productImage}
        />
    );

    renderProductName = () => (
        <View style={styles.productNameContainer}>
            <Text style={styles.productName} numberOfLines={4}>{this.props.product.name}</Text>
            {this.isOutOfStock() && <Text style={styles.productOutOfStockText}>
                {getOutOfStockText(this.getProductQty())}
            </Text>}
        </View>
    );

    renderPicker = () => {
        const {value} = this.state;
        const {qty: maxProductQuantity} = this.props.product;

        return (
            <Picker
                ref={ref => this.pickerRef = ref}
                onPickerConfirm={this.onChangeQuantity}
                pickerData={makeIntRange(1, maxProductQuantity)}
                selectedValue={value}
                titleText={tr("checkout_cart_qty_picker_title")}
                cancelText={tr("checkout_cart_qty_picker_cancel_button_text")}
                confirmText={tr("checkout_cart_qty_picker_confirm_button_text")}
            />
        );
    };

    render() {
        return (
            <View style={styles.cartItem}>
                {this.renderRemoveButton()}
                {this.renderProductImage()}
                {this.renderProductName()}
                {this.renderQuantitySelection()}
                {this.renderPrices()}
                {this.renderPicker()}
            </View>
        );
    }
}

export default CartItem;
