import React, {Component} from "react";
import {Image, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import styles from "./styles";
import {changeViewMode, clearDealList, fetchDealList} from "src/pages/DealList/dealListActions";
import commonStyles from "src/styles/commonStyles";
import NavBar from "src/containers/NavBar/NavBar";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import FilterBar from "src/components/base/FilterBar/FilterBar";
import {dealSortBy, dealTimeline, productViewModes} from "src/constants/enums";
import NavBarButton from "src/components/base/NavBarButton/NavBarButton";
import {popBack} from "src/utils/misc/navigator";
import {tr} from "src/localization/localization";
import {isEmptyArray} from "src/utils/extensions/arrays";
import ProductList from "src/components/base/ProductList/ProductList";
import LoadMore from "src/components/base/LoadMore/LoadMore";
import LazyTabView from "src/components/base/LazyTabView/LazyTabView";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";
import giigaaLogo from "src/assets/static/logo/giigaaLogo.png";

const DEFAULT_DEAL_PAGE_SIZE = 50;

const routes = [
    {
        key: dealTimeline.CURRENT,
        title: tr("deal_list_current_tab")
    },
    {
        key: dealTimeline.FUTURE,
        title: tr("deal_list_future_tab")
    }
];

class DealList extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        maxNumberOfItems: PropTypes.number,
        deals: PropTypes.object,
        viewMode: PropTypes.string,
        sortBy: PropTypes.string,
        timeline: PropTypes.string,
        fetchDealList: PropTypes.func,
        changeViewMode: PropTypes.func,
        clearDealList: PropTypes.func,
    };

    static defaultProps = {
        maxNumberOfItems: 0,
        deals: {},
        viewMode: productViewModes.GRID,
        sortBy: dealSortBy.BEST_SALE,
        timeline: dealTimeline.CURRENT,
        fetchDealList: () => {
        },
        changeViewMode: () => {
        },
        clearDealList: () => {
        },
    };

    constructor(props) {
        super(props);

        const {sortBy, timeline} = props;

        this.state = {
            sortBy,
            timeline,
            tabIndex: 0
        };
    }

    componentDidMount() {
        this.fetchProductIfNeeded();
    }

    componentWillUnmount() {
        const {clearDealList} = this.props;
        const {timeline} = this.state;

        clearDealList({timeline});
    }

    getCurrentPageIndex = () => {
        const {products = []} = this.getCurrentDeals();

        return Math.ceil(products.length / DEFAULT_DEAL_PAGE_SIZE);
    };

    getCurrentDeals = () => {
        const {deals} = this.props;

        return deals[this.state.timeline || dealTimeline.CURRENT];
    };

    fetchProductIfNeeded = () => {
        const currentDeal = this.getCurrentDeals();
        const {products = [], fetching} = currentDeal;
        const {sortBy, tabIndex} = this.state;
        const {key: newTimeline = dealTimeline.CURRENT} = routes[tabIndex] || {};

        if (!fetching && isEmptyArray(products)) {
            this.props.fetchDealList({
                sortBy,
                timeline: newTimeline,
                pageSize: DEFAULT_DEAL_PAGE_SIZE,
                pageIndex: this.getCurrentPageIndex() + 1
            });
        }
    };

    changeTabIndex = index => {
        const {key: newTimeline = dealTimeline.CURRENT} = routes[index] || {};

        this.setState({
            tabIndex: index,
            timeline: newTimeline
        }, this.fetchProductIfNeeded);
    };

    handleGoBack = () => {
        const {navigator} = this.props;

        popBack(navigator, {animated: true});
    };

    onFetchNextProduct = () => {
        const currentDeal = this.getCurrentDeals();
        const {fetching} = currentDeal;
        const nextPageIndex = this.getCurrentPageIndex() + 1;
        const {timeline, sortBy} = this.state;

        if (!fetching) {
            this.props.fetchDealList({
                timeline,
                sortBy,
                pageIndex: nextPageIndex,
                pageSize: DEFAULT_DEAL_PAGE_SIZE
            });
        }
    };

    refreshDealList = () => {
        const {clearDealList, fetchDealList} = this.props;
        const {timeline, sortBy} = this.state;

        clearDealList({timeline});
        fetchDealList({
            timeline,
            sortBy,
            pageIndex: 1,
            pageSize: DEFAULT_DEAL_PAGE_SIZE
        });
    };

    renderNavBar = () => (
        <NavBar style={styles.navBar}>
            <NavBarButton onPress={this.handleGoBack}>
                <MaterialCommunityIcons name="arrow-left" style={styles.defaultBackButton}/>
            </NavBarButton>
            <Image
                source={giigaaLogo}
                resizeMode="contain"
                style={commonStyles.matchParent}
            />
            <View style={commonStyles.matchParent}/>
            <NavBarShortcuts
                showsCart
                showsSearch
                showsVerticalEllipsis
                navigator={this.props.navigator}
            />
        </NavBar>
    );

    renderProductList = () => {
        const {viewMode, changeViewMode, navigator, maxNumberOfItems} = this.props;
        const currentDeals = this.getCurrentDeals();
        const {timeline} = this.state;
        const {products, fetching} = currentDeals;
        const hasNextPage = (products.length < maxNumberOfItems);

        return (
            <ProductList
                products={products}
                showsHorizontalProduct={viewMode === productViewModes.LIST}
                dealTimeline={timeline}
                navigator={navigator}
                isRefreshing={fetching}
                onRefresh={this.refreshDealList}
                emptyListHeight={50}
                emptyList={tr("deal_list_empty")}
                headerHeight={50}
                header={
                    <FilterBar viewMode={viewMode} changeViewMode={changeViewMode}/>
                }
                footerHeight={50}
                footer={
                    hasNextPage && <LoadMore
                        searching={fetching}
                        onFetchNextProduct={this.onFetchNextProduct}
                    />
                }
            />
        );
    };

    render() {
        const {tabIndex} = this.state;

        return (
            <View style={commonStyles.matchParent}>
                {this.renderNavBar()}
                <LazyTabView
                    onChangeTab={this.changeTabIndex}
                    currentRouteIndex={tabIndex}
                    routes={routes}
                    renderScene={this.renderProductList}
                />
                <FacebookMessenger floating/>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    deals: state.deals,
    viewMode: state.deals.view_mode,
    maxNumberOfItems: state.deals.number_items
});

export default connect(mapStateToProps, {
    fetchDealList,
    changeViewMode,
    clearDealList
})(DealList);
