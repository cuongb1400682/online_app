import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import dealListActionTypes from "src/redux/actionTypes/dealListActionTypes";
import {dealTimeline, productViewModes} from "src/constants/enums";

const defaultTimeline = {
    products: [],
    number_items: 0,
    page_index: 1,
    fetching: false,
};

const initialState = {
    [dealTimeline.CURRENT]: {
        ...defaultTimeline
    },
    [dealTimeline.FUTURE]: {
        ...defaultTimeline
    },
    view_mode: productViewModes.GRID
};

const normalizeProductListByDeal = ({products: items}) => items.map(({product, deal, ...others}) => {
    return {...product, deal: {...deal, ...others}};
});

const dealListReducers = (state = initialState, {type, payload, meta}) => {
    switch (type) {
        case toRequest(dealListActionTypes.FETCH_LIST_DEALS):
            return {
                ...state,
                [meta.timeline]: {
                    ...state[meta.timeline],
                    fetching: true
                }
            };
        case toSuccess(dealListActionTypes.FETCH_LIST_DEALS):
            return {
                ...state,
                [meta.timeline]: {
                    ...state[meta.timeline],
                    products: [...state[meta.timeline].products, ...normalizeProductListByDeal(payload.reply)],
                    number_items: payload.reply.number_items,
                    page_index: meta.page_index,
                    fetching: false
                }
            };
        case toFailure(dealListActionTypes.FETCH_LIST_DEALS):
            return {
                ...state,
                [meta.timeline]: {
                    ...state[meta.timeline],
                    fetching: false
                }
            };
        case dealListActionTypes.CHANGE_DEAL_VIEW_MODE:
            return {
                ...state,
                view_mode: payload.mode
            };
        case dealListActionTypes.CLEAR_DEAL_LIST:
            return {
                ...state,
                [payload.timeline]: {
                    ...defaultTimeline
                }
            };
        default:
            return state;
    }
};

export default dealListReducers;
