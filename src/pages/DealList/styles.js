import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    navBar: {
        flexDirection: "row",
    },
    defaultBackButton: {
        fontSize: 24,
        color: "#fff"
    }
});

export default styles;
