import {createAction, createAsyncAction} from "src/utils/api/createAction";
import dealsActionTypes from "src/redux/actionTypes/dealListActionTypes";

const fetchDealList = ({timeline, sortBy, pageSize = 50, pageIndex = 1}) => createAsyncAction({
    type: dealsActionTypes.FETCH_LIST_DEALS,
    payload: {
        request: "/deal/product/list",
        params: {
            page_size: pageSize,
            page_index: Math.max(pageIndex - 1, 0),
            timeline: timeline,
            sort_by: sortBy
        }
    },
    meta: {
        timeline,
        page_index: pageIndex
    }
});

const clearDealList = ({timeline}) => createAction({
    type: dealsActionTypes.CLEAR_DEAL_LIST,
    payload: {timeline}
});

const changeViewMode = (mode) => createAction({
    type: dealsActionTypes.CHANGE_DEAL_VIEW_MODE,
    payload: {
        mode: mode
    }
});

export {
    fetchDealList,
    changeViewMode,
    clearDealList
};
