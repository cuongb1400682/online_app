import {StyleSheet, Platform} from "react-native";
import commonStyles from "src/styles/commonStyles";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    textInput: {
        flex: 1,
        ...systemFont.regular,
    },
    couponCodeInputBar: {
        width: "100%",
        paddingVertical: 16,
        paddingHorizontal: 8,
        backgroundColor: "#fff",
        flexDirection: "row"
    },
    couponCodeInputContainer: {
        height: Platform.select({android: 40, ios: 36}),
        flex: 1,
        marginRight: 8,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: "#d7d7d7",
        paddingHorizontal: 4,
    },
    couponCodeApplyButton: {
        backgroundColor: "#10a049",
        borderColor: "#0e8a3f",
        height: "100%"
    },
    couponListContainer: {
        flex: 1,
        backgroundColor: "#f8f8f8"
    },
    couponListTitleContainer: {
        width: "100%",
        height: 37,
        backgroundColor: "#f8f8f8",
        ...commonStyles.centerChildren
    },
    couponList: {
        margin: 6
    },
    couponListTitle: {
        fontSize: 12,
        color: "#757575",
    },
    submitButtonContainer: {
        ...commonStyles.hasShadow,
        padding: 8,
        backgroundColor: "#fff"
    },
    submitButtonTitle: {
        fontSize: 12,
        color: colors.primaryColor,
        marginBottom: 8
    },
    submitButton: {
        height: 40
    },
    checkIcon: {
        fontSize: 15,
        color: colors.primaryColor,
        position: "absolute",
        right: 8,
        top: 6
    },
    separator: {
        height: 4
    }
});

export default styles;
