import React, {Component} from "react";
import {FlatList, RefreshControl, TextInput, TouchableWithoutFeedback, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import commonStyles from "src/styles/commonStyles";
import NavBar from "src/containers/NavBar/NavBar";
import {tr} from "src/localization/localization";
import {applyCouponCode} from "src/redux/common/commonActions";
import {fetchMyCouponsForCheckout} from "src/pages/Checkout/checkoutActions";
import Button from "src/components/base/Button/Button";
import CouponTicket from "src/components/base/CouponTicket/CouponTicket";
import colors from "src/constants/colors";
import FontIcon from "src/components/base/FontIcon/FontIcon";
import {isEmptyString} from "src/utils/extensions/strings";
import Message from "src/components/platformSpecific/Message/Message";
import {popBack} from "src/utils/misc/navigator";
import {isEmptyArray} from "src/utils/extensions/arrays";
import Text from "src/components/platformSpecific/Text/Text";

class ApplyCoupon extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        isApplying: PropTypes.bool,
        coupons: PropTypes.array,
        applyCouponCode: PropTypes.func,
        fetchMyCouponsForCheckout: PropTypes.func,
    };

    static defaultProps = {
        isApplying: false,
        coupons: [],
        applyCouponCode: () => {
        },
        fetchMyCouponsForCheckout: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            couponCode: "",
            selectedCode: "",
        };
    }

    componentDidMount() {
        const {fetchMyCouponsForCheckout} = this.props;

        fetchMyCouponsForCheckout({status: "ACTIVE"});
    }

    onSelectCouponCode = code => () => {
        this.setState(prevState => ({
            selectedCode: prevState.selectedCode !== code ? code : ""
        }));
    };

    onInputCouponCode = text => {
        this.setState({
            couponCode: text
        });
    };

    applyCouponCodeSuccess = () => {
        popBack(this.props.navigator);
        this.clearState();
    };

    clearState = () => {
        this.setState({
            couponCode: "",
            selectedCode: ""
        });
    };

    handleApplyCoupon = couponCode => {
        if (isEmptyString(couponCode)) {
            Message.show(
                tr("error_apply_coupon_message"),
                tr("error_apply_coupon_title")
            );
            return;
        }

        this.props.applyCouponCode(couponCode, {onSuccess: this.applyCouponCodeSuccess});
    };

    onApplyCouponCode = () => {
        this.handleApplyCoupon(this.state.couponCode);
    };

    onSelectCoupon = () => {
        const {selectedCode = ""} = this.state;

        if (isEmptyString(selectedCode)) {
            Message.show("", tr("apply_coupon_no_selection"));
        } else {
            this.handleApplyCoupon(selectedCode);
        }
    };

    renderCouponInputBar = () => (
        <View style={styles.couponCodeInputBar}>
            <View style={styles.couponCodeInputContainer}>
                <TextInput
                    style={styles.textInput}
                    editable
                    maxLength={256}
                    onSubmitEditing={this.onApplyCouponCode}
                    onChangeText={this.onInputCouponCode}
                    value={this.state.text}
                    selectionColor={colors.primaryColor}
                    underlineColorAndroid="transparent"
                    autoCapitalize="characters"
                    placeholder={tr("apply_coupon_input_placeholder")}
                />
            </View>
            <Button
                type="fully-colored"
                title={tr("apply_coupon_apply_button")}
                style={styles.couponCodeApplyButton}
                onPress={this.onApplyCouponCode}
            />
        </View>
    );

    renderCouponItem = ({item: coupon = {}}) => {
        const isSelected = coupon.code === this.state.selectedCode;

        const itemContainerStyle = {
            backgroundColor: isSelected ? "#fff9f7" : "#fff"
        };

        return (
            <TouchableWithoutFeedback onPress={this.onSelectCouponCode(coupon.code)}>
                <View style={itemContainerStyle}>
                    <CouponTicket coupon={coupon}/>
                    <FontIcon
                        fontName="FontAwesome"
                        name={isSelected ? "check-circle" : "circle-thin"}
                        style={styles.checkIcon}
                    />
                </View>
            </TouchableWithoutFeedback>
        );
    };

    renderRefreshControl = () => {
        const {isApplying, fetchMyCouponsForCheckout} = this.props;

        return (
            <RefreshControl
                refreshing={isApplying}
                onRefresh={() => {
                    fetchMyCouponsForCheckout({status: "ACTIVE"});
                }}
            />
        );
    };

    renderCouponList = () => {
        const {coupons} = this.props;

        return (
            <View style={styles.couponListContainer}>
                <View style={styles.couponListTitleContainer}>
                    <Text style={styles.couponListTitle}>
                        {tr(
                            isEmptyArray(coupons)
                                ? "apply_coupon_empty_list"
                                : "apply_coupon_coupon_list_title"
                        )}
                    </Text>
                </View>
                <FlatList
                    style={styles.couponList}
                    renderItem={this.renderCouponItem}
                    data={coupons}
                    keyExtractor={(_, index) => `${index}`}
                    refreshControl={this.renderRefreshControl()}
                    ListEmptyComponent={<View/>}
                    ItemSeparatorComponent={() => <View style={styles.separator}/>}
                />
            </View>
        );
    };

    renderSubmitButton = () => {
        const {isApplying} = this.props;
        const {selectedCode = ""} = this.state;

        return (
            <View style={styles.submitButtonContainer}>
                <Text style={styles.submitButtonTitle}>
                    {isEmptyString(selectedCode)
                        ? tr("apply_coupon_no_selection")
                        : tr("apply_coupon_selected")}
                </Text>
                <Button
                    type="fully-colored"
                    style={styles.submitButton}
                    title={tr("apply_coupon_submit_button")}
                    loading={isApplying}
                    onPress={this.onSelectCoupon}
                />
            </View>
        );
    };

    render() {
        return (
            <View style={commonStyles.matchParent}>
                <NavBar
                    navigator={this.props.navigator}
                    title={tr("apply_coupon_nav_bar_title")}
                />
                {this.renderCouponInputBar()}
                {this.renderCouponList()}
                {this.renderSubmitButton()}
            </View>
        );
    }
}

const mapStateToProps = state => ({
    coupons: state.checkout.apply_coupon.coupons,
    isApplying: state.checkout.apply_coupon.fetching
});

export default connect(mapStateToProps, {
    applyCouponCode,
    fetchMyCouponsForCheckout,
})(ApplyCoupon);
