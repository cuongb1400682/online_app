import {createAction, createAsyncAction} from "src/utils/api/createAction";
import checkoutActionTypes from "src/redux/actionTypes/checkoutActionTypes";
import {fetchCart} from "src/redux/common/commonActions";
import {tr} from "src/localization/localization";
import Message from "src/components/platformSpecific/Message/Message";

const clearShippingTime = () => createAction({
    type: checkoutActionTypes.CLEAR_SHIPPING_TIME,
    payload: {}
});

const fetchShippingTime = (id) => createAsyncAction({
    type: checkoutActionTypes.FETCH_SHIPPING_TIME,
    payload: {
        request: "/shipping/get_shipping_time",
        params: {
            cart_id: id
        }
    },
});

const fetchShippingAddressById = addressId => createAsyncAction({
    type: checkoutActionTypes.FETCH_CUSTOMER_ADDRESS_BY_ID,
    payload: {
        request: "/customer/address/get",
        params: {
            address_id: addressId
        }
    },
});

const updateShippingAddress = ({cartId, shippingAddressId}, callback) => createAsyncAction({
    type: checkoutActionTypes.UPDATE_SHIPPING_ADDRESS,
    payload: {
        request: "/sales_order/update_shipping",
        method: "POST",
        body: {
            cart_id: cartId,
            shipping_address_id: shippingAddressId
        }
    },
    onSuccess: () => {
        if (typeof callback === "function") {
            callback();
        } else {
            console.error("Callback function is not correct");
        }
    }
});

const onSelectShippingAddress = addressId => createAction({
    type: checkoutActionTypes.SELECT_SHIPPING_ADDRESS,
    payload: {
        address_id: addressId
    }
});

const clearShippingAddress = () => createAction({
    type: checkoutActionTypes.CLEAR_SHIPPING_ADDRESS,
    payload: {}
});

const createOrder = ({cartId, orderSource, paymentMethod, shippingMethod, note, from_expected_shipping_time, to_expected_shipping_time, asap}, callback) => {
    return createAsyncAction({
        type: checkoutActionTypes.CREATE_ORDER,
        payload: {
            request: "/sales_order/order",
            method: "POST",
            body: {
                note,
                cart_id: cartId,
                order_source: orderSource,
                delivery_method: shippingMethod,
                payment_method: paymentMethod,
                from_expected_shipping_time,
                to_expected_shipping_time
            }
        },
        onSuccess: (dispatch, response) => {
            if (typeof callback === "function") {
                callback(response.reply);
            }

            dispatch(fetchCart());
        },
        onFailure: (dispatch, err) => {
            const error = tr(err.result);

            Message.show(error, tr("create_order_error_title"))
        }
    });
};

const fetchOrderDetail = orderId => createAsyncAction({
    type: checkoutActionTypes.FETCH_ORDER_DETAIL,
    payload: {
        request: "/sales_order/get_detail",
        params: {
            id: orderId
        }
    }
});

const fetchMyCouponsForCheckout = () => createAsyncAction({
    type: checkoutActionTypes.FETCH_MY_COUPONS,
    payload: {
        request: "/coupon/list",
        params: {
            status: "ACTIVE"
        }
    }
});

const trackingCheckout = (page_type) => createAsyncAction({
    type: checkoutActionTypes.TRACKING_CHECKOUT,
    payload: {
        method: "POST",
        request: "/sales_order/tracking",
        body: {
            page_type: page_type
        },
        doesIgnoreNotified: true
    }
});

export {
    trackingCheckout,
    fetchOrderDetail,
    clearShippingTime,
    fetchShippingTime,
    fetchShippingAddressById,
    updateShippingAddress,
    onSelectShippingAddress,
    clearShippingAddress,
    createOrder,
    fetchMyCouponsForCheckout
};
