import React, {Component} from "react";
import {View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {MenuProvider} from "react-native-popup-menu";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import {tr} from "src/localization/localization";
import CheckoutProgress from "src/pages/Checkout/components/CheckoutProgress/CheckoutProgress";
import screenIds from "src/constants/screenIds";
import PaymentButton from "src/containers/PaymentButton/PaymentButton";
import {fetchCart, fetchCustomerAddress} from "src/redux/common/commonActions";
import {
    clearShippingAddress,
    onSelectShippingAddress,
    trackingCheckout,
    updateShippingAddress
} from "src/pages/Checkout/checkoutActions";
import Addresses from "src/pages/Checkout/containers/Addresses/Addresses";
import Message from "src/components/platformSpecific/Message/Message";
import {popBack, pushTo} from "src/utils/misc/navigator";
import {addressDetailScreenTypes, pageTrackingType} from "src/constants/enums";
import {isEmptyArray} from "src/utils/extensions/arrays";
import systemFont from "src/styles/systemFont";

const {CREATE, EDIT} = addressDetailScreenTypes;

class ShippingStep extends Component {
    static nextStep = screenIds.CHECKOUT_PAYMENT_STEP;

    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        addresses: PropTypes.array,
        mainAddressId: PropTypes.number,
        cartId: PropTypes.number,
        shippingAddressId: PropTypes.number,
        fetchCart: PropTypes.func,
        fetchCustomerAddress: PropTypes.func,
        updateShippingAddress: PropTypes.func,
        onSelectShippingAddress: PropTypes.func,
        clearShippingAddress: PropTypes.func,
        trackingCheckout: PropTypes.func,
    };

    static defaultProps = {
        addresses: [],
        mainAddressId: 0,
        cartId: 0,
        shippingAddressId: 0,
        fetchCart: () => {
        },
        fetchCustomerAddress: () => {
        },
        updateShippingAddress: () => {
        },
        onSelectShippingAddress: () => {
        },
        clearShippingAddress: () => {
        },
        trackingCheckout: () => {
        },
    };

    componentDidMount() {
        const {
            fetchCustomerAddress,
            fetchCart,
            onSelectShippingAddress,
            shippingAddressId
        } = this.props;

        onSelectShippingAddress(undefined); // deselect cached shipping_address_id
        fetchCustomerAddress({defaultAddressId: shippingAddressId, pageType: pageTrackingType.SHIPPING});
        fetchCart();
    }

    showAddressScreenForCreating = () => {
        const {navigator, addresses, clearShippingAddress} = this.props;

        pushTo(navigator, {
            screen: screenIds.ADDRESS_DETAIL,
            passProps: {
                firstAddress: isEmptyArray(addresses),
                onAddressDidSave: () => popBack(navigator)
            }
        });
        clearShippingAddress();
    };

    showAddressScreenForEditing = (addressId) => {
        const {navigator} = this.props;

        pushTo(navigator, {
            screen: screenIds.ADDRESS_DETAIL,
            passProps: {
                addressId,
                onAddressDidSave: () => popBack(navigator)
            }
        });
    };

    goToNextStep = () => {
        const {navigator} = this.props;

        pushTo(navigator, {screen: ShippingStep.nextStep});
    };

    onChangeShippingAddress = addressId => {
        this.props.onSelectShippingAddress(addressId);
    };

    submitShippingAddress = () => {
        const {cartId, shippingAddressId} = this.props;

        if (!shippingAddressId) {
            Message.show(
                tr("checkout_shipping_step_missing_address"),
                tr("checkout_shipping_step_missing_address_title")
            );

            return;
        }

        this.props.updateShippingAddress({cartId, shippingAddressId}, this.goToNextStep);
    };

    render() {
        const {addresses, mainAddressId, shippingAddressId, navigator, fetchCustomerAddress} = this.props;

        return (
            <MenuProvider style={systemFont.regular}>
                <View style={styles.shippingStep}>
                    <NavBar title={tr("checkout_shipping_step_title")} navigator={navigator}/>
                    <CheckoutProgress currentScreenId={screenIds.CHECKOUT_SHIPPING_STEP}/>
                    <Addresses
                        addresses={addresses}
                        mainAddressId={mainAddressId}
                        selectedAddressId={shippingAddressId}
                        onAddAddress={this.showAddressScreenForCreating}
                        onEditAddress={this.showAddressScreenForEditing}
                        onChange={this.onChangeShippingAddress}
                        onFetchAddress={fetchCustomerAddress}
                    />
                    <PaymentButton
                        onButtonPress={this.submitShippingAddress}
                        navigator={navigator}
                        nextStep={ShippingStep.nextStep}
                    />
                </View>
            </MenuProvider>
        );
    }
}

const mapStateToProps = state => ({
    addresses: state.common.customer.addresses,
    mainAddressId: state.common.customer.main_address_id,
    cartId: state.common.cart.id,
    shippingAddressId: state.checkout.shipping_address.id || state.common.customer.main_address_id
});

export default connect(mapStateToProps, {
    fetchCart,
    fetchCustomerAddress,
    updateShippingAddress,
    onSelectShippingAddress,
    clearShippingAddress,
    trackingCheckout
})(ShippingStep);
