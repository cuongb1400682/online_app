import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    radio: {
        backgroundColor: "#fff",
        paddingLeft: 16,
        paddingVertical: 8
    },
    flatList: {
        flex: 1,
        marginTop: 8
    },
    separator: {
        width: "100%",
        height: 1,
        backgroundColor: "#eee"
    },
    addAddressButton: {
        width: "100%",
        height: 50,
        backgroundColor: "#fff",
        paddingHorizontal: 16,
        flexDirection: "row",
        alignItems: "center",
        marginTop: 16
    },
    circlePlusIcon: {
        color: colors.primaryColor,
        fontSize: 20
    },
    addAddressButtonText: {
        color: colors.primaryColor,
        fontSize: 15
    }
});

export default styles;
