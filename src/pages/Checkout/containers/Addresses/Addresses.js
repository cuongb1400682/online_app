import React, {Component} from "react";
import {FlatList, View, RefreshControl} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";

import styles from "./styles";
import {deleteCustomerAddress} from "src/redux/common/commonActions";
import Radio from "src/components/base/Radio/Radio";
import Address from "src/components/base/Address/Address";
import {makeFullAddress} from "src/utils/extensions/strings";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {tr} from "src/localization/localization";
import Text from "src/components/platformSpecific/Text/Text";

const ADD_ADDRESS_BUTTON = "ADD_ADDRESS_BUTTON";

class Addresses extends Component {
    static propTypes = {
        addresses: PropTypes.array,
        mainAddressId: PropTypes.number,
        selectedAddressId: PropTypes.number,
        onChange: PropTypes.func,
        onAddAddress: PropTypes.func,
        onEditAddress: PropTypes.func,
        onFetchAddress: PropTypes.func,
    };

    static defaultProps = {
        addresses: [],
        onChange: () => {
        },
        onAddAddress: () => {
        },
        onEditAddress: () => {
        },
        onFetchAddress: () => {
        },
    };

    onChangeAddress = addressId => () => {
        this.props.onChange(addressId);
    };

    onDeleteAddress = addressId => () => {
        this.props.deleteCustomerAddress({addressId});
    };

    renderAddress = (item = {}) => {
        const {
            id: addressId = "",
            contact_name = "",
            contact_phone = "",
        } = item;
        const {selectedAddressId, mainAddressId, onEditAddress} = this.props;

        return (
            <Radio
                size={20}
                style={styles.radio}
                selected={addressId === selectedAddressId}
                onPress={this.onChangeAddress(addressId)}
            >
                <Address
                    name={contact_name}
                    address={makeFullAddress(item)}
                    phone={contact_phone}
                    isDefault={addressId === mainAddressId}
                    onDeleteAddress={this.onDeleteAddress(addressId)}
                    onEditAddress={() => onEditAddress(addressId)}
                />
            </Radio>
        );
    };

    renderAddAddressButton = () => {
        return (
            <Touchable onPress={this.props.onAddAddress}>
                <View style={styles.addAddressButton}>
                    <SimpleLineIcons name="plus" style={styles.circlePlusIcon}/>
                    <View style={{width: 16}}/>
                    <Text style={styles.addAddressButtonText}>{tr("address_add_address_button")}</Text>
                </View>
            </Touchable>
        );
    };

    renderItem = ({item = {}}) => {
        const {
            viewType = ""
        } = item;

        if (viewType === ADD_ADDRESS_BUTTON) {
            return this.renderAddAddressButton();
        } else {
            return this.renderAddress(item);
        }
    };

    render() {
        const {addresses, onFetchAddress} = this.props;

        return (
            <FlatList
                data={[
                    ...addresses,
                    {viewType: ADD_ADDRESS_BUTTON}
                ]}
                renderItem={this.renderItem}
                style={styles.flatList}
                refreshControl={
                    <RefreshControl
                        refreshing={false}
                        onRefresh={onFetchAddress}
                    />
                }
                keyExtractor={(_, index) => `${index}`}
                ItemSeparatorComponent={() => <View style={styles.separator}/>}
                ListEmptyComponent={<View/>}
            />
        );
    }
}

export default connect(null, {deleteCustomerAddress})(Addresses);
