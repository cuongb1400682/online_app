import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import checkoutActionTypes from "src/redux/actionTypes/checkoutActionTypes";
import commonActionTypes from "src/redux/actionTypes/commonActionTypes";

const initialState = {
    apply_coupon: {
        coupons: [],
        fetching: false
    },
    cart: {
        items: []
    },
    shipping_address: {
        fetching: false
    },
    shipping_time: {
        error: false,
        fetched: false,
        fetching: false
    },
    order_success: {},
    order_detail: {
        data: {},
        fetching: false
    },
};

const markOutOfStockProduct = (items, outOfStockList = []) => {
    return items.map(item => {

        const numberOfStocks = outOfStockList.length;
        for (let index = 0; index < numberOfStocks; index++) {
            const product = outOfStockList[index];

            if (item.product_id === product.id) {
                return {
                    ...item,
                    product: {...product},
                    out_of_stock: true
                };
            }
        }

        return {...item};
    });
};


const checkoutReducers = (state = initialState, {type, payload}) => {
    switch (type) {
        case toSuccess(checkoutActionTypes.FETCH_CUSTOMER_ADDRESS_BY_ID):
            return {
                ...state,
                shipping_address: {
                    ...payload.reply,
                    fetching: false
                }
            };
        case checkoutActionTypes.SELECT_SHIPPING_ADDRESS:
            return {
                ...state,
                shipping_address: {
                    ...state.shipping_address,
                    id: payload.address_id
                }
            };
        case toRequest(commonActionTypes.DELETE_CUSTOMER_ADDRESS):
            return {
                ...state,
                shipping_address: {
                    ...state.shipping_address,
                    id: null
                }
            };
        case toSuccess(commonActionTypes.FETCH_CART):
            return {
                ...state,
                cart: {
                    ...state.cart,
                    ...payload.reply
                }
            };
        case toSuccess(checkoutActionTypes.CREATE_ORDER):
            return {
                ...state,
                order_success: {
                    ...payload.reply
                }
            };
        case toFailure(checkoutActionTypes.CREATE_ORDER):
            if (payload.result === "error_so_out_of_stock") {
                return {
                    ...state,
                    cart: {
                        ...state.cart,
                        items: [...markOutOfStockProduct(state.cart.items, payload.reply.out_of_stocks)]
                    }
                };
            }

            return state;
        case checkoutActionTypes.CLEAR_SHIPPING_ADDRESS:
            return {
                ...state,
                shipping_address: {
                    fetching: false
                }
            };
        case toRequest(checkoutActionTypes.FETCH_SHIPPING_TIME):
            return {
                ...state,
                shipping_time: {
                    ...state.shipping_time,
                    fetching: true
                }
            };
        case toSuccess(checkoutActionTypes.FETCH_SHIPPING_TIME):
            return {
                ...state,
                shipping_time: {
                    ...state.shipping_time,
                    ...payload.reply,
                    error: false,
                    fetching: false,
                    fetched: true
                }
            };
        case toFailure(checkoutActionTypes.FETCH_SHIPPING_TIME):
            return {
                ...state,
                shipping_time: {
                    ...state.shipping_time,
                    error: true,
                    fetching: false
                }
            };
        case checkoutActionTypes.CLEAR_SHIPPING_TIME:
            return {
                ...state,
                shipping_time: {
                    fetching: false,
                    fetched: false
                }
            };
        case toRequest(checkoutActionTypes.FETCH_ORDER_DETAIL):
            return {
                ...state,
                order_detail: {
                    ...state.order_detail,
                    data: {},
                    fetching: true
                }
            };
        case toSuccess(checkoutActionTypes.FETCH_ORDER_DETAIL):
            const {id, address, order_number, from_expected_shipping_time, to_expected_shipping_time, delivery_method, payment_method, waiting_for} = payload.reply;

            return {
                ...state,
                order_detail: {
                    ...state.order_detail,
                    data: {
                        id,
                        address,
                        order_number,
                        from_expected_shipping_time,
                        to_expected_shipping_time,
                        delivery_method,
                        payment_method,
                        waiting_for
                    },
                    fetching: false,
                }
            };
        case toFailure(checkoutActionTypes.FETCH_ORDER_DETAIL):
            return {
                ...state,
                order_detail: {
                    ...state.order_detail,
                    fetching: false,
                }
            };
        case toRequest(checkoutActionTypes.FETCH_MY_COUPONS):
            return {
                ...state,
                apply_coupon: {
                    coupons: [],
                    fetching: true
                }
            };
        case toSuccess(checkoutActionTypes.FETCH_MY_COUPONS):
            return {
                ...state,
                apply_coupon: {
                    ...state.apply_coupon,
                    coupons: payload.reply,
                    fetching: false
                }
            };
        case toFailure(checkoutActionTypes.FETCH_MY_COUPONS):
            return {
                ...state,
                apply_coupon: {
                    ...state.apply_coupon,
                    fetching: false
                }
            };
        default:
            return state;
    }
};

export default checkoutReducers;
