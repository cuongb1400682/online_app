import React, {Component} from "react";
import {Platform, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import NavBar from "src/containers/NavBar/NavBar";
import screenIds from "src/constants/screenIds";
import CheckoutProgress from "src/pages/Checkout/components/CheckoutProgress/CheckoutProgress";
import PaymentButton from "src/containers/PaymentButton/PaymentButton";
import commonStyles from "src/styles/commonStyles";
import {
    clearShippingTime,
    createOrder,
    fetchShippingAddressById,
    fetchShippingTime, trackingCheckout
} from "src/pages/Checkout/checkoutActions";
import {changeShippingMethod, fetchCart, fetchShippingMethods, resetShippingFee} from "src/redux/common/commonActions";
import ShippingMethods from "src/pages/Checkout/components/ShippingMethods/ShippingMethods";
import {
    pageTrackingType,
    paymentMethods,
    shippingMethods,
    shippingMethods as shippingMethodTypes
} from "src/constants/enums";
import ShippingTime from "src/pages/Checkout/components/ShippingTime/ShippingTime";
import {DEFAULT_TIME_VALUE, generateShippingTime, parseToOrderTime} from "src/utils/extensions/dateTime";
import PaymentMethod from "src/pages/Checkout/components/PaymentMethod/PaymentMethod";
import ShippingCard from "src/pages/Checkout/components/ShippingCard/ShippingCard";
import OrderInfo from "src/pages/Checkout/components/OrderInfo/OrderInfo";
import NoteInput from "src/pages/Checkout/components/NoteInput/NoteInput";
import {popToRoot, pushTo} from "src/utils/misc/navigator";
import Message from "src/components/platformSpecific/Message/Message";
import KeyboardAwareView from "src/components/platformSpecific/KeyboardAwareView/KeyboardAwareView";

const DATE_VALUE_MAPPING = {
    "today": {value: "today", displayValue: "Hôm nay", nextDays: 0, order: 0},
    "others": {value: "others", displayValue: "Ngày mai", nextDays: 1, order: 1}
};

class PaymentStep extends Component {
    static nextStep = screenIds.CHECKOUT_SUCCESS_STEP;

    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        cart: PropTypes.object,
        shippingAddress: PropTypes.object,
        shippingMethods: PropTypes.array,
        shippingTime: PropTypes.object,
        fetchedShippingTime: PropTypes.bool,
        fetchingShippingTime: PropTypes.bool,
        fetchShippingTimeError: PropTypes.any,
        createOrder: PropTypes.func,
        fetchCart: PropTypes.func,
        fetchShippingMethods: PropTypes.func,
        fetchShippingAddressById: PropTypes.func,
        fetchShippingTime: PropTypes.func,
        clearShippingTime: PropTypes.func,
        resetShippingFee: PropTypes.func,
        changeShippingMethod: PropTypes.func,
        trackingCheckout: PropTypes.func,
    };

    static defaultProps = {
        cart: {},
        shippingAddress: {},
        shippingMethods: [],
        shippingTime: {},
        fetchedShippingTime: false,
        fetchingShippingTime: false,
        fetchShippingTimeError: null,
        createOrder: () => {
        },
        fetchCart: () => {
        },
        fetchShippingMethods: () => {
        },
        fetchShippingAddressById: () => {
        },
        fetchShippingTime: () => {
        },
        clearShippingTime: () => {
        },
        resetShippingFee: () => {
        },
        changeShippingMethod: () => {
        },
        trackingCheckout: () => {
        },
    };

    constructor(props) {
        super(props);

        this.timeData = generateShippingTime(0, 0);
        this.dateData = this.generateDateValue();
        this.state = {
            paymentMethod: paymentMethods.COD,
            shippingMethod: shippingMethods.STANDARD,
            selectedDate: undefined,
            selectedTime: DEFAULT_TIME_VALUE,
        };
    }

    componentDidMount() {
        this.props.clearShippingTime();
        this.props.resetShippingFee();
        this.props.fetchCart({onSuccess: this.fetchCartComplete, onFailure: this.fetchCartComplete});
        this.props.changeShippingMethod(this.state.shippingMethod);
    }

    componentDidUpdate(prevProps) {
        this.initDateData();
    }

    componentWillUnmount() {
        this.setState({
            selectedDate: undefined,
            selectedTime: DEFAULT_TIME_VALUE
        });
    }

    initDateData = () => {
        const {fetchedShippingTime, fetchShippingTimeError} = this.props;

        if (!fetchedShippingTime || fetchShippingTimeError || this.state.selectedDate) {
            return;
        }

        const {today: todayTimeData, others: othersTimeData} = this.props.shippingTime;
        const defaultTimeData = todayTimeData || othersTimeData || {};
        this.timeData = generateShippingTime(defaultTimeData.start, defaultTimeData.end, defaultTimeData.range);
        this.dateData = this.generateDateValue(this.props.shippingTime);

        this.setState({
            selectedDate: this.getDefaultDateValue(this.dateData)
        });
    };

    getDefaultDateValue = (dateData = []) => {
        if (dateData.length === 0) {
            return 0;
        }

        return dateData[0].value;
    };

    generateDateValue = (shippingTime = {}) => {
        return Object.keys(shippingTime)
            .map(key => DATE_VALUE_MAPPING[key])
            .filter(dateValue => !!dateValue)
            .sort((value1, value2) => value1.order - value2.order);
    };

    fetchCartComplete = ({reply: cart} = {}) => {
        if (!cart || !cart.id) {
            this.backToCartPage();
        }

        if (!cart.shipping_address_id) {
            Message.show("Giỏ hàng chưa có địa chỉ giao hàng. Vui lòng lựa chọn địa chỉ giao hàng!");
            this.backToCheckoutShippingStepPage();

            return;
        }

        this.props.fetchShippingAddressById(cart.shipping_address_id);
        this.props.fetchShippingMethods(cart.id, pageTrackingType.PAYMENT);
        this.props.fetchShippingTime(cart.id);
    };

    backToCartPage = () => {
        const {navigator} = this.props;

        popToRoot(navigator, {animated: false});
        pushTo(navigator, {screen: screenIds.CART});
    };

    backToCheckoutShippingStepPage = () => {
        const {navigator} = this.props;

        popToRoot(navigator, {animated: false});
        pushTo(navigator, {screen: screenIds.CHECKOUT_SHIPPING_STEP});
    };

    goToNextStep = (orderDetail = {}) => {
        const {navigator} = this.props;

        pushTo(navigator, {
            screen: PaymentStep.nextStep,
            passProps: {
                orderId: orderDetail.id
            }
        });
    };

    onSubmitCreateOrder = () => {
        const {selectedTime, shippingMethod} = this.state;
        const {shippingTime: shippingTimeData} = this.props;
        const dataValue = DATE_VALUE_MAPPING[this.state.selectedDate] || {};
        const countDay = dataValue.nextDays || 0;
        const estimatedShippingTimeParams = parseToOrderTime(countDay, selectedTime, shippingMethod, shippingTimeData);

        this.props.createOrder({
            cartId: this.props.cart.id,
            orderSource: Platform.select({ios: "IOS", android: "ANDROID"}),
            paymentMethod: this.state.paymentMethod,
            shippingMethod: this.state.shippingMethod,
            note: this.noteInputRef ? this.noteInputRef.noteContent : "",
            ...estimatedShippingTimeParams
        }, this.goToNextStep);
    };

    onChangeShippingMethod = selectedShippingMethodIndex => {
        const {
            shippingMethods,
            changeShippingMethod
        } = this.props;

        const shippingMethodType = shippingMethods[selectedShippingMethodIndex].type;

        this.setState({shippingMethod: shippingMethodType});

        const selectedShippingMethod = shippingMethods.find(
            shippingMethod => shippingMethod.type === shippingMethodType
        ) || {};

        changeShippingMethod(selectedShippingMethod);
    };

    changeShippingDate = (selectedItem = {}) => {
        const selectedDate = selectedItem.value;

        if (selectedDate === this.state.selectedDate) {
            return null;
        }

        const timeRange = this.props.shippingTime[selectedDate];
        this.timeData = generateShippingTime(timeRange.start, timeRange.end, timeRange.range);

        this.setState({selectedDate, selectedTime: DEFAULT_TIME_VALUE});
    };

    changeShippingTime = (selectedItem = {}) => {
        const {value} = selectedItem;

        this.setState({selectedTime: value});
    };

    render() {
        const {navigator, shippingMethods, cart} = this.props;
        const {shippingMethod} = this.state;
        const showShippingTime = shippingMethod === shippingMethodTypes.STANDARD;

        return (
            <View style={styles.paymentStep}>
                <NavBar title={tr("checkout_payment_step_title")} navigator={navigator}/>
                <KeyboardAwareView style={commonStyles.matchParent}>
                    <CheckoutProgress currentScreenId={screenIds.CHECKOUT_PAYMENT_STEP}/>
                    <ShippingMethods
                        shippingMethods={shippingMethods}
                        value={shippingMethod}
                        onChange={this.onChangeShippingMethod}
                    />
                    {showShippingTime && <ShippingTime
                        onDateChange={this.changeShippingDate}
                        onTimeChange={this.changeShippingTime}
                        dateValue={this.state.selectedDate}
                        timeValue={this.state.selectedTime}
                        dateData={this.dateData}
                        timeData={this.timeData}
                    />}
                    <PaymentMethod/>
                    <ShippingCard
                        address={this.props.shippingAddress}
                        navigator={navigator}
                    />
                    <OrderInfo
                        order={cart}
                        navigator={navigator}
                    />
                    <NoteInput ref={ref => this.noteInputRef = ref}/>
                </KeyboardAwareView>
                <PaymentButton
                    onButtonPress={this.onSubmitCreateOrder}
                    title={tr("checkout_payment_step_payment_button_title")}
                    nextStep={PaymentStep.nextStep}
                    navigator={navigator}
                    showsPromotionCodeSelect
                />
            </View>
        );
    }
}

const mapStateToProps = state => ({
    cart: state.common.cart,
    shippingAddress: state.checkout.shipping_address,
    shippingMethods: state.common.shipping_methods.data,
    shippingTime: state.checkout.shipping_time,
    fetchedShippingTime: state.checkout.shipping_time.fetched,
    fetchingShippingTime: state.checkout.shipping_time.fetching,
    fetchShippingTimeError: state.checkout.shipping_time.error
});

export default connect(mapStateToProps, {
    createOrder,
    fetchCart,
    fetchShippingMethods,
    fetchShippingAddressById,
    fetchShippingTime,
    clearShippingTime,
    resetShippingFee,
    changeShippingMethod,
    trackingCheckout
})(PaymentStep);
