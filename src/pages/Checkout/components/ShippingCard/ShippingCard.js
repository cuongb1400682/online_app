import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import SectionTitle from "src/pages/Checkout/components/SectionTitle/SectionTitle";
import {tr} from "src/localization/localization";
import EditButton from "src/pages/Checkout/components/EditButton/EditButton";
import {capitalizeString, makeFullAddress} from "src/utils/extensions/strings";
import {popBack} from "src/utils/misc/navigator";
import Text from "src/components/platformSpecific/Text/Text";

class ShippingCard extends Component {
    static propTypes = {
        address: PropTypes.any
    };

    static defaultProps = {
        address: {}
    };

    goBack = () => {
        popBack(this.props.navigator);
    };

    render() {
        const {address} = this.props;
        const {
            contact_name = "",
            contact_phone = ""
        } = address;

        return (
            <View style={styles.shippingCard}>
                <SectionTitle
                    title={tr("checkout_payment_step_shipping_address_section_title")}
                    right={<EditButton onPress={this.goBack}/>}
                />
                <View style={styles.shippingCardContent}>
                    <Text style={styles.shipTo}>{tr("checkout_payment_step_shipping_address_ship_to")}</Text>
                    <Text style={styles.contactName}>{capitalizeString(contact_name)}</Text>
                    <View>
                        <Text style={styles.customerInfo}>
                            {tr("checkout_payment_step_shipping_address_address")} {makeFullAddress(address)}
                        </Text>
                        <Text style={styles.customerInfo}>
                            {tr("checkout_payment_step_shipping_address_phone")} {contact_phone}
                        </Text>
                    </View>
                </View>
            </View>
        );
    }
}

export default ShippingCard;
