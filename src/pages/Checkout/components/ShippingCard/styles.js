import {StyleSheet} from "react-native";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    shippingCard: {
        width: "100%",
        backgroundColor: "#fff",
        marginTop: 10,
        flexDirection: "column",
        paddingBottom: 16
    },
    shipTo: {
        fontSize: 14,
        color: "#666",
        paddingTop: 16
    },
    shippingCardContent: {
        paddingHorizontal: 16,
        flexWrap: "wrap"
    },
    contactName: {
        fontSize: 14,
        fontWeight: "bold",
        color: colors.textColor,
        paddingVertical: 10,
    },
    customerInfo: {
        fontSize: 14,
    },
});

export default styles;
