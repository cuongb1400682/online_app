import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import SectionTitle from "src/pages/Checkout/components/SectionTitle/SectionTitle";
import {tr} from "src/localization/localization";
import WithinCity from "src/pages/Checkout/components/WithinCity/WithinCity";
import RadioGroup from "src/components/base/RadioGroup/RadioGroup";
import Radio from "src/components/base/Radio/Radio";
import commonStyles from "src/styles/commonStyles";
import {paymentMethods} from "src/constants/enums";
import Text from "src/components/platformSpecific/Text/Text";

class PaymentMethod extends Component {
    static propTypes = {
        onChange: PropTypes.func
    };

    static defaultProps = {
        onChange: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            showOnlineMethod: false
        };
    }

    changePaymentMethod = paymentMethodIndex => () => {
        const type = (paymentMethodIndex === 0 ? paymentMethods.COD : paymentMethods.ONLINE_PAYMENT);

        if (type === paymentMethods.ONLINE_PAYMENT) {
            this.setState({showOnlineMethod: true});
        } else if (type === paymentMethods.COD) {
            this.setState({showOnlineMethod: false});
        }

        this.props.onChange(type);
    };

    renderCODOption = () => (
        <Radio size={18}>
            <View style={commonStyles.flexColumn}>
                <Text style={styles.paymentMethodName}>{tr("checkout_payment_step_payment_method_cod_text")}</Text>
                <Text style={styles.codPrice}>
                    ({tr("checkout_payment_step_payment_method_cod_subtext")}
                    <Text style={styles.freeOfChargeText}>
                        {tr("checkout_payment_step_payment_method_cod_free_of_charge")}
                    </Text>)
                </Text>
            </View>
        </Radio>
    );

    renderOnlinePaymentMethods = () => {
        return null;
    };

    render() {
        return (
            <View style={styles.paymentMethod}>
                <SectionTitle title={tr("checkout_payment_step_payment_method_section_title")}/>
                <RadioGroup
                    style={styles.radioGroup}
                    onChildrenPress={this.changePaymentMethod}
                    selectedIndex={0}
                >
                    {this.renderCODOption()}
                </RadioGroup>
            </View>
        );
    }
}

export default PaymentMethod;
