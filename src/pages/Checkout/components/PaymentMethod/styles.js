import {StyleSheet} from "react-native";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    paymentMethod: {
        width: "100%",
        backgroundColor: "#fff",
        marginTop: 10,
        flexDirection: "column",
        paddingBottom: 20
    },
    freeOfChargeText: {
        color: "#26AD5D"
    },
    radioGroup: {
        paddingLeft: 16,
        marginTop: 16
    },
    paymentMethodName: {
        fontSize: 14,
        fontWeight: "bold",
    },
    codPrice: {
        fontSize: 12,
    }
});

export default styles;
