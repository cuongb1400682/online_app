import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import SectionTitle from "src/pages/Checkout/components/SectionTitle/SectionTitle";
import {tr} from "src/localization/localization";
import EditButton from "src/pages/Checkout/components/EditButton/EditButton";
import CheckoutProduct from "src/pages/Checkout/components/CheckoutProduct/CheckoutProduct";
import commonStyles from "src/styles/commonStyles";
import DottedSeparator from "src/components/base/DottedSeparator/DottedSeparator";
import {pushTo, popToRoot} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";

class OrderInfo extends Component {
    static propTypes = {
        order: PropTypes.object,
    };

    static defaultProps = {
        order: {},
    };

    backToCheckoutProduct = () => {
        const {navigator} = this.props;

        popToRoot(navigator, {animated: false});
        pushTo(navigator, {screen: screenIds.CART});
    };

    renderProduct = (item = {}, index) => (
        <View key={index} style={commonStyles.flexColumn}>
            {index > 0 && <DottedSeparator/>}
            <CheckoutProduct
                key={index}
                product={item.product}
                quantity={item.qty}
                totalLine={item.total_line}
                isOutOfStock={item.out_of_stock}
            />
        </View>
    );

    render() {
        const {order} = this.props;
        const {items, number_items} = order;

        return (
            <View style={styles.orderInfo}>
                <SectionTitle
                    title={tr("checkout_payment_step_order_info_section_title")}
                    subtitle={tr("checkout_payment_step_order_info_number_item", number_items)}
                    right={<EditButton onPress={this.backToCheckoutProduct}/>}
                />
                <View style={styles.productList}>
                    {items.map(this.renderProduct)}
                </View>
            </View>
        );
    }
}

export default OrderInfo;
