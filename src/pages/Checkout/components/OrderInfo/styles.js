import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    orderInfo: {
        width: "100%",
        backgroundColor: "#fff",
        marginTop: 10,
        flexDirection: "column",
    },
    productList: {
        paddingHorizontal: 16
    },
});

export default styles;
