import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import SectionTitle from "src/pages/Checkout/components/SectionTitle/SectionTitle";
import RadioGroup from "src/components/base/RadioGroup/RadioGroup";
import Radio from "src/components/base/Radio/Radio";
import {formatWithCurrency} from "src/utils/extensions/strings";
import WithinCity from "src/pages/Checkout/components/WithinCity/WithinCity";
import Text from "src/components/platformSpecific/Text/Text";

class ShippingMethods extends Component {
    static propTypes = {
        shippingMethods: PropTypes.array.isRequired,
        value: PropTypes.string.isRequired,
        onChange: PropTypes.func,
    };

    static defaultProps = {
        shippingMethods: [],
        onChange: () => {
        }
    };

    renderShippingMethodRadio = (shippingMethod = {}, index) => {
        const {
            fee = 0,
            extra_text = "",
            name = "",
        } = shippingMethod;

        return (
            <Radio key={index} size={18}>
                <View style={styles.radioButtonContent}>
                    <Text style={styles.shippingMethodName}>
                        {name + ": "}
                        <Text style={styles.shippingFee}>
                            {fee === 0
                                ? tr("checkout_payment_step_shipping_method_free")
                                : tr("checkout_payment_step_shipping_method_charge") + formatWithCurrency(fee)}
                        </Text>
                    </Text>
                    <Text style={styles.extraText}>
                        {extra_text}
                    </Text>
                </View>
            </Radio>
        );
    };


    render() {
        const {shippingMethods, onChange, value} = this.props;
        const selectedIndex = shippingMethods.findIndex(method => method.type === value);

        return (
            <View style={styles.shippingMethods}>
                <SectionTitle title={tr("checkout_payment_step_shipping_method_section_title")}/>
                <RadioGroup
                    style={styles.radioGroup}
                    selectedIndex={selectedIndex}
                    onChildrenPress={onChange}
                >
                    {shippingMethods.map(this.renderShippingMethodRadio)}
                </RadioGroup>
            </View>
        );
    }
}

export default ShippingMethods;
