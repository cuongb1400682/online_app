import {StyleSheet} from "react-native";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    shippingMethods: {
        width: "100%",
        backgroundColor: "#fff",
        marginTop: 10,
        flexDirection: "column"
    },
    radioGroup: {
        paddingLeft: 20,
        marginTop: 20
    },
    radioButtonContent: {
        flexDirection: "column",
        paddingBottom: 8,
        flex: 1,
        flexWrap: "wrap"
    },
    shippingMethodName: {
    },
    shippingFee: {
        fontWeight: "bold"
    },
    extraText: {
        color: colors.primaryColor,
        fontStyle: "italic"
    }
});

export default styles;
