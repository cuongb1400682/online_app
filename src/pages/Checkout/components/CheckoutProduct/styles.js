import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    checkoutProduct: {
        minHeight: 74,
        width: "100%",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    productImage: {
        width: 72,
        height: 72
    },
    productNameContainer: {
        flex: 1,
        height: "100%",
        flexWrap: "wrap",
        paddingTop: 16,
        paddingLeft: 10,
    },
    productQuantityContainer: {
        width: 50,
        height: "100%",
        paddingTop: 16,
        paddingLeft: 10,
        flexDirection: "row",
        justifyContent: "flex-end"
    },
    totalLineContainer: {
        height: "100%",
        paddingTop: 16,
        paddingLeft: 10,
        flexDirection: "column",
        alignItems: "flex-end"
    },
    productInfoText: {
        fontSize: 12,
        color: "#333"
    },
});

export default styles;
