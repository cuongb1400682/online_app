import React, {Component} from "react";
import {Image, View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {makeCDNImageURL} from "src/utils/cdn/images";
import FastImage from "src/components/base/FastImage/FastImage";
import {imageTypes} from "src/constants/enums";
import {getOutOfStockText} from "src/utils/misc/businessLogics";
import {formatWithCurrency} from "src/utils/extensions/strings";
import giftIcon from "src/assets/static/icons/gift.png";
import Text from "src/components/platformSpecific/Text/Text";

class CheckoutProduct extends Component {
    static propTypes = {
        product: PropTypes.object,
        quantity: PropTypes.number,
        totalLine: PropTypes.number,
        isOutOfStock: PropTypes.bool,
        showsGift: PropTypes.bool,
    };

    static defaultProps = {
        product: {},
        quantity: 0,
        totalLine: 0,
        isOutOfStock: false,
        showsGift: false,
    };

    render() {
        const {
            product,
            quantity,
            totalLine,
            isOutOfStock,
            showsGift
        } = this.props;

        const {
            original_thumbnail = "",
            name = ""
        } = product;

        return (
            <View style={styles.checkoutProduct}>
                <FastImage
                    source={{uri: makeCDNImageURL(original_thumbnail, imageTypes.CART)}}
                    style={styles.productImage}
                />
                <View style={styles.productNameContainer}>
                    <Text style={styles.productInfoText}>{name}</Text>
                    {isOutOfStock && <Text>{getOutOfStockText(product.qty)}</Text>}
                </View>
                <View style={styles.productQuantityContainer}>
                    <Text style={styles.productInfoText}>x{quantity}</Text>
                </View>
                <View style={styles.totalLineContainer}>
                    <Text style={styles.productInfoText}>{formatWithCurrency(totalLine)}</Text>
                    {showsGift && <Image source={giftIcon}/>}
                </View>
            </View>
        );
    }
}

export default CheckoutProduct;
