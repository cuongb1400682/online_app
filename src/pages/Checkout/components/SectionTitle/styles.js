import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    titleWrapper: {
        width: "100%",
        height: 40,
        backgroundColor: "#fafafa",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 16,
        borderStyle: "solid",
        borderBottomWidth: 1,
        borderColor: "#ddd"
    },
    titleText: {
        fontSize: 14,
        color: "#333",
    },
    subtitleText: {
        fontSize: 12,
        color: "#666"
    },
    rightContainer: {
        alignSelf: "flex-end"
    }
});

export default styles;
