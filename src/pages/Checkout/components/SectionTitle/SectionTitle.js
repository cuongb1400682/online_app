import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {capitalizeString, isEmptyString} from "src/utils/extensions/strings";
import Text from "src/components/platformSpecific/Text/Text";

class SectionTitle extends Component {
    static propTypes = {
        title: PropTypes.string,
        subtitle: PropTypes.string,
        right: PropTypes.any
    };

    static defaultProps = {
        title: "",
        subtitle: "",
        right: null
    };

    render() {
        const {title, subtitle, right} = this.props;

        return (
            <View style={styles.titleWrapper}>
                <Text style={styles.titleText}>
                    {title.toUpperCase()}
                    {!isEmptyString(subtitle) && <Text style={styles.subtitleText}>{" " + capitalizeString(subtitle)}</Text>}
                </Text>
                {right}
            </View>
        );
    }
}

export default SectionTitle;
