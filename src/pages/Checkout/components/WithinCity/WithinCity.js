import React, {Component} from "react";
import {View} from "react-native";

import styles from "./styles";
import {tr} from "src/localization/localization";
import Text from "src/components/platformSpecific/Text/Text";

class WithinCity extends Component {
    render() {
        return (
            <View style={styles.shippingMethodsContent}>
                <Text style={styles.shippingInsideCityText}>
                    {tr("shipping_inside_hcm_city_only")}
                </Text>
            </View>
        );
    }
}

export default WithinCity;
