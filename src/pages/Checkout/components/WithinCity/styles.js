import {StyleSheet} from "react-native";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    shippingMethodsContent: {
        paddingHorizontal: 16,
        paddingVertical: 20
    },
    shippingInsideCityText: {
        fontWeight: "bold",
        flexDirection: "column"
    },
});

export default styles;
