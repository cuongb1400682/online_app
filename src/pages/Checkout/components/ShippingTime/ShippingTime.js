import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import SelectionBox from "src/components/base/SelectionBox/SelectionBox";
import Text from "src/components/platformSpecific/Text/Text";

class ShippingTime extends Component {
    static propTypes = {
        shippingMethods: PropTypes.array.isRequired,
        timeValue: PropTypes.any,
        dateValue: PropTypes.any,
        timeData: PropTypes.array,
        dateData: PropTypes.array,
        onDateChange: PropTypes.func,
        onTimeChange: PropTypes.func,
    };

    static defaultProps = {
        shippingMethods: [],
        timeValue: {},
        dateValue: {},
        timeData: [],
        dateData: [],
        onDateChange: () => {
        },
        onTimeChange: () => {
        },
    };

    render() {
        const {onDateChange, onTimeChange, timeValue, dateValue, timeData, dateData} = this.props;

        return (
            <View style={styles.shippingTime}>
                <View style={styles.shippingTimeInner}>
                    <Text>{tr("checkout_payment_step_shipping_time")}</Text>
                    <SelectionBox
                        style={styles.selectionBox}
                        leftIconName="calendar-alt"
                        rightIconName="caret-down"
                        onSelect={onDateChange}
                        dataSource={dateData}
                        value={dateValue}
                        placeholder={tr("checkout_payment_step_date_picker_placeholder")}
                    />
                    <SelectionBox
                        style={styles.selectionBox}
                        leftIconName="clock"
                        rightIconName="caret-down"
                        onSelect={onTimeChange}
                        dataSource={timeData}
                        value={timeValue}
                        placeholder={tr("checkout_payment_step_time_picker_placeholder")}
                    />
                </View>
            </View>
        );
    }
}

export default ShippingTime;
