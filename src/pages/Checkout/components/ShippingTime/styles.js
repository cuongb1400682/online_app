import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    shippingTime: {
        paddingHorizontal: 20,
        paddingVertical: 20,
        backgroundColor: "#fff",
        ...commonStyles.centerChildren
    },
    shippingTimeInner: {
        paddingHorizontal: 30,
        paddingVertical: 20,
        width: "100%",
        backgroundColor: "#f7f7f7"
    },
    selectionBox: {
        marginTop: 14
    }
});

export default styles;
