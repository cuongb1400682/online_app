import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    nodeInput: {
        paddingHorizontal: 20,
        paddingVertical: 24,
        backgroundColor: "#fff",
        marginTop: 10
    },
    textInput: {
        ...systemFont.regular,
        width: "100%",
        height: 65,
        color: "#222",
        fontSize: 12,
        borderStyle: "solid",
        borderWidth: 1,
        borderRadius: 3,
        borderColor: "#ddd",
        backgroundColor: "#fafafa",
        paddingHorizontal: 10,
        marginBottom: 10,
    }
});

export default styles;
