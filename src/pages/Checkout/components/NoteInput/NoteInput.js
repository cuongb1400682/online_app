import React, {Component} from "react";
import {View, TextInput} from "react-native";

import styles from "./styles";
import {tr} from "src/localization/localization";
import Text from "src/components/platformSpecific/Text/Text";

class NoteInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nodeContent: ""
        };
    }

    get noteContent() {
        return this.state.nodeContent
    }

    render() {
        return (
            <View style={styles.nodeInput}>
                <TextInput
                    style={styles.textInput}
                    editable
                    maxLength={256}
                    multiline
                    numberOfLines={10}
                    placeholder={tr("checkout_payment_step_note_input_placeholder")}
                    onChangeText={text => this.setState({nodeContent: text})}
                    value={this.state.nodeContent}
                />
                <Text>{tr("checkout_payment_step_note_input_prompt")}</Text>
            </View>
        );
    }
}

export default NoteInput;
