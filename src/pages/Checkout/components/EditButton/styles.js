import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    dashedButton: {
        borderStyle: "dashed"
    },
    editButtonText: {
        fontSize: 12,
    }
});

export default styles;
