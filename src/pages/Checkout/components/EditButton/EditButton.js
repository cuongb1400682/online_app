import React, {Component} from "react";

import styles from "./styles";
import Button from "src/components/base/Button/Button";
import {tr} from "src/localization/localization";

class EditButton extends Component {
    render() {
        return (
            <Button
                title={tr("checkout_payment_step_edit_button")}
                iconFontName="fontawesome5"
                iconName="edit"
                style={styles.dashedButton}
                iconStyle={styles.editButtonText}
                textStyle={styles.editButtonText}
                {...this.props}
            />
        );
    }
}

export default EditButton;
