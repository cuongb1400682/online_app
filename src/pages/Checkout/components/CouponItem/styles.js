import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    couponItemWrapper: {
        height: 21,
        flexDirection: "row",
        backgroundColor: "#ff9601",
        alignSelf: "center",
        alignItems: "center",
        overflow: "hidden",
        borderRadius: 4
    },
    couponItemCloseButton: {
        color: "#fff",
        fontSize: 13
    },
    couponItemCloseButtonTouchable: {
        width: 20,
        height: 20,
        backgroundColor: "#bf7000",
        ...commonStyles.centerChildren
    },
    couponItemText: {
        color: "#fff",
        paddingHorizontal: 4,
        fontSize: 11
    }
});

export default styles;
