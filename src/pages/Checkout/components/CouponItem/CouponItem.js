import React from "react";
import {View} from "react-native";

import styles from "./styles";
import FontIcon from "src/components/base/FontIcon/FontIcon";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import Text from "src/components/platformSpecific/Text/Text";

const CouponItem = ({children, onClose, style, ...rest}) => {
    return (
        <View style={[styles.couponItemWrapper, style]} {...rest}>
            <Text style={styles.couponItemText}>{children}</Text>
            <Touchable style={styles.couponItemCloseButtonTouchable} onPress={onClose}>
                <FontIcon
                    style={styles.couponItemCloseButton}
                    fontName="FontAwesome"
                    name="times"
                />
            </Touchable>
        </View>
    );
};

export default CouponItem;
