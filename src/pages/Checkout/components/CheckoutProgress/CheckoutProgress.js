import React, {Component} from "react";
import {Image, View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import screenIds from "src/constants/screenIds";
import Text from "src/components/platformSpecific/Text/Text";
import checkIconInactive from "src/assets/static/icons/check-icon-inactive.png";
import checkIcon from "src/assets/static/icons/check-icon.png";
import locateIcon from "src/assets/static/icons/locate-icon.png";
import paymentIconInactive from "src/assets/static/icons/payment-icon-inactive.png";
import paymentIcon from "src/assets/static/icons/payment-icon.png";

const CheckoutStep = ({activeIcon, inactiveIcon, passed = false, text = ""}) => {
    return (
        <View style={styles.checkoutStep}>
            <Image style={styles.checkoutStepIcon} source={passed ? activeIcon : inactiveIcon}/>
            <Text style={styles.checkoutProgressName} numberOfLines={1}>{text}</Text>
        </View>
    );
};

const Bar = ({style = {}, active = false}) => <View style={[style, active ? styles.activeBar : {}]}/>;

class CheckoutProgress extends Component {
    static propTypes = {
        currentScreenId: PropTypes.string
    };

    static defaultProps = {
        currentScreenId: ""
    };

    render() {
        const {currentScreenId} = this.props;
        const passStepPayment = screenIds.CHECKOUT_PAYMENT_STEP === currentScreenId;
        const passStepOrderSuccess = screenIds.CHECKOUT_SUCCESS_STEP === currentScreenId;

        return (
            <View style={styles.checkoutProgressContainer}>
                <View style={styles.checkoutProgress}>
                    <CheckoutStep
                        activeIcon={locateIcon}
                        inactiveIcon={locateIcon}
                        text={tr("checkout_process_address")}
                        passed
                    />
                    <Bar style={styles.bar1} active={passStepPayment || passStepOrderSuccess}/>
                    <CheckoutStep
                        activeIcon={paymentIcon}
                        inactiveIcon={paymentIconInactive}
                        text={tr("checkout_process_payment")}
                        passed={passStepPayment || passStepOrderSuccess}
                    />
                    <Bar style={styles.bar2} active={passStepOrderSuccess}/>
                    <CheckoutStep
                        activeIcon={checkIcon}
                        inactiveIcon={checkIconInactive}
                        text={tr('checkout_process_confirm')}
                        passed={passStepOrderSuccess}
                    />
                </View>
            </View>
        );
    }
}

export default CheckoutProgress;
