import {StyleSheet} from "react-native";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    checkoutProgressBackground: {
        width: "100%",
        height: 80,
        backgroundColor: "#fff",
        paddingTop: 4
    },
    checkoutProgressContainer: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: 350,
        height: 42
    },
    circleNumber: {
        width: 30,
        height: 30,
        borderRadius: 15,
        marginBottom: 6
    },
    filledStepNumber: {
        backgroundColor: colors.primaryColor,
    },
    unfilledStepNumber: {
        backgroundColor: "#fff",
        borderWidth: 1,
        borderColor: colors.primaryColor
    },
    stepNumberText: {
        fontSize: 12,
        fontWeight: "bold"
    },
    filledStepNumberText: {
        color: "#fff"
    },
    unfilledStepNumberText: {
        color: colors.primaryColor
    },
    filledBar: {
        backgroundColor: colors.primaryColor
    },
    unfilledBar: {
        backgroundColor: "#e6e6e6"
    },
    bar: {
        position: "absolute",
        top: 3,
        left: 83,
        width: 158,
        height: 3,
        borderRadius: 3
    },
    stepText: {
        fontSize: 12,
        color: "#333"
    }
});

export default {
    checkoutProgressContainer: {
        backgroundColor: "#fff",
        ...commonStyles.centerChildren
    },
    checkoutProgress: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: 310,
        height: 84,
    },
    checkoutStepIcon: {
        width: 38,
        height: 38,
    },
    checkoutProgressName: {
        marginTop: 8,
        fontSize: 12
    },
    checkoutStep: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        overflow: "visible",
        ellipsizeMode: "head",
    },
    bar1: {
        position: "absolute",
        top: 25,
        left: 43,
        width: 80,
        height: 6,
        borderRadius: 3,
        backgroundColor: "#ddd"
    },
    bar2: {
        position: "absolute",
        top: 25,
        left: 172,
        width: 86,
        height: 6,
        borderRadius: 3,
        backgroundColor: "#ddd"
    },
    activeBar: {
        backgroundColor: colors.primaryColor
    }
};
