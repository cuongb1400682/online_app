import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    orderSuccess: {
        flex: 1,
        backgroundColor: "#F5F5F9",
    },
    orderSuccessContent: {
        width: "100%",
        flexDirection: "column",
        alignItems: "center"
    },
    boldText: {
        fontWeight: "bold"
    },
    whiteBackground: {
        backgroundColor: "#fff",
        paddingHorizontal: 16,
        borderRadius: 3
    },
    shippingInfoPart: {
        width: "100%",
        paddingBottom: 20,
        flexWrap: "wrap",
    },
    shippingInfoTitle: {
        fontSize: 12,
        color: "#000"
    },
    customerInfo: {
        fontSize: 12,
        color: colors.textColor,
        textAlign: "justify"
    },
    customerName: {
        marginVertical: 4,
        fontSize: 12,
        fontWeight: "bold",
        textAlign: "justify"
    },
    orderDetailPart: {
        width: "100%",
        paddingTop: 16,
        backgroundColor: "#fff"
    },
    orderNumber: {
        fontSize: 16,
        color: "#222"
    },
    estimatedShippingTime: {
        fontSize: 13,
        color: "#222",
        marginTop: 6
    },
    noteMessageContainer: {
        width: "100%",
        flexWrap: "wrap",
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: "#FFFBF5",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#FA9002",
        marginTop: 10
    },
    noteMessage: {
        fontSize: 10,
        color: "#000",
        textAlign: "justify"
    },
    acknowledgement: {
        width: "100%",
        paddingVertical: 32,
        paddingHorizontal: 16,
        flexWrap: "wrap",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    circleCheck: {
        marginBottom: 17
    },
    thankText: {
        fontSize: 20,
        textAlign: "center",
        color: "#0BA049"
    },
    hotlineContact: {
        textAlign: "center",
        fontStyle: "italic",
        fontSize: 13,
        marginTop: 16
    },
    hotline: {
        fontStyle: "italic",
        fontWeight: "bold",
        color: colors.primaryColor
    },
    navBar: {
        flexDirection: "row",
        justifyContent: "flex-start",
        paddingHorizontal: 16
    },
    giigaaLogo: {
        height: 30,
        width: 120
    },
    checkIcon: {
        fontSize: 70
    },
    button: {
        marginTop: 10,
        alignSelf: "stretch",
        marginHorizontal: 12,
        height: 40
    },
    buttonText: {
        fontSize: 16
    },
    separator: {
        borderStyle: "solid",
        borderBottomWidth: 1,
        borderColor: "#ddd",
        marginVertical: 16,
    },
});

export default styles;
