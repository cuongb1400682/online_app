import React, {Component} from "react";
import {Image, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import CircleCheck from "src/components/base/svgIcons/CircleCheck/CircleCheck";
import {tr} from "src/localization/localization";
import {fetchOrderDetail} from "src/pages/Checkout/checkoutActions";
import screenIds from "src/constants/screenIds";
import {shippingMethods} from "src/constants/enums";
import {getEstimateShippingTime} from "src/utils/extensions/dateTime";
import {capitalizeString, makeFullAddress} from "src/utils/extensions/strings";
import Button from "src/components/base/Button/Button";
import {popToRoot, pushTo} from "src/utils/misc/navigator";
import giigaaLogo from "src/assets/static/logo/giigaaLogo.png";
import Text from "src/components/platformSpecific/Text/Text";
import KeyboardAwareView from "src/components/platformSpecific/KeyboardAwareView/KeyboardAwareView";

class OrderSuccess extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        orderDetail: PropTypes.any,
        orderId: PropTypes.any,
        fetchOrderDetail: PropTypes.func,
    };

    static defaultProps = {
        orderDetail: {},
        orderId: -1,
        fetchOrderDetail: () => {
        },
    };

    componentDidMount() {
        const {fetchOrderDetail, orderId} = this.props;

        if (orderId) {
            fetchOrderDetail(orderId);
        } else {
            this.goBackToHome();
        }
    }

    goBackToHome = () => {
        const {navigator} = this.props;

        popToRoot(navigator);
    };

    showOrderDetail = () => {
        const {navigator, orderId} = this.props;

        popToRoot(navigator, {animated: false});
        pushTo(navigator, {
            screen: screenIds.ORDER_MANAGER,
            passProps: {orderId},
            animated: false
        });
        pushTo(navigator, {
            screen: screenIds.ORDER_DETAIL,
            passProps: {orderId}
        });
    };

    getEstimatedShippingTime = () => {
        const {orderDetail} = this.props;

        if (orderDetail.delivery_method === shippingMethods.FAST) {
            return "90 phút tới";
        }

        return getEstimateShippingTime(orderDetail.from_expected_shipping_time, orderDetail.to_expected_shipping_time);
    };

    renderNavBar = () => (
        <NavBar style={styles.navBar}>
            <Image
                source={giigaaLogo}
                style={styles.giigaaLogo}
                resizeMode="contain"
            />
        </NavBar>
    );

    renderAcknowledgement = () => (
        <View style={styles.acknowledgement}>
            <CircleCheck style={styles.circleCheck}/>
            <Text style={styles.thankText}>{tr("checkout_order_success_thank")}</Text>
            <Text style={styles.hotlineContact}>
                {tr("checkout_order_success_hotline_contact_1")}
                <Text style={styles.hotline}>{tr("checkout_order_success_hotline_contact_2")}</Text>
                {tr("checkout_order_success_hotline_contact_3")}
            </Text>
        </View>
    );

    renderWaitingMessage = () => {
        const {orderDetail} = this.props;

        if (orderDetail.waiting_for && orderDetail.waiting_for.length > 0) {
            return (
                <View style={styles.noteMessageContainer}>
                    <Text style={styles.noteMessage}>{tr("checkout_order_success_order_detail_note_message")}</Text>
                </View>
            );
        }

        return null;
    };

    renderOrderDetailPart = () => {
        const {orderDetail} = this.props;

        return (
            <View style={styles.orderDetailPart}>
                <Text style={styles.orderNumber}>
                    {tr("checkout_order_success_order_detail_order_number")}
                    <Text style={styles.hotline}>{orderDetail.order_number}</Text>
                </Text>
                <Text style={styles.estimatedShippingTime}>
                    {tr("checkout_order_success_order_detail_estimated_shipping_time")}
                    <Text style={styles.boldText}>{this.getEstimatedShippingTime()}</Text>
                </Text>
                {this.renderWaitingMessage()}
            </View>
        );
    };

    renderShippingInfoPart = () => {
        const {orderDetail} = this.props;
        const {address = {}} = orderDetail;

        return (
            <View style={styles.shippingInfoPart}>
                <Text style={styles.shippingInfoTitle}>{tr("checkout_order_success_shipping_info_title")}</Text>
                <Text style={styles.customerName}>
                    {capitalizeString(address.contact_name)}
                </Text>
                <Text style={styles.customerInfo}>
                    {tr("checkout_order_success_shipping_info_address")}
                    {makeFullAddress(address)}
                </Text>
                <Text style={styles.customerInfo}>
                    {tr("checkout_order_success_shipping_info_phone")}
                    {address.contact_phone}
                </Text>
            </View>
        );
    };

    renderButtons = () => (
        <React.Fragment>
            <Button
                onPress={this.goBackToHome}
                type="fully-colored"
                style={styles.button}
                textStyle={styles.buttonText}
                title={tr("checkout_order_success_back_to_home_button")}
            />
            <Button
                onPress={this.showOrderDetail}
                style={styles.button}
                textStyle={styles.buttonText}
                title={tr("checkout_order_success_check_your_order")}
            />
        </React.Fragment>
    );

    render() {
        return (
            <View style={styles.orderSuccess}>
                {this.renderNavBar()}
                <KeyboardAwareView>
                    <View style={styles.orderSuccessContent}>
                        {this.renderAcknowledgement()}
                        <View style={styles.whiteBackground}>
                            {this.renderOrderDetailPart()}
                            <View style={styles.separator}/>
                            {this.renderShippingInfoPart()}
                        </View>
                        {this.renderButtons()}
                    </View>
                </KeyboardAwareView>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    orderDetail: state.checkout.order_detail.data,
    fetching: state.checkout.order_detail.fetching
});

export default connect(mapStateToProps, {
    fetchOrderDetail
})(OrderSuccess);
