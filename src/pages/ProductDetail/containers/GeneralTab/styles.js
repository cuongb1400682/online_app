import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    scrollView: {
        flex: 1,
        backgroundColor: "#eee"
    }
});

export default styles;
