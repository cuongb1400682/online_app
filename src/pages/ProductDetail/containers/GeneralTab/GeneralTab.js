import React, {Component} from "react";
import {ScrollView, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import {fetchRelatedProducts, onChangeTab, toggleAddToCartPanel} from "src/pages/ProductDetail/productDetailActions";
import Gallery from "src/pages/ProductDetail/components/Gallery/Gallery";
import commonStyles from "src/styles/commonStyles";
import GeneralInfo from "src/pages/ProductDetail/components/GeneralInfo/GeneralInfo";
import ProductSelect from "src/pages/ProductDetail/components/ProductSelect/ProductSelect";
import DeliveryPolicy from "src/pages/ProductDetail/components/DeliveryPolicy/DeliveryPolicy";
import ProductSpecification from "src/pages/ProductDetail/components/ProductSpecification/ProductSpecification";
import ProductDescription from "src/pages/ProductDetail/components/ProductDescription/ProductDescription";
import RelatedProducts from "src/pages/ProductDetail/components/RelatedProducts/ReleatedProducts";
import ProductChildrenList from "src/pages/ProductDetail/components/ProductChildrenList/ProductChildrenList";
import DealBar from "src/pages/ProductDetail/components/DealBar/DealBar";
import {isEmptyArray} from "src/utils/extensions/arrays";
import {productTypes} from "src/constants/enums";

class GeneralTab extends Component {
    static propTypes = {
        selectedProduct: PropTypes.object,
        relatedProducts: PropTypes.any,
        totalItems: PropTypes.number,
        mainProduct: PropTypes.object,
        fetchRelatedProducts: PropTypes.func,
        description: PropTypes.string,
        onChangeTab: PropTypes.func,
        toggleAddToCartPanel: PropTypes.func,
    };

    static defaultProps = {
        selectedProduct: {
            images: []
        },
        relatedProducts: {},
        totalItems: 0,
        mainProduct: {},
        description: "",
        fetchRelatedProducts: () => {
        },
        onChangeTab: () => {
        },
        toggleAddToCartPanel: () => {
        },
    };

    componentDidUpdate(prevProps) {
        const {id} = this.props.selectedProduct || {};
        const {id: prevId} = prevProps.selectedProduct || {};

        if (this.props.mainProduct.id && id !== prevId) {
            this.props.fetchRelatedProducts(this.props.mainProduct.sub_category_id);
        }
    }

    goToTabIndex = (index) => () => {
        this.props.onChangeTab(index);
    };

    getChildrenProductInCombo = (parentProduct = {}) => {
        const {children_combo: products} = parentProduct;

        if (!products || isEmptyArray(products)) {
            return [];
        }

        return products.map(product => ({...product, item_qty: product.combo_product_qty}));
    };

    getChildrenProductInGroup = (parentProduct = {}) => {
        const {group_children: products} = parentProduct;

        if (!products || isEmptyArray(products)) {
            return [];
        }

        return products.map(product => ({...product, item_qty: product.group_product_qty}));
    };

    mapGetChildrenProductFromProductType = productType => {
        const mapProductTypeWithFunctions = {
            [productTypes.COMBO]: this.getChildrenProductInCombo,
            [productTypes.GROUP]: this.getChildrenProductInGroup,
        };
        const getChildrenProduct = mapProductTypeWithFunctions[productType];

        if (typeof getChildrenProduct !== "function") {
            return (() => {
            });
        }

        return getChildrenProduct;
    };

    renderListChildrenProduct = () => {
        const getChildrenProduct = this.mapGetChildrenProductFromProductType(this.props.selectedProduct.type);
        const childrenProduct = getChildrenProduct(this.props.selectedProduct);
        const propsMapping = {
            [productTypes.COMBO]: {},
            [productTypes.GROUP]: {
                showOutOfStock: true
            },
        };
        const props = propsMapping[this.props.selectedProduct.type] || {};

        return (
            <ProductChildrenList
                {...props}
                navigator={this.props.navigator}
                products={childrenProduct}
            />
        );
    };

    render() {
        const {selectedProduct, relatedProducts, totalItems, description, toggleAddToCartPanel, navigator} = this.props;

        return (
            <ScrollView
                style={styles.scrollView}
                contentContainerStyle={commonStyles.flexColumnCenter}
            >
                <Gallery images={selectedProduct.images}/>
                <DealBar product={selectedProduct}/>
                <GeneralInfo product={selectedProduct}/>
                <ProductSelect
                    product={selectedProduct}
                    numberOfItems={totalItems}
                    onMoreOptionPress={toggleAddToCartPanel}
                />
                {this.renderListChildrenProduct()}
                <DeliveryPolicy/>
                <ProductSpecification/>
                <ProductDescription
                    content={description}
                    onSeeAllPressed={this.goToTabIndex(1)}
                />
                {/*<ActionButton*/}
                {/*title={tr("product_detail_general_action_button_rating_and_comment")}*/}
                {/*subtitle={`${0} ${tr("product_detail_general_action_button_rating_and_comment_qty_unit")}`}*/}
                {/*onPress={this.goToTabIndex(2)}*/}
                {/*/>*/}
                {/*<ActionButton*/}
                {/*title={tr("product_detail_general_action_button_qa")}*/}
                {/*subtitle={`${0} ${tr("product_detail_general_action_button_qa_qty_unit")}`}*/}
                {/*onPress={this.goToTabIndex(3)}*/}
                {/*/>*/}
                <RelatedProducts products={relatedProducts} navigator={navigator}/>
                <View style={{height: 80}}/>
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => ({
    selectedProduct: state.product_detail.selected_product,
    relatedProducts: state.product_detail.related_products.products,
    totalItems: state.product_detail.main_product.children.length,
    mainProduct: state.product_detail.main_product,
    description: state.product_detail.description.data,
});

export default connect(mapStateToProps, {
    onChangeTab,
    fetchRelatedProducts,
    toggleAddToCartPanel
})(GeneralTab);
