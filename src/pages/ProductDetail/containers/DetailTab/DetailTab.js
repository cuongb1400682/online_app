import React, {Component} from "react";
import {Image, Platform, ScrollView, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import HTMLView from "react-native-htmlview";
import HTMLRenderer from "react-native-render-html";

import styles from "./styles";
import colors from "src/constants/colors";
import Text from "src/components/platformSpecific/Text/Text";
import systemFont from "src/styles/systemFont";
import {screenWidth} from "src/utils/reactNative/dimensions";

const SCREEN_WIDTH = screenWidth();
const CONTENT_PADDING = 8;

const htmlViewStyleSheet = {
    a: {
        color: colors.primaryColor
    },
    p: {
        textAlign: "justify",
        paddingHorizontal: 10
    },
    h3: {
        paddingHorizontal: 10,
        fontSize: 20
    },
    strong: {
        fontWeight: "bold"
    },
    bold: {
        fontWeight: "bold"
    },
    i: {
        fontStyle: "italic"
    },
    em: {
        fontStyle: "italic"
    }
};

const customTextRenderer = (attribs, children) => {
    return <Text style={{fontSize: 14, ...systemFont.regular}}>{children}</Text>;
};

class DetailTab extends Component {
    static propTypes = {
        description: PropTypes.string,
    };

    static defaultProps = {
        description: "",
    };

    renderHTMLView = () => (
        <ScrollView containContainerStyle={styles.scrollView}>
            <HTMLView
                value={this.props.description}
                style={styles.htmlView}
                stylesheet={htmlViewStyleSheet}
                lineBreak=""
                paragraphBreak=""
                TextComponent={Text}
            />
            <View style={{height: 80}}/>
        </ScrollView>
    );

    htmlTagRenderer = () => ({
        img: (attribs = {}) => {
            const {
                width = 100,
                height = 100,
                src = ""
            } = attribs;
            const ratio = height / width;

            return (
                <Image
                    style={{
                        width: SCREEN_WIDTH - 2 * CONTENT_PADDING,
                        height: SCREEN_WIDTH * ratio - 2 * CONTENT_PADDING
                    }}
                    source={{uri: src}}
                />
            );
        },
        strong: customTextRenderer,
        bold: customTextRenderer,
        em: customTextRenderer,
        italic: customTextRenderer,
    });

    renderHTMLRenderer = () => {
        return (
            <ScrollView containContainerStyle={styles.scrollView}>
                <HTMLRenderer
                    containerStyle={{paddingHorizontal: CONTENT_PADDING}}
                    html={this.props.description}
                    ignoredTags={["iframe"]}
                    ignoredStyles={["display", "font-size", "font-style", "font-family"]}
                    allowFontScaling={false}
                    textSelectable={false}
                    emSize={14}
                    ptSize={1.3}
                    renderers={this.htmlTagRenderer()}
                    baseFontStyle={{
                        fontFamily: "Roboto-Regular",
                        fontStyle: "normal",
                        fontSize: 13,
                        textAlign: "justify"
                    }}
                />
                <View style={{height: 80}}/>
            </ScrollView>
        );
    };

    render() {
        if (Platform.OS === "android") {
            return this.renderHTMLRenderer();
        }

        return this.renderHTMLView();
    }
}

const mapStateToProps = (state) => ({
    description: state.product_detail.description.data,
});

export default connect(mapStateToProps, {})(DetailTab);
