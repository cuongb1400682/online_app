import {StyleSheet} from "react-native";

import systemFont, {systemFontName} from "src/styles/systemFont";

const styles = StyleSheet.create({
    htmlView: {
        flex: 1,
        marginVertical: 10
    },
    scrollView: {
        marginHorizontal: 16
    },
});

export default styles;
