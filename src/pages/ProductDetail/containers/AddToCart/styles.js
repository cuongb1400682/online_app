import {StyleSheet} from "react-native";
import colors from "src/constants/colors";
import {screenWidth} from "src/utils/reactNative/dimensions";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    addToCartContainer: {
        width: "100%",
        backgroundColor: "#fff",
        zIndex: 1000,
        paddingHorizontal: 16,
    },
    hasBorderBottom: {
        borderBottomWidth: 1,
        borderColor: "#ddd",
        borderStyle: "solid",
        paddingVertical: 16,
    },
    productInfo: {
        height: 86,
        width: "100%",
        flexDirection: "row",
        alignItems: "center"
    },
    productInfoLeftPart: {
        flexDirection: "row",
        alignItems: "center",
        flexWrap: "wrap",
        flex: 1,
        marginRight: 10
    },
    productInfoRightPart: {
        flexDirection: "column",
        alignItems: "flex-end",
        textDecorationLine: "line-through",
        textDecorationStyle: "solid"
    },
    productInfoImage: {
        height: 48,
        width: 62,
        marginRight: 12
    },
    productInfoName: {
        maxWidth: "100%",
        fontSize: 14,
        flex: 1
    },
    productInfoUnitPrice: {
        fontSize: 12,
    },
    productInfoMarketPrice: {
        fontSize: 10,
        textDecorationLine: "line-through",
        textDecorationStyle: "solid",
        color: "#a2a2a2",
        marginTop: 4
    },
    productWebLabels: {
        flexDirection: "column",
        width: "100%"
    },
    productWebLabelsList: {
        flexDirection: "row",
        justifyContent: "flex-start",
        flexWrap: "wrap"
    },
    productPriceWebLabel: {
        marginTop: 8,
        marginLeft: 0,
        marginRight: 8
    },
    quantitySelection: {
        paddingVertical: 16,
    },
    addToCartButtonWrapper: {
        backgroundColor: "#fff",
        width: screenWidth(),
        position: "absolute",
        bottom: 0,
        zIndex: 1000,
        ...commonStyles.hasShadow
    },
    addToCartButton: {
        height: 40,
        marginVertical: 8,
        marginHorizontal: 10,
    },
    addToCartButtonIcon: {
        color: "#fff",
        fontSize: 17
    },
    numberSpinner: {
        marginTop: 10
    },
    topBorder: {
        height: 4,
        width: "100%",
        backgroundColor: colors.primaryColor
    },
    topBorderContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "flex-end",
        alignItems: "flex-end",
        width: "100%",
    },
    closeButton: {
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        width: 46,
        height: 30,
        backgroundColor: colors.primaryColor
    }
});

export default styles;
