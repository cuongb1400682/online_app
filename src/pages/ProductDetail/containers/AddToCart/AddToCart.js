import React, {Component} from "react";
import {Modal, TouchableWithoutFeedback, View, Platform} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import {selectProduct, toggleAddToCartPanel} from "src/pages/ProductDetail/productDetailActions";
import {addToCart} from "src/redux/common/commonActions";
import FastImage from "src/components/base/FastImage/FastImage";
import {makeCDNImageURL} from "src/utils/cdn/images";
import {imageTypes} from "src/constants/enums";
import {formatWithCurrency} from "src/utils/extensions/strings";
import WebLabel from "src/pages/ProductDetail/components/WebLabel/WebLabel";
import {tr} from "src/localization/localization";
import Button from "src/components/base/Button/Button";
import NumberSpinner from "src/components/base/NumberSpinner/NumberSpinner";
import Cross from "src/components/base/svgIcons/Cross/Cross";
import commonStyles from "src/styles/commonStyles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import KeyboardPaddingView from "src/components/base/KeyboardPaddingView/KeyboardPaddingView";
import Text from "src/components/platformSpecific/Text/Text";
import CartAnimator from "src/pages/ProductDetail/components/CartAnimator/CartAnimator";

class AddToCart extends Component {
    static propTypes = {
        showAddToCartPanel: PropTypes.bool,
        selectedProduct: PropTypes.object,
        productChildren: PropTypes.array,
        toggleAddToCartPanel: PropTypes.func,
        selectProduct: PropTypes.func,
        addToCart: PropTypes.func,
    };

    static defaultProps = {
        showAddToCartPanel: false,
        selectedProduct: {},
        productChildren: [],
        toggleAddToCartPanel: () => {
        },
        selectProduct: () => {
        },
        addToCart: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            qty: 1
        };
    }

    isMarketPriceVisible = () => {
        const {selectedProduct} = this.props;
        const {
            unit_price = 0,
            market_price = 0
        } = selectedProduct;

        return (market_price > unit_price);
    };

    isOutOfStock = () => {
        const {selectedProduct} = this.props;

        return selectedProduct.qty <= 0;
    };

    showAddToCart = () => {
        const {showAddToCartPanel, toggleAddToCartPanel} = this.props;

        toggleAddToCartPanel();

        if (showAddToCartPanel) {
            if (this.cartAnimatorRef) {
                this.cartAnimatorRef.startAnimation();
            }
        }
    };

    handleAddToCart = () => {
        const {qty} = this.state;

        this.props.addToCart({
            productId: this.props.selectedProduct.id,
            quantity: qty
        });
    };

    closeModal = () => this.props.toggleAddToCartPanel();

    selectProduct = (product = {}) => () => {
        const {selectProduct} = this.props;

        selectProduct(product.id);
    };

    renderProductInfo = () => {
        const {selectedProduct} = this.props;
        const {
            original_thumbnail = "",
            name = "",
            unit_price = 0,
            market_price = 0
        } = selectedProduct;
        const productInfoImageUrl = makeCDNImageURL(original_thumbnail, imageTypes.BEST_SALES);

        return (
            <View style={[styles.productInfo, styles.hasBorderBottom]}>
                <View style={styles.productInfoLeftPart}>
                    <FastImage
                        source={{uri: productInfoImageUrl}}
                        style={styles.productInfoImage}
                    />
                    <Text style={styles.productInfoName}>{name}</Text>
                </View>
                <View style={styles.productInfoRightPart}>
                    <Text style={styles.productInfoUnitPrice}>{formatWithCurrency(unit_price)}</Text>
                    {this.isMarketPriceVisible() &&
                    <Text style={styles.productInfoMarketPrice}>{formatWithCurrency(market_price)}</Text>}
                </View>
            </View>
        );
    };

    renderProductWebLabels = () => {
        const {productChildren, selectedProduct} = this.props;

        return (
            <View style={[styles.productWebLabels, styles.hasBorderBottom]}>
                <Text>{tr("product_detail_general_product_weight")}</Text>
                <View style={styles.productWebLabelsList}>
                    {productChildren.map((child, index) =>
                        <WebLabel
                            key={index}
                            style={styles.productPriceWebLabel}
                            outOfStock={child.qty <= 0}
                            label={child.web_label}
                            selected={child.id === selectedProduct.id}
                            onPress={this.selectProduct(child)}
                        />
                    )}
                </View>
            </View>
        );
    };

    renderQuantitySelection = () => {
        return (
            <View style={styles.quantitySelection}>
                <Text>{tr("product_detail_general_quantity_selection")}</Text>
                <NumberSpinner
                    style={styles.numberSpinner}
                    defaultValue={1}
                    min={1}
                    max={Number.MAX_SAFE_INTEGER}
                    onChange={qty => this.setState({qty})}
                />
            </View>
        );
    };

    renderAddToCartButton = () => (
        <View style={styles.addToCartButtonWrapper}>
            <Button
                type="fully-colored"
                style={styles.addToCartButton}
                iconFontName="fontawesome5"
                iconName="shopping-cart"
                iconStyle={styles.addToCartButtonIcon}
                title={this.isOutOfStock()
                    ? tr("product_detail_general_add_to_cart_button_out_of_stock")
                    : tr("product_detail_general_add_to_cart_button")}
                disabled={this.isOutOfStock()}
                onPress={this.showAddToCart}
            />
        </View>
    );

    renderTopBorder = () => {
        return (
            <TouchableWithoutFeedback onPress={this.closeModal}>
                <View style={styles.topBorderContainer}>
                    <Touchable
                        style={[styles.closeButton, commonStyles.centerChildren]}
                        onPress={this.closeModal}
                    >
                        <Cross/>
                    </Touchable>
                    <View style={styles.topBorder}/>
                </View>
            </TouchableWithoutFeedback>
        );
    };

    renderAddToCartModal = () => {
        const {showAddToCartPanel} = this.props;

        return (
            <Modal
                animationType={Platform.OS === "ios" ? "slide" : "fade"}
                hardwareAccelerated
                transparent
                visible={showAddToCartPanel}
                onRequestClose={this.closeModal}
            >
                {this.renderTopBorder()}
                <View style={styles.addToCartContainer}>
                    {this.renderProductInfo()}
                    {this.renderProductWebLabels()}
                    {this.renderQuantitySelection()}
                    <KeyboardPaddingView initialHeight={70}/>
                    {this.renderAddToCartButton()}
                </View>
            </Modal>
        );
    };

    renderCartAnimator = () => {
        const {original_thumbnail = ""} = this.props.selectedProduct;

        return (
            <CartAnimator
                ref={ref => this.cartAnimatorRef = ref}
                productThumbnail={original_thumbnail}
                onAnimationEnd={this.handleAddToCart}
            />
        );
    };

    render() {
        return (
            <React.Fragment>
                {this.renderAddToCartModal()}
                {this.renderAddToCartButton()}
                {this.renderCartAnimator()}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    showAddToCartPanel: state.product_detail.add_to_cart.show_panel,
    selectedProduct: state.product_detail.selected_product,
    productChildren: state.product_detail.main_product.children
});

export default connect(mapStateToProps, {
    toggleAddToCartPanel,
    selectProduct,
    addToCart
})(AddToCart);
