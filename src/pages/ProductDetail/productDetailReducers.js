import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import productDetailActionTypes from "src/redux/actionTypes/productDetailActionTypes";
import {productTypes} from "src/constants/enums";

const initialState = {
    main_product: {
        children: []
    },
    selected_product: {},
    add_to_cart: {
        adding: false,
        show_panel: false
    },
    related_products: {
        products: [],
        fetching: false
    },
    display: {
        tab_index: 0
    },
    fetching: false,
    description: {
        data: "",
        error: false,
        loading: false
    },
};

const normalizeData = product => {
    if (product.children) {
        return product;
    }

    return {
        ...product,
        children: [{...product}]
    };
};

const selectProductById = ({children, ...mainProduct}, id) => {
    if (mainProduct.type !== productTypes.CONFIG) {
        return {...mainProduct};
    }

    let selectedProduct = children.find(product => product.id.toString() === id.toString());

    if (!selectedProduct) {
        selectedProduct = children.find(product => product.qty > 0)
    }

    return selectedProduct || children[0];
};

const productDetailReducers = (state = initialState, {type, payload, meta}) => {
    switch (type) {
        case toRequest(productDetailActionTypes.FETCH_RELATED_PRODUCTS):
            return {
                ...state,
                related_products: {
                    ...state.related_products,
                    fetching: true
                }
            };
        case toSuccess(productDetailActionTypes.FETCH_RELATED_PRODUCTS):
            return {
                ...state,
                related_products: {
                    ...state.related_products,
                    ...payload.reply,
                    fetching: false
                }
            };
        case toFailure(productDetailActionTypes.FETCH_RELATED_PRODUCTS):
            return {
                ...state,
                related_products: {
                    ...state.related_products,
                    fetching: false
                }
            };
        case toRequest(productDetailActionTypes.FETCH_PRODUCT_DESCRIPTION):
            return {
                ...state,
                description: {
                    data: "",
                    error: false,
                    fetching: true
                }
            };
        case toFailure(productDetailActionTypes.FETCH_PRODUCT_DESCRIPTION):
            return {
                ...state,
                description: {
                    data: "",
                    error: true,
                    fetching: false
                }
            };
        case toSuccess(productDetailActionTypes.FETCH_PRODUCT_DESCRIPTION):
            return {
                ...state,
                description: {
                    data: payload.reply.description,
                    error: false,
                    fetching: false
                }
            };
        case toRequest(productDetailActionTypes.FETCH_PRODUCT_DETAIL):
            return {
                ...state,
                main_product: {
                    children: []
                },
                selected_product: {},
                fetching: true
            };
        case toSuccess(productDetailActionTypes.FETCH_PRODUCT_DETAIL):
            return {
                ...state,
                main_product: {
                    ...normalizeData(payload.reply)
                },
                selected_product: {
                    ...selectProductById(payload.reply, meta.id)
                },
                fetching: false
            };
        case toFailure(productDetailActionTypes.FETCH_PRODUCT_DETAIL):
            return {
                ...state,
                fetching: false
            };
        case productDetailActionTypes.SELECT_PRODUCT:
            return {
                ...state,
                selected_product: {...selectProductById(state.main_product, payload.id)}
            };
        case productDetailActionTypes.TOGGLE_ADD_TO_CART_PANEL:
            return {
                ...state,
                add_to_cart: {
                    ...state.add_to_cart,
                    show_panel: !state.add_to_cart.show_panel
                }
            };
        case productDetailActionTypes.CHANGE_TAB_INDEX:
            return {
                ...state,
                display: {
                    ...state,
                    tab_index: payload.tab_index
                }
            };
        case productDetailActionTypes.CLEAR_PRODUCT_DETAIL:
            return {
                ...initialState
            };
        default:
            return state;
    }
};

export default productDetailReducers;
