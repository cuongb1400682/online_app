import {createAction, createAsyncAction} from "src/utils/api/createAction";
import productDetailActionTypes from "src/redux/actionTypes/productDetailActionTypes";

const fetchDescription = productId => createAsyncAction({
    type: productDetailActionTypes.FETCH_PRODUCT_DESCRIPTION,
    payload: {
        request: "/product/description/get_detail",
        params: {
            product_id: productId
        }
    }
});

const fetchProductDetail = id => createAsyncAction({
    type: productDetailActionTypes.FETCH_PRODUCT_DETAIL,
    payload: {
        request: `/product/get_detail?product_id=${id}`
    },
    meta: {
        id
    },
    onSuccess: (dispatch) => {
        dispatch(fetchDescription(id));
    }
});

const fetchRelatedProducts = subCategoryId => createAsyncAction({
    type: productDetailActionTypes.FETCH_RELATED_PRODUCTS,
    payload: {
        request: "/product/search",
        params: {
            page_size: 20,
            page_index: 0,
            category_id: -1,
            sub_category_id: subCategoryId
        }
    }
});

const selectProduct = id => createAction({
    type: productDetailActionTypes.SELECT_PRODUCT,
    payload: {id}
});

const toggleAddToCartPanel = () => createAction({
    type: productDetailActionTypes.TOGGLE_ADD_TO_CART_PANEL,
    payload: {}
});

const onChangeTab = tabIndex => createAction({
    type: productDetailActionTypes.CHANGE_TAB_INDEX,
    payload: {
        tab_index: tabIndex
    }
});

const clearProductDetail = () => createAction({
    type: productDetailActionTypes.CLEAR_PRODUCT_DETAIL,
    payload: {}
});

export {
    clearProductDetail,
    fetchProductDetail,
    fetchDescription,
    fetchRelatedProducts,
    selectProduct,
    toggleAddToCartPanel,
    onChangeTab,
};
