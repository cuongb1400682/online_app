import {StyleSheet} from "react-native";

import {smallerDimension} from "src/utils/reactNative/dimensions";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    productImageContainer: {
        width: smallerDimension(),
        height: smallerDimension(),
        backgroundColor: "#fff"
    },
    productImage: {
        position: "absolute",
        top: 0,
        left: 0,
        width: smallerDimension(),
        height: smallerDimension()
    },
    imageBottomBar: {
        position: "absolute",
        height: 72,
        width: "100%",
        left: 0,
        bottom: 0,
    },
    imagesCarouselPagination: {
    },
    imagesCarouselPaginationInactiveDot: {
        width: 12,
        height: 12,
        borderWidth: 1,
        borderRadius: 6,
        borderColor: "#c8c8c8",
        backgroundColor: "#fff"
    },
    imagesCarouselPaginationDot: {
        width: 6,
        height: 6,
        borderWidth: 1,
        borderRadius: 6,
        borderColor: colors.primaryColor,
        backgroundColor: colors.primaryColor
    }
});

export default styles;
