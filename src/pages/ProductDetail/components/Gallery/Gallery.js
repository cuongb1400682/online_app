import React, {Component} from "react";
import {ActivityIndicator, View} from "react-native";
import Carousel, {Pagination} from "react-native-snap-carousel";
import PropTypes from "prop-types";

import styles from "./styles";
import {smallerDimension} from "src/utils/reactNative/dimensions";
import commonStyles from "src/styles/commonStyles";
import FastImage from "src/components/base/FastImage/FastImage";
import CircleButton from "src/pages/ProductDetail/components/CircleButton/CircleButton";
import {makeCDNImageURL} from "src/utils/cdn/images";
import colors from "src/constants/colors";

class Gallery extends Component {
    static propTypes = {
        images: PropTypes.array
    };

    static defaultProps = {
        images: []
    };

    constructor(props) {
        super(props);

        this.state = {
            productImageIndex: 0,
            images: []
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const {images = []} = nextProps;
        const {productImageIndex = 0} = this.state;

        this.setState({
            images: images.map((image, index) => ({
                ...image,
                isLoaded: index === productImageIndex
            }))
        });
    }

    handleImageChange = (index) => {
        const {images = []} = this.state;

        if (index < images.length) {
            images[index].isLoaded = true;
        }

        this.setState({
            productImageIndex: index,
            images: [...images]
        });
    };

    renderActivityIndicator = () => (
        <View style={[commonStyles.matchParent, commonStyles.centerChildren]}>
            <ActivityIndicator color={colors.primaryColor}/>
        </View>
    );

    renderImage = ({item: image}) => {
        const {index, original: originalUrl} = image;
        const {images = []} = this.state;

        if (index < images.length && !images[index].isLoaded) {
            return this.renderActivityIndicator();
        }

        return (
            <FastImage
                key={index}
                source={{uri: makeCDNImageURL(originalUrl)}}
                style={styles.productImage}
                resizeMode={FastImage.resizeMode.contain}
            />
        );
    };

    render() {
        const {productImageIndex, images} = this.state;
        const carouselSize = smallerDimension();

        return (
            <View style={styles.productImageContainer}>
                <Carousel
                    ref={ref => this.carouselRef = ref}
                    data={images}
                    renderItem={this.renderImage}
                    sliderWidth={carouselSize}
                    itemWidth={carouselSize}
                    itemHeight={carouselSize}
                    onSnapToItem={this.handleImageChange}
                    sliderHeight={carouselSize}
                    loopClonesPerSide={0}
                    autoplay={false}
                    loop={false}
                    firstItem={0}
                    lockScrollWhileSnapping
                />
                <View style={[styles.imageBottomBar, commonStyles.flexRowCenter]}>
                    <Pagination
                        carouselRef={this.carouselRef}
                        tappableDots={!!this.carouselRef}
                        activeDotIndex={productImageIndex}
                        dotsLength={images.length}
                        containerStyle={styles.imagesCarouselPagination}
                        inactiveDotStyle={styles.imagesCarouselPaginationInactiveDot}
                        dotStyle={styles.imagesCarouselPaginationDot}
                    />
                    <View style={commonStyles.matchParent}/>
                    <CircleButton iconName="facebook-f"/>
                    <CircleButton iconName="heart"/>
                </View>
            </View>
        )
    };
}

export default Gallery;
