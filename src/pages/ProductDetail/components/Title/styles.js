import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    title: {
        height: 40,
        width: "100%",
        borderStyle: "solid",
        borderBottomWidth: 1,
        borderColor: "#ddd",
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 16,
        backgroundColor: "#fafafa"
    },
    titleText: {
        fontSize: 15
    }
});

export default styles;
