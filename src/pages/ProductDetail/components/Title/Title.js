import React from "react";
import {View} from "react-native";

import styles from "./styles";
import Text from "src/components/platformSpecific/Text/Text";

const Title = ({text = ""}) => {
    return (
        <View style={styles.title}>
            <Text style={styles.titleText}>{text}</Text>
        </View>
    );
};

export default Title;
