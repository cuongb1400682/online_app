import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    actionButton: {
        width: "100%",
        height: 40,
        marginTop: 10,
        backgroundColor: "#fff",
        paddingHorizontal: 16
    },
    actionButtonContent: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center"
    },
    actionButtonTitle: {
    },
    actionButtonSubtitle: {
        color: "#049BB9"
    },
    actionButtonAngleRight: {
        position: "absolute",
        right: 0
    }
});

export default styles;
