import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import Text from "src/components/platformSpecific/Text/Text";

class ActionButton extends Component {
    static propTypes = {
        title: PropTypes.string,
        subtitle: PropTypes.string,
        onPress: PropTypes.func
    };

    static defaultProps = {
        title: "",
        subtitle: "",
        onPress: () => {
        }
    };

    render() {
        const {title, subtitle, onPress} = this.props;

        return (
            <Touchable style={styles.actionButton} onPress={onPress}>
                <View style={styles.actionButtonContent}>
                    <Text style={styles.actionButtonTitle}>{title}</Text>
                    <Text style={styles.actionButtonSubtitle}> ({subtitle})</Text>
                    <FontAwesome5 style={styles.actionButtonAngleRight} name="angle-right"/>
                </View>
            </Touchable>
        );
    }
}

export default ActionButton;
