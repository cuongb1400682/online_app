import React, {Component} from "react";
import {FlatList, View} from "react-native"
import PropTypes from "prop-types";

import styles from "./styles";
import Title from "src/pages/ProductDetail/components/Title/Title";
import {tr} from "src/localization/localization";
import Product from "src/components/base/Product/Product";
import {pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";

class RelatedProducts extends Component {
    static propTypes = {
        products: PropTypes.array,
        navigator: PropTypes.any
    };

    static defaultProps = {
        products: [],
        navigator: null
    };

    showProductDetailWithId = (productId) => () => {
        const {navigator} = this.props;

        pushTo(navigator, {
            screen: screenIds.PRODUCT_DETAIL,
            passProps: {
                productId
            }
        });
    };

    renderItem = ({item: product = {}}) => {
        return (
            <View style={styles.productContainer}>
                <Product
                    source={product}
                    onPress={this.showProductDetailWithId(product.id)}
                    imageSizeRatio={0.8}
                />
            </View>
        )
    };

    render() {
        const {products} = this.props;

        return (
            <View style={styles.relatedProducts}>
                <Title text={tr("product_detail_general_related_product_title")}/>
                <FlatList
                    renderItem={this.renderItem}
                    ListEmptyComponent={<View/>}
                    data={products}
                    keyExtractor={(_, index) => `${index}`}
                    horizontal
                />
            </View>
        );
    }
}

export default RelatedProducts;
