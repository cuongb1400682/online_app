import {StyleSheet} from "react-native";
import {screenWidth} from "src/utils/reactNative/dimensions";
import {PRODUCT_HEIGHT} from "src/utils/layouts/productLayout";

const styles = StyleSheet.create({
    relatedProducts: {
        width: "100%",
        backgroundColor: "#fff",
        marginTop: 8
    },
    productContainer: {
        width: screenWidth() * 0.45,
        height: PRODUCT_HEIGHT * 0.85,
        flexWrap: "wrap"
    }
});

export default styles;
