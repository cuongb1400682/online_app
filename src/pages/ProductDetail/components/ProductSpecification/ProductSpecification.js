import React, {Component} from "react";
import {View} from "react-native";

import styles from "./styles";
import {tr} from "src/localization/localization";
import Title from "src/pages/ProductDetail/components/Title/Title";
import Text from "src/components/platformSpecific/Text/Text";

const specifications = [
    {
        title: "Quy cách đóng gói",
        value: "Gói",
    },
    {
        title: "Hướng dẫn bảo quản",
        value: "Bảo quản nơi thoáng mát"
    },
/*    {
        title: "Xuất xứ",
        value: "Việt Nam"
    },*/
/*    {
        title: "Hướng dẫn sử dụng",
        value: "Dùng chế biến thành các món ăn"
    }*/
];

class ProductSpecification extends Component {
    renderSpecificationRow = (spec = {}, index) => {
        const {title = "", value = ""} = spec;

        return (
            <View key={index} style={styles.productSpecificationRow}>
                <View style={styles.productSpecificationCell}>
                    <Text style={styles.productSpecificationCellData}>{title}</Text>
                </View>
                <View style={styles.productSpecificationCell}>
                    <Text style={styles.productSpecificationCellData}>{value}</Text>
                </View>
            </View>
        );
    };

    render() {
        return (
            <View style={styles.productSpecification}>
                <Title text={tr("product_detail_general_product_specification_title")}/>
                <View style={styles.productSpecificationContent}>
                    {specifications.map(this.renderSpecificationRow)}
                </View>
            </View>
        );
    }
}

export default ProductSpecification;
