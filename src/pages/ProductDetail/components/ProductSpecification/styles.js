import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    productSpecification: {
        width: "100%",
        backgroundColor: "#fff",
        marginTop: 8
    },
    productSpecificationContent: {
        paddingHorizontal: 16,
        paddingVertical: 10,
        backgroundColor: "#fff"
    },
    productSpecificationRow: {
        backgroundColor: "#f7f7f7",
        minHeight: 35,
        flexDirection: "row"
    },
    productSpecificationCell: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 10,
        borderStyle: "solid",
        borderWidth: 0.5,
        borderColor: "#ddd",
    },
    productSpecificationCellData: {
        color: "#666",
        fontSize: 14,
        paddingVertical: 8
    },
});

export default styles;
