import React, {Component} from "react";
import {ScrollView, View, Platform} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import HTMLView from "react-native-htmlview";
import PropTypes from "prop-types";
import LinearGradient from "react-native-linear-gradient";

import styles from "./styles";
import Title from "src/pages/ProductDetail/components/Title/Title";
import {tr} from "src/localization/localization";
import colors from "src/constants/colors";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import commonStyles from "src/styles/commonStyles";
import Text from "src/components/platformSpecific/Text/Text";
import systemFont from "src/styles/systemFont";

const htmlViewStylesheet = {
    a: {
        color: colors.primaryColor
    },
    p: {
        ...systemFont.regular,
        fontSize: 14
    },
    strong: {
        fontWeight: "bold",
        fontSize: 14
    },
    bold: {
        fontWeight: "bold",
        fontSize: 14
    }
};

class ProductDescription extends Component {
    static propTypes = {
        content: PropTypes.string,
        onSeeAllPressed: PropTypes.func
    };

    static defaultProps = {
        content: "",
        onSeeAllPressed: () => {
        }
    };

    renderHTMLNode = (node) => {
        if (["img", "iframe"].includes(node.name)) {
            return null;
        }
    };

    render() {
        const {content, onSeeAllPressed} = this.props;

        return (
            <View style={styles.productDescription}>
                <Title text={tr("product_detail_general_product_description_title")}/>
                <ScrollView contentContainerStyle={styles.productDescriptionContent}>
                    <HTMLView
                        value={content}
                        style={styles.productDescriptionHTMLView}
                        stylesheet={htmlViewStylesheet}
                        renderNode={this.renderHTMLNode}
                        lineBreak=""
                        paragraphBreak=""
                    />
                    <LinearGradient
                        style={styles.gradientOverlay}
                        colors={["rgba(255, 255, 255, 0.3)", "#fff"]}
                        locations={[0, 0.59]}
                    />
                    <Touchable style={styles.seeAllButtonTouchable} onPress={onSeeAllPressed}>
                        <View style={commonStyles.flexColumnCenter}>
                            <Text style={styles.seeAllButtonText}>
                                {tr("product_detail_general_product_description_see_all")}
                            </Text>
                            <FontAwesome5 style={styles.seeAllButtonText} name="caret-down"/>
                        </View>
                    </Touchable>
                </ScrollView>
            </View>
        );
    }
}

export default ProductDescription;
