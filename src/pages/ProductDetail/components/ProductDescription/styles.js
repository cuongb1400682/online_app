import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    productDescription: {
        width: "100%",
        marginTop: 8,
        backgroundColor: "#fff",
    },
    productDescriptionContent: {
        flexDirection: "column",
        alignItems: "center"
    },
    productDescriptionHTMLView: {
        height: 110,
        width: "100%",
        paddingHorizontal: 16,
        paddingVertical: 10
    },
    gradientOverlay: {
        position: "absolute",
        top: 0,
        left: 0,
        height: "100%",
        width: "100%"
    },
    seeAllButtonText: {
        fontSize: 12,
        color: "#1a9cb7"
    },
    seeAllButtonTouchable: {
        position: "absolute",
        bottom: 10
    }
});

export default styles;

