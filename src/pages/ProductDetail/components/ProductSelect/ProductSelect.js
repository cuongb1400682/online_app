import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import FontAwesome from "react-native-vector-icons/FontAwesome";

import styles from "./styles";
import {tr} from "src/localization/localization";
import commonStyles from "src/styles/commonStyles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import WebLabel from "src/pages/ProductDetail/components/WebLabel/WebLabel";
import Text from "src/components/platformSpecific/Text/Text";

class ProductSelect extends Component {
    static propTypes = {
        product: PropTypes.object,
        numberOfItems: PropTypes.number,
        onMoreOptionPress: PropTypes.func
    };

    static defaultProps = {
        product: {},
        numberOfItems: 0,
        onMoreOptionPress: () => {
        }
    };

    isOutOfStock = () => this.props.product.qty <= 0;

    renderWebLabel = () => {
        const {web_label = ""} = this.props.product;

        return (
            <WebLabel
                label={web_label}
                selected
                outOfStock={this.isOutOfStock()}
            />
        );
    };

    renderMoreOptions = () => {
        const {numberOfItems, onMoreOptionPress} = this.props;

        if (numberOfItems <= 1) {
            return null;
        }

        return (
            <Touchable onPress={onMoreOptionPress}>
                <View style={commonStyles.flexRowCenter}>
                    <Text style={styles.moreOptionsText}>
                        {numberOfItems - 1} {tr("product_detail_general_more_options")}
                    </Text>
                    <FontAwesome style={styles.angleRight} name="chevron-right"/>
                </View>
            </Touchable>
        );
    };

    render() {
        return (
            <View style={styles.productSelect}>
                <Text>{tr("product_detail_general_product_weight")}</Text>
                {this.renderWebLabel()}
                <View style={commonStyles.matchParent}/>
                {this.renderMoreOptions()}
            </View>
        );
    }
}

export default ProductSelect;
