import {StyleSheet} from "react-native";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    productSelect: {
        width: "100%",
        minHeight: 53,
        flexDirection: "row",
        alignItems: "center",
        borderStyle: "solid",
        borderTopWidth: 1,
        borderColor: "#ddd",
        paddingHorizontal: 16,
        backgroundColor: "#fff"
    },
    moreOptionsText: {
        color: colors.primaryColor
    },
    angleRight: {
        fontSize: 12,
        fontWeight: "bold",
        marginLeft: 8
    }
});

export default styles;
