import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import StarRating from "react-native-star-rating";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {tr} from "src/localization/localization";
import {formatMoney, formatWithCurrency} from "src/utils/extensions/strings";
import {calculateSaving} from "src/utils/misc/businessLogics";
import PricePerKg from "src/components/base/PricePerKg/PricePerKg";
import Text from "src/components/platformSpecific/Text/Text";

const starColor = "#ffc120";

const UserInteractive = ({text = "", onPress}) => (
    <Touchable onPress={onPress}>
        <Text style={styles.userInteractiveText}>
            {text}
        </Text>
    </Touchable>
);

class GeneralInfo extends Component {
    static propTypes = {
        product: PropTypes.any
    };

    static defaultProps = {
        product: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            starCount: 0
        };
    }

    handleStarRatingPress = (rating) => this.setState({starCount: rating});

    renderStarRating = () => (
        <StarRating
            disabled={false}
            maxStars={5}
            rating={this.state.starCount}
            emptyStarColor={starColor}
            fullStarColor={starColor}
            halfStarColor={starColor}
            halfStarEnabled
            animation="tada"
            iconSet="FontAwesome"
            selectedStar={this.handleStarRatingPress}
            containerStyle={styles.starRating}
            starSize={16}
        />
    );

    renderUnitPrice = () => {
        const {market_price = 0, unit_price = 0, discount_percent = 0} = this.props.product;
        const savingPrice = formatMoney(discount_percent);
        const discountAmountFormatted = formatWithCurrency(market_price - unit_price);
        const isMarketPriceVisible = market_price && market_price > unit_price;

        if (!isMarketPriceVisible) {
            return null;
        }

        return (
            <Text style={styles.productUnitPrice}>
                {tr("product_detail_general_market_price")}
                {formatWithCurrency(market_price)}
                {tr("product_detail_general_saving_amount")}
                {discountAmountFormatted} ({savingPrice}%)
            </Text>
        );
    };

    renderProductPrices = () => {
        const {unit_price = 0, price_per_kg = 0} = this.props.product;

        return (
            <View>
                <View style={styles.productMarketPriceContainer}>
                    <Text style={styles.productMarketPrice}>
                        {formatWithCurrency(unit_price)}
                    </Text>
                    <PricePerKg
                        containerStyle={styles.productPricePerKg}
                        pricePerKg={price_per_kg}
                    />
                </View>
                {this.renderUnitPrice()}
            </View>
        );
    };

    renderUserInteractiveZone = () => {
        return (
            <View style={[styles.userInteractiveZone]}>
                {this.renderStarRating()}
                <UserInteractive text={`(${0}) ${tr("product_detail_general_comment")}`}/>
                <Text style={styles.verticalBar}>|</Text>
                <UserInteractive text={`(${0}) ${tr("product_detail_general_qa")}`}/>
            </View>
        );
    };

    render() {
        const {name = ""} = this.props.product;

        return (
            <View style={styles.generalInfo}>
                <Text style={styles.productName}>{name}</Text>
                {/*{this.renderUserInteractiveZone()}*/}
                {this.renderProductPrices()}
            </View>
        );
    }
}

export default GeneralInfo;
