import {StyleSheet} from "react-native";

import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    generalInfo: {
        width: "100%",
        minHeight: 120,
        paddingHorizontal: 16,
        paddingTop: 10,
        paddingBottom: 14,
        backgroundColor: "#fff"
    },
    productName: {
        fontSize: 18,
        color: colors.textColor
    },
    starRating: {
        width: 91,
        height: 16,
        marginRight: 16
    },
    verticalBar: {
        marginHorizontal: 4
    },
    userInteractiveZone: {
        marginTop: 12,
        flexDirection: "row",
        alignItems: "center"
    },
    userInteractiveText: {
        fontSize: 12,
        color: "#049bb9"
    },
    productMarketPrice: {
        color: colors.primaryColor,
        fontWeight: "bold",
        fontSize: 18,
    },
    productUnitPrice: {
        color: "#9b9b9b",
        fontSize: 12,
        marginTop: 6
    },
    productMarketPriceContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 12
    },
    productPricePerKg: {
        marginLeft: 12
    }
});

export default styles;
