import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    webLabelContainer: {
        borderRadius: 3,
        borderStyle: "solid",
        borderWidth: 1,
        height: 28,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    unselectedWebLabelContainer: {
        borderColor: "#999"
    },
    selectedWebLabelContainer: {
        borderColor: colors.primaryColor
    },
    webLabelText: {
        paddingHorizontal: 8
    },
    unselectedWebLabelText: {
        color: "#999"
    },
    selectedWebLabelText: {
        color: colors.primaryColor
    },
    slopeLine: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%"
    },
});

export default styles;
