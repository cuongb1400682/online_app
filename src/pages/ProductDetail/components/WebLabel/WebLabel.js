import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import SlopeLine from "src/components/base/svgIcons/SlopeLine/SlopeLine";
import colors from "src/constants/colors";
import Text from "src/components/platformSpecific/Text/Text";

class WebLabel extends Component {
    static propTypes = {
        outOfStock: PropTypes.bool,
        selected: PropTypes.bool,
        label: PropTypes.string,
        style: PropTypes.object,
        onPress: PropTypes.func,
    };

    static defaultProps = {
        outOfStock: false,
        selected: false,
        label: "",
        style: {},
        onPress: () => {
        }
    };

    render() {
        const {outOfStock, selected, label, style, onPress} = this.props;

        const webLabelContainerStyles = [
            styles.webLabelContainer,
            selected
                ? styles.selectedWebLabelContainer
                : styles.unselectedWebLabelContainer,
            style
        ];

        const webLabelTextStyles = [
            styles.webLabelText,
            selected
                ? styles.selectedWebLabelText
                : styles.unselectedWebLabelText
        ];

        const slopeLineColor = selected
            ? colors.primaryColor
            : "#999";

        return (
            <Touchable onPress={onPress}>
                <View style={webLabelContainerStyles}>
                    {outOfStock && <SlopeLine color={slopeLineColor}
                                              style={styles.slopeLine}/>}
                    <Text style={webLabelTextStyles}>{label}</Text>
                </View>
            </Touchable>
        );
    }
}

export default WebLabel;
