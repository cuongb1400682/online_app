import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    deliveryPolicy: {
        flexDirection: "column",
        textAlign: "justify",
        backgroundColor: "#fff"
    },
    highlightedText: {
        color: colors.primaryColor,
    },
    seeDetailText: {
        color: "#049bb9"
    },
    policyLine: {
        flexDirection: "row",
        width: "100%",
        borderStyle: "solid",
        borderTopWidth: 1,
        borderColor: "#ddd",
        paddingVertical: 10
    },
    policyLineText: {
        flex: 1,
        flexWrap: "wrap",
        textAlign: "justify",
        marginRight: 18
    },
    icon: {
        justifyContent: "center",
        alignItems: "center",
        marginLeft: 20,
        marginRight: 10
    }
});

export default styles;
