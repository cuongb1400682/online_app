import React, {Component} from "react";
import {Image, View} from "react-native";

import styles from "./styles";
import DrivingShipper from "src/components/base/svgIcons/DrivingShipper/DrivingShipper";
import giftIcon from "src/assets/static/icons/gift.png";
import Message from "src/components/platformSpecific/Message/Message";
import commonStyles from "src/styles/commonStyles";
import Text from "src/components/platformSpecific/Text/Text";

class DeliveryPolicy extends Component {
    handleSeeDetail = () => {
        Message.show("Chi tiết về chính sách giao hàng của Giigaa.com");
    };

    render() {
        return (
            <View style={styles.deliveryPolicy}>
                <View style={[styles.policyLine, commonStyles.hidden]}>
                    <Text style={styles.policyLineText}>
                        HCM: Bạn muốn nhận hàng trước
                        <Text style={styles.highlightedText}> 20h </Text>
                        hôm nay. Đặt hàng trong
                        <Text style={styles.highlightedText}> 1 giờ 51 phút </Text>
                        tới và chọn giao hàng dưới
                        <Text style={styles.highlightedText}> 120 phút </Text>
                        ở bước thanh toán.
                        <Text style={styles.seeDetailText}
                              onPress={this.handleSeeDetail}> Xem chi tiết</Text>
                    </Text>
                </View>
                <View style={[styles.policyLine, commonStyles.hidden]}>
                    <Image source={giftIcon} style={styles.icon}/>
                    {/*<Text style={styles.policyLineText}>*/}
                        {/*{"Với hoá đơn rau bất kì trên 50,000đ, bạn được tặng tuỳ" +*/}
                        {/*" thích 1 phần thịt bò, heo, gà sạch.\nTrị giá"}*/}
                        {/*<Text style={styles.highlightedText}> 100,000đ</Text>*/}
                    {/*</Text>*/}
                </View>
                <View style={styles.policyLine}>
                    <DrivingShipper style={styles.icon}/>
                    <Text style={styles.policyLineText}>
                        Miễn phí giao hàng cho hóa đơn trên 200K
                    </Text>
                </View>
            </View>
        );
    }
}

export default DeliveryPolicy;
