import React, {Component} from "react";
import {Image, Platform} from "react-native";
import PropTypes from "prop-types";
import * as Animatable from "react-native-animatable";

import styles, {CART_ANIMATOR_SIZE} from "./styles";
import {makeCDNImageURL} from "src/utils/cdn/images";
import {imageTypes} from "src/constants/enums";
import {screenHeight, screenWidth} from "src/utils/reactNative/dimensions";
import {getStatusBarHeight} from "src/utils/reactNative/statusBar";

const flyingAnimationIOS = {
    0: {
        bottom: 0,
        left: (screenWidth() - CART_ANIMATOR_SIZE) / 2,
        opacity: 1
    },
    0.9: {
        bottom: screenHeight() - 2 * CART_ANIMATOR_SIZE - getStatusBarHeight(),
        left: screenWidth() - 2 * CART_ANIMATOR_SIZE,
        opacity: 0.7
    },
    1: {
        bottom: screenHeight() - 1.5 * CART_ANIMATOR_SIZE - getStatusBarHeight(),
        left: screenWidth() - 1.5 * CART_ANIMATOR_SIZE,
        opacity: 0
    }
};

const flyingAnimationAndroid = {
    0: {
        bottom: 0,
        left: (screenWidth() - CART_ANIMATOR_SIZE) / 2,
        opacity: 1
    },
    0.8: {
        bottom: screenHeight() - getStatusBarHeight(),
        left: screenWidth() - CART_ANIMATOR_SIZE,
        opacity: 0.3
    },
    1: {
        bottom: screenHeight() - getStatusBarHeight(),
        left: screenWidth() - CART_ANIMATOR_SIZE,
        opacity: 0
    }
};

class CartAnimator extends Component {
    static propTypes = {
        productThumbnail: PropTypes.string,
        onAnimationEnd: PropTypes.func
    };

    static defaultProps = {
        productThumbnail: "",
        onAnimationEnd: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            visible: false
        };
    }

    startAnimation = () => {
        this.setState({
            visible: true
        });
    };

    cleanUpAfterAnimation = () => {
        const {onAnimationEnd} = this.props;

        this.setState({
            visible: false
        }, onAnimationEnd);
    };

    render() {
        const {productThumbnail} = this.props;
        const {visible} = this.state;
        const animation = Platform.select({
            ios: flyingAnimationIOS,
            android: flyingAnimationAndroid
        });

        if (!visible) {
            return null;
        }

        return (
            <Animatable.View
                style={styles.cartAnimator}
                animation={animation}
                onAnimationEnd={this.cleanUpAfterAnimation}
                easing="ease-out"
                duration={1100}
            >
                <Image
                    style={styles.productThumbnail}
                    source={{uri: makeCDNImageURL(productThumbnail, imageTypes.BEST_SALES)}}
                />
            </Animatable.View>
        );
    }
}

export default CartAnimator;
