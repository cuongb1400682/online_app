import {StyleSheet} from "react-native";

import commonStyles from "src/styles/commonStyles";
import colors from "src/constants/colors";

const CART_ANIMATOR_SIZE = 25;

const styles = StyleSheet.create({
    productThumbnail: {
        flex: 1
    },
    cartAnimator: {
        width: CART_ANIMATOR_SIZE,
        height: CART_ANIMATOR_SIZE,
        zIndex: 1000,
        position: "absolute",
        borderRadius: 50,
        borderColor: colors.primaryColor,
        borderWidth: 0.5,
        overflow: "hidden",
        ...commonStyles.hasShadow
    },
});

export default styles;
export {
    CART_ANIMATOR_SIZE
};
