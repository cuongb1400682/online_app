import React from "react";
import {View} from "react-native";

import styles from "./styles";
import {isEmptyArray} from "src/utils/extensions/arrays";
import {tr} from "src/localization/localization";
import FastImage from "src/components/base/FastImage/FastImage";
import {makeCDNImageURL} from "src/utils/cdn/images";
import {redirectLinkTypes, imageTypes} from "src/constants/enums";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import openRedirectLink from "src/utils/reactNative/openRedirectLink";
import Text from "src/components/platformSpecific/Text/Text";

const showProductDetail = ({navigator, productUrl = ""}) => () => openRedirectLink({
    link_type: redirectLinkTypes.PRODUCT,
    link: productUrl,
    navigator: navigator
});

const getOutOfStockText = ({qty, out_of_stock}) => {
    if (out_of_stock) {
        return tr("product_children_list_out_of_stock");
    }

    return qty
        ? tr("product_children_list_in_stock_qty", qty)
        : tr("product_children_list_out_of_stock");
};

const ProductChildrenList = ({products = [], showOutOfStock = false, navigator}) => {
    if (!products || isEmptyArray(products)) {
        return null;
    }

    return (
        <View style={styles.productsInCombo}>
            <Text style={styles.productsInComboTitle}>
                {tr("product_children_list_title").toUpperCase()}
            </Text>
            {products.map(product => {
                const imageUrl = makeCDNImageURL(product.original_thumbnail, imageTypes.IMAGE_LIST);
                const isOutOfStock = product.out_of_stock;

                return (
                    <Touchable key={product.id} onPress={showProductDetail({navigator, productUrl: product.url})}>
                        <View style={styles.comboItem}>
                            <View style={styles.comboItemImageContainer}>
                                <FastImage
                                    style={styles.comboItemImage}
                                    source={{uri: imageUrl}}
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                            </View>
                            <View style={styles.comboItemNameContainer}>
                                <Text>{product.name}</Text>
                                {showOutOfStock && isOutOfStock &&
                                <Text style={styles.outOfStock}>{getOutOfStockText(product)}</Text>}
                            </View>
                            <View style={styles.comboItemQtyContainer}>
                                <Text style={styles.comboItemQty}>x{product.item_qty}</Text>
                            </View>
                        </View>
                    </Touchable>
                );
            })}
        </View>
    );
};

export default ProductChildrenList;
