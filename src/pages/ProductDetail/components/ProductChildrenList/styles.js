import {StyleSheet} from "react-native";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    productsInCombo: {
        flex: 1,
        width: "100%",
        flexDirection: "column",
        paddingVertical: 16,
        paddingHorizontal: 16,
        backgroundColor: "#fff",
        borderStyle: "solid",
        borderTopWidth: 1,
        borderColor: "#ddd",
    },
    productsInComboTitle: {
        fontSize: 15,
        fontWeight: "bold",
        marginBottom: 8,
        color: "#222"
    },
    comboItem: {
        width: "100%",
        height: 50,
        marginTop: 8,
        flexDirection: "row",
        alignItems: "center",
    },
    comboItemImageContainer: {
        borderStyle: "solid",
        borderWidth: 0.5,
        marginRight: 10,
        borderColor: "#ddd"
    },
    comboItemImage: {
        width: 50,
        height: 50,
    },
    comboItemNameContainer: {
        flex: 1,
        flexWrap: "wrap",
        flexDirection: "column",
    },
    comboItemQtyContainer: {
        width: 50
    },
    comboItemQty: {
        fontWeight: "bold",
        textAlign: "right"
    },
    outOfStock: {
        color: "red",
        fontSize: 9
    }
});

export default styles;
