import React, {Component} from "react";
import PropTypes from "prop-types";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import LinearGradient from "react-native-linear-gradient";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import commonStyles from "src/styles/commonStyles";

class CircleButton extends Component {
    static propTypes = {
        onPress: PropTypes.func,
        iconName: PropTypes.string
    };

    static defaultProps = {
        iconName: "",
        onPress: () => {
        }
    };

    render() {
        const {iconName, onPress} = this.props;

        return (
            <Touchable onPress={onPress}>
                <LinearGradient
                    style={[styles.linearGradient, commonStyles.centerChildren]}
                    colors={["#fff", "#eee"]}
                    locations={[0, 1]}
                >
                    <FontAwesome5 name={iconName} style={styles.icon}/>
                </LinearGradient>
            </Touchable>
        );
    }
}

export default CircleButton;
