import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    touchable: {
    },
    linearGradient: {
        width: 30,
        height: 30,
        borderRadius: 15,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#eee",
        marginRight: 8
    },
    icon: {
        fontSize: 12,
        color: "#757575"
    }
});

export default styles;
