import React, {Component} from "react";
import {View} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import FontAwesome from "react-native-vector-icons/FontAwesome";

import styles from "./styles";
import commonStyles from "src/styles/commonStyles";
import {tr} from "src/localization/localization";
import CountDownText from "src/components/base/CountDownText/CountDownText";
import {addLeadingZeros} from "src/utils/extensions/strings";
import PropTypes from "prop-types";
import Text from "src/components/platformSpecific/Text/Text";

class DealBar extends Component {
    static propTypes = {
        product: PropTypes.object
    };

    static defaultProps = {
        product: {}
    };

    countDownFormatter = ({years, days, hours, min, sec}) => {
        return (
            <Text style={styles.dealBarText}>
                <Text style={styles.lightText}>{tr("product_detail_deal_bar_end_after")}</Text>
                {` ${days} ${tr("deal_part_day_unit")} ` +
                `${addLeadingZeros(hours)}:${addLeadingZeros(min)}:${addLeadingZeros(sec)}`}
            </Text>
        );
    };

    render() {
        const {deal} = this.props.product;

        if (!deal) {
            return null;
        }

        return (
            <View style={[styles.dealBar, commonStyles.flexRowCenter]}>
                <Text style={styles.dealBarText}>
                    <FontAwesome5 style={styles.dealBarText} name="fire"/>
                    {tr("product_detail_deal_bar_title")}
                </Text>
                <View style={commonStyles.matchParent}/>
                <FontAwesome style={styles.clockIcon} name="clock-o"/>
                <CountDownText
                    style={styles.dealBarText}
                    endDate={deal.end_time}
                    formatter={this.countDownFormatter}
                />
            </View>
        );
    }
}

export default DealBar;
