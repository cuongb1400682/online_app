import {StyleSheet} from "react-native";

import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    dealBar: {
        width: "100%",
        height: 35,
        backgroundColor: colors.primaryColor,
        paddingHorizontal: 16,
    },
    dealBarText: {
        color: "#fff",
        fontSize: 14,
    },
    lightText: {
        fontWeight: "normal"
    },
    clockIcon: {
        color: "#fff",
        fontSize: 18,
        fontWeight: "bold",
        marginRight: 8
    },
});

export default styles;
