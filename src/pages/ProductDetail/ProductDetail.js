import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {TabView} from "react-native-tab-view";
import {View} from "react-native";

import {clearProductDetail, fetchProductDetail, onChangeTab} from "src/pages/ProductDetail/productDetailActions";
import commonStyles from "src/styles/commonStyles";
import {tr} from "src/localization/localization";
import GeneralTab from "src/pages/ProductDetail/containers/GeneralTab/GeneralTab";
import DetailTab from "src/pages/ProductDetail/containers/DetailTab/DetailTab";
import CommentsTab from "src/pages/ProductDetail/containers/CommentsTab/CommentsTab";
import QATab from "src/pages/ProductDetail/containers/QATab/QATab";
import NavBar from "src/containers/NavBar/NavBar";
import AddToCart from "src/pages/ProductDetail/containers/AddToCart/AddToCart";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import UnderDevelopment from "src/pages/UnderDevelopment/UnderDevelopment";
import TabBar from "src/components/base/TabBar/TabBar";
import {setOnNavigatorEvent} from "src/utils/misc/navigator";
import {navigatorEvents} from "src/constants/enums";
import LazyTabView from "src/components/base/LazyTabView/LazyTabView";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";

const routeKeys = {
    GENERAL: "general",
    DETAIL: "detail",
    COMMENTS: "comments",
    QA: "qa",
};

const routes = [
    {
        key: routeKeys.GENERAL,
        title: tr("product_detail_tab_general")
    },
    {
        key: routeKeys.DETAIL,
        title: tr("product_detail_tab_detail")
    },
    // {
    //     key: routeKeys.COMMENTS,
    //     title: tr("product_detail_tab_comments")
    // },
    // {
    //     key: routeKeys.QA,
    //     title: tr("product_detail_tab_qa")
    // }
];

class ProductDetail extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        productId: PropTypes.number,
        childProductId: PropTypes.number,
        tabIndex: PropTypes.number,
        numberOfCartItems: PropTypes.number,
        selectProduct: PropTypes.func,
        fetchProductDetail: PropTypes.func,
        onChangeTab: PropTypes.func,
        clearProductDetail: PropTypes.func,
    };

    static defaultProps = {
        productId: -1,
        childProductId: -1,
        tabIndex: 0,
        numberOfCartItems: 0,
        selectProduct: () => {
        },
        fetchProductDetail: () => {
        },
        onChangeTab: () => {
        },
        clearProductDetail: () => {
        },
    };

    componentDidMount() {
        const {navigator} = this.props;

        setOnNavigatorEvent(navigator, this.onNavigatorEvent);
    }

    componentWillUnmount() {
        const {clearProductDetail} = this.props;

        clearProductDetail();
    }

    onNavigatorEvent = (event) => {
        if (event.id === navigatorEvents.DID_APPEAR) {
            const {fetchProductDetail, productId, childProductId} = this.props;

            fetchProductDetail(childProductId !== -1 ? childProductId : productId);
        }
    };

    renderNavBarShortcuts = () => (
        <NavBarShortcuts
            showsCart
            showsSearch
            showsVerticalEllipsis
            navigator={this.props.navigator}
        />
    );

    renderScene = ({route = {}}) => {
        const {navigator} = this.props;

        return ({
            [routeKeys.GENERAL]: <GeneralTab navigator={navigator}/>,
            [routeKeys.DETAIL]: <DetailTab/>,
            [routeKeys.COMMENTS]: <UnderDevelopment/>,//<CommentsTab/>,
            [routeKeys.QA]: <UnderDevelopment/> //<QATab/>,
        })[route.key] || null;
    };

    render() {
        const {navigator, tabIndex, onChangeTab} = this.props;

        return (
            <View style={commonStyles.matchParent}>
                <NavBar
                    title={tr("product_detail_nav_bar_title")}
                    right={this.renderNavBarShortcuts()}
                    navigator={navigator}
                />
                <LazyTabView
                    renderScene={this.renderScene}
                    routes={routes}
                    currentRouteIndex={tabIndex}
                    onChangeTab={onChangeTab}
                />
                <AddToCart/>
                <FacebookMessenger floating/>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    tabIndex: state.product_detail.display.tab_index,
    numberOfCartItems: state.common.cart.number_items
});

export default connect(mapStateToProps, {
    clearProductDetail,
    fetchProductDetail,
    onChangeTab
})(ProductDetail);
