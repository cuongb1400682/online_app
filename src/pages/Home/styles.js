import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    justForYouTitle: {
        paddingHorizontal: 16,
        backgroundColor: "#eee"
    },
    pageUpperPart: {
        flex: 1,
        backgroundColor: "#fff"
    },
    shadowyBannerAndShortcutBar: {
        backgroundColor: "#fff",
        shadowColor: "#000",
        shadowOpacity: 0.3,
        shadowOffset: {
            width: -2,
            height: 2
        },
        elevation: 1
    },
    floatingButtons: {
        marginRight: 4,
        flexDirection: "column",
        position: "absolute",
        bottom: 0,
        right: 0,
        ...commonStyles.centerChildren
    }
});

export default styles;
