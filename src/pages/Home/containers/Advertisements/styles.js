import {StyleSheet} from "react-native";
import {advertisementHeight, advertisementWidth} from "src/utils/layouts/homeLayout";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    ad: {
        width: advertisementWidth(),
        height: advertisementHeight(),
        paddingHorizontal: 16
    },
    adContainer: {
        flex: 1,
        marginTop: 20,
        marginBottom: 10,
        ...commonStyles.centerChildren
    }
});

export default styles;
