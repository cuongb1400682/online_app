import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import styles from "./styles";
import {makeCDNImageURL} from "src/utils/cdn/images";
import FastImage from "src/components/base/FastImage/FastImage";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {imageTypes} from "src/constants/enums";
import {fetchMainBanners} from "src/pages/Home/homeActions";
import {isEmptyArray} from "src/utils/extensions/arrays";

class Advertisements extends Component {
    static MARGIN_TOP = styles.adContainer.marginTop;
    static MARGIN_BOTTOM = styles.adContainer.marginBottom;

    static propTypes = {
        banners: PropTypes.array,
        fetchMainBanners: PropTypes.func,
        onAdvertisementPress: PropTypes.func
    };

    static defaultProps = {
        banners: [],
        fetchMainBanners: () => {
        },
        onAdvertisementPress: () => {
        }
    };

    componentDidMount() {
        this.props.fetchMainBanners();
    }

    handleOnAdvertisementPress = (link, link_type) => () => {
        const {onAdvertisementPress} = this.props;

        onAdvertisementPress({link, link_type})
    };

    render() {
        const {banners} = this.props;

        if (isEmptyArray(banners)) {
            return (
                <View style={styles.adContainer}/>
            );
        }

        const {image_url = "", redirect_url = "", redirect_url_type = ""} = banners[0];

        return (
            <Touchable
                style={styles.adContainer}
                onPress={this.handleOnAdvertisementPress(redirect_url ,redirect_url_type)}
            >
                <FastImage
                    resizeMode="cover"
                    style={styles.ad}
                    source={{uri: makeCDNImageURL(image_url, imageTypes.FULL_SIZE)}}
                />
            </Touchable>
        );
    };
}

const mapStateToProps = (state) => ({
    banners: state.home.main_banners.urls,
});

export default connect(mapStateToProps, {
    fetchMainBanners
})(Advertisements);
