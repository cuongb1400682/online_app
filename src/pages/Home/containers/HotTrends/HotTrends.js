import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import styles from "./styles"
import {fetchHotTrends} from "src/pages/Home/homeActions";
import FastImage from "src/components/base/FastImage/FastImage";
import commonStyles from "src/styles/commonStyles";
import {makeCDNImageURL} from "src/utils/cdn/images";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {tr} from "src/localization/localization";
import Title from "src/pages/Home/components/Title/Title";
import openRedirectLink from "src/utils/reactNative/openRedirectLink";
import Text from "src/components/platformSpecific/Text/Text";

class HotTrends extends Component {
    static ROW_MARGIN_TOP = styles.trendRow.marginTop;

    static propTypes = {
        styles: PropTypes.any,
        navigator: PropTypes.any,
        trends: PropTypes.array,
        categories: PropTypes.array,
        fetchHotTrends: PropTypes.func
    };

    static defaultProps = {
        styles: {},
        trends: [],
        categories: [],
        fetchHotTrends: () => {
        },
    };

    componentDidMount() {
        this.props.fetchHotTrends();
    }

    handleTrendPress = (link, link_type) => () => {
        const {navigator, categories} = this.props;

        openRedirectLink({categories, navigator, link, link_type});
    };

    renderTrend = (trend = {}) => {
        const {icon = "", name = "", link = "", link_type = ""} = trend;

        return (
            <Touchable
                style={styles.trendTouchable}
                onPress={this.handleTrendPress(link, link_type)}
            >
                <View style={styles.trend}>
                    <FastImage
                        source={{uri: makeCDNImageURL(icon)}}
                        style={styles.trendImage}
                    >
                        <Text style={styles.trendName}>{name.replace("\n", "")}</Text>
                    </FastImage>
                </View>
            </Touchable>
        );
    };

    render() {
        const {trends, style} = this.props;

        return (
            <View style={[styles.hotTrend, style]}>
                <Title text={tr("main_tab_trend_title")}/>
                <View style={[styles.trendsGrid, commonStyles.centerChildren]}>
                    <View style={[styles.trendRow, styles.firstRow]}>
                        {this.renderTrend(trends[0])}
                        <View style={styles.trendGridVerticalPlaceholder}/>
                        {this.renderTrend(trends[1])}
                    </View>
                    <View style={styles.trendRow}>
                        {this.renderTrend(trends[2])}
                        <View style={styles.trendGridVerticalPlaceholder}/>
                        {this.renderTrend(trends[3])}
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    trends: state.home.hot_trends.trends,
    categories: state.common.categories.list
});

export default connect(mapStateToProps, {
    fetchHotTrends
})(HotTrends);

