import {StyleSheet} from "react-native";

import {hotTrendHeight, hotTrendWidth} from "src/utils/layouts/homeLayout";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    firstRow: {
        marginTop: 0
    },
    hotTrend: {
        marginHorizontal: 16
    },
    trendsGrid: {
        flexDirection: "column",
    },
    trendRow: {
        height: hotTrendHeight(),
        width: "100%",
        flexDirection: "row",
        marginTop: 6
    },
    trendGridVerticalPlaceholder: {
        width: 6
    },
    trend: {
        flex: 1,
    },
    trendTouchable: {
        width: hotTrendWidth(),
        height: hotTrendHeight(),
        flex: 1,
    },
    trendImage: {
        flex: 1,
        overflow: "hidden",
        borderRadius: 4,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "flex-end",
        paddingBottom: 10
    },
    trendName: {
        color: "#fff",
        fontSize: 15,
        fontWeight: "bold",
        shadowColor: "#000",
        shadowOpacity: 0.5,
        shadowOffset: {
            width: -1,
            height: 1
        },
        elevation: 1
    },
    trendTitle: {
        color: "#333",
        fontSize: 13,
        fontWeight: "bold",
        marginBottom: 6
    }
});

export default styles;
