import {StyleSheet} from "react-native";
import {screenWidth} from "src/utils/reactNative/dimensions";
import {estimateBannerHeight} from "src/utils/layouts/homeLayout";

const DOT_SIZE = 6;

const styles = StyleSheet.create({
    productImage: {
        flex: 1
    },
    swiper: {
    },
    bannerContainer: {
        width: "100%",
        height: estimateBannerHeight(),
        flexDirection: "row",
        justifyContent: "center"
    },
    imagesCarouselPagination: {
        position: "absolute",
        bottom: -18
    },
    inactiveDotStyle: {
        width: DOT_SIZE,
        height: DOT_SIZE,
        backgroundColor: "rgba(255, 255, 255, 0.4)"
    },
    dotStyle: {
        width: DOT_SIZE,
        height: DOT_SIZE,
        backgroundColor: "#fff"
    }
});

export default styles;
