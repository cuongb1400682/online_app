import React, {Component} from "react";
import {ScrollView, View} from "react-native";
import PropTypes from "prop-types";
import {Pagination} from "react-native-snap-carousel";

import styles from "./styles";
import {makeCDNImageURL} from "src/utils/cdn/images";
import FastImage from "src/components/base/FastImage/FastImage";
import {imageTypes} from "src/constants/enums";
import {screenWidth} from "src/utils/reactNative/dimensions";
import {estimateBannerHeight} from "src/utils/layouts/homeLayout";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import commonStyles from "src/styles/commonStyles";

const carouselDimensions = {
    width: screenWidth(),
    height: estimateBannerHeight()
};

class Banner extends Component {
    static propTypes = {
        currentPageIndex: PropTypes.number,
        autoplay: PropTypes.bool,
        mainBannerData: PropTypes.array,
        onBannerChanged: PropTypes.func,
        onBannerPress: PropTypes.func,
    };

    static defaultProps = {
        currentPageIndex: 0,
        autoplay: true,
        mainBannerData: [],
        onBannerChanged: () => {
        },
        onBannerPress: () => {
        }
    };

    constructor(props) {
        super(props);

        this.pageIndex = props.currentPageIndex;
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.currentPageIndex !== this.pageIndex) {
            this._snapToItem(nextProps.currentPageIndex);
        }
    }

    handleOnBannerPress = (link, link_type) => () => {
        const {onBannerPress} = this.props;

        onBannerPress({link, link_type});
    };

    handleBannerChanged = (index) => {
        const {mainBannerData, onBannerChanged} = this.props;

        onBannerChanged(mainBannerData[index], index);
    };

    renderImage = (item = {}, index) => {
        const {id, image_url = "", redirect_url = "", redirect_url_type = ""} = item;

        return (
            <Touchable
                key={index}
                style={carouselDimensions}
                onPress={this.handleOnBannerPress(redirect_url, redirect_url_type)}
            >
                <FastImage
                    key={id}
                    source={{uri: makeCDNImageURL(image_url, imageTypes.FULL_SIZE)}}
                    style={styles.productImage}
                    resizeMode={FastImage.resizeMode.cover}
                />
            </Touchable>
        );
    };

    scrollTo = (offsetX) => {
        if (this.scrollViewRef) {
            if (typeof this.scrollViewRef.scrollTo === "function") {
                this.scrollViewRef.scrollTo({y: 0, x: offsetX, animated: true});
            }
        }
    };

    _snapToItem = (index) => {
        this.scrollTo(index * carouselDimensions.width);
        this.handleBannerChanged(index);
    };

    onScroll = (event) => {
        const {x = 0} = event.nativeEvent.contentOffset;
        const nextIndex = Math.ceil(x / carouselDimensions.width);

        if (nextIndex !== this.pageIndex) {
            this.pageIndex = nextIndex;
            this.handleBannerChanged(nextIndex);
        }
    };

    render() {
        const {mainBannerData, currentPageIndex} = this.props;

        return (
            <View style={styles.bannerContainer}>
                <ScrollView
                    ref={ref => this.scrollViewRef = ref}
                    horizontal
                    style={commonStyles.matchParent}
                    showsHorizontalScrollIndicator={false}
                    pagingEnabled
                    onScroll={this.onScroll}
                >
                    {mainBannerData.map(this.renderImage)}
                </ScrollView>
                <Pagination
                    activeDotIndex={currentPageIndex}
                    dotsLength={mainBannerData.length}
                    containerStyle={styles.imagesCarouselPagination}
                    dotStyle={styles.dotStyle}
                    inactiveDotStyle={styles.inactiveDotStyle}
                />
            </View>
        );
    }
}

export default Banner;
