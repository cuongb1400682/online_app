import {StyleSheet, Platform} from "react-native";

import {screenWidth} from "src/utils/reactNative/dimensions";

const HOT_DEAL_HEIGHT = Platform.select({ios: 350, android: 320});

const styles = StyleSheet.create({
    hotDeal: {
        width: "100%",
        height: HOT_DEAL_HEIGHT,
        marginTop: 8
    },
    flatList: {
        backgroundColor: "#eee"
    },
    separator: {
        width: 5
    },
    productContainer: {
        width: screenWidth() * 0.45,
        height: "100%"
    },
    seeMore: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#eee",
        height: 30,
    },
    chevronRight: {
        fontSize: 12,
        color: "#1A9CB7",
        marginLeft: 4
    },
    linkButton: {
        fontSize: 12,
        color: "#1A9CB7"
    }
});

export default styles;
