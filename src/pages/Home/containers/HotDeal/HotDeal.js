import React, {Component} from "react";
import {FlatList, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import {fetchDeals} from "src/pages/Home/homeActions";
import {isEmptyArray} from "src/utils/extensions/arrays";
import {dealTimeline} from "src/constants/enums";
import HotDealTitle from "src/pages/Home/components/HotDealTitle/HotDealTitle";
import Product from "src/components/base/Product/Product";
import defaultOnPressHandler from "src/utils/reactNative/defaultOnPressHandler";
import screenIds from "src/constants/screenIds";
import LinkButton from "src/components/base/LinkButton/LinkButton";
import {tr} from "src/localization/localization";
import FontIcon from "src/components/base/FontIcon/FontIcon";

class HotDeal extends Component {
    static HEIGHT = styles.hotDeal.height;

    static propTypes = {
        products: PropTypes.array,
        currentDeal: PropTypes.object,
        fetchDeals: PropTypes.func
    };

    static defaultProps = {
        products: [],
        currentDeal: {},
        fetchDeals: () => {
        }
    };

    componentDidMount() {
        const {fetchDeals} = this.props;

        fetchDeals({
            timeline: dealTimeline.CURRENT,
            pageSize: 20,
            pageIndex: 0
        });
    }

    renderItem = ({item = {}}) => (
        <View style={styles.productContainer}>
            <Product
                source={item}
                dealTimeline={dealTimeline.CURRENT}
                imageSizeRatio={0.8}
                onPress={
                    defaultOnPressHandler({
                        navigator: this.props.navigator,
                        screenId: screenIds.PRODUCT_DETAIL,
                        passProps: {
                            productId: item.id
                        }
                    })
                }
            />
        </View>
    );

    render() {
        const {products, currentDeal, navigator} = this.props;

        if (isEmptyArray(products)) {
            return null;
        }

        return (
            <View style={styles.hotDeal}>
                <HotDealTitle currentDeal={currentDeal}/>
                <FlatList
                    style={styles.flatList}
                    renderItem={this.renderItem}
                    ItemSeparatorComponent={() => <View style={styles.separator}/>}
                    ListEmptyComponent={<View/>}
                    data={products}
                    keyExtractor={(_, index) => `${index}`}
                    horizontal
                />
                <View style={styles.seeMore}>
                    <LinkButton
                        title={tr("main_tab_hot_deal_see_more_button")}
                        textStyle={styles.linkButton}
                        navigator={navigator}
                        screenId={screenIds.DEAL_LIST}
                    />
                    <FontIcon fontName="FontAwesome5" name="chevron-right" style={styles.chevronRight}/>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    products: state.home.deals.products,
    currentDeal: state.home.deals.current_deal,
});

export default connect(mapStateToProps, {
    fetchDeals
})(HotDeal);
