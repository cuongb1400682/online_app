import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {Keyboard} from "react-native";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import {tr} from "src/localization/localization";
import NavBarInput from "src/components/base/NavBarInput/NavBarInput";
import HomeMagnifier from "src/components/base/svgIcons/Magnifier/Magnifier";
import colors from "src/constants/colors";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import screenIds from "src/constants/screenIds";
import {pushTo} from "src/utils/misc/navigator";

class HomeNavBar extends Component {
    static propTypes = {
        backgroundColor: PropTypes.string,
        navigator: PropTypes.any,
    };

    static defaultProps = {
        backgroundColor: colors.primaryColor,
        navigator: null,
    };

    showSearchResult = (e) => {
        const {navigator} = this.props;
        const searchQuery = e.nativeEvent.text;

        Keyboard.dismiss();

        pushTo(navigator, {
            screen: screenIds.SEARCH_RESULT,
            passProps: {
                searchParams: {
                    q: searchQuery
                }
            }
        });
    };

    render() {
        const {backgroundColor} = this.props;

        return (
            <NavBar
                style={styles.navBar}
                backgroundColor={backgroundColor}
            >
                {/*<NavBarButton style={[commonStyles.flexColumnCenter, styles.navBarScanButton]}>*/}
                {/*<HomeScanCamera/>*/}
                {/*<Text style={styles.navBarScanButtonTitle}>{tr("main_tab_header_scan_button_title")}</Text>*/}
                {/*</NavBarButton>*/}
                <NavBarInput
                    leftViews={<HomeMagnifier style={styles.magnifier}/>}
                    placeholder={tr("main_tab_header_search_box_placeholder")}
                    onSubmitEditing={this.showSearchResult}
                    selectTextOnFocus
                    showsClearButton
                />
                <NavBarShortcuts
                    navigator={this.props.navigator}
                    showsCart
                />
            </NavBar>
        );
    }
}

const mapStateToProps = () => ({});

export default connect(mapStateToProps, {})(HomeNavBar);

