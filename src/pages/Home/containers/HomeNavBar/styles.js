import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    navBar: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        paddingLeft: 16
    },
    navBarScanButton: {
        marginLeft: 4
    },
    navBarScanButtonTitle: {
        color: "#fff",
        fontSize: 9,
        marginTop: 4
    },
    NavBarShortcuts: {
        marginRight: 4
    },
    circleNumberContainer: {
        width: 14,
        height: 14,
        backgroundColor: "#fff",
        borderRadius: 7,
        position: "absolute",
        top: 8,
        left: 20
    },
    circleNumber: {
        color: colors.primaryColor,
        fontSize: 10
    },
    magnifier: {
        marginLeft: 8
    }
});

export default styles;
