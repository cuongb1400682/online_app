import {StyleSheet} from "react-native";

import {screenWidth} from "src/utils/reactNative/dimensions";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    listCategoryItem: {
        maxWidth: 110,
        minWidth: 85,
        width: screenWidth() / 6,
        height: 120,
        marginHorizontal: 20,
        overflow: "visible",
        flexDirection: "column",
        ...commonStyles.centerChildren
    },
    listCategoryImage: {
        width: 70,
        height: 70,
        marginBottom: 8
    },
    listCategoryName: {
        fontSize: 13,
        textAlign: "center",
    },
    listCategoryScrollView: {
        height: 270
    },
    title: {
        paddingHorizontal: 16
    }
});

export default styles;
