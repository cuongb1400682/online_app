import React, {Component} from "react";
import {ScrollView, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import FastImage from "src/components/base/FastImage/FastImage";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {makeCDNImageURL} from "src/utils/cdn/images";
import Title from "src/pages/Home/components/Title/Title";
import commonStyles from "src/styles/commonStyles";
import {fetchCategories} from "src/redux/common/commonActions";
import {pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";
import Text from "src/components/platformSpecific/Text/Text";

class CategoriesMenu extends Component {
    static LIST_HEIGHT = styles.listCategoryScrollView.height;

    static propTypes = {
        style: PropTypes.any,
        title: PropTypes.string,
        categories: PropTypes.array,
        hideTitle: PropTypes.bool,
        fetchCategories: PropTypes.func,
        onCategoryPress: PropTypes.func,
    };

    static defaultProps = {
        style: {},
        title: tr("main_tab_category_menu_title"),
        categories: [],
        hideTitle: false,
        fetchCategories: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedCategoryIndex: -1
        };
    }

    componentDidMount() {
        const {fetchCategories} = this.props;

        fetchCategories();
    }

    showCatalogByCategory = (category = {}, index) => () => {
        const {navigator} = this.props;

        this.setState({selectedCategoryIndex: index});

        pushTo(navigator, {
            screen: screenIds.CATALOG_BY_CATEGORY,
            passProps: {
                selectedCategory: category
            }
        });
    };

    renderListCategoryItem = (category, index) => {
        if (!category) {
            return null;
        }

        const {mobile_icon = "", name = ""} = category;

        return (
            <Touchable
                key={index}
                onPress={this.showCatalogByCategory(category, index)}
            >
                <View style={styles.listCategoryItem}>
                    <FastImage
                        source={{uri: makeCDNImageURL(mobile_icon)}}
                        style={styles.listCategoryImage}
                    />
                    <Text style={styles.listCategoryName}>{name}</Text>
                </View>
            </Touchable>
        );
    };

    renderTitle = () => (
        !this.props.hideTitle && <Title
            text={this.props.title}
            containerStyle={styles.title}
        />
    );

    renderListCategoryColumns = () => {
        const {categories} = this.props;
        let result = [];

        for (let i = 0; i < categories.length; i += 2) {
            result.push(
                <View key={i} style={commonStyles.flexColumn}>
                    {this.renderListCategoryItem(categories[i], i)}
                    {this.renderListCategoryItem(categories[i + 1], i + 1)}
                </View>
            );
        }

        return result;
    };

    renderListCategoriesMenu = () => {
        return (
            <ScrollView
                style={styles.listCategoryScrollView}
                horizontal
                showsHorizontalScrollIndicator={false}
            >
                <View style={commonStyles.flexRow}>
                    {this.renderListCategoryColumns()}
                </View>
            </ScrollView>
        );
    };

    render() {
        const {style} = this.props;

        return (
            <View style={[commonStyles.flexColumn, style]}>
                {this.renderTitle()}
                {this.renderListCategoriesMenu()}
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    categories: state.common.categories.list
});

export default connect(mapStateToProps, {
    fetchCategories
})(CategoriesMenu);
