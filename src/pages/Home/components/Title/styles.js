import {StyleSheet} from "react-native";
import {TITLE_HEIGHT} from "src/utils/layouts/homeLayout";
import commonStyles from "src/styles/commonStyles";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    title: {
        color: "#333",
        width: "100%",
        height: TITLE_HEIGHT,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "flex-end",
        paddingBottom: 16
    },
    titleText: {
        fontSize: 15,
        fontWeight: "bold"
    },
});

export default styles;
