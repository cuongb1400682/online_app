import React from "react";
import {View} from "react-native";

import styles from "./styles";
import Text from "src/components/platformSpecific/Text/Text";

const Title = ({text = "", right = null, containerStyle = {}}) => {
    return (
        <View style={[styles.title, containerStyle]}>
            <Text style={styles.titleText}>{text}</Text>
            {right}
        </View>
    );
};

export default Title;
