import React, {Component} from "react";
import {View} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import PropTypes from "prop-types";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import commonStyles from "src/styles/commonStyles";
import {pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";
import Text from "src/components/platformSpecific/Text/Text";

const shortcutBarItems = [
    {
        id: 2,
        name: "Bán chạy",
        link: screenIds.BEST_SALE_PRODUCT,
        color: "#2D9CDB",
        icon: "tags"
    },
    {
        id: 3,
        name: "Mới về",
        link: screenIds.NEW_PRODUCT,
        color: "#F2994A",
        icon: "fire"
    },
    {
        id: 4,
        name: "Khuyến mãi",
        link: screenIds.PROMOTED_PRODUCT,
        color: "#EB5757",
        icon: "gift"
    }
];

class ShortcutBar extends Component {
    static HEIGHT = styles.shortcutBar.height;

    handleShortcutPress = (shortcutBarItem = {}) => () => {
        const {navigator} = this.props;
        const {link = "", passProps = {}} = shortcutBarItem;

        if (navigator) {
            pushTo(navigator, {
                screen: link,
                passProps
            });
        }
    };

    renderShortcutBarItem = (shortcutBarItem = {}) => {
        const {id, name, color, icon} = shortcutBarItem;
        const circleBackground = {
            backgroundColor: color
        };

        return (
            <Touchable
                key={id}
                style={[commonStyles.flexColumnCenter, styles.shortcutBarItemTouchable]}
                onPress={this.handleShortcutPress(shortcutBarItem)}
            >
                <View style={[styles.circle, circleBackground, commonStyles.centerChildren]}>
                    <FontAwesome5 name={icon} style={styles.innerIcon}/>
                </View>
                <Text style={styles.innerIconText}>{name}</Text>
            </Touchable>
        );
    };

    render() {
        return (
            <View style={[styles.shortcutBar, commonStyles.centerChildren]}>
                {shortcutBarItems.map(this.renderShortcutBarItem)}
            </View>
        );
    }
}

export default ShortcutBar;
