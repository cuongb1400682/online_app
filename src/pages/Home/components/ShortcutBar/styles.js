import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    shortcutBar: {
        height: 90,
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 16,
    },
    shortcutBarItemTouchable: {
        flex: 1
    },
    circle: {
        height: 50,
        width: 50,
        borderRadius: 50,
        marginBottom: 7
    },
    innerIcon: {
        fontSize: 18,
        color: "#fff"
    },
    innerIconText: {
        color: "#333",
        fontSize: 12,
    }
});

export default styles;
