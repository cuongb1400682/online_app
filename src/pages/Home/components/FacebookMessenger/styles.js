import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    icon: {
        width: 50,
        height: 50
    },
    iconContainer: {
        borderRadius: 60,
        padding: 6,
        backgroundColor: "#fff",
        ...commonStyles.hasShadow
    },
    wrapper: {
        position: "absolute",
        bottom: 63,
        right: 10,
        zIndex: 1000
    }
});

export default styles;
