import React from "react";
import {Image, Linking, View} from "react-native";

import styles from "./styles";
import facebookMessenger from "src/assets/static/icons/facebookMessenger.png";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";

const GIIGAA_MESSENGER_DEEP_LINK = "fb-messenger-public://user-thread/giigaacom";
const GIIGAA_MESSENGER_WEB_LINK = "https://www.messenger.com/t/giigaacom";

const launchFacebookMessenger = async () => {
    if (await Linking.canOpenURL(GIIGAA_MESSENGER_DEEP_LINK)) {
        Linking.openURL(GIIGAA_MESSENGER_DEEP_LINK);
    } else {
        Linking.openURL(GIIGAA_MESSENGER_WEB_LINK);
    }
};

const FacebookMessenger = ({floating = false}) => {
    return (
        <View style={floating ? styles.wrapper : {}}>
            <Touchable onPress={launchFacebookMessenger}>
                <View style={styles.iconContainer}>
                    <Image style={styles.icon} source={facebookMessenger}/>
                </View>
            </Touchable>
        </View>
    );
};

export default FacebookMessenger;
