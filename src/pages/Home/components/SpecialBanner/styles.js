import {StyleSheet} from "react-native";

import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    specialBannerContainer: {
        backgroundColor: "transparent",
        marginBottom: 8,
        ...commonStyles.flexColumnCenter,
        ...commonStyles.hasShadow
    },
    specialBannerImage: {
        width: 80,
        height: 109
    }
});

export default styles;
