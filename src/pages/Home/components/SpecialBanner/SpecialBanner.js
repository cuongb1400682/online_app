import React, {Component} from "react";

import styles from "./styles";
import PropTypes from "prop-types";
import {makeCDNImageURL} from "src/utils/cdn/images";
import FastImage from "src/components/base/FastImage/FastImage";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {isEmptyString} from "src/utils/extensions/strings";

class SpecialBanner extends Component {
    static propTypes = {
        specialBanner: PropTypes.any,
        onBannerPress: PropTypes.func
    };

    static defaultProps = {
        specialBanner: {},
        onBannerPress: () => {
        }
    };

    handleOnBannerPress = (link, link_type) => () => {
        const {onBannerPress} = this.props;

        onBannerPress({link, link_type})
    };

    render() {
        const {specialBanner} = this.props;
        const {image_url = "", redirect_url = "", redirect_url_type = ""} = specialBanner;

        if (isEmptyString(image_url)) {
            return null;
        }

        return (
            <Touchable
                style={styles.specialBannerContainer}
                onPress={this.handleOnBannerPress(redirect_url, redirect_url_type)}
            >
                <FastImage
                    source={{uri: makeCDNImageURL(image_url)}}
                    style={styles.specialBannerImage}
                />
            </Touchable>
        );
    }
}

export default SpecialBanner;
