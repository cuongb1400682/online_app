import React from "react";
import {View} from "react-native";

import styles from "./styles";
import {addLeadingZeros} from "src/utils/extensions/strings";
import Text from "src/components/platformSpecific/Text/Text";

const ClockComponent = ({value, visible = true}) => {
    if (!visible) {
        return null;
    }

    return (
        <View style={styles.dealClockComponent}>
            <Text style={styles.dealClockComponentText}>{addLeadingZeros(value)}</Text>
        </View>
    );
};

const DealClock = ({days, hours, min, sec}) => (
    <View style={styles.dealClock}>
        <ClockComponent value={days} visible={days > 0}/>
        <ClockComponent value={hours}/>
        <ClockComponent value={min}/>
        <ClockComponent value={sec}/>
    </View>
);

export default DealClock;
