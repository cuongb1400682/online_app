import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    dealClock: {
        flexDirection: "row"
    },
    dealClockComponent: {
        width: 18,
        height: 18,
        backgroundColor: "#222",
        borderRadius: 5,
        marginLeft: 5,
        ...commonStyles.centerChildren
    },
    dealClockComponentText: {
        color: "#fff",
        fontSize: 11
    }
});

export default styles;
