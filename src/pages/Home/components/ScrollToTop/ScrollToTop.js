import React, {Component} from "react";
import {View, Image} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import scrollToTop from "src/assets/static/icons/scrollToTop.png";
import commonStyles from "src/styles/commonStyles";

class ScrollToTop extends Component {
    static propTypes = {
        visible: PropTypes.bool,
        productListRef: PropTypes.any
    };

    static defaultProps = {
        visible: true
    };

    scrollToTop = () => {
        const {productListRef} = this.props;

        if (productListRef && productListRef.scrollToTop) {
            productListRef.scrollToTop();
        }
    };

    render() {
        const {visible} = this.props;

        if (!visible) {
            return <View style={styles.scrollToTop}/>;
        }

        return (
            <Touchable style={[styles.scrollToTop, styles.scrollToTopIcon, commonStyles.hasShadow]} onPress={this.scrollToTop}>
                <Image style={[styles.scrollToTopIcon]} source={scrollToTop}/>
            </Touchable>
        );
    }
}

export default ScrollToTop;
