import {StyleSheet} from "react-native";

import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    scrollToTop: {
        margin: 8,
        height: 40,
        width: 40,
    },
    scrollToTopIcon: {
        height: 40,
        width: 40,
        backgroundColor: "#fff",
        borderRadius: 20,
    },
    visibleIcon: {
    }
});

export default styles;
