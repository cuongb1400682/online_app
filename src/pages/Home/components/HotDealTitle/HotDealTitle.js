import React from "react";
import {View} from "react-native";

import styles from "./styles";
import DealClockIcon from "src/components/base/svgIcons/DealClock/DealClock";
import {tr} from "src/localization/localization";
import commonStyles from "src/styles/commonStyles";
import CountDownText from "src/components/base/CountDownText/CountDownText";
import DealClock from "src/pages/Home/components/DealClock/DealClock";
import Text from "src/components/platformSpecific/Text/Text";

const HotDealTitle = ({currentDeal = {}}) => {
    return (
        <View style={styles.hotDealTitle}>
            <DealClockIcon/>
            <Text style={styles.hotDealTitleText}>{currentDeal.name}</Text>
            <View style={commonStyles.matchParent}/>
            <Text style={styles.hotDealClockLabel}>{tr("main_tab_hot_deal_clock_label")}</Text>
            <CountDownText
                endDate={currentDeal.end_time}
                formatter={DealClock}
            />
        </View>
    );
};

export default HotDealTitle;
