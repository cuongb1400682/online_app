import {StyleSheet} from "react-native";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    hotDealTitle: {
        width: "100%",
        height: 38,
        backgroundColor: colors.primaryColor,
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 16
    },
    hotDealTitleText: {
        fontSize: 13,
        fontWeight: "bold",
        color: "#fff",
        marginLeft: 10,
    },
    hotDealClockLabel: {
        fontSize: 12,
        fontStyle: "italic",
        color: "#fff"
    }
});

export default styles;
