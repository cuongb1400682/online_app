import homeActionsType from "src/redux/actionTypes/homeActionTypes";
import {createAction, createAsyncAction} from "src/utils/api/createAction";
import {dealTimeline} from "src/constants/enums";
import {fetchCategories} from "src/redux/common/commonActions";

const fetchDeals = ({timeline, sortBy, pageSize = 20, pageIndex = 0}) => {
    return createAsyncAction({
        type: homeActionsType.FETCH_DEALS,
        payload: {
            request: "/deal/product/list",
            params: {
                page_size: pageSize,
                page_index: pageIndex,
                timeline: timeline,
                sort_by: sortBy
            }
        }
    });
};

const fetchMainBanners = () => createAsyncAction({
    type: homeActionsType.FETCH_MAIN_BANNERS,
    payload: {
        request: "/home/banner/list",
        params: {
            device_type: "MOBILE"
        }
    }
});

const fetchHotTrends = () => {
    return createAsyncAction({
        type: homeActionsType.FETCH_HOT_TRENDS,
        payload: {
            request: "/trend/list"
        }
    });
};

const fetchJustForYou = (onSuccess) => createAsyncAction({
    type: homeActionsType.FETCH_JUST_FOR_YOU,
    payload: {
        request: "/product/just_for_you",
        params: {
            device_type: "MOBILE"
        }
    },
    onSuccess: onSuccess
});

const fetchHomeProducts = ({pageIndex, pageSize}) => createAsyncAction({
    type: homeActionsType.FETCH_HOME_PRODUCTS,
    payload: {
        request: "/product/search",
        params: {
            page_size: pageSize,
            page_index: Math.max(pageIndex - 1, 0),
            category_id: "-1"
        }
    },
    meta: {
        page_index: pageIndex
    }
});

const clearJustForYou = () => createAction({
    type: homeActionsType.CLEAR_JUST_FOR_YOU,
    payload: {}
});

const refreshHome = ({onFinished} = {}) => async (dispatch) => {
    await Promise.all([
        dispatch(clearJustForYou()),
        dispatch(fetchDeals({timeline: dealTimeline.CURRENT, pageSize: 20, pageIndex: 0})),
        dispatch(fetchHotTrends()),
        dispatch(fetchJustForYou()),
        dispatch(fetchHomeProducts({pageIndex: 1, pageSize: 50})),
        dispatch(fetchMainBanners()),
        dispatch(fetchCategories()),
    ]);

    if (typeof onFinished === "function") {
        setTimeout(onFinished, 500);
    }
};

export {
    refreshHome,
    clearJustForYou,
    fetchDeals,
    fetchHomeProducts,
    fetchJustForYou,
    fetchMainBanners,
    fetchHotTrends
};
