import React, {Component} from "react";
import {AppState, InteractionManager, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import commonStyles from "src/styles/commonStyles";
import HomeNavBar from "src/pages/Home/containers/HomeNavBar/HomeNavBar";
import Banner from "src/pages/Home/containers/Banner/Banner";
import colors from "src/constants/colors";
import ShortcutBar from "src/pages/Home/components/ShortcutBar/ShortcutBar";
import HotTrends from "src/pages/Home/containers/HotTrends/HotTrends";
import CategoriesMenu from "src/pages/Home/containers/CategoriesMenu/CategoriesMenu";
import ProductList from "src/components/base/ProductList/ProductList";
import SpecialBanner from "src/pages/Home/components/SpecialBanner/SpecialBanner";
import ScrollToTop from "src/pages/Home/components/ScrollToTop/ScrollToTop";
import Advertisements from "src/pages/Home/containers/Advertisements/Advertisements";
import LoadMore from "src/components/base/LoadMore/LoadMore";
import Title from "src/pages/Home/components/Title/Title";
import {tr} from "src/localization/localization";
import {isEmptyArray} from "src/utils/extensions/arrays";
import {
    clearJustForYou,
    fetchDeals,
    fetchHomeProducts,
    fetchHotTrends,
    fetchJustForYou,
    fetchMainBanners,
    refreshHome
} from "src/pages/Home/homeActions";
import {advertisementHeight, estimateBannerHeight, hotTrendHeight, TITLE_HEIGHT} from "src/utils/layouts/homeLayout";
import openRedirectLink from "src/utils/reactNative/openRedirectLink";
import {checkPermissionPushNotify, createNotificationListeners} from "src/utils/firebase/notifications";
import HotDeal from "src/pages/Home/containers/HotDeal/HotDeal";
import {fetchCategories, saveHomeNavigatorObject} from "src/redux/common/commonActions";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";
import {registerDeepLinkHandler} from "src/utils/reactNative/deepLinkHandler";

const DEFAULT_PAGE_SIZE = 50;

const HEADER_HEIGHT = (
    estimateBannerHeight() +
    ShortcutBar.HEIGHT +
    hotTrendHeight() * 2 +
    HotTrends.ROW_MARGIN_TOP +
    advertisementHeight() +
    Advertisements.MARGIN_BOTTOM +
    Advertisements.MARGIN_TOP +
    CategoriesMenu.LIST_HEIGHT +
    TITLE_HEIGHT * 3
);

class Home extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        tabBarHidden: false
    };

    static propTypes = {
        productEntities: PropTypes.any,
        mainBannerData: PropTypes.array,
        categories: PropTypes.array,
        dealProducts: PropTypes.array,
        specialBanner: PropTypes.any,
        numberItemsJustForYou: PropTypes.number,
        productIdsJustForYou: PropTypes.array,
        pageIndex: PropTypes.number,
        fetchingJustForYou: PropTypes.bool,
        isFetchingDeals: PropTypes.bool,
        isFetchingHotTrends: PropTypes.bool,
        isFetchingMainBanners: PropTypes.bool,
        fetchHomeProducts: PropTypes.func,
        fetchJustForYou: PropTypes.func,
        fetchMainBanners: PropTypes.func,
        clearJustForYou: PropTypes.func,
        saveHomeNavigatorObject: PropTypes.func,
        fetchDeals: PropTypes.func,
        fetchHotTrends: PropTypes.func,
        fetchCategories: PropTypes.func,
        refreshHome: PropTypes.func,
    };

    static defaultProps = {
        productEntities: [],
        mainBannerData: [],
        categories: [],
        dealProducts: [],
        specialBanner: {},
        numberItemsJustForYou: 0,
        productIdsJustForYou: [],
        pageIndex: 0,
        fetchingJustForYou: false,
        isFetchingDeals: false,
        isFetchingHotTrends: false,
        isFetchingMainBanners: false,
        fetchHomeProducts: () => {
        },
        fetchJustForYou: () => {
        },
        fetchMainBanners: () => {
        },
        clearJustForYou: () => {
        },
        saveHomeNavigatorObject: () => {
        },
        fetchDeals: () => {
        },
        fetchHotTrends: () => {
        },
        fetchCategories: () => {
        },
        refreshHome: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            isRefreshing: false,
            navBarBackgroundColor: colors.primaryColor,
            isScrollToTopVisible: false,
            allowChangeNavBarColor: true,
            bannerIndex: 0,
        };

        this.latestTimeInForeground = Date.now();
    }

    // TODO: replace by getDerivedStateFromProps
    UNSAFE_componentWillReceiveProps(nextProps) {
        if (!isEmptyArray(nextProps.mainBannerData) && isEmptyArray(this.props.mainBannerData)) {
            this.updateNavBarBackgroundColor();
        }

        this.updateRefreshingState(nextProps);
    }

    componentDidMount() {
        const {
            clearJustForYou,
            fetchJustForYou,
            fetchMainBanners,
            saveHomeNavigatorObject,
            navigator,
            categories
        } = this.props;

        InteractionManager.runAfterInteractions(() => {
            registerDeepLinkHandler({navigator, categories});
        });

        fetchMainBanners();

        clearJustForYou();
        fetchJustForYou(this.onFetchingNextJustForYou);

        saveHomeNavigatorObject(navigator);

        checkPermissionPushNotify();
        createNotificationListeners(this.handleOnBannerPress);
        AppState.addEventListener("change", this.refreshAfterEnteringBackground);
    }

    componentWillUnmount() {
        AppState.removeEventListener("change", this.refreshAfterEnteringBackground);
    }

    refreshAfterEnteringBackground = (appState) => {
        const {refreshHome} = this.props;
        const currentTimestamp = Date.now();
        const timeDiff = currentTimestamp - this.latestTimeInForeground;

        if (appState === "active" && timeDiff >= 120000) {
            refreshHome({onFinished: this.productListRef.scrollToTop});
        } else {
            this.latestTimeInForeground = currentTimestamp;
        }
    };

    updateNavBarBackgroundColor = () => {
        const {bannerIndex, allowChangeNavBarColor} = this.state;
        const {mainBannerData} = this.props;
        let newBackgroundColor = colors.primaryColor;

        if (bannerIndex < mainBannerData.length && allowChangeNavBarColor) {
            newBackgroundColor = mainBannerData[bannerIndex].color || colors.primaryColor;
        }

        this.setState({navBarBackgroundColor: newBackgroundColor});
    };

    toggleNavBarColorChanging = (offsetY) => {
        const {allowChangeNavBarColor} = this.state;
        const isBannerVisible = (offsetY <= estimateBannerHeight());

        if (isBannerVisible !== allowChangeNavBarColor) {
            this.setState({
                allowChangeNavBarColor: isBannerVisible
            }, this.updateNavBarBackgroundColor);
        }
    };

    toggleScrollToTop = (offsetY) => {
        const {isScrollToTopVisible} = this.state;
        const isHeaderObscured = (offsetY > HEADER_HEIGHT);

        if (isHeaderObscured !== isScrollToTopVisible) {
            this.setState({
                isScrollToTopVisible: isHeaderObscured
            });
        }
    };

    handleProductListScroll = (_, offsetX, offsetY) => {
        this.toggleNavBarColorChanging(offsetY);
        this.toggleScrollToTop(offsetY);
    };

    handleBannerChanged = (_, index) => {
        this.setState({
            bannerIndex: index
        }, this.updateNavBarBackgroundColor);
    };

    handleOnBannerPress = ({link = "", link_type = ""}) => {
        const {categories, navigator} = this.props;

        openRedirectLink({link, link_type, categories, navigator});
    };

    onFetchingNextJustForYou = () => {
        const nextPageIndex = this.props.pageIndex + 1;

        this.props.fetchHomeProducts({pageIndex: nextPageIndex, pageSize: DEFAULT_PAGE_SIZE});
    };

    getHeaderHeight = () => {
        const {dealProducts} = this.props;

        if (!isEmptyArray(dealProducts)) {
            return HEADER_HEIGHT + HotDeal.HEIGHT;
        }

        return HEADER_HEIGHT;
    };

    updateRefreshingState = (props) => {
        const {fetchingJustForYou, isFetchingDeals, isFetchingHotTrends, isFetchingMainBanners} = props;
        const isRefreshing = fetchingJustForYou || isFetchingDeals || isFetchingHotTrends || isFetchingMainBanners;

        this.setState({isRefreshing});
    };

    renderPageUpperPart = () => {
        const {mainBannerData, navigator} = this.props;
        const {bannerIndex, allowChangeNavBarColor} = this.state;

        return (
            <View style={styles.pageUpperPart}>
                <View style={styles.shadowyBannerAndShortcutBar}>
                    <Banner
                        onBannerChanged={this.handleBannerChanged}
                        onBannerPress={this.handleOnBannerPress}
                        autoplay={allowChangeNavBarColor}
                        currentPageIndex={bannerIndex}
                        mainBannerData={mainBannerData}
                    />
                    <ShortcutBar navigator={navigator}/>
                </View>
                <HotDeal navigator={navigator}/>
                <HotTrends
                    styles={styles.section}
                    navigator={navigator}
                />
                <CategoriesMenu navigator={navigator}/>
                <Advertisements
                    banners={mainBannerData}
                    onAdvertisementPress={this.handleOnBannerPress}
                />
                <Title containerStyle={styles.justForYouTitle}
                       text={tr("main_tab_just_for_you_title")}/>
            </View>
        );
    };

    renderPageLowerPart = () => {
        const {fetchingJustForYou, productIdsJustForYou, numberItemsJustForYou} = this.props;

        return (
            <LoadMore
                searching={fetchingJustForYou}
                onFetchNextProduct={this.onFetchingNextJustForYou}
                maximumNumberOfItems={numberItemsJustForYou}
                currentNumberOfItems={productIdsJustForYou.length}
            />
        );
    };

    renderFloatingButtons = () => {
        const {specialBanner} = this.props;
        const {isScrollToTopVisible} = this.state;

        return (
            <View style={styles.floatingButtons}>
                <SpecialBanner
                    specialBanner={specialBanner}
                    onBannerPress={this.handleOnBannerPress}
                />
                <FacebookMessenger/>
                <ScrollToTop
                    productListRef={this.productListRef}
                    visible={isScrollToTopVisible}
                />
            </View>
        );
    };

    render() {
        const {productEntities, productIdsJustForYou, navigator, refreshHome} = this.props;

        return (
            <View style={commonStyles.matchParent}>
                <HomeNavBar
                    backgroundColor={this.state.navBarBackgroundColor}
                    navigator={navigator}
                />
                <ProductList
                    ref={ref => this.productListRef = ref}
                    header={this.renderPageUpperPart()}
                    footer={this.renderPageLowerPart()}
                    title={tr("main_tab_just_for_you_title")}
                    products={
                        productIdsJustForYou.map(id => productEntities[id])
                    }
                    headerHeight={this.getHeaderHeight()}
                    footerHeight={50}
                    onScroll={this.handleProductListScroll}
                    navigator={navigator}
                    onRefresh={refreshHome}
                    isRefreshing={this.state.isRefreshing}
                />
                {this.renderFloatingButtons()}
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    productEntities: state.home.just_for_you.product_entities,
    categories: state.common.categories.list,
    mainBannerData: state.home.main_banners.urls,
    specialBanner: state.home.special_banner,
    dealProducts: state.home.deals.products,
    numberItemsJustForYou: state.home.just_for_you.number_items,
    productIdsJustForYou: state.home.just_for_you.product_ids,
    pageIndex: state.home.just_for_you.page_index,
    fetchingJustForYou: state.home.just_for_you.fetching,
    isFetchingDeals: state.home.deals.fetching,
    isFetchingHotTrends: state.home.deals.fetching,
    isFetchingMainBanners: state.home.main_banners.fetching,
});

export default connect(mapStateToProps, {
    refreshHome,
    saveHomeNavigatorObject,
    clearJustForYou,
    fetchJustForYou,
    fetchMainBanners,
    fetchHomeProducts,
    fetchDeals,
    fetchHotTrends,
    fetchCategories
})(Home);
