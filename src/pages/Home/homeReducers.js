import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import homeActionsTypes from "src/redux/actionTypes/homeActionTypes";

const SPECIAL_BANNER_TYPE = "SPECIAL_PROMOTED_MOBILE";
const MAIN_BANNER_TYPE = "MOBILE_MAIN";

const defaultJustForYou = {
    product_ids: [],
    product_entities: {},
    page_index: 0,
    fetching: false
};

const initialState = {
    deals: {
        current_deal: {},
        products: [],
        number_items: 0,
        fetching: false
    },
    just_for_you: {
        ...defaultJustForYou
    },
    hot_trends: {
        trends: [],
        fetching: false,
    },
    main_banners: {
        urls: [],
        fetching: false,
    },
    special_banner: {}
};

const normalizeProductEntities = (entities = {}, products) => {
    return products.reduce((productEntities, product) => ({
        ...productEntities,
        [product.id]: {...product}
    }), {...entities});
};

const normalizeHomeProducts = (currentEntities, productIds, products) => {
    const newEntities = normalizeProductEntities(currentEntities, products);
    const newProductIds = products.filter(({id}) => !currentEntities[id]).map(({id}) => id);

    return {
        product_ids: [...productIds, ...newProductIds],
        product_entities: {...newEntities}
    };
};

const normalizeProductListByDeal = ({products: items}) => items.map(({product, deal, ...others}) => {
    return {...product, deal: {...deal, ...others}};
});

const homeReducers = (state = initialState, {type, payload, meta}) => {
    switch (type) {
        case toRequest(homeActionsTypes.FETCH_DEALS):
            return {
                ...state,
                deals: {
                    ...state.deals,
                    products: [],
                    fetching: true
                }
            };
        case toFailure(homeActionsTypes.FETCH_DEALS):
            return {
                ...state,
                deals: {
                    ...state.deals,
                    products: [],
                    number_items: 0,
                    fetching: false
                }
            };
        case toSuccess(homeActionsTypes.FETCH_DEALS):
            const {current_deal = {}, number_items} = payload.reply;
            const dealItems = normalizeProductListByDeal(payload.reply);

            return {
                ...state,
                deals: {
                    ...state.deals,
                    products: [...dealItems],
                    number_items,
                    current_deal: {...current_deal},
                    fetching: false
                }
            };
        case toRequest(homeActionsTypes.FETCH_MAIN_BANNERS):
            return {
                ...state,
                main_banners: {
                    ...state.main_banners,
                    urls: [],
                    fetching: true
                },
                special_banner: {}
            };
        case toSuccess(homeActionsTypes.FETCH_MAIN_BANNERS):
            return {
                ...state,
                main_banners: {
                    ...state.main_banners,
                    urls: [...payload.reply.filter(banner => banner.type === MAIN_BANNER_TYPE)],
                    fetching: false
                },
                special_banner: {...payload.reply.find(banner => banner.type === SPECIAL_BANNER_TYPE)}
            };
        case toFailure(homeActionsTypes.FETCH_MAIN_BANNERS):
            return {
                ...state,
                main_banners: {
                    ...state.main_banners,
                    urls: [],
                    fetching: false
                },
                special_banner: {}
            };
        case toRequest(homeActionsTypes.FETCH_HOT_TRENDS):
            return {
                ...state,
                hot_trends: {
                    ...state.hot_trends,
                    trends: [],
                    fetching: true
                }
            };
        case toFailure(homeActionsTypes.FETCH_HOT_TRENDS):
            return {
                ...state,
                hot_trends: {
                    ...state.hot_trends,
                    trends: [],
                    fetching: false
                }
            };
        case toSuccess(homeActionsTypes.FETCH_HOT_TRENDS):
            return {
                ...state,
                hot_trends: {
                    ...state.hot_trends,
                    trends: [...payload.reply.slice(0, 4)],
                    fetching: false
                }
            };
        case toRequest(homeActionsTypes.FETCH_JUST_FOR_YOU):
            return {
                ...state,
                just_for_you: {
                    ...state.just_for_you,
                    fetching: true
                }
            };
        case toSuccess(homeActionsTypes.FETCH_JUST_FOR_YOU):
            return {
                ...state,
                just_for_you: {
                    ...state.just_for_you,
                    ...normalizeHomeProducts(
                        state.just_for_you.product_entities,
                        state.just_for_you.product_ids,
                        payload.reply
                    ),
                    number_items: payload.reply.number_items,
                    fetching: false
                }
            };
        case toFailure(homeActionsTypes.FETCH_JUST_FOR_YOU):
            return {
                ...state,
                just_for_you: {
                    ...state.just_for_you,
                    fetching: false
                }
            };
        case toRequest(homeActionsTypes.FETCH_HOME_PRODUCTS):
            return {
                ...state,
                just_for_you: {
                    ...state.just_for_you,
                    fetching: true
                }
            };
        case toSuccess(homeActionsTypes.FETCH_HOME_PRODUCTS):
            return {
                ...state,
                just_for_you: {
                    ...state.just_for_you,
                    ...normalizeHomeProducts(
                        state.just_for_you.product_entities,
                        state.just_for_you.product_ids,
                        payload.reply.products
                    ),
                    number_items: payload.reply.number_items,
                    page_index: meta.page_index,
                    fetching: false
                }
            };
        case toFailure(homeActionsTypes.FETCH_HOME_PRODUCTS):
            return {
                ...state,
                just_for_you: {
                    ...state.just_for_you,
                    fetching: true
                }
            };

        case homeActionsTypes.CLEAR_JUST_FOR_YOU:
            return {
                ...state,
                just_for_you: {
                    ...defaultJustForYou
                }
            };
        default:
            return state;
    }
};

export default homeReducers;

