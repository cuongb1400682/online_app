import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import CustomStatusBar from "src/components/base/CustomStatusBar/CustomStatusBar";
import commonStyles from "src/styles/commonStyles";
import FontIcon from "src/components/base/FontIcon/FontIcon";
import Button from "src/components/base/Button/Button";
import {popToRoot} from "src/utils/misc/navigator";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";
import Text from "src/components/platformSpecific/Text/Text";

class UnderDevelopment extends Component {
    static navigatorStyle = {
        navBarHidden: true
    };

    static propTypes = {
        allowsGoBack: PropTypes.bool
    };

    static defaultProps = {
        allowsGoBack: false
    };

    goBack = () => {
        const {navigator} = this.props;

        popToRoot(navigator);
    };

    render() {
        return (
            <View style={commonStyles.matchParent}>
                <CustomStatusBar/>
                <View style={styles.underDevelopmentContent}>
                    <FontIcon style={styles.constructIcon} fontName="Ionicons" name="ios-construct"/>
                    <Text style={styles.promiseText}>{tr("under_development_promise")}</Text>
                    {this.props.allowsGoBack && <Button
                        onPress={this.goBack}
                        type="fully-colored"
                        title={tr("under_development_back_button")}
                        style={styles.backButton}
                    />}
                </View>
                <FacebookMessenger floating/>
            </View>
        );
    }
}

export default UnderDevelopment;
