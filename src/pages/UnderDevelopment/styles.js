import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    underDevelopmentContent: {
        ...commonStyles.centerChildren,
        flex: 1,
        backgroundColor: "#ddd"
    },
    promiseText: {
        textAlign: "center"
    },
    constructIcon: {
        fontSize: 100,
    },
    backButton: {
        marginTop: 20
    }
});

export default styles;
