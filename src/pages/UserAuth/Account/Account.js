import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {Image, Linking, ScrollView, TouchableWithoutFeedback, View} from "react-native";
import SimpleLineIcon from "react-native-vector-icons/SimpleLineIcons";

import styles from "./styles";
import flashDeal from "src/assets/static/images/flashDeal.png";
import backgroundImage from "src/assets/static/images/accountTabBackground.jpeg";
import Avatar from "src/components/base/svgIcons/Avatar/Avatar";
import commonStyles from "src/styles/commonStyles";
import LinkButton from "src/components/base/LinkButton/LinkButton";
import {tr} from "src/localization/localization";
import CustomStatusBar from "src/components/base/CustomStatusBar/CustomStatusBar";
import MenuItem from "src/pages/UserAuth/components/MenuItem/MenuItem";
import AccountOrderDetail from "src/components/base/svgIcons/AccountOrderDetail/AccountOrderDetail";
import AccountManager from "src/components/base/svgIcons/AccountManager/AccountManager";
import AccountHome from "src/components/base/svgIcons/AccountHome/AccountHome";
import screenIds from "src/constants/screenIds";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {isUserLoggedIn} from "src/utils/userAuth/userAuth";
import {pushTo, switchToTab} from "src/utils/misc/navigator";
import {showDeploymentChangingOptions} from "src/utils/codePush/deploymentChanging";
import colors from "src/constants/colors";
import ProgressView from "src/components/platformSpecific/ProgressView/ProgressView";
import ReceiptIcon from "src/components/base/svgIcons/Receipt/Receipt";
import Text from "src/components/platformSpecific/Text/Text";

class Account extends Component {
    static navigatorStyle = {
        navBarHidden: true
    };

    static propTypes = {
        userInfo: PropTypes.any
    };

    static defaultProps = {
        userInfo: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            bundleDownloadProgress: -1
        };
    }

    switchToHomeTab = () => {
        const {navigator} = this.props;

        switchToTab(navigator, {
            tabIndex: 0
        });
    };

    goToDealList = () => {
        const {navigator} = this.props;

        pushTo(navigator, {screen: screenIds.DEAL_LIST});
    };

    makeCallToGiigaaHotline = () => {
        const hotlineNumber = tr("giigaa_customer_service_phone_number");

        Linking.openURL(`tel:${hotlineNumber}`);
    };

    handleBackgroundLongPress = () => showDeploymentChangingOptions(progress => {
        this.setState({bundleDownloadProgress: progress});
    });

    renderBackground = () => (
        <TouchableWithoutFeedback onLongPress={this.handleBackgroundLongPress}>
            <Image
                style={styles.backgroundImage}
                source={backgroundImage}
                resizeMode="stretch"
            />
        </TouchableWithoutFeedback>
    );

    renderUserInfo = () => {
        const {userInfo} = this.props;

        return (
            <View style={styles.userInfo}>
                <Avatar style={styles.avatar}/>
                <View>
                    <Text style={styles.userFullName}>{userInfo.full_name}</Text>
                    <Text style={styles.userEmail}>{userInfo.email}</Text>
                </View>
            </View>
        );
    };

    renderLoginLinks = () => {
        const {navigator} = this.props;

        return (
            <View style={styles.userInfo}>
                <Avatar style={styles.avatar}/>
                <LinkButton
                    textStyle={styles.linkButton}
                    title={tr("account_tab_login_link")}
                    navigator={navigator}
                    screenId={screenIds.LOGIN}
                />
                <Text style={styles.verticalSeparator}>|</Text>
                <LinkButton
                    textStyle={styles.linkButton}
                    title={tr("account_tab_register_link")}
                    navigator={navigator}
                    screenId={screenIds.USER_REGISTRATION}
                />
            </View>
        )
    };

    renderUpperPart = () => (
        <View style={styles.upperPart}>
            {this.renderBackground()}
            {isUserLoggedIn()
                ? this.renderUserInfo()
                : this.renderLoginLinks()}
        </View>
    );

    renderMenu = () => (
        <View style={styles.menuItemContainer}>
            <MenuItem
                text={tr("account_tab_menu_item_home")}
                icon={<AccountHome/>}
                onPress={this.switchToHomeTab}
            />
            <MenuItem
                text={tr("account_tab_menu_item_order_detail")}
                icon={<AccountManager/>}
                navigator={this.props.navigator}
                screenId={screenIds.ORDER_MANAGER}
                requireLogin
            />
            <MenuItem
                text={tr("account_tab_menu_item_coupon_manager")}
                icon={<ReceiptIcon/>}
                navigator={this.props.navigator}
                screenId={screenIds.COUPON_MANAGER}
                requireLogin
            />
            <MenuItem
                text={tr("account_tab_menu_item_viewed_products")}
                icon={<SimpleLineIcon style={styles.menuItemIcon} name="eye"/>}
                navigator={this.props.navigator}
                screenId={screenIds.VIEWED_PRODUCTS}
            />
            <MenuItem
                text={tr("account_tab_menu_item_show_account_manager")}
                icon={<AccountOrderDetail/>}
                navigator={this.props.navigator}
                screenId={screenIds.PROFILE_MANAGER}
                requireLogin
            />
        </View>
    );

    renderFlashDeal = () => (
        <Touchable onPress={this.goToDealList}>
            <Image source={flashDeal} resizeMode="contain" style={styles.flashDealImage}/>
        </Touchable>
    );

    renderHotLineNumber = () => (
        <Touchable onPress={this.makeCallToGiigaaHotline}>
            <View style={styles.hotlineNumberContainer}>
                <Text style={styles.hotline}>
                    HOTLINE:
                    <Text style={styles.hotlineNumber}>{" " + tr("giigaa_customer_service_phone_number")}</Text>
                    {tr("account_tab_free_hot_line")}
                </Text>
            </View>
        </Touchable>
    );

    renderLowerPart = () => (
        <View style={styles.lowerPart}>
            {this.renderMenu()}
            {this.renderFlashDeal()}
            {this.renderHotLineNumber()}
        </View>
    );

    renderProgressView = () => {
        const {bundleDownloadProgress} = this.state;

        if (bundleDownloadProgress < 0) {
            return null;
        }

        return (
            <ProgressView
                progressTintColor={colors.primaryColor}
                progress={bundleDownloadProgress / 100}
            />
        );
    };

    render() {
        return (
            <View style={styles.account}>
                <CustomStatusBar style={styles.statusBar}/>
                <ScrollView style={commonStyles.matchParent}>
                    {this.renderUpperPart()}
                    {this.renderLowerPart()}
                    {this.renderProgressView()}
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    userInfo: state.session.user_info
});

export default connect(mapStateToProps, {})(Account);
