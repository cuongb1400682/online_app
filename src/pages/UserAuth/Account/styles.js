import {StyleSheet, Platform} from "react-native";
import {screenWidth} from "src/utils/reactNative/dimensions";
import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const BACKGROUND_IMAGE_RATIO = 0.28;

const styles = StyleSheet.create({
    account: {
        flex: 1,
        backgroundColor: "#fff"
    },
    upperPart: {
        borderStyle: "solid",
        borderColor: "#E8E8E8",
        borderBottomWidth: 1
    },
    statusBar: {
        backgroundColor: "#CA2E10"
    },
    backgroundImage: {
        width: "100%",
        height: screenWidth() * BACKGROUND_IMAGE_RATIO
    },
    userInfo: {
        backgroundColor: "transparent",
        flexDirection: "row",
        alignItems: "center",
        width: "100%",
        marginLeft: 20,
        marginBottom: 36
    },
    avatar: {
        marginRight: 20
    },
    verticalSeparator: {
        fontSize: 16,
        marginHorizontal: 4,
        color: "#4A4A4A"
    },
    linkButton: {
        fontSize: 14,
        color: "#4A4A4A"
    },
    lowerPart: {},
    menuItemContainer: {
        paddingVertical: 10
    },
    flashDealImage: {
        width: "100%"
    },
    hotline: {
        ...Platform.select({
            android: {
                fontSize: 14
            },
            ios: {},
        })
    },
    hotlineNumber: {
        color: colors.primaryColor,
        fontWeight: "bold",
        ...Platform.select({
            android: {
                fontSize: 14
            },
            ios: {},
        })
    },
    hotlineNumberContainer: {
        width: "100%",
        height: 64,
        justifyContent: "center",
        paddingHorizontal: 20
    },
    userFullName: {
        fontWeight: "bold",
        fontSize: Platform.select({ios: 14, android: 15}),
        color: "#4A4A4A"
    },
    userEmail: {
        fontSize: Platform.select({ios: 12, android: 13}),
        color: "#4A4A4A",
        marginTop: 3
    },
    environmentChanger: {
        fontSize: 24
    },
    menuItemIcon: {
        fontSize: 17,
        color: "#4A4A4A"
    },
});

export default styles;
