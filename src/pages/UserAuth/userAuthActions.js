import {Sentry} from "react-native-sentry";
import DeviceInfo from "react-native-device-info";

import {createAction, createAsyncAction} from "src/utils/api/createAction";
import userAuthActionTypes from "src/redux/actionTypes/userAuthActionTypes";
import {
    logInWithReadPermissions,
    logoutFromFacebook,
    receiveAccessToken,
    setFacebookLoginBehavior
} from "src/utils/userAuth/facebook";
import {tr} from "src/localization/localization";
import Message from "src/components/platformSpecific/Message/Message";
import {
    loginSteps,
    loginTypes as userLoginTypes,
    loginTypes,
    registrationSteps,
    userForgotPasswordStep
} from "src/constants/enums";
import errorCodes from "src/constants/errorCodes";
import {isEmptyString} from "src/utils/extensions/strings";
import {addDeviceToken, updateUserInfo} from "src/redux/common/sessionActions";
import {hashPasswordForLogin, hashPasswordForRegistration} from "src/utils/misc/hashPassword";
import {configGoogleLogin, logoutFromGoogle, receiveServerAuthCode} from "src/utils/userAuth/google";
import {popToRoot} from "src/utils/misc/navigator";
import {isValidEmail} from "src/utils/misc/formValidators";

const extractErrorMessage = ({error, message, result}) => error || message || result || "";

const failureHandler = (_, error) => {
    const message = extractErrorMessage(error);

    if (message === "error_params") {
        return;
    }

    if (!isEmptyString(message)) {
        Message.show("", tr(message));
    } else {
        Message.show("", tr(errorCodes.ERROR_LOGIN_FAILED));
    }
};

const getSocialLoginApiUrl = (loginType) => ({
    [loginTypes.FACEBOOK]: "/api/auth/facebook/sign_in",
    [loginTypes.GOOGLE]: "/api/auth/google/sign_in"
})[loginType] || "";

const updateLoginUserInfo = (userAuthData) => {
    return createAction({
        type: userAuthActionTypes.UPDATE_LOGIN_USER_INFO,
        payload: {...userAuthData}
    });
};

const verifyValidationCode = (phoneEmail, code, access_token, navigator, onLoginSucceed) => {
    const fieldName = isValidEmail(phoneEmail) ? "email" : "phone";

    return createAsyncAction({
        type: userAuthActionTypes.VERIFY_VALIDATION_CODE,
        payload: {
            method: "POST",
            request: "/api/auth/sms/verify",
            body: {
                [fieldName]: phoneEmail,
                code
            },
            headers: {
                "x-giigaa-access-token": access_token
            },
            options: {
                isAuth: true
            }
        },
        onSuccess: (dispatch, response) => {
            if (response.reply) {
                dispatch(updateLoginUserInfo(response.reply)); // to update the new access token
            }
            dispatch(saveUserAuthInfoToAsyncStorage(navigator, onLoginSucceed));
        },
        onFailure: failureHandler
    });
};

const linkSession = () => createAsyncAction({
    type: userAuthActionTypes.SESSION_LINK,
    payload: {
        method: "POST",
        request: "/session/link"
    }
});

const changeLoginStep = (step) => createAction({
    type: userAuthActionTypes.CHANGE_LOGIN_STEP,
    payload: {
        step
    }
});

const clearLoginData = () => {
    return createAction({
        type: userAuthActionTypes.CLEAR_LOGIN_DATA,
        payload: {}
    });
};

const saveUserAuthInfoToAsyncStorage = (navigator, onLoginSucceed) => async (dispatch, getState) => {
    const {user_info = {}} = getState().user_auth.login;
    let fcmToken = getState().session.notification.fcm_token;
    const uniqueId = DeviceInfo.getUniqueID();

    dispatch(updateUserInfo(user_info));
    dispatch(linkSession());
    dispatch(addDeviceToken({
        user_id: user_info.uid,
        token: fcmToken,
        device_id: uniqueId
    }));

    if (typeof onLoginSucceed === "function") {
        onLoginSucceed();
    } else {
        popToRoot(navigator);
    }
};

const assureCustomerExistence = (user_id, access_token, navigator, onLoginSucceed) => createAsyncAction({
    type: userAuthActionTypes.CHECK_CUSTOMER_EXISTENCE,
    payload: {
        method: "GET",
        request: "/customer/check_exist",
        params: {
            user_id
        },
        headers: {
            "x-giigaa-access-token": access_token
        }
    },
    onSuccess: (dispatch, response) => {
        if (!response.reply.is_existed) {
            dispatch(changeLoginStep(loginSteps.PHONE_INPUT));
        } else {
            dispatch(saveUserAuthInfoToAsyncStorage(navigator, onLoginSucceed));
        }
    },
    onFailure: failureHandler
});

const sendOneTimeCode = (code, loginType, navigator, onLoginSucceed) => createAsyncAction({
    type: userAuthActionTypes.SEND_ONE_TIME_CODE,
    payload: {
        method: "POST",
        request: getSocialLoginApiUrl(loginType),
        body: {code},
        options: {
            isAuth: true
        }
    },
    onSuccess: (dispatch, response) => {
        const {uid, access_token} = response.reply;

        dispatch(assureCustomerExistence(uid, access_token, navigator, onLoginSucceed));
    },
    onFailure: failureHandler
});

const loginWithFacebook = (navigator, onLoginSucceed) => async (dispatch) => {
    try {
        logoutFromFacebook();
        setFacebookLoginBehavior();
        await logInWithReadPermissions();
        const accessToken = await receiveAccessToken();

        dispatch(sendOneTimeCode(accessToken, loginTypes.FACEBOOK, navigator, onLoginSucceed));
    } catch (e) {
        const message = extractErrorMessage(e);

        Sentry.captureException(e);

        if (message !== errorCodes.ERROR_LOGIN_CANCELLED) {
            Message.show(JSON.stringify(e), tr("login_failed_message_box_title"));
        }
    }
};

const loginWithGoogle = (navigator, onLoginSucceed) => async (dispatch) => {
    try {
        await logoutFromGoogle();
        configGoogleLogin();
        const serverAuthCode = await receiveServerAuthCode();
        dispatch(sendOneTimeCode(serverAuthCode, loginTypes.GOOGLE, navigator, onLoginSucceed));
    } catch (e) {
        const message = extractErrorMessage(e);

        Sentry.captureException(e);
        Message.show("", tr(message));
    }
};

const deactivateValidationCode = () => {
    return createAction({
        type: userAuthActionTypes.EXPIRE_VALIDATION_CODE,
        payload: {}
    });
};

const generateValidationCode = (phoneEmail, access_token) => {
    const fieldName = isValidEmail(phoneEmail) ? "email" : "phone";

    return createAsyncAction({
        type: userAuthActionTypes.GENERATE_VERIFY_CODE,
        payload: {
            method: "POST",
            request: "/api/auth/sms/send_verify_code",
            body: {
                [fieldName]: phoneEmail
            },
            headers: {
                "x-giigaa-access-token": access_token
            },
            options: {
                isAuth: true
            }
        },
        onSuccess: (dispatch) => {
            dispatch(changeLoginStep(loginSteps.VALIDATION_CODE_INPUT));
        },
        onFailure: failureHandler
    });
};

const loginWithGiigaaID = ({account, password}, navigator, onLoginSucceed) => createAsyncAction({
    type: userAuthActionTypes.PRE_LOGIN_WITH_GIIGAA_ID,
    payload: {
        method: "POST",
        request: "/api/auth/prelogin",
        body: {
            account
        },
        options: {
            isAuth: true
        }
    },
    onSuccess: (dispatch, response) => {
        const {salt, verify_code} = response.reply;
        const password_hash = hashPasswordForLogin(password, salt, verify_code);

        dispatch(fetchAccessToken({account, password_hash}, navigator, onLoginSucceed));
    },
    onFailure: failureHandler
});

const fetchAccessToken = ({account, password_hash}, navigator, onLoginSucceed) => createAsyncAction({
    type: userAuthActionTypes.FETCH_ACCESS_TOKEN,
    payload: {
        method: "POST",
        request: "/api/auth/login",
        body: {
            account,
            password_hash
        },
        options: {
            isAuth: true
        }
    },
    onSuccess: (dispatch, response) => {
        dispatch(updateLoginUserInfo({...response.reply, login_type: loginTypes.GIIGAA_ID}));
        dispatch(saveUserAuthInfoToAsyncStorage(navigator, onLoginSucceed));
    },
    onFailure: failureHandler
});

const userRegistrationDeactivateValidationCode = () => {
    return createAction({
        type: userAuthActionTypes.USER_REGISTRATION_DEACTIVATE_VALIDATION_CODE,
        payload: {}
    });
};

const userRegistrationGenerateValidationCode = (phoneEmail) => {
    const fieldName = isValidEmail(phoneEmail) ? "email" : "phone";

    return createAsyncAction({
        type: userAuthActionTypes.USER_REGISTRATION_GENERATE_VALIDATION_CODE,
        payload: {
            method: "POST",
            request: "/api/auth/register/sms/send_verify_code",
            body: {
                [fieldName]: phoneEmail
            },
            options: {
                isAuth: true
            }
        },
        onSuccess: (dispatch) => {
            dispatch(changeUserRegistrationStep(registrationSteps.VALIDATION_CODE_INPUT));
        },
        onFailure: failureHandler
    });
};

const userRegistrationVerifyValidationCode = (phoneEmail, code) => {
    const fieldName = isValidEmail(phoneEmail) ? "email" : "phone";

    return createAsyncAction({
        type: userAuthActionTypes.USER_REGISTRATION_VERIFY_VALIDATION_CODE,
        payload: {
            method: "POST",
            request: "/api/auth/register/sms/verify",
            body: {
                [fieldName]: phoneEmail,
                code
            },
            options: {
                isAuth: true
            }
        },
        onSuccess: (dispatch, response) => {
            dispatch(userRegistrationPreRegister(response.reply.access_token));
        },
        onFailure: failureHandler
    });
};

const userRegistrationPreRegister = (access_token) => createAsyncAction({
    type: userAuthActionTypes.USER_REGISTRATION_PRE_REGISTER,
    payload: {
        method: "POST",
        request: "/api/auth/register/preregister",
        headers: {
            "x-giigaa-access-token": access_token
        },
        options: {
            isAuth: true
        }
    },
    onSuccess: (dispatch) => {
        dispatch(changeUserRegistrationStep(registrationSteps.USER_INFO_INPUT));
    },
    onFailure: failureHandler
});

const userRegistrationRegister = ({full_name, password, verify_code, salt, access_token}, navigator, onRegistrationSucceed) => {
    const hashedPassword = hashPasswordForRegistration(password, salt);

    return createAsyncAction({
        type: userAuthActionTypes.USER_REGISTRATION_REGISTER,
        payload: {
            method: "POST",
            request: "/api/auth/register/register",
            body: {
                full_name,
                password_hash: hashedPassword
            },
            headers: {
                "x-giigaa-access-token": access_token
            },
            options: {
                isAuth: true
            }
        },
        onSuccess: (dispatch, response) => {
            dispatch(userRegistrationSaveUserInfoToAsyncStorage({
                uid: response.reply.uid,
                full_name: response.reply.full_name,
                email: response.reply.email,
                login_type: userLoginTypes.GIIGAA_ID,
            }, navigator, onRegistrationSucceed));
        },
        onFailure: failureHandler
    });
};

const userRegistrationSaveUserInfoToAsyncStorage = ({uid, full_name, email, login_type}, navigator, onRegistrationSucceed) => {
    return (dispatch, getState) => {
        const access_token = getState().user_auth.registration.access_token;
        const expiry_time = getState().user_auth.registration.access_token_expiry_time;
        const fcmToken = getState().session.notification.fcm_token;
        const uniqueId = DeviceInfo.getUniqueID();
        const user_info = {
            uid,
            full_name,
            email,
            login_type,
            access_token,
            expiry_time
        };

        dispatch(updateUserInfo(user_info));
        dispatch(linkSession());
        dispatch(addDeviceToken({
            user_id: user_info.uid,
            token: fcmToken,
            device_id: uniqueId
        }));

        if (typeof onRegistrationSucceed === "function") {
            onRegistrationSucceed();
        } else {
            popToRoot(navigator);
        }
    };
};

const clearRegistrationData = () => createAction({
    type: userAuthActionTypes.CLEAR_REGISTRATION_DATA,
    payload: {}
});

const changeUserRegistrationStep = (step) => createAction({
    type: userAuthActionTypes.CHANGE_USER_REGISTRATION_STEP,
    payload: {step}
});


const clearForgotPasswordData = () => createAction({
    type: userAuthActionTypes.CLEAR_FORGOT_PASSWORD_DATA,
    payload: {}
});

const userForgotPasswordDeactivateValidationCode = () => createAction({
    type: userAuthActionTypes.USER_FORGOT_PASSWORD_DEACTIVATE_VALIDATION_CODE,
    payload: {}
});

const changeForgotPasswordStep = (step) => createAction({
    type: userAuthActionTypes.CHANGE_USER_FORGOT_PASSWORD_STEP,
    payload: {
        step
    }
});

const userForgotPasswordGenerateValidationCode = (phoneEmail) => {
    const fieldName = isValidEmail(phoneEmail) ? "email" : "phone";

    return createAsyncAction({
        type: userAuthActionTypes.USER_FORGOT_PASSWORD_GENERATE_VALIDATION_CODE,
        payload: {
            method: "POST",
            request: "/api/auth/forgot_password/sms/send_verify_code",
            body: {
                [fieldName]: phoneEmail
            },
            options: {
                isAuth: true
            }
        },
        onSuccess: (dispatch) => {
            dispatch(changeForgotPasswordStep(userForgotPasswordStep.VALIDATION_CODE_INPUT));
        },
        onFailure: failureHandler
    });
};

const userRestorePassword = ({password, salt, access_token}) => {
    const hashPassword = hashPasswordForRegistration(password, salt);

    return createAsyncAction({
        type: userAuthActionTypes.USER_RESTORE_PASSWORD,
        payload: {
            method: "POST",
            request: "/api/auth/forgot_password/change_password",
            body: {
                password_hash: hashPassword,
            },
            headers: {
                "x-giigaa-access-token": access_token
            },
            options: {
                isAuth: true
            }
        },
        onSuccess: (dispatch) => {
            dispatch(changeForgotPasswordStep(userForgotPasswordStep.RESET_PASSWORD_SUCCESSFULLY));
        },
        onFailure: failureHandler
    });
};

const userPreRestorePassword = (access_token) => createAsyncAction({
    type: userAuthActionTypes.USER_FORGOT_PASSWORD_PRE_RESTORE,
    payload: {
        method: "POST",
        request: "/api/auth/forgot_password/pre_change_password",
        headers: {
            "x-giigaa-access-token": access_token
        },
        options: {
            isAuth: true
        }
    },
    onSuccess: (dispatch) => {
        dispatch(changeForgotPasswordStep(userForgotPasswordStep.RESTORE_PASSWORD_INPUT));
    },
    onFailure: failureHandler
});

const userForgotPasswordVerifyValidationCode = (phoneEmail, code) => {
    const fieldName = isValidEmail(phoneEmail) ? "email" : "phone";

    return createAsyncAction({
        type: userAuthActionTypes.USER_FORGOT_PASSWORD_VERIFY_VALIDATION_CODE,
        payload: {
            method: "POST",
            request: "/api/auth/forgot_password/sms/verify",
            body: {
                [fieldName]: phoneEmail,
                code
            },
            options: {
                isAuth: true
            }
        },
        onSuccess: (dispatch, response) => {
            dispatch(userPreRestorePassword(response.reply.access_token));
        },
        onFailure: failureHandler
    });
};

export {
    verifyValidationCode,
    changeLoginStep,
    clearLoginData,
    generateValidationCode,
    deactivateValidationCode,
    loginWithFacebook,
    loginWithGiigaaID,
    loginWithGoogle,
    userRegistrationDeactivateValidationCode,
    userRegistrationGenerateValidationCode,
    userRegistrationVerifyValidationCode,
    userRegistrationRegister,
    clearRegistrationData,
    clearForgotPasswordData,
    userForgotPasswordDeactivateValidationCode,
    userForgotPasswordGenerateValidationCode,
    userForgotPasswordVerifyValidationCode,
    userRestorePassword,
    userPreRestorePassword,
    changeForgotPasswordStep
};
