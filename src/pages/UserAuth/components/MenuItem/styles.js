import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const MENU_ITEM_HEIGHT = 44;

const styles = StyleSheet.create({
    menuItem: {
        height: MENU_ITEM_HEIGHT,
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 10
    },
    menuItemText: {
        fontSize: 14,
        color: "#4A4A4A"
    },
    menuItemIconContainer: {
        width: MENU_ITEM_HEIGHT,
        height: MENU_ITEM_HEIGHT,
        marginRight: 6,
        ...commonStyles.centerChildren
    }
});

export default styles;
