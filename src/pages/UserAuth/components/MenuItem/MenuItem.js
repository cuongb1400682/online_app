import React from "react";
import {TouchableHighlight, View} from "react-native";

import styles from "./styles";
import defaultOnPressHandler from "src/utils/reactNative/defaultOnPressHandler";
import screenIds from "src/constants/screenIds";
import {isUserLoggedIn} from "src/utils/userAuth/userAuth";
import Text from "src/components/platformSpecific/Text/Text";

const MenuItem = ({text = "", icon, screenId, navigator, onPress, passProps = {}, requireLogin = false}) => (
    <TouchableHighlight
        onPress={
            defaultOnPressHandler({
                navigator,
                screenId: (!isUserLoggedIn() && requireLogin) ? screenIds.LOGIN : screenId,
                onPress,
                passProps
            })
        }
        underlayColor="#EEE"
    >
        <View style={styles.menuItem}>
            <View style={styles.menuItemIconContainer}>
                {icon}
            </View>
            <Text style={styles.menuItemText}>{text}</Text>
        </View>
    </TouchableHighlight>
);

export default MenuItem;
