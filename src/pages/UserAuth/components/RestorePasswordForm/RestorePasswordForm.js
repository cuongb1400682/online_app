import React, {Component} from "react";
import * as tComb from "tcomb-form-native";

import styles from "./styles";
import {tr} from "src/localization/localization";
import Message from "src/components/platformSpecific/Message/Message";
import {isEmptyArray} from "src/utils/extensions/arrays";
import {commonTextInputProps, PasswordString, RequiredString} from "src/utils/misc/formValidators";
import Button from "src/components/base/Button/Button";
import PropTypes from "prop-types";
import KeyboardAwareView from "src/components/platformSpecific/KeyboardAwareView/KeyboardAwareView";

const formStructure = tComb.struct({
    password: PasswordString,
    repeated_password: RequiredString,
});

const formOptions = {
    fields: {
        password: {
            ...commonTextInputProps,
            label: tr("user_registration_form_password_label"),
            placeholder: tr("user_registration_form_password_placeholder"),
            secureTextEntry: true,
        },
        repeated_password: {
            ...commonTextInputProps,
            label: tr("user_registration_form_repeated_password_label"),
            placeholder: tr("user_registration_form_repeated_password_placeholder"),
            secureTextEntry: true,
        }
    }
};

class RestorePasswordForm extends Component {
    static propTypes = {
        isWaiting: PropTypes.bool,
        onSubmit: PropTypes.func
    };

    static defaultProps = {
        isWaiting: false,
        onSubmit: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            formValues: {}
        };
    }

    isPasswordsMatched = () => {
        const {formValues = {}} = this.state;
        const matched = (formValues.password === formValues.repeated_password);

        if (!matched) {
            Message.show(
                tr("user_registration_form_password_unmatched_message"),
                tr("user_registration_form_password_unmatched_title")
            );
        }

        return matched;
    };

    validateValues = () => {
        const {errors = []} = this.formRef.validate();

        return isEmptyArray(errors);
    };

    onFormValuesChanged = (formValues = {}) => {
        this.setState({
            formValues
        });
    };

    onSubmit = () => {
        const {onSubmit} = this.props;

        if (!this.validateValues() || !this.isPasswordsMatched()) {
            return;
        }

        onSubmit(this.state.formValues);
    };

    render() {
        const {isWaiting} = this.props;

        return (
            <KeyboardAwareView style={styles.restorePasswordForm}>
                <tComb.form.Form
                    ref={ref => this.formRef = ref}
                    style={styles.form}
                    type={formStructure}
                    options={formOptions}
                    onChange={this.onFormValuesChanged}
                    value={this.state.formValues}
                />
                <Button
                    title={tr("restore_password_form_submit")}
                    type="fully-colored"
                    style={styles.submitButton}
                    onPress={this.onSubmit}
                    loading={isWaiting}
                />
            </KeyboardAwareView>
        );
    }
}

export default RestorePasswordForm;
