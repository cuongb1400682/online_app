import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    restorePasswordForm: {
        paddingHorizontal: 8,
        paddingVertical: 16
    },
    form: {
        flex: 1,
    },
    submitButton: {
        width: "100%",
        height: 40
    }
});

export default styles;
