import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import * as tComb from "tcomb-form-native";

import styles from "./styles";
import {tr} from "src/localization/localization";
import colors from "src/constants/colors";
import CircleNumber from "src/pages/UserAuth/components/CircleNumber/CircleNumber";
import LinkButton from "src/components/base/LinkButton/LinkButton";
import Button from "src/components/base/Button/Button";
import {requireField} from "src/utils/misc/formValidators";
import Text from "src/components/platformSpecific/Text/Text";
import KeyboardAwareView from "src/components/platformSpecific/KeyboardAwareView/KeyboardAwareView";

const formStructure = tComb.struct({
    validationCode: tComb.String
});

const formOptions = {
    fields: {
        validationCode: {
            label: tr("account_tab_user_validation_form_otp_input_prompt2"),
            selectionColor: colors.primaryColor,
            error: requireField
        }
    }
};

class UserValidationForm extends Component {
    static navigatorStyle = {
        navBarHidden: true
    };

    static propTypes = {
        phone: PropTypes.string.isRequired,
        submitButtonText: PropTypes.string,
        expiryTime: PropTypes.number,
        isValidCode: PropTypes.bool,
        isWaiting: PropTypes.bool,
        onSubmit: PropTypes.func.isRequired,
        deactivateValidationCode: PropTypes.func,
        generateNewCode: PropTypes.func,
    };

    static defaultProps = {
        phone: "",
        submitButtonText: tr("account_tab_user_validation_form_submit_button"),
        expiryTime: 0,
        isValidCode: false,
        isWaiting: false,
        onSubmit: () => {
        },
        deactivateValidationCode: () => {
        },
        generateNewCode: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            currentCount: 0,
            formValues: {
                validationCode: ""
            }
        };
    }

    componentDidMount() {
        this.timer = setInterval(this.countDown, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (!this.isValidationCodeActive()) {
            this.setState({
                currentCount: nextProps.expiryTime
            });
        }
    }

    countDown = () => {
        const {currentCount} = this.state;
        const {deactivateValidationCode, isValidCode} = this.props;

        if (currentCount === 0 && isValidCode) {
            deactivateValidationCode();
        }

        if (!this.isValidationCodeActive()) {
            return;
        }

        this.setState({
            currentCount: currentCount - 1
        });
    };

    isValidationCodeActive = () => {
        const {currentCount} = this.state;
        const {expiryTime, isValidCode} = this.props;

        return isValidCode && (currentCount > 0 && currentCount <= expiryTime);
    };

    handleGenerateNewCode = () => {
        const {phone, generateNewCode} = this.props;

        if (!this.isValidationCodeActive()) {
            generateNewCode(phone);
        }
    };

    handleSubmit = () => {
        const {onSubmit} = this.props;
        const {formValues = {}} = this.state;

        onSubmit(formValues);
    };

    renderCountDown = () => (
        <View style={styles.countDownContainer}>
            <CircleNumber number={this.isValidationCodeActive() ? this.state.currentCount : 0}/>
            <LinkButton
                title={tr("account_tab_user_validation_form_resend_button")}
                textStyle={styles.resendButton}
                disabled={this.isValidationCodeActive()}
                onPress={this.handleGenerateNewCode}
            />
        </View>
    );

    renderLabels = () => {
        const {phone} = this.props;

        return (
            <React.Fragment>
                <View style={styles.otpPromptContainer}>
                    <Text>{tr("account_tab_user_validation_form_phone_email_introduce")}</Text>
                    <Text style={styles.phoneEmail}>{phone}</Text>
                </View>
                <View style={styles.otpPromptContainer}>
                    <Text>{tr("account_tab_user_validation_form_otp_input_prompt1")}</Text>
                </View>
            </React.Fragment>
        );
    };

    renderButton = () => {
        const {submitButtonText, submitting, isWaiting} = this.props;

        return (
            <Button
                type="fully-colored"
                style={styles.submitButton}
                title={submitButtonText}
                loading={submitting || isWaiting}
                disabled={!this.isValidationCodeActive()}
                onPress={this.handleSubmit}
            />
        );
    };

    render() {
        return (
            <KeyboardAwareView style={styles.userValidationFormContent}>
                {this.renderLabels()}
                <tComb.form.Form
                    ref={ref => this.formRef = ref}
                    type={formStructure}
                    options={formOptions}
                    onChange={formValues => this.setState({formValues})}
                    value={this.state.formValues}
                />
                {this.renderCountDown()}
                {this.renderButton()}
            </KeyboardAwareView>
        );
    }
}

export default UserValidationForm;
