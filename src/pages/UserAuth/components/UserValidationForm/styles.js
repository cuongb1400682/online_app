import {StyleSheet} from "react-native";

import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    otpPromptContainer: {
        height: 38,
        flexDirection: "row",
        flexWrap: "wrap"
    },
    phoneEmail: {
        fontWeight: "bold"
    },
    userValidationFormContent: {
        flex: 1,
        paddingHorizontal: 12,
        paddingVertical: 10
    },
    countDownContainer: {
        width: "100%",
        height: 70,
        marginVertical: 30,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "space-between"
    },
    resendButton: {
        color: colors.primaryColor
    },
    submitButton: {
        height: 40,
    }
});

export default styles;
