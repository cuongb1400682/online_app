import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import Text from "src/components/platformSpecific/Text/Text";

class CircleNumber extends Component {
    static propTypes = {
        number: PropTypes.number,
    };

    static defaultProps = {
        number: 0,
    };

    render() {
        return (
            <View style={styles.circleNumber}>
                <Text>{this.props.number}</Text>
            </View>
        );
    }
}

export default CircleNumber;
