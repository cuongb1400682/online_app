import {StyleSheet} from "react-native";

import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    circleNumber: {
        width: 36,
        height: 36,
        borderRadius: 18,
        backgroundColor: "#ddd",
        ...commonStyles.centerChildren
    }
});

export default styles;
