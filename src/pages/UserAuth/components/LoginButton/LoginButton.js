import React from "react";
import {View, ActivityIndicator} from "react-native";

import styles from "./styles";
import Button from "src/components/base/Button/Button";
import colors from "src/constants/colors";
import {isEmptyString} from "src/utils/extensions/strings";
import FontIcon from "src/components/base/FontIcon/FontIcon";
import Text from "src/components/platformSpecific/Text/Text";

const LoginButton = ({onPress, style, title = "", color = colors.primaryColor, fontName = "fontawesome", name = "", loading, disabled}) => {
    const buttonBackgroundColor = {
        backgroundColor: color,
        borderColor: color
    };

    return (
        <Button
            type="fully-colored"
            onPress={onPress}
            style={[styles.loginButton, buttonBackgroundColor, style]}
            loading={loading}
            disabled={disabled}
        >
            <View style={styles.loginButtonIconContainer}>
                {!isEmptyString(name) &&
                <FontIcon fontName={fontName} name={name} style={styles.buttonIcon}/>}
                <View style={styles.verticalLine}/>
            </View>
            {loading && <ActivityIndicator color="#fff" animating style={styles.activityIndicator}/>}
            <Text style={styles.loginButtonText}>{title}</Text>
        </Button>
    );
};

export default LoginButton;
