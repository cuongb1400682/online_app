import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    loginButton: {
        ...commonStyles.hasShadow,
        height: 44,
        marginBottom: 8,
        marginHorizontal: 10
    },
    verticalLine: {
        width: 1,
        height: "70%",
        backgroundColor: "#fff",
        position: "absolute",
        right: 0
    },
    loginButtonIconContainer: {
        position: "absolute",
        left: 0,
        height: 44,
        width: 44,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        ...commonStyles.centerChildren
    },
    loginButtonText: {
        color: "#fff",
        fontSize: 14,
        fontWeight: "400"
    },
    buttonIcon: {
        fontSize: 18,
        color: "#fff"
    },
    activityIndicator: {
        marginRight: 8
    }
});

export default styles;
