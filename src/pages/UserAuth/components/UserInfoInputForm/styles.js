import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    content: {
        paddingHorizontal: 8,
        paddingVertical: 16
    },
    button: {
        width: "100%",
        height: 40,
        marginTop: 30
    },
    linkContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 20
    },
    link: {
        color: colors.primaryColor
    }
});

export default styles;
