import React, {Component} from "react";
import {View} from "react-native";
import * as tComb from "tcomb-form-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import PropTypes from "prop-types";

import styles from "./styles";
import commonStyles from "src/styles/commonStyles";
import {tr} from "src/localization/localization";
import {isEmptyArray} from "src/utils/extensions/arrays";
import Button from "src/components/base/Button/Button";
import Message from "src/components/platformSpecific/Message/Message";
import {commonTextInputProps, PasswordString, RequiredString} from "src/utils/misc/formValidators";
import KeyboardAwareView from "src/components/platformSpecific/KeyboardAwareView/KeyboardAwareView";

const formStructure = tComb.struct({
    name: RequiredString,
    password: PasswordString,
    repeated_password: RequiredString,
});

class UserInfoInputForm extends Component {
    static propTypes = {
        buttonTitle: PropTypes.string,
        isWaiting: PropTypes.bool,
        onSubmit: PropTypes.func,
        navigateToLogin: PropTypes.func,
        navigateToUserRegistration: PropTypes.func,
    };

    static defaultProps = {
        buttonTitle: tr("user_registration_form_register_button"),
        isWaiting: false,
        onSubmit: () => {
        },
        navigateToLogin: () => {
        },
        navigateToUserRegistration: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            formValues: {}
        };
    }

    handleSubmit = () => {
        if (this.validateValues() && this.isPasswordsMatched()) {
            const {name, password} = this.state.formValues || {};

            this.props.onSubmit({
                full_name: name,
                password
            });
        }
    };

    isPasswordsMatched = () => {
        const {formValues = {}} = this.state;
        const matched = (formValues.password === formValues.repeated_password);

        if (!matched) {
            Message.show(
                tr("user_registration_form_password_unmatched_message"),
                tr("user_registration_form_password_unmatched_title")
            );
        }

        return matched;
    };

    validateValues = () => {
        const {errors = []} = this.formRef.validate();

        return isEmptyArray(errors);
    };

    onFormValuesChanged = (formValues = {}) => {
        this.setState({
            formValues
        });
    };

    getFormOptions = () => {
        return {
            fields: {
                name: {
                    ...commonTextInputProps,
                    label: tr("user_registration_form_name_label"),
                    placeholder: tr("user_registration_form_name_placeholder"),
                    autoCapitalize: "words",
                },
                password: {
                    ...commonTextInputProps,
                    label: tr("user_registration_form_password_label"),
                    placeholder: tr("user_registration_form_password_placeholder"),
                    secureTextEntry: true,
                },
                repeated_password: {
                    ...commonTextInputProps,
                    label: tr("user_registration_form_repeated_password_label"),
                    placeholder: tr("user_registration_form_repeated_password_placeholder"),
                    secureTextEntry: true,
                }
            }
        };
    };

    render() {
        const {
            buttonTitle,
            isWaiting,
            navigateToLogin,
            navigateToUserRegistration
        } = this.props;

        return (
            <View style={commonStyles.matchParent}>
                <KeyboardAwareView style={styles.content}>
                    <tComb.form.Form
                        ref={ref => this.formRef = ref}
                        style={styles.form}
                        type={formStructure}
                        options={this.getFormOptions()}
                        onChange={this.onFormValuesChanged}
                        value={this.state.formValues}
                    />
                    <Button
                        type="fully-colored"
                        style={styles.button}
                        loading={isWaiting}
                        title={buttonTitle}
                        onPress={this.handleSubmit}
                    />
                </KeyboardAwareView>
            </View>
        );
    }
}

export default UserInfoInputForm;
