import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    phoneEmailForm: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 16
    },
    statusBar: {
        backgroundColor: "#CA2E10"
    },
    submitButton: {
        height: 40
    }
});

export default styles;
