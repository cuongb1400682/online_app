import React, {Component} from "react";
import {KeyboardAvoidingView} from "react-native";
import * as tComb from "tcomb-form-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import colors from "src/constants/colors";
import {validatePhoneEmail} from "src/utils/misc/formValidators";
import Button from "src/components/base/Button/Button";
import KeyboardAwareView from "src/components/platformSpecific/KeyboardAwareView/KeyboardAwareView";

const formStructure = tComb.struct({
    phoneEmail: tComb.String
});

const formOptions = {
    fields: {
        phoneEmail: {
            label: tr("account_tab_login_phone_input_label"),
            placeholder: tr("account_tab_login_phone_input_placeholder"),
            selectionColor: colors.primaryColor,
            autoCapitalize: "none",
            error: validatePhoneEmail
        }
    }
};

class PhoneEmailForm extends Component {
    static navigatorStyle = {
        navBarHidden: true
    };

    static propTypes = {
        submitButtonText: PropTypes.string,
        isWaiting: PropTypes.bool,
        onSubmit: PropTypes.func,
    };

    static defaultProps = {
        submitButtonText: tr("account_tab_login_phone_input_submit_button"),
        isWaiting: false,
        onSubmit: () => {
        }
    };

    submit = () => {
        const {onSubmit} = this.props;

        if (this.formRef && typeof this.formRef.getValue === "function") {
            const values = this.formRef.getValue() || {};

            onSubmit(values);
        }
    };

    render() {
        const {isWaiting, submitButtonText} = this.props;

        return (
            <KeyboardAwareView style={styles.phoneEmailForm}>
                <tComb.form.Form
                    ref={ref => this.formRef = ref}
                    type={formStructure}
                    options={formOptions}
                />
                <Button
                    type="fully-colored"
                    title={submitButtonText}
                    style={styles.submitButton}
                    loading={isWaiting}
                    onPress={this.submit}
                />
            </KeyboardAwareView>
        );
    }
}

export default PhoneEmailForm;
