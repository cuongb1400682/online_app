import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    passwordRestoreSuccessfully: {
        ...commonStyles.centerChildren,
        paddingVertical: 48
    },
    circleCheck: {
        width: 60,
        height: 60,
        paddingVertical: 36
    },
    passwordRestoreText: {
        paddingVertical: 16,
        fontSize: 16
    },
    passwordRestoreLink: {
        fontSize: 12,
        color: colors.primaryColor
    }
});

export default styles;
