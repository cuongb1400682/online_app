import React, {Component} from "react";
import {View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

import styles from "./styles";
import {
    clearForgotPasswordData,
    userForgotPasswordDeactivateValidationCode,
    userForgotPasswordGenerateValidationCode,
    userForgotPasswordVerifyValidationCode,
    userRestorePassword
} from "src/pages/UserAuth/userAuthActions";
import {userForgotPasswordStep} from "src/constants/enums";
import RestorePasswordForm from "src/pages/UserAuth/components/RestorePasswordForm/RestorePasswordForm";
import {isValidEmail, isValidPhoneNumber} from "src/utils/misc/formValidators";
import PhoneEmailForm from "src/pages/UserAuth/components/PhoneEmailForm/PhoneEmailForm";
import UserValidationForm from "src/pages/UserAuth/components/UserValidationForm/UserValidationForm";
import CircleCheck from "src/components/base/svgIcons/CircleCheck/CircleCheck";
import {tr} from "src/localization/localization";
import LinkButton from "src/components/base/LinkButton/LinkButton";
import {popToRoot, pushTo} from "src/utils/misc/navigator";
import commonStyles from "src/styles/commonStyles";
import NavBar from "src/containers/NavBar/NavBar";
import screenIds from "src/constants/screenIds";
import Text from "src/components/platformSpecific/Text/Text";
import KeyboardAwareView from "src/components/platformSpecific/KeyboardAwareView/KeyboardAwareView";
import Message from "src/components/platformSpecific/Message/Message";

class UserForgotPassword extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        tabBarHidden: true,
    };

    static propTypes = {
        forgotPasswordStep: PropTypes.string,
        validationCodeExpiryTime: PropTypes.number,
        isValidCode: PropTypes.bool,
        isWaitingForRestorePassword: PropTypes.bool,
        clearForgotPasswordData: PropTypes.func,
        deactivateValidationCode: PropTypes.func,
        generateValidationCode: PropTypes.func,
        verifyValidationCode: PropTypes.func,
        restore: PropTypes.func,
        verifyCode: PropTypes.string,
        salt: PropTypes.string,
        accessToken: PropTypes.string,
    };

    static defaultProps = {
        forgotPasswordStep: userForgotPasswordStep.PHONE_INPUT,
        validationCodeExpiryTime: 0,
        isValidCode: false,
        isWaitingForRestorePassword: false,
        verifyCode: "",
        salt: "",
        accessToken: "",
        clearForgotPasswordData: () => {
        },
        deactivateValidationCode: () => {
        },
        generateValidationCode: () => {
        },
        verifyValidationCode: () => {
        },
        restore: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            phone: "",
        };
    }

    componentWillMount() {
        const {clearForgotPasswordData} = this.props;

        clearForgotPasswordData();
    }

    generateNewValidationCode = (phoneEmail) => {
        const {generateValidationCode, deactivateValidationCode} = this.props;

        deactivateValidationCode();
        generateValidationCode(phoneEmail);
    };

    submitPhone = ({phoneEmail}) => {
        if (!isValidPhoneNumber(phoneEmail) && !isValidEmail(phoneEmail)) {
            Message.show(
                tr("user_forgot_password_invalid_phone_email_message"),
                tr("user_forgot_password_invalid_phone_email_title")
            );
            return;
        }

        this.setState({phone: phoneEmail});
        this.generateNewValidationCode(phoneEmail);
    };

    submitValidationCode = ({validationCode}) => {
        const {verifyValidationCode} = this.props;
        const {phone} = this.state;

        verifyValidationCode(phone, validationCode);
    };

    submitNewPassword = ({password}) => {
        const {salt, accessToken} = this.props;

        this.props.restore({
            password,
            salt,
            access_token: accessToken
        });
    };

    goBackToLogin = () => {
        const {navigator} = this.props;

        popToRoot(navigator, {animated: false});

        pushTo(navigator, {
            screen: screenIds.LOGIN
        });
    };

    renderPhoneInputForm = () => {
        const {isWaitingForRestorePassword} = this.props;

        return (
            <PhoneEmailForm
                onSubmit={this.submitPhone}
                isWaiting={isWaitingForRestorePassword}
                navigateToLogin={this.goToLoginWithSocialAccount}
            />
        );
    };

    renderRestorePasswordForm = () => {
        return (
            <RestorePasswordForm
                onSubmit={this.submitNewPassword}
            />
        );
    };

    renderPasswordRestoreSuccessfully = () => {
        return (
            <KeyboardAwareView>
                <View style={styles.passwordRestoreSuccessfully}>
                    <CircleCheck style={styles.circleCheck}/>
                    <Text style={styles.passwordRestoreText}>{tr("password_restore_successfully")}</Text>
                    <LinkButton
                        textStyle={styles.passwordRestoreLink}
                        title={tr("password_restore_successfully_back")}
                        onPress={this.goBackToLogin}
                    />
                </View>
            </KeyboardAwareView>
        );
    };

    renderUserValidationForm = () => {
        const {
            validationCodeExpiryTime,
            isValidCode,
            deactivateValidationCode,
            isWaitingForRestorePassword
        } = this.props;

        return (
            <UserValidationForm
                onSubmit={this.submitValidationCode}
                phone={this.state.phone}
                expiryTime={validationCodeExpiryTime}
                isValidCode={isValidCode}
                generateNewCode={this.generateNewValidationCode}
                deactivateValidationCode={deactivateValidationCode}
                isWaiting={isWaitingForRestorePassword}
            />
        );
    };

    renderUserForgotPasswordStep() {
        const {forgotPasswordStep} = this.props;

        const renderFunc = ({
            [userForgotPasswordStep.PHONE_INPUT]: this.renderPhoneInputForm,
            [userForgotPasswordStep.VALIDATION_CODE_INPUT]: this.renderUserValidationForm,
            [userForgotPasswordStep.RESTORE_PASSWORD_INPUT]: this.renderRestorePasswordForm,
            [userForgotPasswordStep.RESET_PASSWORD_SUCCESSFULLY]: this.renderPasswordRestoreSuccessfully,
        })[forgotPasswordStep] || this.renderPhoneInputForm;

        return renderFunc();
    }

    render() {
        const {navigator} = this.props;

        return (
            <View style={commonStyles.matchParent}>
                <NavBar navigator={navigator} title={tr("user_forgot_password_nav_bar_title")}/>
                {this.renderUserForgotPasswordStep()}
            </View>
        );
    }
}

const mapStateToProps = state => ({
    forgotPasswordStep: state.user_auth.forgot_password.step,
    isWaitingForRestorePassword: state.user_auth.forgot_password.waiting,
    validationCodeExpiryTime: state.user_auth.forgot_password.expiry_time,
    isValidCode: state.user_auth.forgot_password.is_valid_code,
    verifyCode: state.user_auth.forgot_password.code,
    salt: state.user_auth.forgot_password.salt,
    accessToken: state.user_auth.forgot_password.access_token
});

export default connect(mapStateToProps, {
    clearForgotPasswordData,
    deactivateValidationCode: userForgotPasswordDeactivateValidationCode,
    generateValidationCode: userForgotPasswordGenerateValidationCode,
    verifyValidationCode: userForgotPasswordVerifyValidationCode,
    restore: userRestorePassword
})(UserForgotPassword);
