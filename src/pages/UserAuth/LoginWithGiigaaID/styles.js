import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    content: {
        paddingHorizontal: 8,
        paddingVertical: 24,
        flexDirection: "column"
    },
    facebook: {
        color: "#1E529A"
    },
    gmail: {
        color: "#DE4E3C"
    },
    otherLoginMethods: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "center"
    },
    loginButton: {
        height: 36,
        marginTop: 26
    },
    otherLinks: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 12
    },
    linkButton: {
        color: colors.primaryColor
    }
});

export default styles;
