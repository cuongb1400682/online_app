import React, {Component} from "react";
import {View} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import * as tComb from "tcomb-form-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import NavBar from "src/containers/NavBar/NavBar";
import commonStyles from "src/styles/commonStyles";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import Button from "src/components/base/Button/Button";
import LinkButton from "src/components/base/LinkButton/LinkButton";
import {popToRoot, pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";
import {requireField} from "src/utils/misc/formValidators";
import colors from "src/constants/colors";
import {loginWithGiigaaID} from "src/pages/UserAuth/userAuthActions";
import {isEmptyArray} from "src/utils/extensions/arrays";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";
import Text from "src/components/platformSpecific/Text/Text";
import KeyboardAwareView from "src/components/platformSpecific/KeyboardAwareView/KeyboardAwareView";

const formStructure = tComb.struct({
    username: tComb.String,
    password: tComb.String
});

const formOptions = {
    fields: {
        username: {
            label: tr("login_with_giigaa_id_username_label"),
            placeholder: tr("login_with_giigaa_id_username_placeholder"),
            selectionColor: colors.primaryColor,
            autoCapitalize: "none",
            error: requireField
        },
        password: {
            label: tr("login_with_giigaa_id_password_label"),
            placeholder: tr("login_with_giigaa_id_password_placeholder"),
            selectionColor: colors.primaryColor,
            error: requireField,
            secureTextEntry: true
        },
    }
};

class LoginWithGiigaaID extends Component {
    static navigatorStyle = {
        navBarHidden: true,
    };

    static propTypes = {
        isWaitingForLogin: PropTypes.bool,
        loginWithGiigaaID: PropTypes.func,
        onLoginSucceed: PropTypes.func
    };

    static defaultProps = {
        isWaitingForLogin: false,
        loginWithGiigaaID: () => {
        },
        onLoginSucceed: null
    };

    constructor(props) {
        super(props);

        this.state = {
            formValues: {}
        };
    }

    goBack = () => {
        const {navigator} = this.props;

        popToRoot(navigator, {animated: false});
        pushTo(navigator, {screen: screenIds.LOGIN});
    };

    handleFormSubmit = () => {
        const {loginWithGiigaaID, navigator, onLoginSucceed} = this.props;
        const {formValues} = this.state;

        if (this.validateValues()) {
            const {username = "", password = ""} = formValues;

            loginWithGiigaaID({account: username, password}, navigator, onLoginSucceed);
        }
    };

    validateValues = () => {
        const {errors = []} = this.formRef.validate();

        return isEmptyArray(errors);
    };

    renderNavBar = () => (
        <NavBar
            title={tr("login_with_giigaa_id_nav_bar_title")}
            right={
                <NavBarShortcuts
                    showsCart
                    showsSearch
                    showsVerticalEllipsis
                />
            }
            navigator={this.props.navigator}
        />
    );

    renderLinkToOtherLoginMethods = () => (
        <LinkButton
            touchableStyle={styles.otherLoginMethods}
            title={
                <Text>
                    {tr("login_with_giigaa_id_change_login_method_text_1")}
                    <Text style={styles.facebook}>
                        {tr("login_with_giigaa_id_change_login_method_facebook")}
                    </Text>
                    {tr("login_with_giigaa_id_change_login_method_text_2")}
                    <Text style={styles.gmail}>
                        {tr("login_with_giigaa_id_change_login_method_gmail")}
                    </Text>
                </Text>
            }
            onPress={this.goBack}
        />
    );

    renderLoginButton = () => {
        const {isWaitingForLogin} = this.props;

        return (
            <Button
                type="fully-colored"
                style={styles.loginButton}
                title={tr("login_with_giigaa_id_change_login_button")}
                onPress={this.handleFormSubmit}
                loading={isWaitingForLogin}
            />
        );
    };

    renderOtherLinks = () => {
        const {navigator} = this.props;

        return (
            <View style={styles.otherLinks}>
                <LinkButton
                    title={tr("login_with_giigaa_id_forgot_password")}
                    navigator={navigator}
                    screenId={screenIds.USER_FORGOT_PASSWORD}
                    textStyle={styles.linkButton}
                />
                <LinkButton
                    title={tr("login_with_giigaa_id_register")}
                    navigator={navigator}
                    screenId={screenIds.USER_REGISTRATION}
                    textStyle={styles.linkButton}
                />
            </View>
        );
    };

    render() {
        return (
            <View style={commonStyles.matchParent}>
                {this.renderNavBar()}
                <KeyboardAwareView style={styles.content}>
                    <tComb.form.Form
                        ref={ref => this.formRef = ref}
                        style={styles.form}
                        type={formStructure}
                        options={formOptions}
                        onChange={formValues => this.setState({formValues}, this.validateValues)}
                        value={this.state.formValues}
                    />
                    {this.renderLinkToOtherLoginMethods()}
                    {this.renderLoginButton()}
                    {this.renderOtherLinks()}
                </KeyboardAwareView>
                <FacebookMessenger floating/>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    isWaitingForLogin: state.user_auth.login.waiting
});

export default connect(mapStateToProps, {
    loginWithGiigaaID
})(LoginWithGiigaaID);
