import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    loginButtonsContainer: {
        width: "100%",
    },
    loginContent: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center"
    },
    linkButtonsContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginHorizontal: 10,
        marginTop: 18
    },
    linkButton: {
        color: colors.primaryColor
    }
});

export default styles;
