import React, {Component} from "react";
import {View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import {tr} from "src/localization/localization";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import LoginButton from "src/pages/UserAuth/components/LoginButton/LoginButton";
import commonStyles from "src/styles/commonStyles";
import {
    changeLoginStep,
    clearLoginData,
    deactivateValidationCode,
    generateValidationCode,
    loginWithFacebook,
    loginWithGoogle,
    verifyValidationCode
} from "src/pages/UserAuth/userAuthActions";
import PhoneEmailForm from "src/pages/UserAuth/components/PhoneEmailForm/PhoneEmailForm";
import {loginSteps, loginTypes} from "src/constants/enums";
import UserValidationForm from "src/pages/UserAuth/components/UserValidationForm/UserValidationForm";
import LinkButton from "src/components/base/LinkButton/LinkButton";
import {pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";

class Login extends Component {
    static navigatorStyle = {
        navBarHidden: true
    };

    static propTypes = {
        loginStep: PropTypes.string,
        userInfo: PropTypes.any,
        expiryTime: PropTypes.number,
        isValidCode: PropTypes.bool,
        isWaiting: PropTypes.bool,
        onLoginSucceed: PropTypes.func,
        loginWithFacebook: PropTypes.func,
        loginWithGoogle: PropTypes.func,
        clearLoginData: PropTypes.func,
        changeLoginStep: PropTypes.func,
        generateValidationCode: PropTypes.func,
        deactivateValidationCode: PropTypes.func,
        verifyValidationCode: PropTypes.func,
    };

    static defaultProps = {
        loginStep: loginSteps.LOGIN_WITH_SOCIAL_ACCOUNT,
        userInfo: {},
        expiryTime: 0,
        isValidCode: false,
        isWaiting: false,
        onLoginSucceed: null,
        loginWithFacebook: () => {
        },
        loginWithGoogle: () => {
        },
        clearLoginData: () => {
        },
        changeLoginStep: () => {
        },
        generateValidationCode: () => {
        },
        deactivateValidationCode: () => {
        },
        verifyValidationCode: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            phoneEmail: "",
            clickedButton: ""
        };
    }

    componentWillUnmount() {
        this.props.clearLoginData();
    }

    handleLoginWithSocial = (loginType) => () => {
        const {loginWithGoogle, loginWithFacebook, navigator, onLoginSucceed} = this.props;

        const loginHandler = ({
            [loginTypes.GOOGLE]: loginWithGoogle,
            [loginTypes.FACEBOOK]: loginWithFacebook
        })[loginType];

        if (typeof loginHandler === "function") {
            loginHandler(navigator, onLoginSucceed);
            this.setState({clickedButton: loginType});
        }
    };

    generateNewValidationCode = (phoneEmail) => {
        const {generateValidationCode, userInfo, deactivateValidationCode} = this.props;

        deactivateValidationCode();
        generateValidationCode(phoneEmail, userInfo.access_token);
    };

    submitEmailPhone = ({phoneEmail}) => {
        this.setState({phoneEmail});

        if (phoneEmail) {
            this.generateNewValidationCode(phoneEmail);
        }
    };

    submitValidationCode = ({validationCode}) => {
        const {userInfo, verifyValidationCode, navigator, onLoginSucceed} = this.props;
        const {phoneEmail} = this.state;

        if (validationCode) {
            verifyValidationCode(phoneEmail, validationCode, userInfo.access_token, navigator, onLoginSucceed);
        }
    };

    showLoginWithGiigaaID = () => {
        const {navigator, onLoginSucceed} = this.props;

        pushTo(navigator, {
            screen: screenIds.LOGIN_WITH_GIIGAA_ID,
            passProps: {
                onLoginSucceed
            }
        });
    };

    showUserRegistration = () => {
        const {navigator, onLoginSucceed} = this.props;

        pushTo(navigator, {
            screen: screenIds.USER_REGISTRATION,
            passProps: {
                onRegistrationSucceed: onLoginSucceed
            }
        })
    };

    renderPhoneEmailInput = () => {
        const {isWaiting} = this.props;

        return (
            <PhoneEmailForm
                onSubmit={this.submitEmailPhone}
                isWaiting={isWaiting}
            />
        );
    };

    renderUserValidationForm = () => {
        const {
            expiryTime,
            isValidCode,
            isWaiting,
            deactivateValidationCode
        } = this.props;
        const {phoneEmail} = this.state;

        return (
            <UserValidationForm
                onSubmit={this.submitValidationCode}
                generateNewCode={this.generateNewValidationCode}
                phone={phoneEmail}
                expiryTime={expiryTime}
                isValidCode={isValidCode}
                deactivateValidationCode={deactivateValidationCode}
                submitButtonText="Xác nhận"
                isWaiting={isWaiting}
            />
        );
    };

    renderNavBarShortcut = () => (
        <NavBarShortcuts
            showsCart
            showsSearch
            showsVerticalEllipsis
            navigator={this.props.navigator}
        />
    );

    renderNavBar = () => (
        <NavBar
            navigator={this.props.navigator}
            title={tr("account_tab_login_nav_bar_title")}
            right={this.renderNavBarShortcut()}
        />
    );

    renderLoginButtons = () => {
        const {isWaiting, navigator} = this.props;
        const usingFacebookLogin = this.state.clickedButton === loginTypes.FACEBOOK;
        const usingGoogleLogin = this.state.clickedButton === loginTypes.GOOGLE;

        return (
            <View style={styles.loginContent}>
                <View style={styles.loginButtonsContainer}>
                    <LoginButton
                        title={tr("account_tab_login_facebook_button")}
                        color="#224598"
                        name="facebook-f"
                        fontName="fontawesome"
                        onPress={this.handleLoginWithSocial(loginTypes.FACEBOOK)}
                        loading={usingFacebookLogin && isWaiting}
                        disabled={isWaiting}
                    />
                    <LoginButton
                        title={tr("account_tab_login_google_plus_button")}
                        color="#DC4E41"
                        name="google-plus"
                        fontName="fontawesome"
                        onPress={this.handleLoginWithSocial(loginTypes.GOOGLE)}
                        loading={usingGoogleLogin && isWaiting}
                        disabled={isWaiting}
                    />
                    <View style={styles.linkButtonsContainer}>
                        <LinkButton
                            title={tr("account_tab_login_already_has_giigaa_id")}
                            textStyle={styles.linkButton}
                            onPress={this.showLoginWithGiigaaID}
                        />
                        <LinkButton
                            title={tr("account_tab_login_register")}
                            textStyle={styles.linkButton}
                            onPress={this.showUserRegistration}
                        />
                    </View>
                </View>
            </View>
        );
    };

    renderCurrentLoginStep = () => {
        const {loginStep} = this.props;

        const renderFunc = ({
            [loginSteps.PHONE_INPUT]: this.renderPhoneEmailInput,
            [loginSteps.VALIDATION_CODE_INPUT]: this.renderUserValidationForm,
        })[loginStep] || this.renderLoginButtons;

        return renderFunc();
    };

    render() {
        return (
            <View style={commonStyles.matchParent}>
                {this.renderNavBar()}
                {this.renderCurrentLoginStep()}
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    loginStep: state.user_auth.login.step,
    userInfo: state.user_auth.login.user_info,
    expiryTime: state.user_auth.login.expiry_time,
    isValidCode: state.user_auth.login.is_valid_code,
    isWaiting: state.user_auth.login.waiting,
});

export default connect(mapStateToProps, {
    loginWithFacebook,
    loginWithGoogle,
    clearLoginData,
    changeLoginStep,
    generateValidationCode,
    deactivateValidationCode,
    verifyValidationCode,
})(Login);
