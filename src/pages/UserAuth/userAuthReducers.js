import userAuthActionTypes from "src/redux/actionTypes/userAuthActionTypes";
import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import {loginSteps, registrationSteps, userForgotPasswordStep} from "src/constants/enums";

const defaultLoginState = {
    step: loginSteps.LOGIN_WITH_SOCIAL_ACCOUNT,
    code: "",
    is_valid_code: false,
    login_type: "",
    expiry_time: 0,
    user_info: {},
    waiting: false
};

const defaultRegistrationState = {
    step: registrationSteps.PHONE_INPUT,
    code: "",
    expiry_time: 0, // code's expiry time
    is_valid_code: false,
    waiting: false,
    salt: "",
    access_token: "",
    access_token_expiry_time: ""
};

const defaultForgotPasswordState = {
    step: userForgotPasswordStep.PHONE_INPUT,
    code: "",
    expiry_time: 0, // code's expiry time
    is_valid_code: false,
    waiting: false,
    salt: "",
    access_token: "",
    access_token_expiry_time: ""
};


const initialState = {
    login: {
        ...defaultLoginState
    },
    registration: {
        ...defaultRegistrationState
    },
    forgot_password:{
        ...defaultForgotPasswordState
    }
};

const userAuthReducers = (state = initialState, {type, payload}) => {
    switch (type) {
        case userAuthActionTypes.USER_REGISTRATION_DEACTIVATE_VALIDATION_CODE:
            return {
                ...state,
                registration: {
                    ...state.registration,
                    is_valid_code: false
                }
            };
        case toRequest(userAuthActionTypes.USER_REGISTRATION_REGISTER):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    waiting: true
                }
            };
        case toFailure(userAuthActionTypes.USER_REGISTRATION_REGISTER):
        case toSuccess(userAuthActionTypes.USER_REGISTRATION_REGISTER):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    waiting: false
                }
            };
        case userAuthActionTypes.CLEAR_REGISTRATION_DATA:
            return {
                ...state,
                registration: {
                    ...defaultRegistrationState
                }
            };
        case toRequest(userAuthActionTypes.USER_REGISTRATION_VERIFY_VALIDATION_CODE):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    code: payload.body.code,
                    access_token: "",
                    waiting: true
                }
            };
        case toFailure(userAuthActionTypes.USER_REGISTRATION_VERIFY_VALIDATION_CODE):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    code: "",
                    access_token: "",
                    waiting: false
                }
            };
        case toSuccess(userAuthActionTypes.USER_REGISTRATION_VERIFY_VALIDATION_CODE):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    access_token: payload.reply.access_token,
                    access_token_expiry_time: payload.reply.expiry_time,
                    waiting: false
                }
            };
        case toRequest(userAuthActionTypes.USER_REGISTRATION_PRE_REGISTER):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    salt: "",
                    waiting: true
                }
            };
        case toFailure(userAuthActionTypes.USER_REGISTRATION_PRE_REGISTER):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    salt: "",
                    waiting: false
                }
            };
        case toSuccess(userAuthActionTypes.USER_REGISTRATION_PRE_REGISTER):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    salt: payload.reply.salt,
                    waiting: false
                }
            };
        case toRequest(userAuthActionTypes.USER_REGISTRATION_GENERATE_VALIDATION_CODE):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    is_valid_code: false,
                    expiry_time: 0,
                    waiting: true
                }
            };
        case toFailure(userAuthActionTypes.USER_REGISTRATION_GENERATE_VALIDATION_CODE):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    is_valid_code: false,
                    expiry_time: 0,
                    waiting: false
                }
            };
        case toSuccess(userAuthActionTypes.USER_REGISTRATION_GENERATE_VALIDATION_CODE):
            return {
                ...state,
                registration: {
                    ...state.registration,
                    is_valid_code: true,
                    expiry_time: payload.reply.expiry_time,
                    waiting: false
                }
            };
        case userAuthActionTypes.CHANGE_USER_REGISTRATION_STEP:
            return {
                ...state,
                registration: {
                    ...state.registration,
                    step: payload.step
                }
            };
        case userAuthActionTypes.CLEAR_LOGIN_DATA: {
            return {
                ...state,
                login: {
                    ...defaultLoginState
                }
            };
        }
        case toRequest(userAuthActionTypes.ASK_FOR_SOCIAL_PERMISSION):
            return {
                ...state,
                login: {
                    ...state.login,
                    waiting: true
                }
            };
        case toFailure(userAuthActionTypes.ASK_FOR_SOCIAL_PERMISSION):
        case toSuccess(userAuthActionTypes.ASK_FOR_SOCIAL_PERMISSION):
            return {
                ...state,
                login: {
                    ...state.login,
                    waiting: false
                }
            };
        case userAuthActionTypes.SAVE_LOGIN_TYPE:
            return {
                ...state,
                login: {
                    ...state.login,
                    login_type: payload.login_type
                }
            };
        case toFailure(userAuthActionTypes.GENERATE_VERIFY_CODE):
            return {
                ...state,
                login: {
                    ...state.login,
                    expiry_time: 0,
                    is_valid_code: false,
                    waiting: false
                }
            };
        case toRequest(userAuthActionTypes.GENERATE_VERIFY_CODE):
            return {
                ...state,
                login: {
                    ...state.login,
                    expiry_time: 0,
                    is_valid_code: false,
                    waiting: true
                }
            };
        case toSuccess(userAuthActionTypes.GENERATE_VERIFY_CODE):
            return {
                ...state,
                login: {
                    ...state.login,
                    expiry_time: payload.reply.expiry_time,
                    is_valid_code: true,
                    waiting: false
                }
            };
        case userAuthActionTypes.UPDATE_LOGIN_USER_INFO:
            return {
                ...state,
                login: {
                    ...state.login,
                    user_info: {
                        ...payload
                    }
                }
            };
        case userAuthActionTypes.EXPIRE_VALIDATION_CODE:
            return {
                ...state,
                login: {
                    ...state.login,
                    is_valid_code: false
                }
            };
        case userAuthActionTypes.CHANGE_LOGIN_STEP:
            return {
                ...state,
                login: {
                    ...state.login,
                    step: payload.step
                }
            };
        case toRequest(userAuthActionTypes.SEND_ONE_TIME_CODE):
            return {
                ...state,
                login: {
                    ...state.login,
                    user_info: {},
                    waiting: true
                }
            };
        case toFailure(userAuthActionTypes.SEND_ONE_TIME_CODE):
            return {
                ...state,
                login: {
                    ...state.login,
                    user_info: {},
                    waiting: false
                }
            };
        case toSuccess(userAuthActionTypes.SEND_ONE_TIME_CODE):
            return {
                ...state,
                login: {
                    ...state.login,
                    user_info: {
                        ...payload.reply
                    },
                    waiting: false
                }
            };

        case userAuthActionTypes.USER_FORGOT_PASSWORD_DEACTIVATE_VALIDATION_CODE:
            return{
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    is_valid_code: false
                }
            };

        case toRequest(userAuthActionTypes.USER_FORGOT_PASSWORD_GENERATE_VALIDATION_CODE):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    is_valid_code: false,
                    expiry_time: 0,
                    waiting: true
                }
            };

        case toFailure(userAuthActionTypes.USER_FORGOT_PASSWORD_GENERATE_VALIDATION_CODE):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    is_valid_code: false,
                    expiry_time: 0,
                    waiting: false
                }
            };

        case toSuccess(userAuthActionTypes.USER_FORGOT_PASSWORD_GENERATE_VALIDATION_CODE):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    is_valid_code: true,
                    expiry_time: payload.reply.expiry_time,
                    waiting: false
                }
            };

        case toRequest(userAuthActionTypes.USER_FORGOT_PASSWORD_VERIFY_VALIDATION_CODE):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    code: payload.body.code,
                    access_token: "",
                    waiting: true
                }
            };

        case toFailure(userAuthActionTypes.USER_FORGOT_PASSWORD_VERIFY_VALIDATION_CODE):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    code: "",
                    access_token: "",
                    waiting: false
                }
            };

        case toSuccess(userAuthActionTypes.USER_FORGOT_PASSWORD_VERIFY_VALIDATION_CODE):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    access_token: payload.reply.access_token,
                    access_token_expiry_time: payload.reply.expiry_time,
                    waiting: false
                }
            };

        case toRequest(userAuthActionTypes.USER_FORGOT_PASSWORD_PRE_RESTORE):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    salt: "",
                    waiting: true
                }
            };

        case toFailure(userAuthActionTypes.USER_FORGOT_PASSWORD_PRE_RESTORE):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    salt: "",
                    waiting: false
                }
            };

        case toSuccess(userAuthActionTypes.USER_FORGOT_PASSWORD_PRE_RESTORE):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    salt: payload.reply.salt,
                    waiting: false
                }
            };

        case toRequest(userAuthActionTypes.USER_RESTORE_PASSWORD):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    waiting: true
                }
            };
        case toFailure(userAuthActionTypes.USER_RESTORE_PASSWORD):
        case toSuccess(userAuthActionTypes.USER_RESTORE_PASSWORD):
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    waiting: false
                }
            };

        case userAuthActionTypes.CHANGE_USER_FORGOT_PASSWORD_STEP:
            return {
                ...state,
                forgot_password: {
                    ...state.forgot_password,
                    step: payload.step
                }
            };

        case userAuthActionTypes.CLEAR_FORGOT_PASSWORD_DATA:
            return{
                ...state,
                forgot_password: {
                    ...defaultForgotPasswordState
                }
            };
        case toRequest(userAuthActionTypes.PRE_LOGIN_WITH_GIIGAA_ID):
        case toRequest(userAuthActionTypes.FETCH_ACCESS_TOKEN):
            return {
                ...state,
                login: {
                    ...state.login,
                    waiting: true
                }
            };
        case toFailure(userAuthActionTypes.PRE_LOGIN_WITH_GIIGAA_ID):
        case toFailure(userAuthActionTypes.FETCH_ACCESS_TOKEN):
        case toSuccess(userAuthActionTypes.PRE_LOGIN_WITH_GIIGAA_ID):
            return {
                ...state,
                login: {
                    ...state.login,
                    waiting: false
                }
            };
        case toSuccess(userAuthActionTypes.FETCH_ACCESS_TOKEN):
            return {
                ...state,
                login: {
                    ...state.login,
                    user_info: {...payload.reply},
                    waiting: false
                }
            };
        default:
            return state;
    }
};

export default userAuthReducers;
