import React, {Component} from "react";
import {View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import commonStyles from "src/styles/commonStyles";
import NavBar from "src/containers/NavBar/NavBar";
import {tr} from "src/localization/localization";
import {
    clearLoginData,
    clearRegistrationData,
    userRegistrationDeactivateValidationCode,
    userRegistrationGenerateValidationCode,
    userRegistrationRegister,
    userRegistrationVerifyValidationCode
} from "src/pages/UserAuth/userAuthActions";
import {registrationSteps} from "src/constants/enums";
import {generalValidEmailPhone} from "src/utils/misc/formValidators";
import PhoneEmailForm from "src/pages/UserAuth/components/PhoneEmailForm/PhoneEmailForm";
import UserValidationForm from "src/pages/UserAuth/components/UserValidationForm/UserValidationForm";
import UserInfoInputForm from "src/pages/UserAuth/components/UserInfoInputForm/UserInfoInputForm";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";

class UserRegistration extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        tabBarHidden: true,
    };

    static propTypes = {
        registrationStep: PropTypes.string,
        validationCodeExpiryTime: PropTypes.number,
        isValidCode: PropTypes.bool,
        isWaitingForRegistration: PropTypes.bool,
        verifyCode: PropTypes.string,
        salt: PropTypes.string,
        accessToken: PropTypes.string,
        clearRegistrationData: PropTypes.func,
        deactivateValidationCode: PropTypes.func,
        generateValidationCode: PropTypes.func,
        verifyValidationCode: PropTypes.func,
        register: PropTypes.func,
        navigateTo: PropTypes.func,
        onRegistrationSucceed: PropTypes.func,
    };

    static defaultProps = {
        registrationStep: registrationSteps.PHONE_INPUT,
        validationCodeExpiryTime: 0,
        isValidCode: false,
        isWaitingForRegistration: false,
        verifyCode: "",
        salt: "",
        accessToken: "",
        clearRegistrationData: () => {
        },
        deactivateValidationCode: () => {
        },
        generateValidationCode: () => {
        },
        verifyValidationCode: () => {
        },
        register: () => {
        },
        navigateTo: () => {
        },
        onRegistrationSucceed: null,
    };

    constructor(props) {
        super(props);

        this.state = {
            phone: "",
        };
    }

    componentWillUnmount() {
        this.props.clearRegistrationData();
    }

    generateNewValidationCode = (phoneEmail) => {
        const {generateValidationCode, deactivateValidationCode} = this.props;

        deactivateValidationCode();
        generateValidationCode(phoneEmail);
    };

    submitEmailPhone = ({phoneEmail}) => {
        if (!generalValidEmailPhone(phoneEmail)) {
            return;
        }

        this.setState({phone: phoneEmail});

        this.generateNewValidationCode(phoneEmail);
    };

    submitValidationCode = ({validationCode}) => {
        const {verifyValidationCode} = this.props;
        const {phone} = this.state;

        verifyValidationCode(phone, validationCode);
    };

    submitUserInfo = ({full_name, password}) => {
        const {verifyCode, salt, accessToken, navigator, onRegistrationSucceed} = this.props;

        this.props.register({
            full_name,
            password,
            salt,
            verify_code: verifyCode,
            access_token: accessToken
        }, navigator, onRegistrationSucceed);
    };

    goToUserRegistration = () => {

    };

    goToLoginWithSocialAccount = () => {

    };

    renderPhoneInputForm = () => {
        const {isWaitingForRegistration} = this.props;

        return (
            <PhoneEmailForm
                onSubmit={this.submitEmailPhone}
                isWaiting={isWaitingForRegistration}
                submitButtonText={tr("account_tab_login_phone_input_submit_button")}
            />
        );
    };

    renderUserValidationForm = () => {
        const {
            validationCodeExpiryTime,
            isValidCode,
            deactivateValidationCode,
            isWaitingForRegistration
        } = this.props;

        return (
            <UserValidationForm
                onSubmit={this.submitValidationCode}
                phone={this.state.phone}
                expiryTime={validationCodeExpiryTime}
                isValidCode={isValidCode}
                generateNewCode={this.generateNewValidationCode}
                deactivateValidationCode={deactivateValidationCode}
                submitButtonText={tr("account_tab_user_validation_form_submit_button")}
                isWaiting={isWaitingForRegistration}
                navigateToUserRegistration={this.goToUserRegistration}
                navigateToLogin={this.goToLoginWithSocialAccount}
            />
        );
    };

    renderUserInfoInputForm = () => {
        const {isWaitingForRegistration} = this.props;

        return (
            <UserInfoInputForm
                isWaiting={isWaitingForRegistration}
                onSubmit={this.submitUserInfo}
                buttonTitle={tr("user_registration_form_register_button")}
                navigateToLogin={this.goToLoginWithSocialAccount}
                navigateToUserRegistration={this.goToUserRegistration}
            />
        );
    };

    renderCurrentRegistrationStep = () => {
        const {registrationStep} = this.props;

        const renderFunc = ({
            [registrationSteps.PHONE_INPUT]: this.renderPhoneInputForm,
            [registrationSteps.VALIDATION_CODE_INPUT]: this.renderUserValidationForm,
            [registrationSteps.USER_INFO_INPUT]: this.renderUserInfoInputForm,
        })[registrationStep] || this.renderPhoneInputForm;

        return renderFunc();
    };

    render() {
        const {navigator} = this.props;

        return (
            <View style={commonStyles.matchParent}>
                <NavBar
                    navigator={navigator}
                    title={tr("user_registration_nav_bar_title")}
                />
                {this.renderCurrentRegistrationStep()}
                <FacebookMessenger floating/>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    registrationStep: state.user_auth.registration.step,
    isWaitingForRegistration: state.user_auth.registration.waiting,
    validationCodeExpiryTime: state.user_auth.registration.expiry_time,
    isValidCode: state.user_auth.registration.is_valid_code,
    verifyCode: state.user_auth.registration.code,
    salt: state.user_auth.registration.salt,
    accessToken: state.user_auth.registration.access_token
});

export default connect(mapStateToProps, {
    clearRegistrationData,
    clearLoginData,
    deactivateValidationCode: userRegistrationDeactivateValidationCode,
    generateValidationCode: userRegistrationGenerateValidationCode,
    verifyValidationCode: userRegistrationVerifyValidationCode,
    register: userRegistrationRegister,
})(UserRegistration);
