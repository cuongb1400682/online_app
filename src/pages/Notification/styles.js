import {StyleSheet} from "react-native";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    matchParent: {
        flex: 1,
        backgroundColor: "#eeeeee"
    },
    navBar: {
        flexDirection: "row",
        justifyContent: "flex-start",
        paddingLeft: 16
    },
    navBarTitle: {
        fontSize: 15,
        fontWeight: "bold",
        color: "#fff"
    },
    content: {
        flex: 1,
        marginTop: 4,
    },
    itemContainer: {
        flexDirection: "row",
        width: "100%",
        marginBottom: 3,
        marginTop: 3,
        backgroundColor: "#ffffff",
    },
    itemLeftContent: {
        flex: 0.1,
        paddingHorizontal: 10,
        padding: 10
    },
    itemRightContent: {
        flex: 0.9,
        padding: 10
    },
    itemTitle: {
        fontWeight: "bold",
        color: "#222222",
        fontSize: 14,
        paddingTop: 6
    },
    itemContent: {
        color: "#868686",
        fontSize: 13,
        paddingTop: 6
    },
    itemTime: {
        color: "#2a8efb",
        fontSize: 13,
        paddingTop: 6,
        fontWeight: "bold",
    },
    icon: {
        width: 32, height: 32
    }
});

export default styles;
