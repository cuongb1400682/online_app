import React, {Component} from "react";
import {FlatList, Image, RefreshControl, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import moment from "moment";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import {tr} from "src/localization/localization";
import {fetchNotification, refreshListNotification} from "src/pages/Notification/notificationActions";
import sale from "src/assets/static/icons/sale.png";
import config from "src/assets/static/icons/config.png";
import order from "src/assets/static/icons/order.png";
import openRedirectLink from "src/utils/reactNative/openRedirectLink";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {setOnNavigatorEvent} from "src/utils/misc/navigator";
import {navigatorEvents} from "src/constants/enums";
import Text from "src/components/platformSpecific/Text/Text";

class Notification extends Component {
    static navigatorStyle = {
        navBarHidden: true,
    };

    static propTypes = {
        notifications: PropTypes.array,
        isFetching: PropTypes.bool
    };

    static defaultProps = {
        notifications: [],
        isFetching: false
    };

    constructor(props) {
        super(props);
        this.state = {
            isRefresh: false
        }
    }

    componentDidMount() {
        const {navigator} = this.props;

        setOnNavigatorEvent(navigator, this.onNavigationEvent)
    }

    onNavigationEvent = (event) => {
        if (event.id === navigatorEvents.DID_APPEAR) {
            this.props.fetchNotification();
        }
    };

    _onRefresh = () => {
        this.setState({isRefresh: true}, () => {
            this.props.refreshListNotification();
        });
        this.props.fetchNotification();
        this.setState({isRefresh: false});
    };

    handleOnNotificationPress = ({link = "", link_type = ""}) => {
        const {categories, navigator} = this.props;

        openRedirectLink({link, link_type, categories, navigator});
    }

    renderItem(item, index) {
        let icon_img = item.type_notification === "PROMOTION" ? sale : item.type_notification === "ORDER_STATUS" ? order : config;
        let auto_height = item.description.length * 0.55;

        return (
            <Touchable
                onPress={() => this.handleOnNotificationPress({
                    link: item.redirect_url,
                    link_type: item.redirect_url_type
                })}
                key={index}
                style={[styles.itemContainer, {height: 110 + auto_height}]}>
                <View style={styles.itemLeftContent}>
                    <Image source={icon_img} resizeMode="contain" style={styles.icon}/>
                </View>
                <View style={styles.itemRightContent}>
                    <Text style={styles.itemTitle}>{item.title}</Text>
                    <Text style={styles.itemContent}>{item.description}</Text>
                    <Text style={styles.itemTime}>{moment(item.sent_date).format("HH:MM - DD/MM/YYYY")}</Text>
                </View>
            </Touchable>
        )
    }

    renderEmpty() {
        return (
            <View style={[styles.content, {alignItems: "center"}]}>
                <Text>{tr("notification_empty_list")}</Text>
            </View>
        )
    }

    render() {
        const {notifications} = this.props;

        return (
            <View style={styles.matchParent}>
                <NavBar style={styles.navBar}>
                    <Text style={styles.navBarTitle}>{tr("notification_nav_bar_title")}</Text>
                </NavBar>
                <View style={styles.content}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={notifications}
                        refreshControl={
                            <RefreshControl
                                title={"Pull to refresh"}
                                refreshing={this.state.isRefresh}
                                onRefresh={this._onRefresh}
                            />
                        }
                        renderItem={({item, index}) => this.renderItem(item, index)}
                        ListEmptyComponent={this.renderEmpty()}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    notifications: state.notification.notifications,
    isFetching: state.notification.fetching,
    categories: state.common.categories.list,
});

export default connect(mapStateToProps, {
    fetchNotification,
    refreshListNotification
})(Notification);
