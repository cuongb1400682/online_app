import {createAction, createAsyncAction} from "src/utils/api/createAction";
import notificationActionTypes from "src/redux/actionTypes/notificationActionTypes";

const fetchNotification = () => createAsyncAction({
    type: notificationActionTypes.FETCH_LIST_NOTIFICATION,
    payload: {
        request: "/notification/list_all_notifications",
        params: {}
    }
});

const refreshListNotification = () => createAction({
    type: notificationActionTypes.REFRESH_LIST_NOTIFICATION,
    payload: {
        request: "",
    }
});

export {
    fetchNotification,
    refreshListNotification
};
