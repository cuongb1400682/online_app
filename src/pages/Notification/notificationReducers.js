import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import notificationActionTypes from "src/redux/actionTypes/notificationActionTypes";

const initialState = {
    notifications: [],
    fetching: false,
};

const notificationReducers = (state = initialState, {type, payload, meta}) => {
    switch (type) {
        case toRequest(notificationActionTypes.FETCH_LIST_NOTIFICATION):
            return {
                ...state,
                fetching: true
            };
        case toSuccess(notificationActionTypes.FETCH_LIST_NOTIFICATION):
            // const {notifications} = payload.reply;

            return {
                ...state,
                notifications: [...payload.reply],
                fetching: false
            };
        case toFailure(notificationActionTypes.FETCH_NEW_PRODUCT):
            return {
                ...state,
                fetching: false
            };
        case notificationActionTypes.REFRESH_LIST_NOTIFICATION:
            return {
                ...state,
                notifications: []
            };
        default:
            return state;
    }
};

export default notificationReducers;