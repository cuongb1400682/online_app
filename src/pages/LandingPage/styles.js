import {StyleSheet} from "react-native";

import {estimateBannerHeight} from "src/utils/layouts/homeLayout";
import commonStyles from "src/styles/commonStyles";
import systemFont from "src/styles/systemFont";

const DEFAULT_SECTION_HEADER_HEIGHT = 26;

const styles = StyleSheet.create({
    defaultBackButton: {
        fontSize: 24,
        color: "#fff"
    },
    navBar: {
        flexDirection: "row",
        alignItems: "center"
    },
    sectionHeader: {
        width: "100%",
        height: DEFAULT_SECTION_HEADER_HEIGHT,
    },
    selectedTabBarText: {
        color: "#fff"
    },
    banner: {
        width: "100%",
        height: estimateBannerHeight()
    },
    jumpMenu: {
        flexDirection: "column",
        ...commonStyles.centerChildren,
        paddingVertical: 6
    },
    jumpMenuItem: {
        flexDirection: "row",
        height: 30,
    },
    jumpMenuTouchable: {
        ...commonStyles.centerChildren,
        paddingHorizontal: 6,
    },
    jumpMenuText: {
        color: "#fff",
        fontSize: 14,
        fontWeight: "bold",
    }
});

export default styles;
