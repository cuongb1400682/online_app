import React, {Component} from "react";
import {Image, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import styles from "./styles";
import FastImage from "src/components/base/FastImage/FastImage";
import {fetchLandingPageData} from "src/pages/LandingPage/landingPageActions";
import commonStyles from "src/styles/commonStyles";
import NavBar from "src/containers/NavBar/NavBar";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import NavBarButton from "src/components/base/NavBarButton/NavBarButton";
import {popBack} from "src/utils/misc/navigator";
import ProductList from "src/components/base/ProductList/ProductList";
import {makeCDNImageURL} from "src/utils/cdn/images";
import {capitalizeString, isEmptyString} from "src/utils/extensions/strings";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {
    calculateLandingPageHeaderHeight,
    estimateSectionImageDimensions,
    estimateSectionsYPositions,
    separateProductsIntoSections
} from "src/utils/layouts/landingPage";
import {estimateBannerHeight} from "src/utils/layouts/homeLayout";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";
import giigaaLogo from "src/assets/static/logo/giigaaLogo.png";
import Text from "src/components/platformSpecific/Text/Text";

export const SECTION_HEADER_PADDING = 16;

const BANNER_HEIGHT = estimateBannerHeight();

class LandingPage extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        tabs: PropTypes.array,
        campaignCode: PropTypes.string,
        bannerUrl: PropTypes.string,
        color: PropTypes.string,
        navBarColor: PropTypes.string,
        description: PropTypes.string,
        fetched: PropTypes.bool,
        fetching: PropTypes.bool,
        fetchLandingPageData: PropTypes.func,
    };

    static defaultProps = {
        tabs: [],
        campaignCode: "",
        bannerUrl: "",
        color: "",
        navBarColor: "",
        description: "",
        fetched: "",
        fetching: "",
        fetchLandingPageData: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            products: [],
            sections: [],
            headerHeight: 0,
            isFloatingJumpMenuVisible: false,
            currentSectionIndex: 0
        };

        this.sectionYOffset = [Number.MAX_SAFE_INTEGER];
    }

    componentDidMount() {
        const {campaignCode, fetchLandingPageData} = this.props;

        fetchLandingPageData({campaignCode});
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const {tabs, bannerUrl} = nextProps;
        const {products, sections} = separateProductsIntoSections(tabs);
        const hasBanner = !isEmptyString(bannerUrl);
        const headerHeight = calculateLandingPageHeaderHeight(tabs.length, hasBanner);

        this.setState({
            products,
            sections,
            headerHeight
        });
    }

    UNSAFE_componentWillUpdate(nextProps, nextState) {
        const {sections = []} = nextState;

        this.sectionYOffset = estimateSectionsYPositions(sections);
    }

    handleGoBack = () => {
        const {navigator} = this.props;

        popBack(navigator, {animated: true});
    };

    toggleFloatingJumpMenu = (offsetY) => {
        const {tabs, bannerUrl} = this.props;
        const {isFloatingJumpMenuVisible} = this.state;
        const hasBanner = !isEmptyString(bannerUrl);

        if (offsetY >= BANNER_HEIGHT) {
            if (!isFloatingJumpMenuVisible) {
                this.setState({
                    isFloatingJumpMenuVisible: true,
                    headerHeight: BANNER_HEIGHT
                });
            }
        } else if (this.state.isFloatingJumpMenuVisible) {
            this.setState({
                isFloatingJumpMenuVisible: false,
                headerHeight: calculateLandingPageHeaderHeight(tabs.length, hasBanner)
            });
        }
    };

    determineCurrentSection = (offsetY) => {
        const {currentSectionIndex} = this.state;
        const lowerBound = this.sectionYOffset[currentSectionIndex] + BANNER_HEIGHT;
        const upperBound = this.sectionYOffset[currentSectionIndex + 1] + BANNER_HEIGHT;

        if (offsetY < lowerBound) {
            this.setState({
                currentSectionIndex: currentSectionIndex - 1
            });
        }

        if (offsetY > upperBound) {
            this.setState({
                currentSectionIndex: currentSectionIndex + 1
            });
        }
    };

    onProductListScroll = (_, offsetX, offsetY) => {
        this.toggleFloatingJumpMenu(offsetY);
        this.determineCurrentSection(offsetY);
    };

    onSectionImageLoadEnd = ({mobile_url = ""} = {}) => () => {
        const {sections = []} = this.state;
        const foundSection = sections.find(section => section.mobile_url === mobile_url);

        if (!foundSection) {
            return;
        }

        estimateSectionImageDimensions(mobile_url, dimensions => {
            foundSection.width = dimensions.width;
            foundSection.height = dimensions.height;

            this.setState({sections});
        });
    };

    jumpTo = (sectionIndex) => () => {
        if (sectionIndex < 0 || sectionIndex >= this.sectionYOffset.length - 1) {
            return;
        }

        const yOffset = BANNER_HEIGHT + this.sectionYOffset[sectionIndex];

        if (this.productListRef && this.productListRef.scrollToOffset) {
            this.productListRef.scrollToOffset(0, yOffset);
            this.setState({currentSectionIndex: sectionIndex});
        }
    };

    getSectionTextStyles = (index) => {
        const {currentSectionIndex} = this.state;
        const {color} = this.props;

        return [
            styles.jumpMenuText,
            currentSectionIndex === index ? {color} : {}
        ];
    };

    onRefresh = () => {
        const {campaignCode, fetchLandingPageData} = this.props;

        fetchLandingPageData({campaignCode});
    };

    renderNavBar = () => {
        const {navigator} = this.props;

        return (
            <NavBar style={styles.navBar}>
                <NavBarButton onPress={this.handleGoBack}>
                    <MaterialCommunityIcons
                        name="arrow-left"
                        style={styles.defaultBackButton}
                    />
                </NavBarButton>
                <Image
                    source={giigaaLogo}
                    resizeMode="contain"
                    style={commonStyles.matchParent}
                />
                <View style={commonStyles.matchParent}/>
                <NavBarShortcuts
                    showsCart
                    showsSearch
                    showsVerticalEllipsis
                    navigator={navigator}
                />
            </NavBar>
        );
    };

    renderJumpMenuItem = (sectionIndex) => {
        const {sections = []} = this.state;
        const currSection = sections[sectionIndex] || {};

        return (
            currSection && <Touchable onPress={this.jumpTo(sectionIndex)} style={styles.jumpMenuTouchable}>
                <Text style={this.getSectionTextStyles(sectionIndex)}>
                    {capitalizeString(currSection.title)}
                </Text>
            </Touchable>
        );
    };

    renderJumpMenu = () => {
        const {navBarColor} = this.props;
        const {sections = []} = this.state;
        let jumpMenuItems = [];

        for (let secIndex = 0; secIndex < sections.length; secIndex += 2) {
            jumpMenuItems.push(
                <View key={secIndex} style={styles.jumpMenuItem}>
                    {this.renderJumpMenuItem(secIndex)}
                    {this.renderJumpMenuItem(secIndex + 1)}
                </View>
            );
        }

        return (
            <View style={[styles.jumpMenu, {backgroundColor: navBarColor}]}>
                {jumpMenuItems}
            </View>
        );
    };

    renderSectionHeader = (section = {}) => {
        const {mobile_url = "", width, height} = section;

        if (isEmptyString(mobile_url)) {
            return null;
        }

        const sectionHeaderStyles = [
            styles.sectionHeader,
            {width, height}
        ];

        return (
            <View style={sectionHeaderStyles}>
                <FastImage
                    source={{uri: makeCDNImageURL(mobile_url)}}
                    style={commonStyles.matchParent}
                    resizeMode={FastImage.resizeMode.contain}
                    onLoadEnd={this.onSectionImageLoadEnd(section)}
                />
            </View>
        );
    };

    renderHeader = () => {
        const {bannerUrl = ""} = this.props;
        const {isFloatingJumpMenuVisible} = this.state;

        return (
            <View>
                {!isEmptyString(bannerUrl) && <FastImage
                    source={{uri: makeCDNImageURL(bannerUrl)}}
                    resizeMode={FastImage.resizeMode.cover}
                    style={styles.banner}
                />}
                {!isFloatingJumpMenuVisible && this.renderJumpMenu()}
            </View>
        );
    };

    render() {
        const {navigator, color, fetching} = this.props;
        const {products, sections, headerHeight, isFloatingJumpMenuVisible} = this.state;

        return (
            <View style={commonStyles.matchParent}>
                {this.renderNavBar()}
                {isFloatingJumpMenuVisible && this.renderJumpMenu()}
                <ProductList
                    ref={ref => this.productListRef = ref}
                    header={this.renderHeader()}
                    headerHeight={headerHeight}
                    products={products}
                    isRefreshing={fetching}
                    onRefresh={this.onRefresh}
                    sections={sections}
                    navigator={navigator}
                    backgroundColor={color}
                    onScroll={this.onProductListScroll}
                    renderSectionHeader={this.renderSectionHeader}
                />
                <FacebookMessenger floating/>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    tabs: state.landing_page.tabs,
    bannerUrl: state.landing_page.mobile_url,
    color: state.landing_page.color,
    navBarColor: state.landing_page.nav_bar_color,
    description: state.landing_page.description,
    fetched: state.landing_page.fetched,
    fetching: state.landing_page.fetching
});

export default connect(mapStateToProps, {
    fetchLandingPageData
})(LandingPage);
