import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import landingPageActionTypes from "src/redux/actionTypes/landingPageActionTypes";

const initialState = {
    banner: {},
    tabs: [],
    color: "",
    nav_bar_color: "",
    mobile_url: null,
    fetched: false,
    fetching: false
};

const landingPageReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case toRequest(landingPageActionTypes.FETCH_LANDING_PAGE_DATA):
            return {
                ...state,
                fetching: true
            };
        case toSuccess(landingPageActionTypes.FETCH_LANDING_PAGE_DATA):
            return {
                ...state,
                ...payload.reply,
                fetched: true,
                fetching: false
            };
        case toFailure(landingPageActionTypes.FETCH_LANDING_PAGE_DATA):
            return {
                ...state,
                fetched: true,
                fetching: false
            };
        default:
            return state;
    }
};

export default landingPageReducer;
