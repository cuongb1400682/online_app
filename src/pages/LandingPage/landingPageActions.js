import {createAsyncAction} from "src/utils/api/createAction";
import landingPageActionTypes from "src/redux/actionTypes/landingPageActionTypes";
import {isEmptyString} from "src/utils/extensions/strings";
import Message from "src/components/platformSpecific/Message/Message";
import {tr} from "src/localization/localization";

const fetchLandingPageData = ({campaignCode}) => createAsyncAction({
    type: landingPageActionTypes.FETCH_LANDING_PAGE_DATA,
    payload: {
        request: "/landing_page/get_detail",
        params: {
            campaign_code: campaignCode
        }
    },
    onFailure: (dispatch, response) => {
        if (!isEmptyString(response.result)) {
            Message.show("", tr(response.result));
        }
    }
});

export {
    fetchLandingPageData
};
