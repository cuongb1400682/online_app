import {createAction, createAsyncAction} from "src/utils/api/createAction";
import promotedProductActionTypes from "src/redux/actionTypes/promotedProductActionTypes";
import {isEmptyString} from "src/utils/extensions/strings";
import Message from "src/components/platformSpecific/Message/Message";
import {tr} from "src/localization/localization";

const PAGE_SIZE = 50;

const fetchPromotedProduct = ({page_index = 0, page_size = PAGE_SIZE} = {}) => createAsyncAction({
    type: promotedProductActionTypes.FETCH_PROMOTED_PRODUCT,
    payload: {
        request: "/product/promoted_product/list",
        params: {
            page_index,
            page_size
        }
    },
    onFailure: (dispatch, response) => {
        if (!isEmptyString(response.result)) {
            Message.show("", tr(response.result));
        }
    }
});

const clearPromotedProducts = () => createAction({
    type: promotedProductActionTypes.CLEAR_PROMOTED_PRODUCTS,
    payload: {}
});

const toggleViewMode = () => createAction({
    type: promotedProductActionTypes.TOGGLE_PROMOTED_PRODUCT_VIEW_MODE,
    payload: {}
});

export {
    clearPromotedProducts,
    fetchPromotedProduct,
    toggleViewMode,
    PAGE_SIZE
};
