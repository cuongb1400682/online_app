import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: "#eee"
    },
    image: {
        width: "100%",
        height: 150,
        marginBottom: 4
    }
});

export default styles;
