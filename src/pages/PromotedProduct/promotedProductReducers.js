import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import promotedProductActionTypes from "src/redux/actionTypes/promotedProductActionTypes";
import {productViewModes} from "src/constants/enums";

const initialState = {
    products: [],
    fetching: false,
    number_items: 0,
    view_mode: productViewModes.GRID
};

const promotedProductReducers = (state = initialState, {type, payload = {}, meta}) => {
    switch (type) {
        case toRequest(promotedProductActionTypes.FETCH_PROMOTED_PRODUCT):
            return {
                ...state,
                number_items: 0,
                fetching: true
            };
        case toSuccess(promotedProductActionTypes.FETCH_PROMOTED_PRODUCT):
            return {
                ...state,
                products: [
                    ...state.products,
                    ...payload.reply.products
                ],
                number_items: payload.reply.number_items,
                fetching: false
            };
        case toFailure(promotedProductActionTypes.FETCH_PROMOTED_PRODUCT):
            return {
                ...state,
                fetching: false
            };
        case promotedProductActionTypes.CLEAR_PROMOTED_PRODUCTS:
            return {
                ...initialState
            };
        case promotedProductActionTypes.TOGGLE_PROMOTED_PRODUCT_VIEW_MODE:
            return {
                ...state,
                view_mode: state.view_mode === productViewModes.GRID
                    ? productViewModes.LIST
                    : productViewModes.GRID
            };
        default:
            return state;
    }
};

export default promotedProductReducers;
