import React, {Component} from "react";
import {View, Text} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import {tr} from "src/localization/localization";
import {productViewModes} from "src/constants/enums";
import {
    clearPromotedProducts,
    fetchPromotedProduct,
    PAGE_SIZE,
    toggleViewMode
} from "src/pages/PromotedProduct/promotedProductActions";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";
import ProductList from "src/components/base/ProductList/ProductList";
import FilterBar from "src/components/base/FilterBar/FilterBar";
import LoadMore from "src/components/base/LoadMore/LoadMore";

class DealList extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        products: PropTypes.array,
        isFetching: PropTypes.bool,
        numberOfItems: PropTypes.number,
        viewMode: PropTypes.oneOf([
            productViewModes.LIST,
            productViewModes.GRID
        ]),
        fetchPromotedProduct: PropTypes.func,
        clearPromotedProducts: PropTypes.func,
        toggleViewMode: PropTypes.func,
    };

    static defaultProps = {
        products: [],
        isFetching: false,
        numberOfItems: 0,
        viewMode: productViewModes.GRID,
        fetchPromotedProduct: () => {
        },
        clearPromotedProducts: () => {
        },
        toggleViewMode: () => {
        }
    };

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        const {fetchPromotedProduct, clearPromotedProducts} = this.props;

        clearPromotedProducts();
        fetchPromotedProduct();
    }

    onFetchNextProduct = () => {
        const {products = [], fetchPromotedProduct} = this.props;
        const nextPageIndex = Math.ceil(products.length / PAGE_SIZE);

        fetchPromotedProduct({page_index: nextPageIndex});
    };

    renderNavBarShortcuts = () => (
        <NavBarShortcuts
            navigator={this.props.navigator}
            showsSearch
            showsCart
            showsVerticalEllipsis
        />
    );

    render() {
        const {navigator, products, viewMode, toggleViewMode, isFetching, fetchPromotedProduct, numberOfItems} = this.props;

        return (
            <View style={styles.content}>
                <NavBar
                    title={tr("promote_product")}
                    right={this.renderNavBarShortcuts()}
                    navigator={navigator}
                />
                <ProductList
                    header={
                        <FilterBar
                            viewMode={viewMode}
                            changeViewMode={toggleViewMode}
                        />
                    }
                    headerHeight={50}
                    footer={
                        <LoadMore
                            maximumNumberOfItems={numberOfItems}
                            currentNumberOfItems={products.length}
                            onFetchNextProduct={this.onFetchNextProduct}
                            searching={isFetching}
                        />
                    }
                    emptyList={tr("promoted_product_empty_list")}
                    emptyListHeight={50}
                    footerHeight={50}
                    products={products}
                    isRefreshing={isFetching}
                    onRefresh={fetchPromotedProduct}
                    navigator={navigator}
                    showsHorizontalProduct={viewMode === productViewModes.LIST}
                />
                <FacebookMessenger floating/>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    products: state.promoted_product.products,
    isFetching: state.promoted_product.fetching,
    viewMode: state.promoted_product.view_mode,
    numberOfItems: state.promoted_product.number_items,
});

export default connect(mapStateToProps, {
    clearPromotedProducts,
    fetchPromotedProduct,
    toggleViewMode
})(DealList);
