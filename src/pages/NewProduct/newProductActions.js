import {createAction, createAsyncAction} from "src/utils/api/createAction";

import newProductActionTypes from "src/redux/actionTypes/newProductActionTypes";

const fetchNewProduct = ({page_index, page_size}) => createAsyncAction({
    type: newProductActionTypes.FETCH_NEW_PRODUCT,
    payload: {
        request: "/product/new_product/list",
        params: {
            page_index: Math.max(page_index - 1, 0),
            page_size: page_size
        },
        meta: {
            page_index
        }
    }
});

const changeViewMode = (mode) => createAction({
    type: newProductActionTypes.CHANGE_VIEW_MODE,
    payload: {
        mode
    }
});

const clearNewProduct = () => createAction({
    type: newProductActionTypes.CLEAR_NEW_PRODUCT,
    payload: {}
});

export {
    fetchNewProduct,
    changeViewMode,
    clearNewProduct
};