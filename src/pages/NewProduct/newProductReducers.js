import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";
import newProductActionTypes from "../../redux/actionTypes/newProductActionTypes";
import {productViewModes} from "src/constants/enums";

const initialState = {
    products: [],
    number_items: 0,
    page_index: 1,
    page_size: 50,
    fetching: false,
    viewMode: productViewModes.GRID,
};

const bestSalesProductReducers = (state = initialState, {type, payload, meta}) => {
    switch (type) {
        case newProductActionTypes.CHANGE_VIEW_MODE:
            return {
                ...state,
                viewMode: payload.mode
            };
        case toRequest(newProductActionTypes.FETCH_NEW_PRODUCT):
            return {
                ...state,
                fetching: true
            };
        case toSuccess(newProductActionTypes.FETCH_NEW_PRODUCT):
            const {products, ...others} = payload.reply;

            return {
                ...state,
                ...others,
                products: [...state.products, ...products],
                page_index: meta.page_index,
                fetching: false
            };
        case toFailure(newProductActionTypes.FETCH_NEW_PRODUCT):
            return {
                ...state,
                fetching: false
            };
        case newProductActionTypes.CLEAR_NEW_PRODUCT:
            return {
                ...initialState,
                viewMode: state.viewMode || productViewModes.GRID
            };
        default:
            return state;
    }
};

export default bestSalesProductReducers;
