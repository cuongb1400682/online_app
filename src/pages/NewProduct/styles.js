import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: "#eee"
    },
});

export default styles;
