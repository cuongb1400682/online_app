import React, {Component} from "react";
import {View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import NavBarShortcuts from "src/containers/NavBarShortcuts/NavBarShortcuts";
import {tr} from "src/localization/localization";
import {changeViewMode, clearNewProduct, fetchNewProduct} from "src/pages/NewProduct/newProductActions";
import {productViewModes} from "src/constants/enums";
import FilterBar from "src/components/base/FilterBar/FilterBar";
import ProductList from "src/components/base/ProductList/ProductList";
import LoadMore from "src/components/base/LoadMore/LoadMore";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";

const DEFAULT_PAGE_SIZE = 50;

class NewProduct extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    static propTypes = {
        products: PropTypes.array,
        isFetching: PropTypes.bool,
        totalItems: PropTypes.number,
        viewMode: PropTypes.oneOf([
            productViewModes.LIST,
            productViewModes.GRID
        ]),
        changeViewMode: PropTypes.func,
        pageSize: PropTypes.number,
        pageIndex: PropTypes.number,
        clearNewProduct: PropTypes.func,
        fetchNewProduct: PropTypes.func,
    };

    static defaultProps = {
        products: [],
        isFetching: false,
        totalItems: 0,
        viewMode: productViewModes.GRID,
        changeViewMode: () => {
        },
        clearNewProduct: () => {
        },
        fetchNewProduct: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            currentPageIndex: 1
        }
    }

    componentDidMount() {
        const {currentPageIndex} = this.state;

        this.props.fetchNewProduct({
            page_size: DEFAULT_PAGE_SIZE,
            page_index: currentPageIndex
        });
    }

    componentWillUnmount() {
        const {clearNewProduct} = this.props;

        clearNewProduct();
    }

    onFetchNextProduct = () => {
        const nextPageIndex = this.state.currentPageIndex + 1;
        this.setState({currentPageIndex: nextPageIndex});

        this.props.fetchNewProduct({
            page_size: DEFAULT_PAGE_SIZE,
            page_index: nextPageIndex
        });
    };

    onRefresh = () => {
        const {clearNewProduct, fetchNewProduct} = this.props;

        clearNewProduct();
        this.setState({currentPageIndex: 1});
        fetchNewProduct({
            page_size: DEFAULT_PAGE_SIZE,
            page_index: 1
        });
    };

    renderNavBarShortcuts = () => (
        <NavBarShortcuts
            navigator={this.props.navigator}
            showsSearch
            showsCart
            showsVerticalEllipsis
        />
    );

    renderFooter = () => {
        const {totalItems, products, isFetching} = this.props;

        return (
            <LoadMore
                onFetchNextProduct={this.onFetchNextProduct}
                searching={isFetching}
                maximumNumberOfItems={totalItems}
                currentNumberOfItems={products.length}
            />
        );
    };

    render() {
        const {navigator, products, viewMode, changeViewMode, isFetching} = this.props;

        return (
            <View style={styles.content}>
                <NavBar
                    title={tr("new_product")}
                    right={this.renderNavBarShortcuts()}
                    navigator={navigator}
                />
                <ProductList
                    headerHeight={54}
                    header={
                        <FilterBar
                            changeViewMode={changeViewMode}
                            viewMode={viewMode}
                        />
                    }
                    isRefreshing={isFetching}
                    onRefresh={this.onRefresh}
                    footerHeight={50}
                    footer={this.renderFooter()}
                    emptyList={tr("new_product_empty_list")}
                    emptyListHeight={50}
                    products={products}
                    navigator={navigator}
                    showsHorizontalProduct={viewMode === productViewModes.LIST}
                />
                <FacebookMessenger floating/>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    products: state.new_product.products,
    totalItems: state.new_product.number_items,
    pageIndex: state.new_product.page_index,
    isFetching: state.new_product.fetching,
    pageSize: state.new_product.page_size,
    viewMode: state.new_product.viewMode
});

export default connect(mapStateToProps, {
    fetchNewProduct,
    changeViewMode,
    clearNewProduct
})(NewProduct);
