import React, {Component} from "react";
import {ScrollView, View, RefreshControl} from "react-native";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import styles from "./styles";

import {fetchCategories} from "src/redux/common/commonActions";
import FastImage from "src/components/base/FastImage/FastImage";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {makeCDNImageURL} from "src/utils/cdn/images";
import {MARGIN_SIZE} from "src/pages/Categories/containers/CategoriesGrid/styles";
import Text from "src/components/platformSpecific/Text/Text";

class CategoriesGrid extends Component {
    static propTypes = {
        categories: PropTypes.array,
        isFetching: PropTypes.bool,
        fetchCategories: PropTypes.func,
        onCategoryPress: PropTypes.func,
    };

    static defaultProps = {
        categories: [],
        isFetching: false,
        fetchCategories: () => {
        },
        onCategoryPress: () => {
        },
    };

    componentDidMount() {
        const {fetchCategories} = this.props;

        fetchCategories();
    }

    handleCategoryPress = (category, index) => () => {
        const {onCategoryPress} = this.props;

        onCategoryPress(category, index);
    };

    getCategories = () => {
        const {categories} = this.props;
        const result = [...categories];
        const categoriesHasOddLength = categories.length % 2 === 1;

        if (categoriesHasOddLength) {
            result.push({isPlaceholder: true});
        }

        return result;
    };

    renderPlaceholder = (index) => {
        return <View key={index} style={styles.placeholder}/>;
    };

    renderCategory = (category = {}, index) => {
        const {mobile_icon = "", name = ""} = category;
        const touchableStyle = {
            marginRight: index % 2 === 0 ? MARGIN_SIZE : 0
        };

        return (
            <Touchable
                key={index}
                onPress={this.handleCategoryPress(category, index)}
                style={touchableStyle}
            >
                <View style={styles.gridCategoryItem}>
                    <FastImage
                        source={{uri: makeCDNImageURL(mobile_icon)}}
                        style={styles.gridCategoryItemImage}
                    />
                    <Text style={styles.gridCategoryItemName}>{name}</Text>
                </View>
            </Touchable>
        );
    };

    renderGridItem = (category = {}, index) => {
        if (category.isPlaceholder) {
            return this.renderPlaceholder(index);
        } else {
            return this.renderCategory(category, index);
        }
    };

    render() {
        const categories = this.getCategories();
        const {fetchCategories} = this.props;

        return (
            <ScrollView
                style={styles.gridCategoriesMenuWrapper}
                refreshControl={
                    <RefreshControl
                        onRefresh={fetchCategories}
                        refreshing={false}
                    />
                }
            >
                <View style={styles.gridCategoriesMenu}>
                    {categories.map(this.renderGridItem)}
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => ({
    categories: state.common.categories.list,
    isFetching: state.common.categories.fetching,
});

export default connect(mapStateToProps, {
    fetchCategories
})(CategoriesGrid);
