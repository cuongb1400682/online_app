import {StyleSheet} from "react-native";

import commonStyles from "src/styles/commonStyles";
import {screenWidth} from "src/utils/reactNative/dimensions";

const MARGIN_SIZE = 8;
const GRID_VIEW_ITEM_SIZE = screenWidth() / 2 - 2 * MARGIN_SIZE;

const styles = StyleSheet.create({
    gridCategoriesMenuWrapper: {
        width: "100%",
        backgroundColor: "#fff"
    },
    gridCategoriesMenu: {
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 20
    },
    gridCategoryLine: {
        flexDirection: "row",
        marginBottom: MARGIN_SIZE
    },
    gridCategoryItem: {
        width: GRID_VIEW_ITEM_SIZE,
        height: GRID_VIEW_ITEM_SIZE,
        borderColor: "#ddd",
        borderWidth: 1,
        borderStyle: "solid",
        borderRadius: 5,
        marginBottom: MARGIN_SIZE,
        ...commonStyles.centerChildren
    },
    gridCategoryItemImage: {
        width: 0.65 * GRID_VIEW_ITEM_SIZE,
        height: 0.65 * GRID_VIEW_ITEM_SIZE,
        marginBottom: MARGIN_SIZE
    },
    gridCategoryItemName: {
        width: "80%",
        textAlign: "center"
    },
    placeholder: {
        width: GRID_VIEW_ITEM_SIZE,
        height: GRID_VIEW_ITEM_SIZE,
    }
});

export {
    styles as default,
    MARGIN_SIZE
};
