import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    categoriesMenu: {
        flex: 1,
    },
    statusBar: {
        backgroundColor: "#BF361C"
    }
});

export default styles;
