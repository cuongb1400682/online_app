import React, {Component} from "react";
import {View} from "react-native";

import styles from "./styles";
import commonStyles from "src/styles/commonStyles";
import CustomStatusBar from "src/components/base/CustomStatusBar/CustomStatusBar";
import {pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";
import CategoriesGrid from "src/pages/Categories/containers/CategoriesGrid/CategoriesGrid";

class Categories extends Component {
    static navigatorStyle = {
        navBarHidden: true
    };

    showCatalogByCategories = (category) => {
        const {navigator} = this.props;

        pushTo(navigator, {
            screen: screenIds.CATALOG_BY_CATEGORY,
            passProps: {
                selectedCategory: category,
                subCategoryIndex: -1
            }
        });
    };

    render() {
        return (
            <View style={commonStyles.matchParent}>
                <CustomStatusBar style={styles.statusBar}/>
                <CategoriesGrid onCategoryPress={this.showCatalogByCategories}/>
            </View>
        );
    }
}

export default Categories;
