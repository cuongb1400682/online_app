import React, {Component} from "react";
import PropTypes from "prop-types";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {Keyboard} from "react-native";

import styles from "./styles";
import NavBar from "src/containers/NavBar/NavBar";
import NavBarButton from "src/components/base/NavBarButton/NavBarButton";
import NavBarInput from "src/components/base/NavBarInput/NavBarInput";
import {tr} from "src/localization/localization";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import Magnifier from "src/components/base/svgIcons/Magnifier/Magnifier";
import screenIds from "src/constants/screenIds";
import {popBack, pushTo} from "src/utils/misc/navigator";

class SearchNavBar extends Component {
    static propTypes = {
        navigator: PropTypes.any
    };

    static defaultProps = {
        navigator: null
    };

    constructor(props) {
        super(props);

        this.state = {
            query: ""
        };
    }

    handleGoBack = () => {
        const {navigator} = this.props;

        popBack(navigator);
    };

    showSearchResult = () => {
        const {navigator} = this.props;
        const {query} = this.state;

        pushTo(navigator, {
            screen: screenIds.SEARCH_RESULT,
            passProps: {
                searchParams: {
                    q: query
                }
            }
        });
    };

    onSearchInputBoxTextChange = (queryString) => {
        this.setState({
            query: queryString
        })
    };

    renderMagnifierButton = () => (
        <Touchable
            style={styles.magnifierButton}
            onPress={this.showSearchResult}
        >
            <Magnifier/>
        </Touchable>
    );

    render() {
        return (
            <NavBar style={styles.navBar}>
                <NavBarButton onPress={this.handleGoBack}>
                    <MaterialCommunityIcons
                        name="arrow-left"
                        style={styles.defaultBackButton}
                    />
                </NavBarButton>
                <NavBarInput
                    rightViews={this.renderMagnifierButton()}
                    placeholder={tr("search_nav_bar_input_box_placeholder")}
                    clearButtonMode="never"
                    selectTextOnFocus
                    onChangeText={this.onSearchInputBoxTextChange}
                    onSubmitEditing={this.showSearchResult}
                    onBlur={Keyboard.dismiss}
                />
            </NavBar>
        );
    }
}

export default SearchNavBar;
