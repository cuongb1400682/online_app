import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    navBar: {
        paddingRight: 16
    },
    defaultBackButton: {
        fontSize: 24,
        color: "#fff"
    },
    magnifierButton: {
        width: 36,
        height: 36,
        ...commonStyles.centerChildren
    }
});

export default styles;
