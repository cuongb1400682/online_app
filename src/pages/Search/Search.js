import React, {Component} from "react";
import {View} from "react-native";

import styles from "./styles";
import SearchNavBar from "src/pages/Search/components/SearchNavBar/SearchNavBar";
import FacebookMessenger from "src/pages/Home/components/FacebookMessenger/FacebookMessenger";

class Search extends Component {
    static navigatorStyle = {
        tabBarHidden: true,
        navBarHidden: true
    };

    render() {
        const {navigator} = this.props;

        return (
            <View style={styles.search}>
                <SearchNavBar navigator={navigator}/>
                <FacebookMessenger floating/>
            </View>
        );
    }
}

export default Search;
