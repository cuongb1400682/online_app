import React from "react";
import {Platform, Text as RNText} from "react-native";

import {injectTextStyles} from "src/utils/reactNative/fonts";

const InjectedStyleText = ({style = {}, ...props}) => {
    return (
        <RNText
            allowFontScaling={false}
            {...props}
            style={injectTextStyles(style)}
        />
    );
};

export default Platform.select({
    ios: RNText,
    android: InjectedStyleText
});
