import React from "react";
import {Platform, ProgressViewIOS, ProgressBarAndroid} from "react-native";

const ProgressView = (props) => {
    const ProgressComponent = Platform.select({
        ios: ProgressViewIOS,
        android: ProgressBarAndroid
    });

    return <ProgressComponent {...props}/>;
};

export default ProgressView;
