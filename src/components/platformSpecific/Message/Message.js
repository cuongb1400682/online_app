import {AlertIOS, ToastAndroid, Platform} from "react-native";

class Message {
    static show(message, title) {
        if (Platform.OS === "ios") {
            AlertIOS.alert(title, message);
        } else {
            ToastAndroid.show(title, ToastAndroid.LONG);
        }
    }
}

export default Message;
