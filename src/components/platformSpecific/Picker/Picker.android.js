import React, {Component} from "react";
import {Modal, ScrollView, TouchableNativeFeedback, TouchableWithoutFeedback, View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import Text from "src/components/platformSpecific/Text/Text";
import commonStyles from "src/styles/commonStyles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";

class Picker extends Component {
    static propTypes = {
        pickerData: PropTypes.array,
        selectedValue: PropTypes.any,
        titleText: PropTypes.string,
        confirmText: PropTypes.string,
        cancelText: PropTypes.string,
        onPickerConfirm: PropTypes.func,
    };

    static defaultProps = {
        pickerData: [],
        selectedValue: null,
        titleText: "",
        confirmText: "",
        cancelText: "",
        onPickerConfirm: () => {
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            visible: false
        };
    }

    _toggleVisibility = () => {
        this.setState({visible: !this.state.visible});
    };

    show = () => {
        this.setState({visible: true});
    };

    hide = () => {
        this.setState({visible: false});
    };

    handlePickerConfirm = (value, index) => () => {
        const {onPickerConfirm} = this.props;

        onPickerConfirm(value, index);
        this.hide();
    };

    render() {
        const {pickerData = [], titleText, selectedValue} = this.props;
        const {visible} = this.state;

        return (
            <Modal
                animationType="fade"
                hardwareAccelerated
                transparent
                visible={visible}
            >
                <View style={styles.modalContentContainer}>
                    <TouchableWithoutFeedback onPress={this._toggleVisibility}>
                        <View style={styles.backgroundOverlay}/>
                    </TouchableWithoutFeedback>
                    <View style={styles.modalContent}>
                        <View style={styles.pickerItemTouchable}>
                            <Text style={styles.titleText}>{titleText}</Text>
                        </View>
                        <ScrollView
                            style={styles.pickerList}
                            contentContainerStyle={commonStyles.centerChildren}
                        >
                            {pickerData.map((value, index) =>
                                <Touchable
                                    key={index}
                                    onPress={this.handlePickerConfirm(value, index)}
                                    style={styles.pickerItemTouchable}
                                >
                                    <View style={[
                                        styles.pickerItemTouchable,
                                        {
                                            backgroundColor: value === selectedValue ? "#e2e2e2" : "#fff",
                                            paddingHorizontal: 16,
                                        }
                                    ]}>
                                        <Text style={styles.pickerItemText}>{value}</Text>
                                    </View>
                                </Touchable>
                            )}
                        </ScrollView>
                        <View style={{height: 8}}/>
                    </View>
                </View>
            </Modal>
        );
    }
}

export default Picker;
