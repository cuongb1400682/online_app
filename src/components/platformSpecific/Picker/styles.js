import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    modalContentContainer: {
        flex: 1,
        backgroundColor: "rgba(0, 0, 0, 0.4)",
        ...commonStyles.centerChildren
    },
    modalContent: {
        width: "75%",
        height: "40%",
        backgroundColor: "white",
        borderRadius: 8,
        elevation: 4
    },
    pickerList: {
        flex: 1
    },
    pickerItemTouchable: {
        width: "100%",
        height: 40,
        flexDirection: "row",
        alignItems: "center",
    },
    pickerItemText: {
        fontSize: 14
    },
    backgroundOverlay: {
        ...commonStyles.absolutelyFill,
        backgroundColor: "transparent"
    },
    titleText: {
        fontWeight: "bold",
        fontSize: 12,
        paddingHorizontal: 8
    }
});

export default styles;
