import {Platform} from "react-native";

export default Platform.select({
    ios: require("./Picker.ios"),
    android: require("./Picker.android")
});

