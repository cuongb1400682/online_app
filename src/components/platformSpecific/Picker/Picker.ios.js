import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import RNPicker from "react-native-picker";
import {isEmptyArray} from "src/utils/extensions/arrays";

class Picker extends Component {
    static propTypes = {
        pickerData: PropTypes.array,
        selectedValue: PropTypes.any,
        titleText: PropTypes.string,
        confirmText: PropTypes.string,
        cancelText: PropTypes.string,
        onPickerConfirm: PropTypes.func,
    };

    static defaultProps = {
        pickerData: [],
        selectedValue: null,
        titleText: "",
        confirmText: "",
        cancelText: "",
        onPickerConfirm: () => {
        },
    };

    constructor(props) {
        super(props);

        RNPicker.init({
            onPickerConfirm: this.onPickerConfirm,
            pickerData: props.pickerData,
            selectedValue: [],
            pickerTitleText: props.titleText,
            pickerCancelBtnText: props.cancelText,
            pickerConfirmBtnText: props.confirmText,
        });
    }

    show = () => {
        const {pickerData, selectedValue, titleText, cancelText, confirmText} = this.props;

        RNPicker.init({
            onPickerConfirm: this.onPickerConfirm,
            pickerData: pickerData,
            selectedValue: [selectedValue],
            pickerTitleText: titleText,
            pickerCancelBtnText: cancelText,
            pickerConfirmBtnText: confirmText,
        });

        RNPicker.show();
    };

    hide = () => {
        RNPicker.hide();
    };

    onPickerConfirm = (data = []) => {
        const {onPickerConfirm} = this.props;
        const {pickerData} = this.props;

        if (!Array.isArray(data) || isEmptyArray(data)) {
            return;
        }

        const selectedValue = data[0];
        const selectedIndex = pickerData.findIndex(value => value === selectedValue);

        onPickerConfirm(selectedValue, selectedIndex);
        this.hide();
    };

    render() {
        return <View/>;
    }
}

export default Picker;
