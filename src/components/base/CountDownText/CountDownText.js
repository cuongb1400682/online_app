import React, {Component} from "react";
import PropTypes from "prop-types";

import styles from "./styles";
import {calculateCountdown} from "src/utils/extensions/dateTime";
import Text from "src/components/platformSpecific/Text/Text";

class CountDownText extends Component {
    static propTypes = {
        endDate: PropTypes.string,
        formatter: PropTypes.func,
        style: PropTypes.any
    };

    static defaultProps = {
        endDate: "2018-09-03T09:05:43Z",
        formatter: () => {
        },
        style: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            years: 0,
            days: 0,
            hours: 0,
            min: 0,
            sec: 0
        };

        this.endDate = new Date(this.props.endDate)
    }

    componentDidMount() {
        this.interval = setInterval(() => {
            const date = calculateCountdown(this.endDate);
            date ? this.setState(date) : this.stopCountDownTimer();
        }, 1000);
    }

    componentWillUnmount() {
        this.stopCountDownTimer();
    }

    stopCountDownTimer() {
        clearInterval(this.interval);
    }

    render() {
        const {style} = this.props;
        const {years, days, hours, min, sec} = this.state;
        const formattedTime = this.props.formatter({years, days, hours, min, sec});

        return (
            typeof formattedTime === "string"
                ? <Text style={[styles.countDownText, style]}>{formattedTime}</Text>
                : formattedTime
        )
    }
}

export default CountDownText;
