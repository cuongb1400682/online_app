import {StyleSheet} from "react-native";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    countDownText: {
        color: "#00A146",
        fontWeight: "bold",
        fontSize: 12
    }
});

export default styles;
