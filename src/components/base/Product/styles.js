import React from "react";
import {StyleSheet, Platform} from "react-native";

import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    product: {
        backgroundColor: "#fff",
        flexDirection: "column",
        alignItems: "center",
    },
    productInfoContainer: {
        flexDirection: "column",
        flexWrap: "wrap",
        paddingHorizontal: 10,
        width: "100%",
        flex: 1
    },
    vietGapIcon: {
        width: 30,
        height: 30,
        position: "absolute",
        top: 5,
        right: 5,
        zIndex: 2
    },
    giftIcon: {
        width: 14,
        height: 14,
        position: "absolute",
        bottom: 0,
        left: 5,
        zIndex: 2
    },
    productImage: {
        width: "100%",
        height: "100%"
    },
    productName: {
        fontSize: 13,
        marginTop: 10,
        height: 36,
        width: "100%"
    },
    unitPrice: {
        fontSize: 15,
        color: colors.primaryColor
    },
    marketPrice: {
        fontSize: 12,
        color: "#a2a2a2",
        textDecorationLine: "line-through",
        textDecorationStyle: "solid",
        marginLeft: 8
    },
    pricePart: {
        width: "100%",
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "flex-start",
        marginTop: 6,
        marginBottom: 4
    },
    discountPercent: {
        color: colors.primaryColor,
        fontSize: 12,
        fontWeight: "bold"
    },
    progressBarBackground: {
        width: "100%",
        height: 3,
        marginBottom: 2,
        backgroundColor: "#d7d7d7"
    },
    progressBarInner: {
        backgroundColor: colors.primaryColor
    },
    dealPart: {
        width: "100%",
    },
    dealText: {
        fontSize: 12,
    },
    pricePerKg: {
        position: "absolute",
        right: 5,
        bottom: 0,
    },
    dealTag: {
        position: "absolute",
        top: 5,
        left: 5
    }
});

export default styles;

