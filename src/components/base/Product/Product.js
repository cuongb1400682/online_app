import React, {PureComponent} from "react";
import {Image, View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import vietGap from "src/assets/static/icons/vietGAP.png";
import gift from "src/assets/static/icons/gift.png";
import FastImage from "src/components/base/FastImage/FastImage";
import {addLeadingZeros, formatWithCurrency} from "src/utils/extensions/strings";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {tr} from "src/localization/localization";
import CountDownText from "src/components/base/CountDownText/CountDownText";
import {makeCDNImageURL} from "src/utils/cdn/images";
import {dealTimeline, imageTypes} from "src/constants/enums";
import commonStyles from "src/styles/commonStyles";
import PricePerKg from "src/components/base/PricePerKg/PricePerKg";
import {IMAGE_SIZE, PRODUCT_HEIGHT, PRODUCT_INFO_HEIGHT} from "src/utils/layouts/productLayout";
import OutOfStockBadge from "src/components/base/OutOfStockBadge/OutOfStockBadge";
import DealTag from "src/components/base/DealTag/DealTag";
import Text from "src/components/platformSpecific/Text/Text";

class Product extends PureComponent {
    static propTypes = {
        dealTimeline: PropTypes.string,
        imageSizeRatio: PropTypes.number,
        source: PropTypes.object,
        onPress: PropTypes.func
    };

    static defaultProps = {
        dealTimeline: dealTimeline.CURRENT,
        imageSizeRatio: 1,
        source: {},
        onPress: () => {
        }
    };

    countdownTextFormatter = ({years, days, hours, min, sec}) => {
        return `${days} ${tr("deal_part_day_unit")} ` +
            `${addLeadingZeros(hours)}:${addLeadingZeros(min)}:${addLeadingZeros(sec)}`;
    };

    isProductOutOfStock = () => {
        const {qty} = this.props.source;

        return (typeof qty === "number") && (qty === 0);
    };

    getEndTime = (deal, timeline) => {
        if (!deal) {
            return "";
        }

        return ({
            [timeline === dealTimeline.CURRENT]: deal.end_time,
            [timeline === dealTimeline.FUTURE]: deal.start_time,
        })[true] || "";
    };

    getSold = (deal) => {
        if (!deal || deal.quantity <= 0 || deal.sold_quantity < 0) {
            return 0;
        }

        return parseFloat((deal.sold_quantity / deal.quantity) * 100).toFixed(2);
    };

    renderIconAndBadges = () => {
        const {
            gift: showsGift,
            viet_gap: showsVietGap,
            price_per_kg: pricePerKg,
            deal,
            discount_percent: discountPercent
        } = this.props.source;

        return (
            <React.Fragment>
                <DealTag deal={deal} discountPercent={discountPercent} style={styles.dealTag}/>
                {showsVietGap && <Image style={styles.vietGapIcon} source={vietGap}/>}
                {showsGift && <Image style={styles.giftIcon} source={gift}/>}
                <PricePerKg pricePerKg={pricePerKg} containerStyle={styles.pricePerKg}/>
                {this.isProductOutOfStock() && <OutOfStockBadge isInDeal={!!deal}/>}
            </React.Fragment>
        );
    };

    renderImagePart = () => {
        const {original_thumbnail: imageUrl = ""} = this.props.source;
        const {imageSizeRatio = 1} = this.props;
        const imagePartStyles = [
            commonStyles.flexColumnCenter,
            {
                width: imageSizeRatio * IMAGE_SIZE,
                height: imageSizeRatio * IMAGE_SIZE
            }
        ];

        return (
            <View style={imagePartStyles}>
                <FastImage
                    style={styles.productImage}
                    resizeMode={FastImage.resizeMode.contain}
                    source={{uri: makeCDNImageURL(imageUrl, imageTypes.PURCHASE_ORDER_DETAIL)}}
                    enableCache
                />
                {this.renderIconAndBadges()}
            </View>
        );
    };

    renderPricePart = () => {
        const {
            market_price: marketPrice,
            unit_price: unitPrice,
            deal,
            discount_percent: discountPercent
        } = this.props.source;
        const showMarketPrice = (marketPrice > unitPrice);
        const showDiscountPercent = (!deal && !!discountPercent);

        return (
            <View style={styles.pricePart}>
                <Text style={styles.unitPrice}>{formatWithCurrency(deal ? deal.price : unitPrice)}</Text>
                {showMarketPrice && <Text style={styles.marketPrice} numberOfLines={2}>{formatWithCurrency(marketPrice)}</Text>}
                {showDiscountPercent && <Text style={styles.discountPercent}>{` (-${discountPercent})`}</Text> }
            </View>
        );
    };

    renderDealPart = () => {
        const {deal} = this.props.source;

        if (!deal) {
            return null;
        }

        const soldRatio = this.getSold(deal);
        const endTime = this.getEndTime(deal, this.props.dealTimeline);
        const isSoldPercentVisible = deal && deal.type === "LIMITED";
        const progressBarInnerStyle = {
            width: `${soldRatio}%`,
            height: "100%"
        };

        return (
            <View style={styles.dealPart}>
                {isSoldPercentVisible && <View style={styles.progressBarBackground}>
                    <View style={[styles.progressBarInner, progressBarInnerStyle]}/>
                </View>}
                {isSoldPercentVisible && <Text style={styles.dealText}>
                    {`${soldRatio}% ${tr("product_deal_part_sold_deal_text")}`}
                </Text>}
                <Text style={styles.dealText}>
                    {`${tr("product_deal_part_remaining_days")} `}
                    <CountDownText
                        endDate={endTime}
                        formatter={this.countdownTextFormatter}
                    />
                </Text>
            </View>
        );
    };

    render() {
        const {onPress, source} = this.props;
        const {name} = source;
        const productStyles = [
            styles.product,
            {height: PRODUCT_HEIGHT}
        ];
        const productInfoContainerStyles = [
            styles.productInfoContainer,
            {height: PRODUCT_INFO_HEIGHT}
        ];

        return (
            <Touchable style={commonStyles.matchParent} onPress={onPress}>
                <View style={productStyles}>
                    {this.renderImagePart()}
                    <View style={productInfoContainerStyles}>
                        <Text style={styles.productName} numberOfLines={2}>{name}</Text>
                        {this.renderPricePart()}
                        {this.renderDealPart()}
                    </View>
                </View>
            </Touchable>
        );
    }
}

export default Product;
