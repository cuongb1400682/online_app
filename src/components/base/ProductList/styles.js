import {StyleSheet} from "react-native";

import commonStyles from "src/styles/commonStyles";
import {PRODUCT_HEIGHT} from "src/utils/layouts/productLayout";

const styles = StyleSheet.create({
    productRowWrapper: {
        flexDirection: "row",
        width: "100%",
        height: PRODUCT_HEIGHT
    },
    productRowSeparator: {
        height: "100%"
    },
    horizontalProductWrapper: {},
    productList: {
        backgroundColor: "#eee"
    },
    refreshActivityIndicator: {
        ...commonStyles.centerChildren,
        ...commonStyles.hasShadow,
        top: 0,
        position: "absolute",
        alignSelf: "center",
        height: 50,
        zIndex: 1000,
    },
    refreshText: {
        color: "#2e2e2e",
        fontSize: 10,
        marginTop: 4
    },
    container: {
        flex: 1,
    },
});

export default styles;
