import React, {Component} from "react";
import {ActivityIndicator, View, RefreshControl, Platform} from "react-native";
import {DataProvider, LayoutProvider, RecyclerListView} from "recyclerlistview";
import PropTypes from "prop-types";

import styles from "./styles";
import Product from "src/components/base/Product/Product";
import commonStyles from "src/styles/commonStyles";
import {screenHeight, screenWidth} from "src/utils/reactNative/dimensions";
import screenIds from "src/constants/screenIds";
import HorizontalProduct from "src/components/base/HorizontalProduct/HorizontalProduct";
import {pushTo} from "src/utils/misc/navigator";
import {dealTimeline, productListViewTypes} from "src/constants/enums";
import {
    HORIZONTAL_PRODUCT_HEIGHT,
    PRODUCT_HEIGHT,
    separateProductListIntoSections
} from "src/utils/layouts/productLayout";
import {isEmptyArray} from "src/utils/extensions/arrays";
import Text from "src/components/platformSpecific/Text/Text";

class ProductList extends Component {
    static propTypes = {
        scrollViewProps: PropTypes.any,
        products: PropTypes.array,
        sections: PropTypes.array,
        showsHorizontalProduct: PropTypes.bool,
        isRefreshing: PropTypes.bool,
        spacing: PropTypes.number,
        headerHeight: PropTypes.number,
        footerHeight: PropTypes.number,
        emptyListHeight: PropTypes.number,
        dealTimeline: PropTypes.string,
        title: PropTypes.string,
        backgroundColor: PropTypes.string,
        header: PropTypes.element,
        footer: PropTypes.any,
        emptyList: PropTypes.any,
        navigator: PropTypes.any,
        renderSectionHeader: PropTypes.func,
        onProductPress: PropTypes.func,
        onRefresh: PropTypes.func,
        onScroll: PropTypes.func
    };

    static defaultProps = {
        scrollViewProps: {},
        products: [],
        sections: [],
        showsHorizontalProduct: false,
        isRefreshing: false,
        spacing: 5,
        headerHeight: 0,
        footerHeight: 0,
        emptyListHeight: 0,
        dealTimeline: dealTimeline.CURRENT,
        title: "",
        backgroundColor: "#eee",
        header: null,
        footer: null,
        emptyList: null,
        navigator: null,
        renderSectionHeader: () => {
        },
        onProductPress: () => {
        },
        onRefresh: null,
        onScroll: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            didPullToRefresh: false
        };

        this.layoutProvider = new LayoutProvider(
            this.categorizeViewType,
            this.setLayoutForEachViewType
        );

        this.dataProvider = new DataProvider((view1, view2) => view1 !== view2);

        this.rawDataSource = [];
    }

    // TODO: replace by getDerivedStateFromProps
    UNSAFE_componentWillReceiveProps(nextProps) {
        const {products = [], emptyList} = nextProps;

        if (isEmptyArray(products) && emptyList) {
            this.rawDataSource = [
                {viewType: productListViewTypes.HEADER},
                {viewType: productListViewTypes.EMPTY_LIST_COMPONENT},
                {viewType: productListViewTypes.FOOTER}
            ];
        } else {
            this.rawDataSource = [
                {viewType: productListViewTypes.HEADER},
                ...separateProductListIntoSections(nextProps),
                {viewType: productListViewTypes.FOOTER}
            ];
        }

        if (!nextProps.isRefreshing && this.props.isRefreshing) {
            this.setState({didPullToRefresh: false});
        }
    }

    categorizeViewType = (index) => {
        const {showsHorizontalProduct} = this.props;
        const {viewType} = this.rawDataSource[index] || {};

        if (viewType) {
            return viewType;
        } else {
            return showsHorizontalProduct
                ? productListViewTypes.HORIZONTAL_PRODUCT
                : productListViewTypes.PRODUCTS_ROW;
        }
    };

    handleOnProductPress = (product, index) => () => {
        const {navigator, onProductPress} = this.props;

        if (navigator) {
            pushTo(navigator, {
                screen: screenIds.PRODUCT_DETAIL,
                passProps: {
                    productId: product.id
                }
            });
        } else if (typeof onProductPress === "function") {
            onProductPress(product, index);
        }
    };

    setLayoutForEachViewType = (viewType, dimension, viewIndex) => {
        const {headerHeight, footerHeight, emptyListHeight, spacing} = this.props;
        const viewPortWidth = screenWidth();

        switch (viewType) {
            case productListViewTypes.PRODUCTS_ROW:
                dimension.height = PRODUCT_HEIGHT + spacing;
                dimension.width = viewPortWidth;
                break;
            case productListViewTypes.HORIZONTAL_PRODUCT:
                dimension.height = HORIZONTAL_PRODUCT_HEIGHT + spacing;
                dimension.width = viewPortWidth;
                break;
            case productListViewTypes.FOOTER:
                dimension.width = viewPortWidth;
                dimension.height = footerHeight;
                break;
            case productListViewTypes.HEADER:
                dimension.width = viewPortWidth;
                dimension.height = headerHeight;
                break;
            case productListViewTypes.SECTION_HEADER:
                const section = this.rawDataSource[viewIndex];

                dimension.width = section.width;
                dimension.height = section.height;
                break;
            case productListViewTypes.EMPTY_LIST_COMPONENT:
                dimension.width = viewPortWidth;
                dimension.height = emptyListHeight;
                break;
            default:
                dimension.width = 0;
                dimension.height = 0;
                break;
        }
    };

    scrollToTop = () => {
        if (this.recyclerRef && this.recyclerRef.scrollToOffset) {
            this.recyclerRef.scrollToOffset(0, 0, true);
        }
    };

    scrollToOffset = (x, y, animated = true) => {
        if (this.recyclerRef && this.recyclerRef.scrollToOffset) {
            this.recyclerRef.scrollToOffset(x, y, animated);
        }
    };

    getCurrentScrollOffset = () => {
        if (this.recyclerRef && this.recyclerRef.getCurrentScrollOffset) {
            return this.recyclerRef.getCurrentScrollOffset();
        }

        return {x: 0, y: 0};
    };

    onProductListScroll = (nativeEvent, offsetX, offsetY) => {
        const {onScroll, onRefresh} = this.props;
        const {didPullToRefresh} = this.state;

        if (typeof onRefresh === "function") {
            if (offsetY < -150 && !didPullToRefresh) {
                this.setState({didPullToRefresh: true});
            }

            if (offsetY === 0 && didPullToRefresh) {
                onRefresh();
            }
        }

        onScroll(nativeEvent, offsetX, offsetY);
    };

    getScrollViewProps = () => {
        const {scrollViewProps, onRefresh} = this.props;
        const {didPullToRefresh} = this.state;
        const allowPullToRefresh = (Platform.OS === "android") && (typeof onRefresh === "function");

        return {
            ...scrollViewProps,
            refreshControl: allowPullToRefresh && (
                <RefreshControl
                    refreshing={didPullToRefresh}
                    onRefresh={onRefresh}
                />
            )
        };
    };

    renderProductRow = (productRow = {}, index) => {
        const {backgroundColor, spacing} = this.props;
        const {
            left: leftProduct,
            right: rightProduct
        } = productRow;
        const productRowSeparatorStyles = [
            styles.productRowSeparator,
            {
                backgroundColor,
                width: spacing
            }
        ];
        const productPlaceHolder = {
            width: (screenWidth() - spacing) / 2
        };

        return (
            <View style={styles.productRowWrapper} key={index}>
                {leftProduct && <Product
                    source={leftProduct}
                    onPress={this.handleOnProductPress(leftProduct, index)}
                    dealTimeline={this.props.dealTimeline}
                />}
                <View style={productRowSeparatorStyles}/>
                {rightProduct
                    ? <Product
                        source={rightProduct}
                        onPress={this.handleOnProductPress(rightProduct, index + 1)}
                        dealTimeline={this.props.dealTimeline}
                    />
                    : <View style={productPlaceHolder}/>}
            </View>
        );
    };

    renderHorizontalProduct = (product, index) => {
        if (!product) {
            return null;
        }

        return (
            <View style={styles.horizontalProductWrapper}>
                <HorizontalProduct
                    source={product}
                    onPress={this.handleOnProductPress(product, index)}
                    dealTimeline={this.props.dealTimeline}
                />
            </View>
        );
    };

    renderEmptyList = () => {
        const {emptyList} = this.props;

        if (typeof emptyList === "string") {
            return (
                <View style={[commonStyles.centerChildren, commonStyles.matchParent]}>
                    <Text>{emptyList}</Text>
                </View>
            );
        }

        return emptyList;
    };

    renderRow = (viewType, itemData, index) => {
        const {header, footer, renderSectionHeader} = this.props;

        switch (viewType) {
            case productListViewTypes.HEADER:
                return header;
            case productListViewTypes.FOOTER:
                return footer;
            case productListViewTypes.PRODUCTS_ROW:
                return this.renderProductRow(itemData, index);
            case productListViewTypes.HORIZONTAL_PRODUCT:
                return this.renderHorizontalProduct(itemData, index);
            case productListViewTypes.SECTION_HEADER:
                return renderSectionHeader(this.rawDataSource[index]);
            case productListViewTypes.EMPTY_LIST_COMPONENT:
                return this.renderEmptyList();
            default:
                return null;
        }
    };

    renderRefreshActivityIndicator = () => {
        const {didPullToRefresh} = this.state;

        return (
            didPullToRefresh && <View style={styles.refreshActivityIndicator}>
                <ActivityIndicator/>
            </View>
        );
    };

    render() {
        const {backgroundColor} = this.props;
        const productListStyles = [
            commonStyles.matchParent,
            styles.productList,
            {backgroundColor}
        ];
        const containerStyles = [
            styles.container,
            {backgroundColor}
        ];

        return (
            <View style={containerStyles}>
                {this.renderRefreshActivityIndicator()}
                <RecyclerListView
                    ref={ref => this.recyclerRef = ref}
                    style={productListStyles}
                    layoutProvider={this.layoutProvider}
                    dataProvider={this.dataProvider.cloneWithRows(this.rawDataSource)}
                    rowRenderer={this.renderRow}
                    renderAheadOffset={screenHeight() / 2}
                    onScroll={this.onProductListScroll}
                    scrollViewProps={this.getScrollViewProps()}
                />
            </View>
        );
    }
}

export default ProductList;
