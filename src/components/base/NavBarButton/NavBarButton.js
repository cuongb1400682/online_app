import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import commonStyles from "src/styles/commonStyles";

class NavBarButton extends Component {
    static propTypes = {
        style: PropTypes.any,
        children: PropTypes.any,
        onPress: PropTypes.func
    };

    static defaultProps = {
        style: {},
        children: null,
        onPress: () => {
        }
    };

    render() {
        const {style, children, onPress} = this.props;

        return (
            <Touchable onPress={onPress}>
                <View style={[styles.navBarButton, commonStyles.centerChildren, style]}>
                    {children}
                </View>
            </Touchable>
        );
    }
}

export default NavBarButton;
