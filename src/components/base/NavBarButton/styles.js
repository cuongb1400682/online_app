import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    navBarButton: {
        height: 48,
        width: 48,
        borderRadius: 24
    }
});

export default styles;
