import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    outOfStockBadgeInDeal: {
        position: "absolute",
        top: 5,
        right: 5,
        height: 40,
        width: 80
    },
    outOfStockBadge: {
        position: "absolute",
        top: 5,
        left: 5
    }
});

export default styles;
