import React, {Component} from "react";
import {TabView} from "react-native-tab-view";
import PropTypes from "prop-types";
import {View, ActivityIndicator} from "react-native";

import TabBar from "src/components/base/TabBar/TabBar";
import commonStyles from "src/styles/commonStyles";

class LazyTabView extends Component {
    static propTypes = {
        routes: PropTypes.array,
        currentRouteIndex: PropTypes.number,
        onChangeTab: PropTypes.func,
        renderScene: PropTypes.func,
    };

    static defaultProps = {
        routes: [],
        currentRouteIndex: 0,
        onChangeTab: () => {
        },
        renderScene: () => {
        }
    };

    constructor(props) {
        super(props);

        const {routes = [], currentRouteIndex} = props;

        this.loadedScene = [];

        if (routes.length > 1) {
            this.loadedScene.push(routes[currentRouteIndex].key);
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const {currentRouteIndex = 0} = nextProps;

        if (currentRouteIndex !== this.props.currentRouteIndex) {
            this.onChangeTab(currentRouteIndex);
        }
    }

    onChangeTab = (tabIndex) => {
        const {onChangeTab, routes = []} = this.props;

        if (tabIndex < routes.length) {
            const {key = ""} = routes[tabIndex];

            if (this.loadedScene.indexOf(key) === -1) {
                this.loadedScene.push(key);
            }
        }

        onChangeTab(tabIndex);
    };

    renderActivityIndicator = () => {
        return (
            <View style={[commonStyles.matchParent, commonStyles.centerChildren]}>
                <ActivityIndicator/>
            </View>
        );
    };

    renderScene = ({route = {}}) => {
        const {key = ""} = route;
        const {renderScene} = this.props;

        if (this.loadedScene.indexOf(key) === -1) {
            return this.renderActivityIndicator();
        }

        return renderScene({route});
    };

    render() {
        const {routes = [], currentRouteIndex,} = this.props;

        return (
            <TabView
                navigationState={{
                    index: currentRouteIndex,
                    routes: routes
                }}
                onIndexChange={this.onChangeTab}
                renderScene={this.renderScene}
                renderTabBar={TabBar}
                animationEnabled
                swipeEnabled
                keyboardDismissMode="on-drag"
                useNativeDriver
                style={commonStyles.matchParent}
                tabBarPosition="top"
            />
        );
    }
}

export default LazyTabView;
