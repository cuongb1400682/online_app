import React, {Component} from "react";
import {TextInput, View} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import PropTypes from "prop-types";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";

class NumberSpinner extends Component {
    static propTypes = {
        min: PropTypes.number,
        max: PropTypes.number,
        style: PropTypes.object,
        defaultValue: PropTypes.number,
        onChange: PropTypes.func
    };

    static defaultProps = {
        min: 0,
        max: 10,
        style: {},
        defaultValue: 0,
        onChange: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            quantity: Math.max(parseInt(props.defaultValue, 10), props.min)
        };
    }

    setQuantity = (quantity) => {
        const {onChange} = this.props;

        onChange(quantity);
        this.setState({quantity: quantity});
    };

    handleNumberInputChange = (text) => {
        let numberFromText = parseInt(text, 10);

        if (isNaN(numberFromText)) {
            numberFromText = 0;
        }

        this.setQuantity(numberFromText);
    };

    modifyQuantity = (value) => () => {
        const {min, max} = this.props;
        const {quantity: oldQuantity} = this.state;
        const newQuantity = oldQuantity + value;

        if ((value < 0 && newQuantity >= min) ||
            (value > 0 && newQuantity <= max)) {
            this.setQuantity(newQuantity);
        }
    };

    render() {
        const {style} = this.props;
        const {quantity} = this.state;

        return (
            <View style={[styles.numberSpinner, style]}>
                <Touchable
                    style={[styles.numberSpinnerButton, styles.subtractButton]}
                    onPress={this.modifyQuantity(-1)}
                >
                    <FontAwesome5 name="minus" style={styles.buttonLabel}/>
                </Touchable>
                <TextInput
                    style={styles.quantityLabel}
                    onChangeText={this.handleNumberInputChange}
                    value={`${quantity}`}
                />
                <Touchable
                    style={[styles.numberSpinnerButton, styles.addButton]}
                    onPress={this.modifyQuantity(1)}
                >
                    <FontAwesome5 name="plus" style={styles.buttonLabel}/>
                </Touchable>
            </View>
        );
    }
}

export default NumberSpinner;
