import {StyleSheet, Platform} from "react-native";
import commonStyles from "src/styles/commonStyles";
import systemFont from "src/styles/systemFont";

const SPINNER_HEIGHT = Platform.select({android: 40, ios: 36});

const styles = StyleSheet.create({
    numberSpinner: {
        width: 132,
        height: SPINNER_HEIGHT,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#ddd",
        flexDirection: "row"
    },
    numberSpinnerButton: {
        width: SPINNER_HEIGHT - 1,
        height: SPINNER_HEIGHT - 2,
        backgroundColor: "#eee",
        borderStyle: "solid",
        borderColor: "#ddd",
        justifyContent: "center",
        alignItems: "center"
    },
    quantityLabel: {
        textAlign: "center",
        flex: 1,
        ...systemFont.regular
    },
    subtractButton: {
        borderEndWidth: 1
    },
    addButton: {
        borderStartWidth: 1
    },
    buttonLabel: {
        color: "#515356",
        fontSize: 12,
    },
});

export default styles;
