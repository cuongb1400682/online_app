import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

class RadioGroup extends Component {
    static propTypes = {
        children: PropTypes.any.isRequired,
        onChildrenPress: PropTypes.func,
        selectedIndex: PropTypes.number,
        style: PropTypes.any
    };

    static defaultProps = {
        children: [],
        selectedIndex: -1,
        onChildrenPress: () => {
        },
        style: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedChildIndex: props.selectedIndex
        };
    }

    // TODO: replace by getDerivedStateFromProps
    UNSAFE_componentWillReceiveProps(nextProps) {
        const {selectedIndex} = nextProps;
        const {selectedChildIndex} = this.state;

        if (selectedIndex !== selectedChildIndex) {
            this.setState({selectedChildIndex: selectedIndex});
        }
    }

    onChildrenPress = (index) => () => {
        this.setState({
            selectedChildIndex: index
        });

        this.props.onChildrenPress(index);
    };

    render() {
        return (
            <View style={this.props.style}>
                {React.Children.map(this.props.children, (child, index) =>
                    React.cloneElement(child, {
                        onPress: this.onChildrenPress(index),
                        selected: this.state.selectedChildIndex === index
                    })
                )}
            </View>
        );
    }
}

export default RadioGroup;
