import React from "react";
import {View} from "react-native";

import styles from "./styles";
import Button from "src/components/base/Button/Button";
import {tr} from "src/localization/localization";

const LoadMore = ({onFetchNextProduct, searching, maximumNumberOfItems = 0, currentNumberOfItems = -1}) => {

    if (currentNumberOfItems >= maximumNumberOfItems) {
        return null;
    }

    return (
        <View style={styles.footer}>
            <Button
                title={tr("see_more_button_title")}
                style={styles.seeMoreButton}
                onPress={onFetchNextProduct}
                loading={searching}
            />
        </View>
    );
};

export default LoadMore;
