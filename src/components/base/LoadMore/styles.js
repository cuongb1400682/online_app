import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    footer: {
        height: 50,
        width: "100%",
        backgroundColor: "transparent"
    },
    seeMoreButton: {
        marginTop: 10,
        marginBottom: 15,
        alignSelf: "center"
    },
    buttonText: {
        color: colors.primaryColor
    }
});

export default styles;
