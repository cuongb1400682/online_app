import {StyleSheet, Platform} from "react-native";

import colors from "src/constants/colors";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    tabBar: {
        backgroundColor: "#fff",
        borderStyle: "solid",
        borderColor: "#ddd",
        borderBottomWidth: 1
    },
    tabBarIndicator: {
        backgroundColor: colors.primaryColor,
        color: colors.primaryColor
    },
    tabBarLabel: {
        color: "#757575",
        fontSize: Platform.select({ios: 15, android: 13}),
        ...systemFont.regular
    },
    tabBarIndividualTab: {
        margin: 0,
        padding: 0,
        height: 48
    }
});

export default styles;
