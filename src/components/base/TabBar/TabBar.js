import React from "react";
import {TabBar as RNTabBar} from "react-native-tab-view";

import styles from "./styles";
import commonStyles from "src/styles/commonStyles";

const TabBar = (props) => (
    <RNTabBar
        style={styles.tabBar}
        indicatorStyle={styles.tabBarIndicator}
        labelStyle={styles.tabBarLabel}
        tabStyle={[commonStyles.centerChildren, styles.tabBarIndividualTab]}
        useNativeDriver
        {...props}
    />
);

export default TabBar;
