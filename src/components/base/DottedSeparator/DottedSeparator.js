import React from "react";
import {View} from "react-native";

import styles from "./styles";

const DottedSeparator = (style, ...props) => (
    <View style={[styles.dottedSeparator, style]}  {...props}/>
);

export default DottedSeparator;
