import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    dottedSeparator: {
        height: 1,
        width: "100%",
        borderStyle: "dashed",
        borderRadius: 0.5,
        borderWidth: 0.5,
        borderColor: "#ddd"
    }
});

export default styles;
