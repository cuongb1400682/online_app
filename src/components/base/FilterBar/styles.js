import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    filterBar: {
        width: "100%",
        height: 46,
        backgroundColor: "#fff",
        marginBottom: 6,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 16
    },
    filterButtonText: {
        fontSize: 12,
        color: "#4a4a4a",
        marginLeft: 4
    }
});

export default styles;
