import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {productViewModes} from "src/constants/enums";
import GridMode from "src/components/base/svgIcons/GridMode/GridMode";
import ListMode from "src/components/base/svgIcons/ListMode/ListMode";
import Filter from "src/components/base/svgIcons/Filter/Filter";
import commonStyles from "src/styles/commonStyles";
import {tr} from "src/localization/localization";
import Text from "src/components/platformSpecific/Text/Text";

class FilterBar extends Component {
    static HEIGHT = styles.filterBar.height;
    static MARGIN_BOTTOM = styles.filterBar.marginBottom;

    static propTypes = {
        viewMode: PropTypes.oneOf([
            productViewModes.GRID,
            productViewModes.LIST
        ]),
        changeViewMode: PropTypes.func
    };

    static defaultProps = {
        viewMode: productViewModes.GRID,
        changeViewMode: () => {
        }
    };

    handleChangeViewMode = () => {
        const {viewMode, changeViewMode} = this.props;
        const newViewMode = viewMode === productViewModes.GRID ? productViewModes.LIST : productViewModes.GRID;

        changeViewMode(newViewMode);
    };

    renderViewModeSwitcher = () => (
        <Touchable onPress={this.handleChangeViewMode}>
            {this.props.viewMode === productViewModes.LIST
                ? <GridMode/>
                : <ListMode/>}
        </Touchable>
    );

    renderFilter = () => (
        <Touchable>
            <View style={commonStyles.flexRowCenter}>
                <Filter/>
                <Text style={styles.filterButtonText}>{tr("search_filter_bar_filter_button_text")}</Text>
            </View>
        </Touchable>
    );

    render() {
        return (
            <View style={styles.filterBar}>
                {this.renderViewModeSwitcher()}
                {this.renderFilter()}
            </View>
        );
    }
}

export default FilterBar;
