import React from "react";
import RNFastImage from "react-native-fast-image";
import {ImageBackground as RNImage, Platform} from "react-native";

import defaultProduct from "src/assets/static/images/defaultProduct.png";

const DEFAULT_PRODUCT = "DEFAULT_PRODUCT";

const FastImage = ({source = {}, enableCache = false, ...props}) => {
    const notUseFastImage = (Platform.OS === "android" && !enableCache);
    const ImageComponent = notUseFastImage ? RNImage : RNFastImage;

    if (source.uri === DEFAULT_PRODUCT) {
        return (
            <ImageComponent
                {...props}
                resizeMode={RNFastImage.resizeMode.contain}
                source={defaultProduct}
            />
        );
    }

    return (
        <ImageComponent
            {...props}
            source={source}
        />
    );
};

FastImage.defaultProduct = DEFAULT_PRODUCT;
FastImage.resizeMode = RNFastImage.resizeMode;
FastImage.priority = RNFastImage.priority;
FastImage.cacheControl = RNFastImage.cacheControl;

export default FastImage;
