import {StyleSheet} from "react-native";
import colors from "src/constants/colors";
import commonStyles from "src/styles/commonStyles";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    outerCircle: {
        width: 37,
        height: 37,
        borderRadius: 37,
        borderWidth: 2.5,
        padding: 1,
        borderColor: colors.primaryColor
    },
    innerCircle: {
        backgroundColor: colors.primaryColor,
        flex: 1,
        borderRadius: 37,
        ...commonStyles.centerChildren
    },
    percentage: {
        color: "#fff",
        fontSize: 10,
        fontStyle: "italic"
    },
});

export default styles;
