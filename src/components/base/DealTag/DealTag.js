import React from "react";
import {View} from "react-native";

import styles from "./styles";
import Text from "src/components/platformSpecific/Text/Text";
import CircleHotDeal from "src/components/base/svgIcons/CircleHotDeal/CircleHotDeal";

const DealTag = ({deal, discountPercent = 0, style = {}}) => {
    if (!deal) {
        return null;
    }

    return (
        <View style={[styles.outerCircle, style]}>
            <View style={styles.innerCircle}>
                {discountPercent
                    ? <Text style={styles.percentage}>-{discountPercent}</Text>
                    : <CircleHotDeal/>}
            </View>
        </View>
    );
};

export default DealTag;
