import React, {Component} from "react";
import {ActivityIndicator, View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import FontIcon, {supportedIconFonts} from "src/components/base/FontIcon/FontIcon";
import Text from "src/components/platformSpecific/Text/Text";

const LIGHT = "light";
const FULLY_COLORED = "fully-colored";

class Button extends Component {
    static propTypes = {
        title: PropTypes.string,
        style: PropTypes.any,
        textStyle: PropTypes.any,
        iconStyle: PropTypes.any,
        type: PropTypes.oneOf([LIGHT, FULLY_COLORED]),
        disabled: PropTypes.bool,
        loading: PropTypes.bool,
        iconFontName: PropTypes.oneOf(Object.keys(supportedIconFonts)),
        iconName: PropTypes.string,
        onPress: PropTypes.func,
    };

    static defaultProps = {
        title: "",
        style: {},
        textStyle: {},
        iconStyle: {},
        type: "light",
        disabled: false,
        loading: false,
        iconFontName: "fontawesome",
        iconName: null,
        onPress: () => {
        }
    };

    getButtonStyle = () => {
        const {type, disabled, textStyle: customTextStyle} = this.props;

        let touchableStyle = "lightTouchable";
        let textStyle = "lightText";

        if (type === FULLY_COLORED) {
            touchableStyle = "fullyColoredTouchable";
            textStyle = "fullyColoredText";
        }

        if (disabled) {
            touchableStyle += "_disabled";
            textStyle += "_disabled";
        }

        return {
            touchableStyle: styles[touchableStyle],
            textStyle: {
                ...styles[textStyle],
                ...customTextStyle
            }
        };
    };

    renderIcon = (textStyle) => {
        const {iconName, iconFontName, iconStyle} = this.props;

        return (
            <View style={styles.iconWrapper}>
                <FontIcon
                    style={[textStyle, iconStyle]}
                    name={iconName}
                    fontName={iconFontName}
                />
            </View>
        );
    };

    renderActivityIndicator = ({color}) => {
        const {loading} = this.props;

        if (!loading) {
            return null;
        }

        return (
            <View style={styles.iconWrapper}>
                <ActivityIndicator size="small" color={color} animating/>
            </View>
        );
    };

    renderDefaultButtonContent = () => {
        const {title, loading} = this.props;
        const {textStyle} = this.getButtonStyle();

        return (
            <React.Fragment>
                {loading
                    ? this.renderActivityIndicator(textStyle)
                    : this.renderIcon(textStyle)}
                <Text style={[styles.buttonText, textStyle]}>{title}</Text>
            </React.Fragment>
        );
    };

    render() {
        const {onPress, style, loading, disabled, children} = this.props;
        const {touchableStyle} = this.getButtonStyle();

        return (
            <Touchable
                disabled={loading || disabled}
                onPress={onPress}
                style={[styles.button, touchableStyle, style]}
            >
                {children || this.renderDefaultButtonContent()}
            </Touchable>
        );
    };
}

export default Button;
