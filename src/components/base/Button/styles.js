import colors from "src/constants/colors";
import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    button: {
        height: 26,
        borderStyle: "solid",
        borderRadius: 4,
        borderColor: colors.primaryColor,
        borderWidth: 1,
        paddingHorizontal: 14,
        ...commonStyles.flexRowCenter
    },
    iconWrapper: {
        marginRight: 4
    },
    buttonText: {
        color: colors.primaryColor
    },
    lightTouchable: {
        backgroundColor: "transparent"
    },
    lightText: {
        color: colors.primaryColor
    },
    fullyColoredTouchable: {
        backgroundColor: colors.primaryColor
    },
    fullyColoredText: {
        color: "#fff"
    },
    lightTouchable_disabled: {
        backgroundColor: "transparent",
        borderColor: "#acacac"
    },
    lightText_disabled: {
        color: "#acacac"
    },
    fullyColoredTouchable_disabled: {
        backgroundColor: "#acacac",
        borderColor: "#acacac"
    },
    fullyColoredText_disabled: {
        color: "#fff"
    },
});

export default styles;
