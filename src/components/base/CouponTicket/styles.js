import {StyleSheet} from "react-native";
import colors from "src/constants/colors";
import commonStyles from "src/styles/commonStyles";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    couponTicket: {
        width: "100%",
        height: 100,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#ddd",
        borderRadius: 6,
        overflow: "hidden",
        flexDirection: "row",
    },
    discountAmountContainer: {
        width: 100,
        height: 100,
        backgroundColor: colors.primaryColor,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 6,
        overflow: "hidden",
        ...commonStyles.centerChildren
    },
    discountAmountText: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#fff",
        textAlign: "center",
    },
    dot: {
        width: 7,
        height: 7,
        borderRadius: 7,
        backgroundColor: "#fff"
    },
    dotContainer: {
        flexDirection: "column",
        justifyContent: "space-around",
        height: "100%",
        position: "absolute",
        right: -3.5
    },
    couponTicketContent: {
        flex: 1,
        paddingHorizontal: 8,
        paddingVertical: 6
    },
    codeContainer: {
        minWidth: 100,
        minHeight: 20,
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "#FF9601",
        flexDirection: "column",
        alignSelf: "flex-start",
        justifyContent: "center",
        alignItems: "center"
    },
    codeText: {
        fontWeight: "bold",
        fontSize: 14,
        color: "#FF9601",
    },
    outOfDateBadgeContainer: {
        height: 13,
        backgroundColor: "#a3a3a3",
        paddingHorizontal: 20,
        alignSelf: "center",
        position: "absolute",
        right: 7,
        top: 7,
        ...commonStyles.centerChildren
    },
    outOfDateBadgeText: {
        fontSize: 10,
        fontStyle: "italic",
        fontWeight: "bold",
        color: "#fff"
    },
    descriptionContainer: {
        flexWrap: "wrap",
        flex: 1,
        marginVertical: 6,
    },
    descriptionText: {
        fontSize: 12,
        color: "#333",
    },
    couponEndTime: {
        fontSize: 10,
        color: "#28a745"
    }
});

export default styles;
