import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {discountTypes} from "src/constants/enums";
import {tr} from "src/localization/localization";
import {formatMomentToString, getMomentFromString, getMomentToday} from "src/utils/extensions/moment";
import {calculateCountdown} from "src/utils/extensions/dateTime";
import {formatDate} from "src/constants/dateTime";
import Text from "src/components/platformSpecific/Text/Text";

const ACTIVE = "ACTIVE";
const INACTIVE = "INACTIVE";
const CURRENCY_BASE = 1000;

class CouponTicket extends Component {
    static propTypes = {
        coupon: PropTypes.object,
        style: PropTypes.object,
    };

    static defaultProps = {
        coupon: {},
        style: {},
    };

    normalizeDiscountText = (discount, discountType) => {
        if (discountType === discountTypes.PERCENT) {
            return `${discount}%`;
        }

        return `${discount / CURRENCY_BASE}K`;
    };

    isInactive = () => {
        const {status = ACTIVE} = this.props.coupon;

        return status !== ACTIVE;
    };

    renderCouponItemDuration = ({status, fromTime, endTime, hasSuffix}) => {
        const today = getMomentToday();
        const fromTimeMoment = getMomentFromString(fromTime);
        const endTimeMoment = getMomentFromString(endTime);
        const fromDateStr = formatMomentToString(fromTimeMoment, formatDate);
        const endDateStr = formatMomentToString(endTimeMoment, formatDate);
        const durationStr = `${fromDateStr} - ${endDateStr}`;

        if (!hasSuffix) {
            return durationStr;
        }

        if (today.isBefore(fromTimeMoment)) {
            const {days: durationDays} = calculateCountdown(fromTime);

            return `${durationDays} ngày nữa`;
        }

        if (today.isBefore(endTimeMoment)) {
            const {days: durationDays} = calculateCountdown(endTime);

            return `Còn ${durationDays} ngày`;
        }

        return `HSD: ${durationStr}`;
    };

    renderVerticalDots = () => {
        let dots = [];

        for (let i = 0; i < 9; i++) {
            dots.push(<View key={i} style={styles.dot}/>);
        }

        return (
            <View style={styles.dotContainer}>
                {dots}
            </View>
        );
    };

    renderDiscountAmount = () => {
        const {discount, discount_type: discountType} = this.props.coupon;
        const discountAmountContainerStyles = [
            styles.discountAmountContainer,
            this.isInactive() ? {backgroundColor: "#dbdbdb"} : {}
        ];
        const discountAmountTextStyles = [
            styles.discountAmountText,
            this.isInactive() ? {color: "#9e9e9e"} : {}
        ];

        return (
            <View style={discountAmountContainerStyles}>
                <Text style={discountAmountTextStyles}>
                    {tr("coupon_ticket_discount_amount")}
                    {"\n"}
                    {this.normalizeDiscountText(discount, discountType).toUpperCase()}
                </Text>
                {this.renderVerticalDots()}
            </View>
        );
    };

    renderCouponTicketContent = () => {
        const {code = "", description, status = ACTIVE, from_time: fromTime, to_time: endTime} = this.props.coupon;
        const outOfDate = status === INACTIVE;
        const codeContainerStyles = [
            styles.codeContainer,
            this.isInactive() ? {borderColor: "#bdbdbd"} : {}
        ];
        const codeTextStyles = [
            styles.codeText,
            this.isInactive() ? {color: "#bdbdbd"} : {}
        ];
        const couponEndTimeStyles = [
            styles.couponEndTime,
            this.isInactive() ? {color: "#bdbdbd"} : {}
        ];

        return (
            <View style={styles.couponTicketContent}>
                <View style={codeContainerStyles}>
                    <Text style={codeTextStyles}>{code.toUpperCase()}</Text>
                </View>
                {outOfDate && <View style={styles.outOfDateBadgeContainer}>
                    <Text style={styles.outOfDateBadgeText}>{tr("apply_coupon_out_of_stock")}</Text>
                </View>}
                <View style={styles.descriptionContainer}>
                    <Text style={styles.descriptionText}>{description}</Text>
                </View>
                <Text style={couponEndTimeStyles}>
                    {this.renderCouponItemDuration({status, fromTime, endTime, hasSuffix: !this.isInactive()})}
                </Text>
            </View>
        );
    };


    render() {
        return (
            <View style={styles.couponTicket}>
                {this.renderDiscountAmount()}
                {this.renderCouponTicketContent()}
            </View>
        );
    }
}

export default CouponTicket;
