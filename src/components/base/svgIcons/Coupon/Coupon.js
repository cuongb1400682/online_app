import React from "react";
import Svg, {Path, G, Circle, Text, TSpan} from "react-native-svg";

const Coupon = ({isInPaymentButton = false, ...props}) => (
    isInPaymentButton
        ? <Svg xmlns="http://www.w3.org/2000/svg" width="60.107" height="33" viewBox="0 0 60.107 33" {...props}>
            <G id="Group_35" dataName="Group 35" transform="translate(-2932 -3791)">
                <G id="Group_34" dataName="Group 34" transform="translate(2898 3083)">
                    <Path id="Union_2" dataName="Union 2"
                          d="M23.265,29.948a4,4,0,0,1-3.614-2.285,4,4,0,0,1-3.614,2.285H4a4,4,0,0,1-4-4V4A4,4,0,0,1,4,0H16.036a4,4,0,0,1,3.614,2.285A4,4,0,0,1,23.265,0H56.107a4,4,0,0,1,4,4V25.949a4,4,0,0,1-4,4Z"
                          transform="translate(34 709.541)" fill="#ffa414"/>
                    <G id="Group_2" dataName="Group 2" transform="translate(52.494 713.889)">
                        <Circle id="Ellipse_1" dataName="Ellipse 1" cx="1.445" cy="1.445" r="1.445" fill="#fff"/>
                        <Circle id="Ellipse_1-2" dataName="Ellipse 1" cx="1.445" cy="1.445" r="1.445"
                                transform="translate(0 5.778)" fill="#fff"/>
                        <Circle id="Ellipse_1-3" dataName="Ellipse 1" cx="1.445" cy="1.445" r="1.445"
                                transform="translate(0 11.557)" fill="#fff"/>
                        <Circle id="Ellipse_1-4" dataName="Ellipse 1" cx="1.445" cy="1.445" r="1.445"
                                transform="translate(0 17.335)" fill="#fff"/>
                    </G>
                    <Text id="_" dataName="%" transform="translate(64.054 734)" fill="#fff"
                          fontSize="26"
                          fontFamily="Quicksand-Bold, Quicksand" fontWeight="700">
                        <TSpan x="0" y="0">%</TSpan>
                    </Text>
                </G>
            </G>
        </Svg>

        : <Svg width="17" height="13" viewBox="0 0 17 13" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
            <Path
                d="M15.9375 0H5.84375C5.70098 0 5.56816 0.0627841 5.46855 0.17358L4.78125 0.938068L4.09395 0.17358C3.99434 0.0627841 3.8582 0 3.71875 0H1.0625C0.478125 0 0 0.531818 0 1.18182V11.8182C0 12.4719 0.478125 13 1.0625 13H3.71875C3.86152 13 3.99434 12.9372 4.09395 12.8264L4.78125 12.0619L5.46855 12.8264C5.56816 12.9372 5.7043 13 5.84375 13H15.9375C16.5252 13 17 12.4719 17 11.8182V1.18182C17 0.531818 16.5252 0 15.9375 0Z"
                fill="#FFA415"/>
            <Path
                d="M9.03125 5.90904C8.15137 5.90904 7.4375 5.11501 7.4375 4.13631C7.4375 3.15762 8.15137 2.36359 9.03125 2.36359C9.91113 2.36359 10.625 3.15762 10.625 4.13631C10.625 5.11501 9.91113 5.90904 9.03125 5.90904ZM9.03125 3.5454C8.73906 3.5454 8.5 3.81131 8.5 4.13631C8.5 4.46131 8.73906 4.72722 9.03125 4.72722C9.32344 4.72722 9.5625 4.46131 9.5625 4.13631C9.5625 3.81131 9.32344 3.5454 9.03125 3.5454Z"
                fill="#FAFAFA"/>
            <Path
                d="M12.2188 10.6363C11.3389 10.6363 10.625 9.8423 10.625 8.86361C10.625 7.88492 11.3389 7.09088 12.2188 7.09088C13.0986 7.09088 13.8125 7.88492 13.8125 8.86361C13.8125 9.8423 13.0986 10.6363 12.2188 10.6363ZM12.2188 8.2727C11.9266 8.2727 11.6875 8.53861 11.6875 8.86361C11.6875 9.18861 11.9266 9.45452 12.2188 9.45452C12.5109 9.45452 12.75 9.18861 12.75 8.86361C12.75 8.53861 12.5109 8.2727 12.2188 8.2727Z"
                fill="#FAFAFA"/>
            <Path
                d="M7.96906 10.6362C7.84953 10.6362 7.73 10.5919 7.63039 10.4996C7.4046 10.2891 7.37472 9.91975 7.56398 9.66861L12.8765 2.57773C13.0658 2.3266 13.4011 2.29705 13.6236 2.50387C13.8494 2.71438 13.8792 3.0837 13.69 3.33853L8.37746 10.4294C8.27121 10.5624 8.1218 10.6362 7.96906 10.6362Z"
                fill="#FAFAFA"/>
            <Path d="M5.3125 2.36359H4.25V3.5454H5.3125V2.36359Z" fill="#FAFAFA"/>
            <Path d="M5.3125 9.45447H4.25V10.6363H5.3125V9.45447Z" fill="#FAFAFA"/>
            <Path d="M5.3125 7.09088H4.25V8.2727H5.3125V7.09088Z" fill="#FAFAFA"/>
            <Path d="M5.3125 4.72717H4.25V5.90899H5.3125V4.72717Z" fill="#FAFAFA"/>
        </Svg>
);

export default Coupon;
