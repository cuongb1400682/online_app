import React, {Component} from "react";
import Svg, {Path} from "react-native-svg";
import PropTypes from "prop-types";

import colors from "src/constants/colors";

class SlopeLine extends Component {
    static propTypes = {
        color: PropTypes.string,
        style: PropTypes.object,
    };

    static defaultProps = {
        color: colors.primaryColor,
        style: {}
    };

    render() {
        const {color, style} = this.props;

        return (
            <Svg style={style} viewBox="0 0 30 30" preserveAspectRatio="none">
                <Path d="M0,0L30,30" fill={color} stroke={color}/>
            </Svg>
        );
    }
}

export default SlopeLine;
