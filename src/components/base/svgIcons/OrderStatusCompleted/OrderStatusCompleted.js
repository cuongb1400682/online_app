import React from "react";
import Svg, {Path} from "react-native-svg";

const OrderStatusCompleted = ({color = "#000"}) => (
    <Svg className="order-status-icon" width="20" height="14" viewBox="0 0 20 14" fill="none">
        <Path d="M18.91 0L7.14 11.76L1 5.62L0 6.62L7.19 13.81L19.95 1.04L18.91 0Z" fill={color}/>
    </Svg>
);

export default OrderStatusCompleted;
