import React from "react";
import Svg, {Path} from "react-native-svg"

const GridMode = () => (
    <Svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <Path d="M0.5 19.5V12.5H7.49999V19.5H0.5Z" fill="white" stroke="#4A4A4A"/>
        <Path d="M11.6428 19.5V12.5H18.6428V19.5H11.6428Z" fill="white" stroke="#4A4A4A"/>
        <Path d="M0.5 7.49999V0.5H7.49999V7.49999H0.5Z" fill="white" stroke="#4A4A4A"/>
        <Path d="M11.6428 7.49999V0.5H18.6428V7.49999H11.6428Z" fill="white" stroke="#4A4A4A"/>
    </Svg>
);

export default GridMode;
