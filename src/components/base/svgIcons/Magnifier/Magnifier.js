import React, {Component} from "react";
import Svg, {Path} from "react-native-svg";
import PropTypes from "prop-types";

class Magnifier extends Component {
    static propTypes = {
        color: PropTypes.string,
        size: PropTypes.number
    };

    static defaultProps = {
        color: "#999",
        size: 15
    };

    render() {
        const {size} = this.props;

        return (
            <Svg style={this.props.style} width={size} height={size} viewBox="0 0 15 15" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <Path
                    d="M0 5.99863C0 2.74514 2.74576 0 6 0C9.35593 0 12 2.74514 12 5.99863C12 9.3538 9.25424 11.9973 6 11.9973C2.74576 12.0989 0 9.3538 0 5.99863ZM0.813559 5.99863C0.813559 8.84544 3.15254 11.1839 6 11.1839C8.84746 11.1839 11.1864 8.84544 11.1864 5.99863C11.1864 3.15182 8.84746 0.813374 6 0.813374C3.15254 0.813374 0.813559 3.15182 0.813559 5.99863Z"
                    fill={this.props.color}
                />
                <Path d="M10 10.566L10.566 10L15 14.5283L14.5283 15L10 10.566Z" fill={this.props.color}/>
            </Svg>

        );
    }
}

export default Magnifier;
