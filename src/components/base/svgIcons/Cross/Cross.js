import React, {Component} from "react";
import Svg, {Path} from "react-native-svg";
import PropTypes from "prop-types";

class Cross extends Component {
    static propTypes = {
        width: PropTypes.number,
        height: PropTypes.number,
        fill: PropTypes.string
    };

    static defaultProps = {
        width: 29,
        height: 29,
        fill: "#fff"
    };

    render() {
        const {width, height, fill} = this.props;

        return (
            <Svg width={width} height={height} viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                <Path
                    d="M22.163 8.02091L20.4744 6.3323L14.1421 12.6646L7.80979 6.3323L6.12118 8.02091L12.4535 14.3532L6.33225 20.4744L8.02087 22.163L14.1421 16.0418L20.2633 22.163L21.9519 20.4744L15.8307 14.3532L22.163 8.02091Z"
                    fill={fill}
                />
            </Svg>
        );
    }
}

export default Cross;
