import React, {Component} from "react";
import Svg, {Path} from "react-native-svg";
import PropTypes from "prop-types";

class VerticalEllipsis extends Component {
    static propTypes = {
        fill: PropTypes.string
    };

    static defaultProps = {
        fill: "#fff"
    };

    render() {
        const {fill} = this.props;

        return (
            <Svg width="6" height="20" viewBox="0 0 6 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <Path d="M0 0V5.03597H5.03597V0H0ZM4.02878 4.02878H1.15108V1.15108H4.02878V4.02878Z"
                      fill={fill}/>
                <Path
                    d="M0 7.48203V12.518H5.03597V7.48203H0ZM4.02878 11.5108H1.15108V8.63311H4.02878V11.5108Z"
                    fill={fill}
                />
                <Path d="M0 14.964V20H5.03597V14.964H0ZM4.02878 18.9928H1.15108V16.1151H4.02878V18.9928Z"
                      fill={fill}/>
            </Svg>
        );
    }
}

export default VerticalEllipsis;
