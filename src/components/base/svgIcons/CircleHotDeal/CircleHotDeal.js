import React from "react";
import Svg, {Path} from "react-native-svg";

const CircleHotDeal = () => {
    return (
        <Svg fill="#fff" preserveAspectRatio="xMidYMid meet" width="20" height="20" rotate="0" stroke-width="1"
             className=" svg-icon" viewBox="0 0 19 19">
            <Path d="M5.4,9.6H4.3V7.3H2.1v2.3H1V4.2h1.1v2.2h2.2V4.2h1.1V9.6z"/>
            <Path d="M18.9,5.1h-1.7v4.5h-1.1V5.1h-1.6V4.2h4.4V5.1z"/>
            <Path
                d="M0.7,16.6v-5.4h1.7c0.4,0,0.9,0.1,1.3,0.3c0.4,0.2,0.7,0.5,0.9,0.9c0.2,0.4,0.3,0.9,0.3,1.4V14 c0,0.5-0.1,0.9-0.3,1.3c-0.2,0.4-0.5,0.7-0.9,0.9c-0.4,0.2-0.8,0.3-1.3,0.3H0.7z M1.8,12.1v3.6h0.5c0.4,0,0.8-0.1,1-0.4 c0.3-0.4,0.4-0.8,0.4-1.2v-0.3c0-0.4-0.1-0.8-0.3-1.2c-0.2-0.3-0.6-0.5-1-0.4L1.8,12.1z"/>
            <Path d="M8.9,14.3H6.8v1.5h2.5v0.9H5.6v-5.4h3.6v0.9H6.8v1.3h2.1L8.9,14.3z"/>
            <Path d="M13,15.5h-2l-0.3,1.1H9.5l2-5.4h1l2.1,5.4h-1.2L13,15.5z M11.3,14.6h1.4l-0.7-2L11.3,14.6z"/>
            <Path d="M16.2,15.7h2.4v0.9H15v-5.4h1.1V15.7z"/>
            <Path
                d="M11.1,1.8c0.2,0.3,0.4,0.6,0.5,0.9c0.1,0.2,0.1,0.4,0.1,0.7c0,0.4,0,0.7-0.1,1.1c0.1,0,0.1-0.1,0.2-0.1 c0.2-0.2,0.3-0.3,0.5-0.6c0,0,0,0,0.1,0c0.6,0.5,1.1,1.2,1.3,2c0.1,0.3,0.1,0.7,0.1,1c0,0.4-0.2,0.8-0.4,1.2c-0.4,0.6-0.9,1-1.5,1.3 c-0.3,0.1-0.5,0.2-0.8,0.3c-0.1,0-0.2,0-0.3,0c0.1-0.1,0.2-0.3,0.2-0.4c0.1-0.2,0.2-0.5,0.2-0.8c0.1-0.4-0.1-0.9-0.4-1.2 c-0.3-0.3-0.6-0.6-0.7-1C10,5.9,9.9,5.7,10,5.5c0-0.2,0-0.3,0.1-0.5C9.9,5.1,9.8,5.2,9.7,5.3c-0.4,0.3-0.7,0.6-1,1 C8.5,6.5,8.4,6.9,8.4,7.2c0,0.4,0,0.8,0.2,1.1c0.2,0.4,0.4,0.7,0.6,1l0.2,0.2C8.7,9.4,8,9.2,7.5,8.8C7.3,8.6,7.1,8.4,6.9,8.3 c-0.4-0.5-0.6-1-0.6-1.6c0-0.4,0.1-0.7,0.2-1.1C6.7,5.3,6.9,5,7.2,4.8c0.3-0.3,0.7-0.6,1-0.9C8.5,3.6,8.8,3.3,9,2.9 C9.4,2.5,9.6,2,9.6,1.5c0-0.3,0-0.5-0.1-0.8l0,0l0,0c0,0,0.1,0,0.1,0c0.4,0.2,0.9,0.4,1.2,0.7C10.9,1.5,11,1.6,11.1,1.8z"/>
        </Svg>
    )
};

export default CircleHotDeal;
