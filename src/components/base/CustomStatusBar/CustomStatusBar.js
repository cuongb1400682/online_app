import React, {Component} from "react";
import {NativeModules, Platform, StatusBar, StatusBarIOS, View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {getStatusBarHeight} from "src/utils/reactNative/statusBar";

const {StatusBarManager} = NativeModules;

class CustomStatusBar extends Component {
    static propTypes = {
        style: PropTypes.any,
    };

    static defaultProps = {
        style: {},
    };

    constructor(props) {
        super(props);

        this.state = {
            // Get the fixed value
            statusBarHeight: getStatusBarHeight()
        };
    }

    componentDidMount() {
        if (Platform.OS === "ios") {
            // Receive default height returned by StatusBarManager
            StatusBarManager.getHeight(this.setDefaultStatusBarHeight);

            // Listen to the height-changing event of StatusBar
            this.setupStatusBarHeightListener();
        }
    }

    componentWillUnmount() {
        if (Platform.OS === "ios" && this.statusBarHeightListener) {
            this.statusBarHeightListener.remove();
        }
    }

    setDefaultStatusBarHeight = (response) => {
        if (response && response.height) {
            this.setState({statusBarHeight: response.height});
        }
    };

    setupStatusBarHeightListener = () => {
        this.statusBarHeightListener = StatusBarIOS.addListener("statusBarFrameWillChange", (response) => {
            if (response && response.frame && response.frame.height) {
                this.setState({
                    statusBarHeight: response.frame.height
                });
            }
        });
    };

    render() {
        const {style} = this.props;
        const statusBarHeightStyle = {
            height: this.state.statusBarHeight
        };

        return (
            <React.Fragment>
                <StatusBar
                    translucent
                    barStyle="light-content"
                    backgroundColor="rgba(255, 255, 255, 0)"
                    animated
                />
                <View style={[styles.statusBarBackground, statusBarHeightStyle, style]}/>
            </React.Fragment>
        );
    }
}

export default CustomStatusBar;
