import {StyleSheet, Platform} from "react-native";
import {getStatusBarHeight} from "src/utils/reactNative/statusBar";

const styles = StyleSheet.create({
    statusBarBackground: {
        height: getStatusBarHeight(),
        backgroundColor: Platform.select({
            ios: "#rgba(0, 0, 0, 0.2)",
            android: "transparent"
        })
    }
});

export default styles;
