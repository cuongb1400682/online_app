import React, {Component} from "react";
import {TextInput, View, Keyboard} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import colors from "src/constants/colors";
import FontIcon from "src/components/base/FontIcon/FontIcon";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {isEmptyString} from "src/utils/extensions/strings";
import commonStyles from "src/styles/commonStyles";
import systemFont from "src/styles/systemFont";

class NavBarInput extends Component {
    static propTypes = {
        style: PropTypes.any,
        containerStyle: PropTypes.any,
        leftViews: PropTypes.any,
        rightViews: PropTypes.any,
        showsClearButton: PropTypes.bool,
        placeholder: PropTypes.string,
        returnKeyLabel: PropTypes.string,
        onSubmitEditing: PropTypes.func,
    };

    static defaultProps = {
        style: {},
        containerStyle: {},
        leftViews: null,
        rightViews: null,
        showsClearButton: false,
        placeholder: "",
        returnKeyLabel: "search",
        onSubmitEditing: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            isClearButtonVisible: false
        };
    }

    onChangeText = (text) => {
        const {onChangeText} = this.props;

        this.setState({
            isClearButtonVisible: !isEmptyString(text)
        });

        if (typeof onChangeText === "function") {
            onChangeText(text);
        }
    };

    clearText = () => {
        Keyboard.dismiss();

        if (this.textInput) {
            if (typeof this.textInput.clear === "function") {
                this.textInput.clear();
                this.onChangeText("");
            }
        }
    };

    render() {
        const {style, showsClearButton, containerStyle, leftViews, rightViews, placeholder, onSubmitEditing, returnKeyLabel, ...rest} = this.props;
        const {isClearButtonVisible} = this.state;

        return (
            <View style={[styles.textInputContainer, containerStyle]}>
                {leftViews}
                <TextInput
                    ref={ref => this.textInput = ref}
                    style={[systemFont.regular, styles.textInput, style]}
                    placeholderTextColor="#999"
                    placeholder={placeholder}
                    selectionColor={colors.primaryColor}
                    onSubmitEditing={onSubmitEditing}
                    underlineColorAndroid="transparent"
                    returnKeyLabel={returnKeyLabel}
                    {...rest}
                    onChangeText={this.onChangeText}
                />
                {showsClearButton && isClearButtonVisible &&
                <Touchable style={styles.clearButtonTouchable} onPress={this.clearText}>
                    <FontIcon name="ios-close-circle" fontName="ionicons" style={styles.clearButtonIcon}/>
                </Touchable>}
                {rightViews}
            </View>
        );
    }
}

export default NavBarInput;
