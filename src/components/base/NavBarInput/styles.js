import {StyleSheet, Platform} from "react-native";

import colors from "src/constants/colors";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    textInputContainer: {
        height: Platform.select({android: 35, ios: 30}),
        flex: 1,
        backgroundColor: "#fff",
        borderRadius: 3,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    textInput: {
        flex: 1,
        marginLeft: 8,
        color: colors.textColor,
        ...Platform.select({
            android: {
                fontSize: 12,
                height: 40,
            },
            ios: {}
        })
    },
    clearButtonIcon: {
        color: "#757575",
        fontSize: 15
    },
    clearButtonTouchable: {
        width: 30,
        height: 30,
        ...commonStyles.centerChildren
    }
});

export default styles;
