import React, {Component} from "react";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import FontAwesome5Pro from "react-native-vector-icons/FontAwesome5Pro";
import Foundation from "react-native-vector-icons/Foundation";
import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Octicons from "react-native-vector-icons/Octicons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Zocial from "react-native-vector-icons/Zocial";
import PropTypes from "prop-types";

export const supportedIconFonts = {
    "antdesign": AntDesign,
    "entypo": Entypo,
    "evilicons": EvilIcons,
    "feather": Feather,
    "fontawesome": FontAwesome,
    "fontawesome5": FontAwesome5,
    "fontawesome5pro": FontAwesome5Pro,
    "foundation": Foundation,
    "ionicons": Ionicons,
    "materialcommunityicons": MaterialCommunityIcons,
    "materialicons": MaterialIcons,
    "octicons": Octicons,
    "simplelineicons": SimpleLineIcons,
    "zocial": Zocial
};

class FontIcon extends Component {
    static propTypes = {
        style: PropTypes.any,
        fontName: PropTypes.string,
        name: PropTypes.string,
    };

    static defaultProps = {
        style: {},
        fontName: "fontawesome",
        name: "",
    };

    render() {
        let {name, fontName, style} = this.props;

        if (typeof name === "string") {
            name = name.toLowerCase();
        }

        if (typeof fontName === "string") {
            fontName = fontName.toLowerCase();
        }

        if (!name) {
            return null;
        }

        const IconFont = supportedIconFonts[fontName];

        return (
            <IconFont name={name} style={style}/>
        );
    }
}

export default FontIcon;
