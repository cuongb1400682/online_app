import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    disabledText: {
        color: "#8a8a8a"
    }
});

export default styles;
