import React from "react";
import {TouchableOpacity} from "react-native";

import styles from "./styles";
import defaultOnPressHandler from "src/utils/reactNative/defaultOnPressHandler";
import Text from "src/components/platformSpecific/Text/Text";

const LinkButton = ({title = "", textStyle = {}, touchableStyle = {}, disabled = false, screenId, navigator, onPress, passProps = {}}) => {
    const textStyles = [
        textStyle,
        disabled ? styles.disabledText : {}
    ];

    return (
        <TouchableOpacity
            style={touchableStyle}
            onPress={defaultOnPressHandler({screenId, navigator, onPress, passProps})}
            disabled={disabled}
        >
            <Text style={textStyles}>{title}</Text>
        </TouchableOpacity>
    );
};

export default LinkButton;
