import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    radioTouchable: {
        flexDirection: "row",
        alignItems: "center"
    },
    radioContent: {
        flexDirection: "row"
    },
    outerCircle: {
        width: 24,
        height: 24,
        borderRadius: 24,
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: colors.primaryColor,
        justifyContent: "center",
        alignItems: "center",
        marginRight: 16,
        marginTop: 6
    },
    innerCircle: {
        height: 12,
        width: 12,
        borderRadius: 24,
        backgroundColor: colors.primaryColor
    },
    radioText: {
        fontSize: 20,
        flexWrap: "wrap",
        flex: 1,
    }
});

export default styles;
