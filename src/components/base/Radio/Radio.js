import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import colors from "src/constants/colors";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import Text from "src/components/platformSpecific/Text/Text";

class Radio extends Component {
    static propTypes = {
        color: PropTypes.string,
        size: PropTypes.number,
        selected: PropTypes.bool,
        children: PropTypes.any,
        style: PropTypes.any,
        radioStyle: PropTypes.any,
        onPress: PropTypes.func
    };

    static defaultProps = {
        color: colors.primaryColor,
        size: 24,
        selected: false,
        children: null,
        radioStyle: {},
        style: {},
        onPress: () => {
        }
    };

    render() {
        const {color, selected, children, style, size, onPress, radioStyle} = this.props;
        const outerCircleStyles = [
            styles.outerCircle,
            {
                borderColor: color,
                width: size,
                height: size
            },
            radioStyle
        ];
        const innerCircleStyles = [styles.innerCircle, {
            backgroundColor: color,
            width: size / 1.5,
            height: size / 1.5
        }];

        return (
            <Touchable style={style} onPress={onPress}>
                <View style={styles.radioContent}>
                    <View style={outerCircleStyles}>
                        {selected && <View style={innerCircleStyles}/>}
                    </View>
                    {typeof children === "string"
                        ? <Text style={styles.radioText}>{children}</Text>
                        : children}
                </View>
            </Touchable>
        );
    }
}

export default Radio;
