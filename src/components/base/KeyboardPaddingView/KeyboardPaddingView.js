import React, {Component} from "react";
import * as Animatable from "react-native-animatable";
import {Keyboard} from "react-native";
import PropTypes from "prop-types";

class KeyboardPaddingView extends Component {
    static propTypes = {
        initialHeight: PropTypes.number,
        customPadding: PropTypes.number,
    };

    static defaultProps = {
        initialHeight: 0,
        customPadding: 0,
    };

    constructor(props) {
        super(props);

        this.state = {
            paddingHeight: props.initialHeight
        };
    }

    componentDidMount() {
        this.keyboardDidShow = Keyboard.addListener("keyboardDidShow", this.keyboardDidShow);
        this.keyboardDidHide = Keyboard.addListener("keyboardDidHide", this.keyboardDidHide);
    }

    componentWillUnmount() {
        if (this.keyboardDidShow) {
            this.keyboardDidShow.remove();
        }

        if (this.keyboardDidHide) {
            this.keyboardDidHide.remove();
        }
    }

    keyboardDidShow = (event) => {
        this.setState({
            paddingHeight: event.endCoordinates.height
        });
    };

    keyboardDidHide = () => {
        this.setState({
            paddingHeight: this.props.initialHeight
        });
    };

    render() {
        const {customPadding} = this.props;
        const {paddingHeight} = this.state;

        return (
            <Animatable.View
                style={{height: paddingHeight + customPadding}}
                transition="height"
                easing="ease-in"
            />
        );
    }
}

export default KeyboardPaddingView;
