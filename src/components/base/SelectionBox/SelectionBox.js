import React, {Component} from "react";
import {Keyboard, Platform, View} from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import {isEmptyArray} from "src/utils/extensions/arrays";
import Text from "src/components/platformSpecific/Text/Text";
import {tr} from "src/localization/localization";
import Picker from "src/components/platformSpecific/Picker/Picker";

class SelectionBox extends Component {
    static propTypes = {
        dataSource: PropTypes.array,
        style: PropTypes.object,
        contentContainerStyle: PropTypes.any,
        title: PropTypes.string,
        leftIconName: PropTypes.string,
        rightIconName: PropTypes.string,
        value: PropTypes.any,
        placeholder: PropTypes.string,
        onSelect: PropTypes.func,
    };

    static defaultProps = {
        dataSource: [],
        style: {},
        contentContainerStyle: {},
        title: "",
        leftIconName: null,
        rightIconName: null,
        value: "",
        placeholder: "",
        onSelect: () => {
        }
    };

    constructor(props) {
        super(props);

        const currentItem = props.dataSource.find(data => data.value === props.value);

        this.state = {
            currentItem: currentItem
        };
    }

    // TODO: replace by getDerivedStateFromProps
    UNSAFE_componentWillReceiveProps(nextProps) {
        const {dataSource} = nextProps;
        const newCurrentItem = dataSource.find(data => data.value === nextProps.value);

        this.setState({
            currentItem: newCurrentItem
        });
    }

    componentDidMount() {
        this.listener = Keyboard.addListener("keyboardWillShow", this.hiderPicker);

        this.hiderPicker();
    }

    componentWillUnmount() {
        if (this.listener) {
            this.listener.remove();
        }

        this.hiderPicker();
    }

    getDataSourceToRender = () => {
        const {dataSource} = this.props;

        return dataSource
            .map(data => data && data.displayValue)
            .filter(item => !!item);
    };

    onPickerConfirm = (_, selectedIndex) => {
        const {onSelect, dataSource = []} = this.props;

        const selectedItem = dataSource[selectedIndex];

        onSelect(selectedItem);
        this.setState({currentItem: selectedItem});
    };

    showPicker = () => {
        if (this.pickerRef) {
            this.pickerRef.show();
        }
    };

    hiderPicker = () => {
        if (this.pickerRef) {
            this.pickerRef.hide();
        }
    };

    render() {
        const {leftIconName, rightIconName, placeholder, style, contentContainerStyle} = this.props;
        const {currentItem = {}} = this.state;
        const showPlaceholder = !currentItem;
        const textStyles = [
            showPlaceholder ? styles.placeholder : {},
            Platform.select({
                android: styles.text,
                ios: {}
            })
        ];

        return (
            <React.Fragment>
                <Touchable
                    style={style}
                    disabled={isEmptyArray(this.getDataSourceToRender())}
                    onPress={this.showPicker}
                >
                    <View style={[styles.selectionBox, contentContainerStyle]}>
                        <FontAwesome5
                            name={leftIconName}
                            style={[styles.selectionBoxIcon, styles.selectionBoxHeadingIcon]}
                        />
                        <Text style={textStyles}>
                            {showPlaceholder ? placeholder : currentItem.displayValue}
                        </Text>
                        <FontAwesome5
                            name={rightIconName}
                            style={[styles.selectionBoxIcon, styles.selectionBoxTrailingIcon]}
                        />
                    </View>
                </Touchable>
                <Picker
                    ref={ref => this.pickerRef = ref}
                    pickerData={this.getDataSourceToRender()}
                    onPickerConfirm={this.onPickerConfirm}
                    titleText={this.props.placeholder}
                    selectedValue={currentItem.displayValue}
                    cancelText={tr("selection_box_picker_cancel_button_text")}
                    confirmText={tr("selection_box_picker_confirm_button_text")}
                />
            </React.Fragment>
        );
    }
}

export default SelectionBox;
