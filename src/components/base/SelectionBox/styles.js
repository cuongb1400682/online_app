import {StyleSheet, Platform} from "react-native";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    selectionBox: {
        height: 36,
        width: "100%",
        backgroundColor: "#fff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#ddd",
        paddingHorizontal: 16,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    selectionBoxIcon: {
        fontSize: 14,
        color: "#222"
    },
    selectionBoxHeadingIcon: {
        position: "absolute",
        left: 16
    },
    selectionBoxTrailingIcon: {
        position: "absolute",
        right: 16
    },
    placeholder: {
        color: "#CDCDD1",
    },
    text: {
        fontSize: 14,
        ...systemFont.regular
    }
});

export default styles;
