import {StyleSheet} from "react-native";
import colors from "src/constants/colors";
import commonStyles from "src/styles/commonStyles";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    address: {
        backgroundColor: "#fff",
        paddingRight: 50,
        flexDirection: "column",
        flex: 1,
        flexWrap: "wrap",
    },
    customerName: {
        fontSize: 14,
        fontWeight: "bold",
    },
    customerInfo: {
        fontSize: 14,
    },
    defaultAddress: {
        fontSize: 12,
        color: colors.primaryColor,
        marginTop: 6
    },
    ellipsisContainer: {
        width: 36,
        height: 36,
        ...commonStyles.centerChildren,
    },
    triggerTouchable: {
        height: 36
    },
    menu: {
        zIndex: 1000,
        width: 36,
        height: 36,
        padding: 0,
        margin: 0,
        position: "absolute",
        top: 0,
        right: 0,
        ...systemFont.regular
    },
    menuOption: {
        height: 40,
        width: 146,
        flexDirection: "column",
        justifyContent: "center",
        paddingLeft: 16,
        ...systemFont.regular
    },
    menuOptionText: {
        fontSize: 14
    }
});

export default styles;
