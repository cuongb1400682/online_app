import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import {Menu, MenuOption, MenuOptions, MenuTrigger} from "react-native-popup-menu";

import styles from "./styles";
import {tr} from "src/localization/localization";
import VerticalEllipsis from "src/components/base/svgIcons/VerticalEllipsis/VerticalEllipsis";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {capitalizeString} from "src/utils/extensions/strings";
import Text from "src/components/platformSpecific/Text/Text";

class Address extends Component {
    static propTypes = {
        name: PropTypes.string,
        address: PropTypes.string,
        phone: PropTypes.string,
        isDefault: PropTypes.bool,
        onDeleteAddress: PropTypes.func,
        onEditAddress: PropTypes.func
    };

    static defaultProps = {
        name: "",
        address: "",
        phone: "",
        isDefault: false,
        onDeleteAddress: () => {
        },
        onEditAddress: () => {
        }
    };

    renderMenu = () => {
        const {isDefault, onDeleteAddress, onEditAddress} = this.props;

        return (
            <Menu style={styles.menu}>
                <MenuTrigger
                    customStyles={{
                        TriggerTouchableComponent: Touchable,
                        triggerTouchable: styles.triggerTouchable
                    }}
                >
                    <View style={styles.ellipsisContainer}>
                        <VerticalEllipsis fill="#757575"/>
                    </View>
                </MenuTrigger>
                <MenuOptions>
                    <MenuOption
                        style={styles.menuOption}
                        customStyles={styles.menuOptionText}
                        text={tr("address_menu_edit_address")}
                        onSelect={onEditAddress}
                    />
                    {!isDefault && <MenuOption
                        style={styles.menuOption}
                        customStyles={styles.menuOptionText}
                        text={tr("address_menu_delete_address")}
                        onSelect={onDeleteAddress}
                    />}
                </MenuOptions>
            </Menu>
        );
    };

    render() {
        const {name, address, phone, isDefault} = this.props;

        return (
            <View style={styles.address}>
                {this.renderMenu()}
                <Text style={styles.customerName}>{capitalizeString(name)}</Text>
                <Text style={styles.customerInfo}>{tr("address_customer_address")}: {address}</Text>
                <Text style={styles.customerInfo}>{tr("address_customer_phone")}: {phone}</Text>
                {isDefault && <Text style={styles.defaultAddress}>{tr("address_customer_default")}</Text>}
            </View>
        );
    }
}

export default Address;
