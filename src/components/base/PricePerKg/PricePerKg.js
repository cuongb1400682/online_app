import React from "react";
import {View} from "react-native";

import styles from "./styles";
import {formatWithCurrency} from "src/utils/extensions/strings";
import Text from "src/components/platformSpecific/Text/Text";

const PricePerKg = ({pricePerKg = 0, containerStyle = {}, textStyle = {}}) => (
    !!pricePerKg && <View style={[styles.pricePerKgContainer, containerStyle]}>
        <Text style={[styles.pricePerKgText, textStyle]}>{formatWithCurrency(pricePerKg)}/kg</Text>
    </View>
);

export default PricePerKg;
