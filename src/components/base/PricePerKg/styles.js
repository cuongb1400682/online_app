import {StyleSheet} from "react-native";
import commonStyles from "src/styles/commonStyles";

const styles = StyleSheet.create({
    pricePerKgContainer: {
        paddingHorizontal: 4,
        paddingVertical: 1,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#51AA1B",
        borderRadius: 3,
        backgroundColor: "#F2FEF2",
        ...commonStyles.centerChildren
    },
    pricePerKgText: {
        color: "#51AA1B",
        fontSize: 12,
    }
});

export default styles;
