import {StyleSheet} from "react-native";

import colors from "src/constants/colors";
import {HORIZONTAL_PRODUCT_HEIGHT} from "src/utils/layouts/productLayout";

const styles = StyleSheet.create({
    horizontalProduct: {
        width: "100%",
        height: HORIZONTAL_PRODUCT_HEIGHT,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#e2e2e2",
        flexDirection: "row",
        backgroundColor: "#fff"
    },
    imagePart: {
        width: HORIZONTAL_PRODUCT_HEIGHT,
        height: HORIZONTAL_PRODUCT_HEIGHT - 3,
        marginRight: 12
    },
    productImage: {
        flex: 1
    },
    vietGapIcon: {
        width: 30,
        height: 30,
        position: "absolute",
        top: 4,
        right: 4,
        zIndex: 2
    },
    hotDealIcon: {
        width: 30,
        height: 30,
        position: "absolute",
        top: 8,
        left: 8
    },
    giftIcon: {
        width: 14,
        height: 14,
        position: "absolute",
        bottom: 6,
        left: 2,
        zIndex: 2
    },
    productInfoContainer: {
        flex: 1,
        flexDirection: "column",
        flexWrap: "wrap",
    },
    productName: {
        fontSize: 14,
        marginTop: 14,
        maxHeight: 36,
        width: "100%"
    },
    unitPrice: {
        fontSize: 14,
        color: colors.primaryColor
    },
    marketPrice: {
        fontSize: 10,
        marginTop: 4,
        color: "#a2a2a2",
    },
    pricePart: {
        width: "100%",
        flexDirection: "column",
        marginTop: 10
    },
    progressBarBackground: {
        width: "100%",
        height: 3,
        backgroundColor: "#d7d7d7",
        marginBottom: 6
    },
    progressBarInner: {
        backgroundColor: colors.primaryColor
    },
    dealPart: {
        width: "100%",
        paddingRight: 20,
        position: "absolute",
        bottom: 10
    },
    dealText: {
        fontSize: 12,
    },
    pricePerKg: {
        position: "absolute",
        right: 0,
        bottom: 10,
    },
    dealTag: {
        position: "absolute",
        top: 5,
        left: 5
    }
});

export default styles;
