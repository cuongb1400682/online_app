import * as tComb from "tcomb-form-native";

import colors from "src/constants/colors";
import {Platform} from "react-native";

const styles = tComb.form.Form.stylesheet;

const overrideFormStyleSheetAndroid = () => {
    styles.textbox.normal.fontSize = 12;
    styles.textbox.normal.fontFamily = "Roboto-Regular";

    styles.textbox.error.fontSize = 12;
    styles.textbox.error.fontFamily = "Roboto-Regular";

    styles.controlLabel.normal.color = colors.textColor;
    styles.controlLabel.normal.fontWeight = "normal";
    styles.controlLabel.normal.fontSize = 12;
    styles.controlLabel.normal.fontFamily = "Roboto-Regular";

    styles.controlLabel.error.fontWeight = "normal";
    styles.controlLabel.error.fontSize = 12;
    styles.controlLabel.error.fontFamily = "Roboto-Regular";

    styles.errorBlock.fontSize = 10;
    styles.errorBlock.fontFamily = "Roboto-Regular";
};

const overrideFormStyleSheetIOS = () => {
    styles.textbox.normal.fontSize = 14;
    styles.textbox.error.fontSize = 14;

    styles.controlLabel.normal.color = colors.textColor;
    styles.controlLabel.normal.fontSize = 14;
    styles.controlLabel.normal.fontWeight = "normal";
    styles.controlLabel.error.fontSize = 14;
    styles.controlLabel.error.fontWeight = "normal";

    styles.errorBlock.fontSize = 10;
};

if (Platform.OS === "android") {
    module.exports = overrideFormStyleSheetAndroid;
} else {
    module.exports = overrideFormStyleSheetIOS;
}
