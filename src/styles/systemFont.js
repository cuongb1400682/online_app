import {Platform, StyleSheet} from "react-native";

const systemFontName = Platform.select({
    ios: "sans-serif",
    android: "Roboto-Regular"
});

const systemFont = StyleSheet.create({
    regular: Platform.select({
        ios: {
            fontWeight: "normal"
        },
        android: {
            fontFamily: "Roboto-Regular"
        }
    }),
    bold: Platform.select({
        ios: {
            fontWeight: "bold"
        },
        android: {
            fontFamily: "Roboto-Bold"
        }
    }),
    italic: Platform.select({
        ios: {
            fontStyle: "italic"
        },
        android: {
            fontFamily: "Roboto-Italic"
        }
    }),
    boldItalic: Platform.select({
        ios: {
            fontWeight: "bold",
            fontStyle: "italic"
        },
        android: {
            fontFamily: "Roboto-BoldItalic"
        }
    })
});

export {
    systemFont as default,
    systemFontName
};
