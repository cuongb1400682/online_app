import {StyleSheet, Platform} from "react-native";

const commonStyles = StyleSheet.create({
    absolutelyFill: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    matchParent: {
        flex: 1
    },
    centerChildren: {
        justifyContent: "center",
        alignItems: "center"
    },
    flexRow: {
        flexDirection: "row"
    },
    flexColumn: {
        flexDirection: "column"
    },
    flexRowCenter: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    flexColumnCenter: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    hasShadow: {
        shadowColor: "#000",
        shadowOpacity: 0.3,
        shadowOffset: {
            width: -2,
            height: 2
        },
        elevation: 5
    },
    hidden: {
        display: "none"
    },
});

export default commonStyles;
