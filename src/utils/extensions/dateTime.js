import "moment/locale/vi";

import {addLeadingZeros} from "src/utils/extensions/strings";
import {shippingMethods} from "src/constants/enums";
import {
    addMinutesMoment,
    convertToDateToUTC,
    formatMomentToString,
    getMomentFromString,
    getMomentNextDay
} from "src/utils/extensions/moment";
import {tr} from "src/localization/localization";

const parseTime = timeString => {
    if (typeof timeString !== "string") {
        return {
            hour: 0,
            minute: 0,
            second: 0
        };
    }

    const times = timeString.split(":");

    return {
        hour: parseInt(times[0], 10),
        minute: parseInt(times[1], 10),
        second: parseInt(times[2], 10)
    };
};

const calculateCountdown = endDate => {
    let diff = (Date.parse(new Date(endDate)) - Date.parse(new Date())) / 1000;

    // clear countdown when date is reached
    if (diff <= 0) return false;

    const timeLeft = {
        years: 0,
        days: 0,
        hours: 0,
        min: 0,
        sec: 0
    };

    // calculate time difference between now and expected date
    if (diff >= (365.25 * 86400)) { // 365.25 * 24 * 60 * 60
        timeLeft.years = Math.floor(diff / (365.25 * 86400));
        diff -= timeLeft.years * 365.25 * 86400;
    }

    if (diff >= 86400) { // 24 * 60 * 60
        timeLeft.days = Math.floor(diff / 86400);
        diff -= timeLeft.days * 86400;
    }

    if (diff >= 3600) { // 60 * 60
        timeLeft.hours = Math.floor(diff / 3600);
        diff -= timeLeft.hours * 3600;
    }

    if (diff >= 60) {
        timeLeft.min = Math.floor(diff / 60);
        diff -= timeLeft.min * 60;
    }

    timeLeft.sec = diff;

    return timeLeft;
};

const formatTimeRange = (hour, minute) => {
    return `${addLeadingZeros(hour)}:${addLeadingZeros(minute)}`
};

const generateShippingTime = (from, to, minuteStep = 60) => {
    const {hour: fromHour, minute: fromMinute} = parseTime(from);
    const {hour: toHour, minute: toMinute} = parseTime(to);
    const result = [];
    const timeRanges = [];
    const fromTime = fromHour + fromMinute / 60;
    const toTime = toHour + toMinute / 60;
    const timeStep = minuteStep / 60;

    for (let i = fromTime; i <= toTime; i += timeStep) {
        const actualHour = Math.floor(i);
        const actualMinute = (i - Math.floor(i)) * 60;
        timeRanges.push({actualHour, actualMinute});
    }
    timeRanges[timeRanges.length - 1] = {actualHour: toHour, actualMinute: toMinute};
    timeRanges.forEach(({actualHour, actualMinute}, index, data) => {
        if (index < data.length - 1) {
            let currentTime = formatTimeRange(actualHour, actualMinute);
            let nextTime = formatTimeRange(data[index + 1].actualHour, data[index + 1].actualMinute);
            result.push({value: currentTime, displayValue: `${currentTime} - ${nextTime}`});
        }
    });
    return [{value: "0", displayValue: `Giao trước ${fromHour || "00"}:${fromMinute || "00"}`}, ...result];
};

const parseToOrderTime = (countDay, time, shippingMethod, shippingTimeData) => {
    if (shippingMethod === shippingMethods.FAST) {
        return {};
    }

    const today = getMomentFromString(shippingTimeData.current_date);
    const parseDate = getMomentNextDay(today, countDay);
    const selectedShippingTime = countDay === 0 ? shippingTimeData.today : shippingTimeData.others;

    if (!selectedShippingTime) {
        return {};
    }

    const earliestTime = selectedShippingTime.start.substring(0, 5);
    const isAsap = (time === DEFAULT_TIME_VALUE);
    const selectedTime = isAsap ? earliestTime : time;

    if (!selectedTime) {
        return {};
    }

    const [hour, minute] = selectedTime.split(":");
    const fromExpectedShippingTime = parseDate.clone().hour(hour).minute(minute);
    const toExpectedShippingTime = isAsap
        ? fromExpectedShippingTime.clone()
        : addMinutesMoment(fromExpectedShippingTime, selectedShippingTime.range || 0);

    return {
        from_expected_shipping_time: convertToDateToUTC(fromExpectedShippingTime).toISOString(),
        to_expected_shipping_time: convertToDateToUTC(toExpectedShippingTime).toISOString()
    };
};

const getEstimateShippingTime = (fromDateString, toDateString, format = "HH:mm dddd, DD/MM/YYYY") => {
    const fromDateMoment = getMomentFromString(fromDateString);
    const fromTimeFormat = formatMomentToString(fromDateMoment, "HH:mm");

    const toDateMoment = getMomentFromString(toDateString);
    const toTimeFormat = formatMomentToString(toDateMoment, format);

    return fromDateString === toDateString ? `Trước ${toTimeFormat}` : `${fromTimeFormat} - ${toTimeFormat}`;
};

const DEFAULT_TIME_VALUE = "0";

export {
    getEstimateShippingTime,
    parseToOrderTime,
    generateShippingTime,
    calculateCountdown,
    DEFAULT_TIME_VALUE
};
