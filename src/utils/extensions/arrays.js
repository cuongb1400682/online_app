const isEmptyArray = (array) => (array instanceof Array && array.length === 0) || false;

const makeIntRange = (from, to, step = 1) => {
    let result = [];

    for (let i = from ;i <= to; i += step) {
        result.push(i);
    }

    return result;
};

const arrayFlatMap = (array, callback) => {
    if (!Array.isArray(array)) {
        return [];
    }

    return array.reduce((accumulatedValue, currentValue, index) => {
        const mappedValue = (typeof callback === "function") ? callback(currentValue, index): [];

        if (Array.isArray(mappedValue)) {
            accumulatedValue = [...accumulatedValue, ...mappedValue];
        } else {
            accumulatedValue.push(mappedValue);
        }

        return accumulatedValue;
    }, []);
};

export {
    isEmptyArray,
    makeIntRange,
    arrayFlatMap
};


