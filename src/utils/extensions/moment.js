import timezone from "moment-timezone";

import localeVi from "src/constants/localeVi";
import {formatDateWithTime} from "src/constants/dateTime";

timezone.defineLocale("vi", localeVi);

const DEFAULT_TIMEZONE = "Asia/Ho_Chi_Minh";

const getMomentToday = () => timezone().tz(DEFAULT_TIMEZONE);

const getMomentFromString = strDate => {
    if (typeof strDate !== "string") {
        return timezone().tz(DEFAULT_TIMEZONE);
    }

    return timezone(strDate).tz(DEFAULT_TIMEZONE);
};

const getMomentNextDay = (fromDate, days = 1) => {
    if (fromDate instanceof timezone) {
        return fromDate.clone().add(days, "days");
    }

    if (typeof fromDate === "string") {
        return getMomentFromString(fromDate).clone().add(days, "days");
    }

    return getMomentFromString().add(days, "days");
};

const convertToDateToUTC = date => {
    if (!(date instanceof timezone)) {
        return date;
    }

    return date.clone().utc();
};

const formatMomentToString = (date, format = formatDateWithTime) => {
    if (!(date instanceof timezone)) {
        return "";
    }

    return date.clone().format(format);
};

const addMinutesMoment = (moment, minutes) => moment instanceof timezone ? moment.clone().add(minutes, "minutes") : null;

export {
    getMomentNextDay,
    getMomentFromString,
    convertToDateToUTC,
    formatMomentToString,
    getMomentToday,
    addMinutesMoment
};
