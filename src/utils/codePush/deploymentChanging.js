import {Alert, AsyncStorage, Platform} from "react-native";
import codePush from "react-native-code-push";
import Sentry from "react-native-sentry";

import deploymentKeys from "src/constants/deploymentKeys";
import Message from "src/components/platformSpecific/Message/Message";
import {tr} from "src/localization/localization";
import {getDefaultSyncOptions, getStoredDeploymentName} from "src/utils/codePush/codePush";
import {firstLetterUppercase} from "src/utils/extensions/strings";
import store from "src/redux/store";
import {changeUpdateMetaData} from "src/redux/common/deploymentActions";
import {codePushDeploymentNames} from "src/constants/enums";

let savedDownloadedPercent = 0;

const DEPLOYMENT_KEY = "DEPLOYMENT_KEY";

const clearStorage = () => {
    AsyncStorage.clear();
};

const syncStatusChanged = statusValue => {
    switch (statusValue) {
        case codePush.SyncStatus.UNKNOWN_ERROR:
            Message.show(tr("unknown_error"));
            break;
        case codePush.SyncStatus.UPDATE_INSTALLED:
            Message.show(tr("update_installed"));
            clearStorage();
            break;
        default:
            break;
    }
};

const downloadProgress = downloadProgressCallback => progress => {
    const {totalBytes, receivedBytes} = progress;
    const percent = Math.trunc(receivedBytes / totalBytes * 100);

    if (percent > savedDownloadedPercent) {
        savedDownloadedPercent = percent;
        downloadProgressCallback(percent);
    }
};

const changeDeployment = (deploymentName, downloadProgressCallback) => async () => {
    try {
        codePush.clearUpdates();

        const deploymentKey = deploymentKeys[deploymentName];
        savedDownloadedPercent = 0;

        await codePush.sync({
                checkFrequency: codePush.CheckFrequency.ON_APP_START,
                updateDialog: false,
                installMode: codePush.InstallMode.IMMEDIATE
            },
            syncStatusChanged,
            downloadProgress(downloadProgressCallback)
        );

        if(Platform.OS === "android") {
            codePush.restartApp(false);
        }

        if (typeof downloadProgressCallback === "function") {
            downloadProgressCallback(-1);
        }

        store.dispatch(changeUpdateMetaData({deploymentKey}));
    } catch (e) {
        Message.show(JSON.stringify(e), tr("error_changing_env"));
        Sentry.captureException(e);
    }
};

const showDeploymentChangingOptions = (downloadProgressCallback) => Alert.alert(
    "In " + firstLetterUppercase(getStoredDeploymentName()) + " Mode",
    tr("env_change_message"),
    [
        {
            text: tr("env_change_development"),
            onPress: changeDeployment(codePushDeploymentNames.DEVELOPMENT, downloadProgressCallback)
        },
        {
            text: tr("env_change_staging"),
            onPress: changeDeployment(codePushDeploymentNames.STAGING, downloadProgressCallback)
        },
        {
            text: tr("env_change_production"),
            onPress: changeDeployment(codePushDeploymentNames.PRODUCTION, downloadProgressCallback)
        },
        {
            text: tr("env_change_cancel"),
            style: "cancel"
        },
    ],
    {cancelable: true}
);

export {
    showDeploymentChangingOptions,
    DEPLOYMENT_KEY
};
