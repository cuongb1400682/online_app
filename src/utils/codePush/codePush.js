import codePush from "react-native-code-push";

import store from "src/redux/store";
import {changeUpdateMetaData} from "src/redux/common/deploymentActions";
import NativeConstants from "src/utils/reactNative/NativeConstants";
import deploymentKeys from "src/constants/deploymentKeys";
import {codePushDeploymentNames} from "src/constants/enums";

const getDefaultSyncOptions = () => ({
    checkFrequency: codePush.CheckFrequency.ON_APP_START,
    updateDialog: false,
    installMode: codePush.InstallMode.IMMEDIATE
});

const findDeploymentNameByKey = (deploymentKey) => {
    const foundDeployment = Object
        .entries(deploymentKeys)
        .find(([depName, depKey]) => depKey === deploymentKey);

    return foundDeployment ? foundDeployment[0].toLowerCase() : codePushDeploymentNames.DEVELOPMENT;
};

const getStoredDeploymentName = () => {
    if (!store) {
        return findDeploymentNameByKey(NativeConstants.deploymentKey);
    }

    return store.getState().deployment.update_meta_data.deploymentName;
};

const syncBundle = async (deploymentKey) => {
    const syncOptions = {
        ...getDefaultSyncOptions(),
        deploymentKey
    };

    store.dispatch(changeUpdateMetaData(syncOptions));
    await codePush.sync(syncOptions);
};

export {
    syncBundle,
    getDefaultSyncOptions,
    findDeploymentNameByKey,
    getStoredDeploymentName
};
