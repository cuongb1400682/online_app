import {Sentry} from "react-native-sentry";
import codePush from "react-native-code-push";

import {findDeploymentNameByKey} from "src/utils/codePush/codePush";
import {getSuitableConfigures} from "src/configs/configs";
import {getUserInfo, isUserLoggedIn} from "src/utils/userAuth/userAuth";
import NativeConstants from "src/utils/reactNative/NativeConstants";

const debugConfigs = require("../../configs/developmentConfigs");

const sendCodePushVersionToSentry = async () => {
    const update = await codePush.getUpdateMetadata();

    if (update) {
        Sentry.setVersion(update.appVersion + "-codepush:" + update.label);
    }
};

const sendUserInfoToSentry = (userInfo = {}) => {
    const userId = `${userInfo.id || userInfo.uid}`;
    const fullName = userInfo.name || userInfo.full_name;
    const email = userInfo.email;

    Sentry.setUserContext({userId, fullName, email});
};

const setupSentry = async (deploymentKey) => {
    const deploymentName = findDeploymentNameByKey(deploymentKey);
    const currentConfigs = NativeConstants.isDebug ? debugConfigs : getSuitableConfigures(deploymentName);
    const {SENTRY_DSN_URL = ""} = currentConfigs;

    await Promise.all([
        Sentry.config(SENTRY_DSN_URL).install(),
        sendCodePushVersionToSentry()
    ]);

    if (isUserLoggedIn()) {
        const userInfo = getUserInfo();

        sendUserInfoToSentry(userInfo);
    }
};

export {
    setupSentry as default,
    sendUserInfoToSentry
};
