import {GoogleSignin, statusCodes} from "react-native-google-signin";
import NativeConstants from "src/utils/reactNative/NativeConstants";
import errorCodes from "src/constants/errorCodes";

const logoutFromGoogle = async () => {
    if (await GoogleSignin.isSignedIn()) {
        try {
            await GoogleSignin.signOut();
        } catch {
            // omit the errors
        }
    }
};

const configGoogleLogin = () => {
    GoogleSignin.configure({
        scopes: [
            "https://www.googleapis.com/auth/user.emails.read",
            "https://www.googleapis.com/auth/user.phonenumbers.read",
            "https://www.googleapis.com/auth/userinfo.profile"
        ],
        webClientId: NativeConstants.googleClientId,
        offlineAccess: true,
    });
};

const checkForPlayServicesAvailability = async () => {
    const hasPlayServices = await GoogleSignin.hasPlayServices();

    if (!hasPlayServices) {
        throw new Error("error_play_services_unavailable");
    }
};

const receiveServerAuthCode = async () => {
    try {
        await checkForPlayServicesAvailability();
        const {idToken = ""} = await GoogleSignin.signIn() || {};

        return idToken;
    } catch (error) {
        const ignoreErrorCodes = [
            errorCodes.ASYNC_OP_IN_PROGRESS,
            statusCodes.SIGN_IN_CANCELLED
        ];

        if (!ignoreErrorCodes.includes(error.code)) {
            throw error; // rethrow the error
        }
    }
};

export {
    logoutFromGoogle,
    configGoogleLogin,
    receiveServerAuthCode
};
