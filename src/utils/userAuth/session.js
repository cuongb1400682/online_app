import {updateSessionKey} from "src/redux/common/sessionActions";
import store from "src/redux/store";
import {getSuitableConfigures} from "src/configs/configs";
import Message from "src/components/platformSpecific/Message/Message";

const initializeSessionKey = async () => {
    let sessionKey = store.getState().session.session_key;
    const configs = getSuitableConfigures();

    if (!sessionKey) {
        const response = await fetch(
            configs.ONLINE_APP_BACKEND_URL + "/session/create",
            {method: "POST"}
        );

        const jsonResponse = await response.json();

        if (jsonResponse.result === "success") {
            sessionKey = jsonResponse.reply.session_key;
        } else {
            Message.show(JSON.stringify(jsonResponse), "Error init session");
        }
    }

    store.dispatch(updateSessionKey(sessionKey));
};

export {
    initializeSessionKey
};
