import {AccessToken, LoginManager} from "react-native-fbsdk";
import {Platform} from "react-native";

import errorCodes from "src/constants/errorCodes";

const logoutFromFacebook = () => LoginManager.logOut();

const setFacebookLoginBehavior = () => LoginManager.setLoginBehavior(
    Platform.select({
        android: "native_with_fallback",
        ios: "native"
    })
);

const logInWithReadPermissions = async () => {
    const grantedPermission = await LoginManager.logInWithReadPermissions(["public_profile", "email"]);

    if (grantedPermission.isCancelled) {
        throw new Error(errorCodes.ERROR_LOGIN_CANCELLED);
    }
};

const receiveAccessToken = async () => {
    const accessTokenInfo = await AccessToken.getCurrentAccessToken();

    if (!accessTokenInfo || typeof accessTokenInfo.accessToken !== "string") {
        throw new Error(errorCodes.ERROR_LOGIN_FAILED);
    }

    return accessTokenInfo.accessToken;
};

export {
    logoutFromFacebook,
    receiveAccessToken,
    setFacebookLoginBehavior,
    logInWithReadPermissions
};
