import store from "src/redux/store";

const isUserLoggedIn = () => !!store.getState().session.access_token;

const getUserInfo = () => store.getState().session.user_info || {};

export {
    isUserLoggedIn,
    getUserInfo
};
