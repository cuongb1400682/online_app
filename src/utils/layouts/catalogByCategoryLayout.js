import {screenWidth} from "src/utils/reactNative/dimensions";

const CATALOG_BY_CATEGORY_BANNER_RATIO = 0.4;

const catalogByCategoryBannerHeight = () => screenWidth() * CATALOG_BY_CATEGORY_BANNER_RATIO;

export {
    catalogByCategoryBannerHeight
};
