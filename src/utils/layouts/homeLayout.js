import {screenWidth} from "src/utils/reactNative/dimensions";

const BANNER_DIMENSION_RATIO = 0.408;

const HOT_CATEGORY_DIM_RATIO = 0.6;

const TITLE_HEIGHT = 58;

const estimateBannerHeight = () => screenWidth() * BANNER_DIMENSION_RATIO;

const hotTrendWidth = () => screenWidth() / 2 - 48;

const hotTrendHeight = () => hotTrendWidth() * HOT_CATEGORY_DIM_RATIO;

const advertisementWidth= () => screenWidth() - 32;

const advertisementHeight = () => advertisementWidth() * BANNER_DIMENSION_RATIO;

export {
    TITLE_HEIGHT,
    estimateBannerHeight,
    hotTrendHeight,
    hotTrendWidth,
    advertisementWidth,
    advertisementHeight
};


