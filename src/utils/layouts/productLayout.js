import {Platform} from "react-native";

import {screenWidth} from "src/utils/reactNative/dimensions";
import {productListViewTypes} from "src/constants/enums";
import {isEmptyArray} from "src/utils/extensions/arrays";

const IMAGE_SIZE = screenWidth() / 2 - 2;

const PRODUCT_INFO_HEIGHT = Platform.select({ios: 110, android: 100});

const PRODUCT_HEIGHT = IMAGE_SIZE + PRODUCT_INFO_HEIGHT;

const HORIZONTAL_PRODUCT_HEIGHT = 158;

const setProductDimension = (dimension = {}, spacing) => {
    dimension.height = PRODUCT_HEIGHT + spacing;
    dimension.width = screenWidth() / 2;
};

const groupProductsInRow = ({products = []}) => {
    let result = [];

    for (let i = 0; i < products.length; i += 2) {
        result.push({
            viewType: productListViewTypes.PRODUCTS_ROW,
            left: products[i],
            right: products[i + 1]
        });
    }

    return result;
};

const separateProductListIntoSections = (nextProps = {}) => {
    const {products = [], sections = [], showsHorizontalProduct = false} = nextProps;

    if (showsHorizontalProduct) {
        return products;
    }

    if (isEmptyArray(sections)) {
        return groupProductsInRow(nextProps);
    }

    let totalItems = 0;
    let newProductsList = [];

    for (const section of sections) {
        const {count = 0} = section;
        const productSlice = products.slice(totalItems, totalItems + count);

        newProductsList = [
            ...newProductsList,
            {viewType: productListViewTypes.SECTION_HEADER, ...section},
            {viewType: productListViewTypes.PLACEHOLDER_SECTION},
            ...groupProductsInRow({products: productSlice})
        ];

        totalItems += count;
    }

    return newProductsList;
};

export {
    HORIZONTAL_PRODUCT_HEIGHT,
    PRODUCT_HEIGHT,
    IMAGE_SIZE,
    PRODUCT_INFO_HEIGHT,
    setProductDimension,
    separateProductListIntoSections
};
