import {Image} from "react-native";

import {estimateBannerHeight} from "src/utils/layouts/homeLayout";
import styles from "src/pages/LandingPage/styles";
import {screenWidth} from "src/utils/reactNative/dimensions";
import {SECTION_HEADER_PADDING} from "src/pages/LandingPage/LandingPage";
import {setProductDimension} from "src/utils/layouts/productLayout";
import {makeCDNImageURL} from "src/utils/cdn/images";
import {arrayFlatMap} from "src/utils/extensions/arrays";

const calculateLandingPageHeaderHeight = (numberOfTabs = 0, hasBanner = false) => {
    return (
        (hasBanner ? estimateBannerHeight() : 0) +
        Math.ceil(numberOfTabs / 2) * styles.jumpMenuItem.height +
        styles.jumpMenu.paddingVertical * 2
    );
};

const estimateSectionImageDimensions = (mobile_url = "", callback) => {
    const imageUrl = makeCDNImageURL(mobile_url);

    Image.getSize(imageUrl, (width, height) => {
        const dimensionRadio = height / width * 0.5;
        const newWidth = screenWidth();
        const newHeight = width * dimensionRadio + SECTION_HEADER_PADDING;

        if (typeof callback === "function") {
            callback({width: newWidth, height: newHeight});
        }
    });
};

const separateProductsIntoSections = (tabs = []) => {
    let sections = [];
    let products = arrayFlatMap(tabs, ({products, name, ...rest}) => {
        const newSection = {
            ...rest,
            count: products.length,
            title: name,
            height: styles.sectionHeader.height + SECTION_HEADER_PADDING,
            width: screenWidth()
        };

        sections.push(newSection);

        return products;
    });

    return {products, sections};
};

const estimateSectionsYPositions = (sections = []) => {
    const dimension = {width: 0, height: 0};
    let yOffset = 0;
    let result = [];

    setProductDimension(dimension, 5);

    for (let i = 0; i < sections.length; i++) {
        const {count = 0, height = 0} = sections[i] || {};

        result.push(yOffset);
        yOffset += dimension.height * Math.ceil(count / 2) + height;
    }

    result.push(Number.MAX_SAFE_INTEGER);
    return result;
};

export {
    calculateLandingPageHeaderHeight,
    separateProductsIntoSections,
    estimateSectionsYPositions,
    estimateSectionImageDimensions
};
