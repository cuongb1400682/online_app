import {findCategory, parseTrendProductLink} from "src/utils/misc/hotTrends";
import {pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";
import {redirectLinkTypes} from "src/constants/enums";
import {isEmptyArray} from "src/utils/extensions/arrays";
import {isEmptyString} from "src/utils/extensions/strings";

const showPromotedProduct = ({navigator, link = "", categories}) => {
    if (!isEmptyString(link) && link !== "/khuyen-mai") {
        return showCatalogByCategory({link, navigator, categories});
    }

    pushTo(navigator, {
        screen: screenIds.PROMOTED_PRODUCT,
        passProps: {}
    });

    return true;
};

const showProductDetailWithLink = ({link = "", navigator}) => {
    const {childProductId = "", productId = ""} = parseTrendProductLink(link);

    if (childProductId === -1 && isNaN(productId)) {
        return false;
    }

    pushTo(navigator, {
        screen: screenIds.PRODUCT_DETAIL,
        passProps: {
            productId,
            childProductId
        }
    });

    return true;
};

const showCatalogByCategory = ({link = "", navigator, categories = []}) => {
    const {selectedCategory, subCategoryIndex} = findCategory(categories, link);

    if (selectedCategory) {
        pushTo(navigator, {
            screen: screenIds.CATALOG_BY_CATEGORY,
            passProps: {
                selectedCategory,
                subCategoryIndex
            }
        });
    }

    return !!selectedCategory;
};

const showSearchResult = ({link = "", navigator}) => {
    if (link.indexOf("?q=") === -1) {
        return false;
    }

    const linkTokens = link.split("?q=");
    const linkContainsSearchKeyword = !isEmptyArray(linkTokens);

    if (linkContainsSearchKeyword) {
        const searchKeyword = linkTokens[linkTokens.length - 1];
        pushTo(navigator, {
            screen: screenIds.SEARCH_RESULT,
            passProps: {
                searchParams: {
                    q: searchKeyword
                }
            }
        });
    }

    return linkContainsSearchKeyword;
};

const showLandingPage = ({link = "", navigator}) => {
    const campaignCode = link.split("/").pop();

    if (!campaignCode || isEmptyString(campaignCode)) {
        return false;
    }

    pushTo(navigator, {
        screen: screenIds.LANDING_PAGE,
        passProps: {
            campaignCode
        }
    });

    return true;
};

const showDealPage = ({navigator}) => {
    pushTo(navigator, {screen: screenIds.DEAL_LIST});

    return true;
};

const openRedirectLink = ({link = "", link_type = "", navigator, categories = []}) => {
    // "handler" will return a boolean value indicating that if link is opened successfully
    // otherwise, it will show PromotedProduct screen by default.
    const handler = ({
        [redirectLinkTypes.PRODUCT]: showProductDetailWithLink,
        [redirectLinkTypes.CATEGORY]: showCatalogByCategory,
        [redirectLinkTypes.SEARCH]: showSearchResult,
        [redirectLinkTypes.LANDING_PAGE]: showLandingPage,
        [redirectLinkTypes.DEAL]: showDealPage,
        [redirectLinkTypes.PROMOTED]: showPromotedProduct,
    })[link_type] || showPromotedProduct;

    const openLinkSuccessfully = handler({link, navigator, categories});

    if (!openLinkSuccessfully) {
        showPromotedProduct({navigator});
    }
};

export default openRedirectLink;
