import React from "react";
import {StyleSheet, Text, TextInput} from "react-native";

import systemFont from "src/styles/systemFont";

const disableAllowFontScaling = (TextView) => {
    TextView.defaultProps = TextView.defaultProps || {};
    TextView.defaultProps.allowFontScaling = false;
};

const determineFontFamily = (originalStyle = {}) => {
    let extraStyle = systemFont.regular;

    if (originalStyle.fontWeight === "bold") {
        if (originalStyle.fontStyle === "italic") {
            extraStyle = systemFont.boldItalic;
        } else {
            extraStyle = systemFont.bold;
        }
    } else if (originalStyle.fontStyle === "italic") {
        extraStyle = systemFont.italic;
    }

    return extraStyle;
};

const injectTextStyles = (originalStyle = {}) => {
    if (Array.isArray(originalStyle)) {
        originalStyle = StyleSheet.flatten(originalStyle);
    }

    const fontSize = Math.max((originalStyle.fontSize || 12) - 1, 10);

    return {
        ...originalStyle,
        ...determineFontFamily(originalStyle),
        fontSize
    };
};

const setupAppFont = () => {
    // disable font scaling for ios
    disableAllowFontScaling(TextInput);
    disableAllowFontScaling(Text);
};

export {
    setupAppFont,
    injectTextStyles
};
