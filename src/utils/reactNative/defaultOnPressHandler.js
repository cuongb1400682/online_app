import {pushTo} from "src/utils/misc/navigator";

const defaultOnPressHandler = ({navigator, screenId, onPress, passProps}) => () => {
    if (screenId) {
        pushTo(navigator, {screen: screenId, passProps});
    } else if (typeof onPress === "function") {
        onPress();
    }
};

export default defaultOnPressHandler;
