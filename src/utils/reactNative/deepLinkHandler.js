import {Linking} from "react-native";
import UrlPattern from "url-pattern";

import {isEmptyString} from "src/utils/extensions/strings";
import {pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";
import {findCategory} from "src/utils/misc/hotTrends";
import {parseQueryString} from "src/utils/api/queryString";

const stripSearchString = (url = "") => {
    if (typeof url !== "string") {
        return "";
    }

    const indexOfQuestionMark = url.indexOf("?");

    if (indexOfQuestionMark < 0) {
        return url;
    }

    return url.substr(0, indexOfQuestionMark);
};

const showProductDetail = ({url = "", navigator}) => {
    const pattern = new UrlPattern("/product/:productId");
    const matcher = pattern.match(stripSearchString(url));

    if (matcher) {
        pushTo(navigator, {
            screen: screenIds.PRODUCT_DETAIL,
            passProps: {
                productId: parseInt(matcher.productId, 10)
            }
        });

        return true;
    }

    return false;
};

const showCategory = ({url = "", navigator, categories = []}) => {
    const pattern = new UrlPattern("/category/:subCategory");
    const matcher = pattern.match(stripSearchString(url));

    if (!matcher) {
        return false;
    }

    const subCategory = matcher.subCategory;
    const {selectedCategory, subCategoryIndex} = findCategory(categories, "/" + subCategory);

    if (selectedCategory) {
        pushTo(navigator, {
            screen: screenIds.CATALOG_BY_CATEGORY,
            passProps: {
                selectedCategory,
                subCategoryIndex
            }
        });

        return true;
    }

    return false;
};

const showLandingPage = ({url = "", navigator}) => {
    const pattern = new UrlPattern("/landing-page/:campaignCode");
    const matcher = pattern.match(stripSearchString(url));

    if (matcher) {
        pushTo(navigator, {
            screen: screenIds.LANDING_PAGE,
            passProps: {
                campaignCode: matcher.campaignCode
            }
        });

        return true;
    }

    return false;
};

const showSearchResult = ({url = "", navigator}) => {
    const searchString = url.substr(url.indexOf("?"), url.length);
    const matcher = parseQueryString(searchString);

    if (url.startsWith("/search")) {
        pushTo(navigator, {
            screen: screenIds.SEARCH_RESULT,
            passProps: {
                searchParams: {
                    q: matcher.q
                }
            }
        });

        return true;
    }

    return false;
};

const showDeals = ({url = "", navigator}) => {
    if (url.startsWith("/deals")) {
        pushTo(navigator, {
            screen: screenIds.DEAL_LIST
        });

        return true;
    }

    return false;
};

const deepLinkHandler = ({url = "", navigator, categories = []}) => {
    if (isEmptyString(url) || typeof url !== "string") {
        return;
    }

    const handlers = [
        showProductDetail,
        showCategory,
        showDeals,
        showLandingPage,
        showSearchResult,
    ];

    url = url.replace("giigaacom20180511:/", "");
    for (const handler of handlers) {
        if (typeof handler === "function") {
            const urlIsHandled = handler({url, navigator, categories});

            if (urlIsHandled) {
                break;
            }
        }
    }
};

const registerDeepLinkHandler = ({navigator, categories = []}) => {
    Linking.getInitialURL().then((url = "") => {
        deepLinkHandler({url, navigator, categories});
    });

    Linking.addEventListener("url", (event = {}) => {
        const {url = ""} = event;

        deepLinkHandler({url, navigator, categories});
    });
};

export {
    registerDeepLinkHandler
};
