import {Platform, StatusBar} from "react-native";

const getStatusBarHeight = () => Platform.select({
    ios: 20,
    android: StatusBar.currentHeight
});

export {
    getStatusBarHeight
};
