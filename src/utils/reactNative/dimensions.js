import {Dimensions} from "react-native";

const screenWidth = () => {
    return Dimensions.get("window").width;
};

const screenHeight = () => {
    return Dimensions.get("window").height;
};

const smallerDimension = () => Math.min(
    screenWidth(),
    screenHeight()
);

const getScreenOrientation = () => {
    return screenWidth() > screenHeight() ? "landscape" : "portrait";
};

export {
    getScreenOrientation,
    screenWidth,
    screenHeight,
    smallerDimension
};
