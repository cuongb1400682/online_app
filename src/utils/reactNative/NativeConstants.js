import {NativeModules} from "react-native";

const {DebugUtils} = NativeModules;

class NativeConstants {
    static hasConstant = (constantName) => DebugUtils && DebugUtils.hasOwnProperty(constantName);

    static get facebookAppId() {
        return NativeConstants.hasConstant("facebookAppId") ? DebugUtils.facebookAppId : "";
    }

    static get deploymentKey() {
        return NativeConstants.hasConstant("currentDeploymentKey") ? DebugUtils.currentDeploymentKey : "";
    }

    static get isDebug() {
        return NativeConstants.hasConstant("isDebug") ? DebugUtils.isDebug : false;
    }

    static get googleClientId() {
        return NativeConstants.hasConstant("googleClientId") ? DebugUtils.googleClientId : "";
    }
}

export default NativeConstants;
