import {getSuitableConfigures} from "src/configs/configs";
import {imageTypes} from "src/constants/enums";
import FastImage from "src/components/base/FastImage/FastImage";

const mapTypeToSize = (type) => ({
    [imageTypes.PURCHASE_ORDER_DETAIL] : 430,
    [imageTypes.PROMOTED]: 210,
    [imageTypes.BEST_SALES]: 115,
    [imageTypes.CART]: 100,
    [imageTypes.VIEWED]: 80,
    [imageTypes.IMAGE_LIST]: 75
})[type] || 430;

const makeCDNImageURL = (originalUrl, type = imageTypes.FULL_SIZE) => {
    const configs = getSuitableConfigures();

    if (!originalUrl) {
        return FastImage.defaultProduct;
    }

    if (type === imageTypes.FULL_SIZE) {
        return configs.CDN_URL + "/" + originalUrl;
    }

    const size = mapTypeToSize(type);
    const sizeDestination = `${size}x${size}`;
    const urls =  originalUrl.split("/");
    urls.splice(2, 0, sizeDestination);

    return configs.CDN_URL + "/" + urls.join("/");
};

export {
    makeCDNImageURL
};
