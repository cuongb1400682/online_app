import {NetInfo} from "react-native";

let isCallbackCalled = false;

const setupNetworkConnectivityListener = (callback) => {
    NetInfo.isConnected.addEventListener("connectionChange", (isConnected) => {
        if (isConnected) {
            if (typeof callback === "function" && !isCallbackCalled) {
                callback();
                isCallbackCalled = true;
            }
        }
    });
};

export {
    setupNetworkConnectivityListener
};
