import {formatNumber} from "src/utils/extensions/strings";
import {discountTypes} from "src/constants/enums";

const {MONEY: DISCOUNT_TYPE_MONEY, PERCENT: DISCOUNT_TYPE_PERCENT} = discountTypes;

const calculateSaving = (marketPrice, unitPrice) => ({
    amount: marketPrice - unitPrice,
    percent: formatNumber((marketPrice - unitPrice) * 100 / marketPrice)
});

const getOutOfStockText = qty => qty > 0 ? `Chỉ còn ${qty} sản phẩm` : "Sản phẩm tạm hết hàng";

const calculateDiscount = (discount, discountType, amount) => {
    if (typeof discount === "undefined" || typeof discountType === "undefined" || discount === 0) {
        return 0;
    }

    if (discountType === DISCOUNT_TYPE_MONEY) {
        return discount;
    }

    const amountDiscount = amount * (parseFloat(discount) / 100);
    const amountDiscountStr = amountDiscount.toFixed(0);

    return parseInt(amountDiscountStr, 10);
};

const getSuffixDiscountLabel = (discount, discountType) => {
    if (typeof discount === "undefined" || typeof discountType === "undefined") {
        return "";
    }

    if (discountType === DISCOUNT_TYPE_PERCENT) {
        return ` (-${discount}%)`;
    }

    return "";
};

export {
    calculateSaving,
    getOutOfStockText,
    calculateDiscount,
    getSuffixDiscountLabel
};
