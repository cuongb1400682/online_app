import crypto from "crypto-js";

const SHA256 = pattern => String(crypto.SHA256(pattern));

const SHA1 = pattern => String(crypto.SHA1(pattern));

const hashPasswordForRegistration = (password, salt) => {
    return SHA1(password + salt);
};

const hashPasswordForLogin = (password, salt, verifyCode) => {
    return String(SHA256(String(SHA256(String(SHA1(password + salt)))) + verifyCode));
};

export {
    hashPasswordForRegistration,
    hashPasswordForLogin
};
