import {parseQueryString} from "src/utils/api/queryString";

const parseTrendProductLink = (link = "") => {
    const [prefix = "", queryString = ""] = link.split(".html");
    const productId = prefix.split("-p").pop();
    const childProductId = parseQueryString(queryString).cid;

    return {
        productId: parseInt(productId || -1, 10),
        childProductId: parseInt(childProductId || -1, 10)
    };
};

const findCategory = (categories, link) => {
    let selectedCategory = categories.find(category => category.url === link);
    let subCategoryIndex = -1;

    if (!selectedCategory) {
        for (const category of categories) {
            const {sub_categories = []} = category;

            subCategoryIndex = sub_categories.findIndex(category => category.url === link);

            if (subCategoryIndex > -1) {
                selectedCategory = category;
                break;
            }
        }
    }

    return {
        selectedCategory,
        subCategoryIndex
    };
};

export {
    parseTrendProductLink,
    findCategory
};
