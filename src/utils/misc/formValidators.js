import {tr} from "src/localization/localization";
import * as tComb from "tcomb-form-native";
import colors from "src/constants/colors";

const isValidPhoneNumber = (phoneNumber) => /^[+]*[(]?\+?[0-9]+[)]?[-\s./0-9]*$/.test(phoneNumber);

const isValidEmail = (email) => {
    let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validatePhoneEmail = value => {
    if (!isValidEmail(value) && !isValidPhoneNumber(value)) {
        return tr("account_tab_phone_email_invalid");
    }
};

const validatePassword = ({password = ""} = {}) => () => {
    const passwordLength = (typeof password === "string") ? password.length : 0;

    return (passwordLength < 6) ? tr("validator_password_at_least_6_chars") : undefined;
};

const validateRepeatedPassword = ({password = "", repeated_password = ""} = {}) => () => {
    return password !== repeated_password
        ? tr("validator_passwords_do_not_match")
        : undefined;
};

const requireField = (value) => !value && tr("required_field");

const generalValidEmailPhone = (emailPhone) => {
    return (isValidEmail(emailPhone) || isValidPhoneNumber(emailPhone));
};

const PasswordString = tComb.refinement(tComb.String, value => value.length >= 6);
PasswordString.getValidationErrorMessage = () => tr("validator_password_at_least_6_chars");

const RequiredString = tComb.refinement(tComb.String, value => value.length > 0);
RequiredString.getValidationErrorMessage = () => tr("required_field");

const commonTextInputProps = {
    selectionColor: colors.primaryColor,
    autoCapitalize: "none",
};

export {
    isValidEmail,
    isValidPhoneNumber,
    commonTextInputProps,
    generalValidEmailPhone,
    validateRepeatedPassword,
    validatePassword,
    validatePhoneEmail,
    requireField,
    PasswordString,
    RequiredString
};
