import screenIds from "src/constants/screenIds";

const pushTo = (navigator, pushArgs) => {
    if (navigator && typeof navigator.push === "function") {
        navigator.push(pushArgs);
    }
};

const resetTo = (navigator, resetArgs) => {
    if (navigator && typeof navigator.resetTo === "function") {
        navigator.resetTo(resetArgs);
    }
};

const popToRoot = (navigator, popToRootArgs) => {
    if (navigator && typeof navigator.popToRoot === "function") {
        navigator.popToRoot(popToRootArgs);
    }
};

const popBack = (navigator, popArgs) => {
    if (navigator && typeof navigator.pop === "function") {
        navigator.pop(popArgs);
    }
};

const switchToTab = (navigator, switchArgs) => {
    if (navigator && typeof navigator.switchToTab === "function") {
        navigator.switchToTab(switchArgs);
    }
};

const showUnderDevelopmentWithGoBack = (navigator) => () => {
    pushTo(navigator, {
        screen: screenIds.UNDER_DEVELOPMENT,
        passProps: {
            allowsGoBack: true
        }
    });
};

const setOnNavigatorEvent = (navigator, callback) => {
    if (navigator && typeof navigator.setOnNavigatorEvent === "function") {
        if (typeof callback === "function") {
            navigator.setOnNavigatorEvent(callback);
        }
    }
};

export {
    switchToTab,
    popBack,
    pushTo,
    resetTo,
    popToRoot,
    showUnderDevelopmentWithGoBack,
    setOnNavigatorEvent
};
