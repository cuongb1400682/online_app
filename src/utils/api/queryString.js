import {objectEntries} from "src/utils/misc/object";

const isHeuristicEncoded = value => decodeURIComponent(value) !== value

const encodeValue = value => isHeuristicEncoded(value) ? value : encodeURIComponent(value)

const encodeKeyValue = ([name, value]) => name + "=" + encodeValue(value)

const convertToObject = (params, [key, value]) => ({
    ...params,
    [key]: decodeURIComponent(value)
})

const filterNullAndEmptyValues = ([, value]) => {
    if (Array.isArray(value)) {
        return value.length
    }

    return value !== null && value !== "" && value !== undefined
}

const stringifyQuery = (params = {}) => {
    return objectEntries(params)
        .filter(filterNullAndEmptyValues)
        .map(encodeKeyValue)
        .join("&")
}

const parseQueryString = queryString => {
    if (queryString && queryString !== "?") {
        return queryString
            .substring(1)
            .split("&")
            .map(pair => pair.split("="))
            .reduce(convertToObject, {}) || {}
    }

    return {}
}

const decodeSearchKeyword = string => {
    return typeof string !== "string" ? string : string.split("+").join(" ");
};

const encodeSearchKeyword = string => {
    return typeof string !== "string" ? string : string.split(" ").join("+");
};

export {
    stringifyQuery,
    parseQueryString,
    decodeSearchKeyword,
    encodeKeyValue,
}
