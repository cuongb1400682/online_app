const validateResponse = async (response) => {
    if (!response.ok) {
        throw new Error(JSON.stringify(response));
    }

    const jsonResponse = await response.json();

    if (jsonResponse.result !== "success") {
        throw new Error(JSON.stringify(jsonResponse));
    }

    return jsonResponse;
};

export default validateResponse;
