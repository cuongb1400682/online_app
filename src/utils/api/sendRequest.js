import {Platform} from "react-native";

import {stringifyQuery} from "src/utils/api/queryString";
import {getSuitableConfigures} from "src/configs/configs";
import store from "src/redux/store";
import DeviceInfo from "react-native-device-info";

const makeFetchHeaders = (headers) => {
    const {
        access_token = "",
        session_key = ""
    } = store.getState().session;

    return {
        "x-giigaa-app-type": Platform.select({
            ios: "IOS",
            android: "ANDROID"
        }),
        "x-giigaa-session-key": session_key,
        "x-giigaa-access-token": access_token,
        "x-giigaa-client-id": DeviceInfo.getUniqueID(),
        "x-giigaa-client-language": "vi",
        "x-giigaa-client-version": Platform.select({
            ios: DeviceInfo.getVersion(),
            android: DeviceInfo.getVersion()
        }),
        ...headers
    };
};

const makeFetchRequestUrl = ({url, isAuth, params}) => {
    const configs = getSuitableConfigures();
    const apiUrl = isAuth ? "AUTH_ROOT_URL" : "ONLINE_APP_BACKEND_URL";
    const searchString = stringifyQuery(params);
    const requestUrl = configs[apiUrl] + url;

    return searchString ? `${requestUrl}?${searchString}` : requestUrl;
};

const hasJSONBody = (body) => !!body && !(body instanceof FormData);

const sendRequest = async ({url = "", method = "GET", headers = {}, params = {}, body, isAuth = false}) => {
    const requestUrl = makeFetchRequestUrl({
        url,
        isAuth,
        params
    });
    const requestHeaders = makeFetchHeaders(headers);
    let requestBody = body;

    if (hasJSONBody(body)) {
        requestHeaders["Content-Type"] = "application/json; charset=UTF-8";
        requestBody = JSON.stringify(body);
    }

    const fetchRequestParams = {
        method: method || "GET",
        headers: requestHeaders,
        body: requestBody,
        credentials: "same-origin"
    };

    if (method === "GET") {
        delete fetchRequestParams.body;
    }

    return await fetch(requestUrl, fetchRequestParams);
};

export {
    sendRequest
};
