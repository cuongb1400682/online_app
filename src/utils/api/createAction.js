import {Sentry} from "react-native-sentry";

import {sendRequest} from "src/utils/api/sendRequest";
import {convertStringToJSON} from "src/utils/extensions/strings";
import handleError from "src/utils/api/handleError";
import validateResponse from "src/utils/api/validateResponse";

const toRequest = type => `${type}_REQUEST`;
const toSuccess = type => `${type}_SUCCESS`;
const toFailure = type => `${type}_FAILURE`;

const makeAction = transform => (
    {type, payload, meta},
    response
) => ({
    type: transform(type),
    payload: response || payload,
    meta: {...(meta || {})}
});

const makeRequestAction = makeAction(toRequest);
const makeSuccessAction = makeAction(toSuccess);
const makeFailureAction = makeAction(toFailure);

const createAction = action => (dispatch, getState) => {
    const {type, payload, meta = {}, onSuccess} = action;

    if (!payload) {
        console.error("The payload must not be empty");
        return false;
    }

    dispatch({type, payload, meta});

    if (typeof onSuccess === "function") {
        onSuccess(dispatch, getState);
    }
};

const createAsyncAction = action => async dispatch => {
    const {payload, onSuccess, onFailure} = action;

    if (!payload || typeof payload.request !== "string") {
        console.error("The request url does not exist");
        return false;
    }

    dispatch(makeRequestAction(action));

    const {request, method, body, params, options, headers, doesIgnoreNotified = false} = payload;

    try {
        const response = await sendRequest({
            url: request,
            method,
            body,
            params,
            headers,
            ...options
        });

        const jsonResponse = await validateResponse(response);

        if (typeof onSuccess === "function") {
            onSuccess(dispatch, jsonResponse);
        }

        return dispatch(makeSuccessAction(action, jsonResponse));
    } catch (error) {
        if (doesIgnoreNotified || handleError({error, action})) {
            return;
        }

        if (typeof onFailure === "function") {
            onFailure(dispatch, convertStringToJSON(error.message));
        }

        Sentry.captureException(error);

        return dispatch(makeFailureAction(action, convertStringToJSON(error.message)));
    }
};

const fakeAPI = (request) => new Promise((resolve) => setTimeout(() => resolve({reply: request}), 500));

const fakeAsyncAction = action => async dispatch => {
    dispatch(makeRequestAction(action));
    const result = await fakeAPI(action.payload.request);
    return dispatch(makeSuccessAction(action, result));
};

export {
    makeRequestAction,
    makeSuccessAction,
    makeFailureAction,
    toRequest,
    toSuccess,
    toFailure,
    createAction,
    createAsyncAction,
    fakeAsyncAction
};
