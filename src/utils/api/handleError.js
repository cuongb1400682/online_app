import Message from "src/components/platformSpecific/Message/Message";
import {tr} from "src/localization/localization";
import {convertStringToJSON} from "src/utils/extensions/strings";
import errorCodes from "src/constants/errorCodes";
import profileManagerActionTypes from "src/redux/actionTypes/profileManagerActionTypes";
import store from "src/redux/store";
import {clearUserInfo} from "src/redux/common/sessionActions";
import {pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";

const getHomeNavigatorObject = () => {
    return store.getState().home.home_navigator;
};

const navigateToLogin = () => {
    const navigator = getHomeNavigatorObject();

    try {
        pushTo(navigator, {screen: screenIds.LOGIN});
    } catch {
        // Omit the error
    }
};

const handleNetworkError = ({error = {}}) => {
    const hasNetworkError = error.readyState === 0;

    if (hasNetworkError) {
        Message.show(
            tr("error_network_problem_message"),
            tr("error_network_problem_title"),
        );
    }

    return hasNetworkError;
};

const handleErrorAccessToken = ({error = {}, action = {}}) => {
    const response = convertStringToJSON(error.message);
    const {result = ""} = response;
    const hasErrorAccessToken = (result === errorCodes.ERROR_ACCESS_TOKEN);

    if (hasErrorAccessToken) {
        if (action.type !== profileManagerActionTypes.LOGOUT) {
            Message.show(
                tr("error_access_token_message"),
                tr("error_access_token_title")
            );
        }

        store.dispatch(clearUserInfo());
        navigateToLogin();
    }

    return hasErrorAccessToken;
};

const handleErrorDuplicatedRequest = (error) => {
    try {
        const jsonError = JSON.parse(error.message) || {};

        return jsonError.result === errorCodes.ERROR_DUPLICATED_REQUEST;
    } catch {
        return false;
    }
};

// If returns true, the caller will be terminated.
const handleError = ({error = {}, action = {}}) => {
    const params = {error, action};

    return handleErrorDuplicatedRequest(error) || handleNetworkError(params) || handleErrorAccessToken(params);
};

export default handleError;
