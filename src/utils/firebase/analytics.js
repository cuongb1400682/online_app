import {ScreenVisibilityListener} from "react-native-navigation";
import firebase from "react-native-firebase";

import {getStoredDeploymentName} from "src/utils/codePush/codePush";
import {codePushDeploymentNames} from "src/constants/enums";

const setupScreenViewing = () => new ScreenVisibilityListener({
    didAppear: ({screen}) => {
        firebase.analytics().setCurrentScreen(screen);
    }
}).register();

const setupGoogleAnalytics = () => {
    const deploymentName = getStoredDeploymentName();

    if (deploymentName === codePushDeploymentNames.PRODUCTION) {
        setupScreenViewing();
        firebase.analytics().setAnalyticsCollectionEnabled(true);
    }
};

export {
    setupGoogleAnalytics
};
