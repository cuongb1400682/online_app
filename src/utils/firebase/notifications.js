import {AsyncStorage, Platform} from "react-native";
import firebase from "react-native-firebase";
import DeviceInfo from "react-native-device-info";

import store from "src/redux/store";
import {addDeviceToken} from "src/redux/common/sessionActions";
import {isEmptyString} from "../extensions/strings";
import {getStoredDeploymentName} from "../codePush/codePush";
import {getUserInfo} from "src/utils/userAuth/userAuth";

const checkPermissionPushNotify = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
        await sendFCMTokenToServer();
    } else {
        await requestPermission();
    }
};

const sendFCMTokenToServer = async () => {
    let fcmToken = store.getState().session.notification.fcm_token;
    let isSend = await AsyncStorage.getItem('isSend');
    let user_id = getUserInfo().uid ? getUserInfo().uid : 0;
    const uniqueId = DeviceInfo.getUniqueID();

    if (getStoredDeploymentName() === "development" && isSend !== '1' && fcmToken) {
        store.dispatch(addDeviceToken({token: fcmToken, user_id: user_id, device_id: uniqueId}));
        await AsyncStorage.setItem('isSend', '1');
    }

    if (!fcmToken || isEmptyString(fcmToken)) {
        fcmToken = await firebase.messaging().getToken();

        if (fcmToken) {
            store.dispatch(addDeviceToken({token: fcmToken, user_id: user_id, device_id: uniqueId}));
        }
    }
};

const requestPermission = async () => {
    try {
        await firebase.messaging().requestPermission();
        await sendFCMTokenToServer();
    } catch (error) {
        // User has rejected permissions
    }
};

const createNotificationListeners = (onNotificationPress) => {
    firebase.notifications().onNotification(showNotificationOnArrival);

    firebase.notifications().onNotificationOpened(notificationOpen => {
        if (!notificationOpen) {
            return;
        }

        const {notification} = notificationOpen;

        notification.data.link && onNotificationPress({
            link: notification.data.link,
            link_type: notification.data.link_type
        });
    });

    firebase.notifications().getInitialNotification().then(openNotificationLink(onNotificationPress));
};

const showNotificationOnArrival = (notification) => {
    const notifications = new firebase.notifications.Notification()
        .setTitle(notification.title)
        .setBody(notification.body)
        .setNotificationId("notification-action")
        .setSound("default")
        .setData(notification.data)
        .android.setChannelId("notification-action")
        .android.setPriority(firebase.notifications.Android.Priority.Max);

    firebase.notifications().displayNotification(notifications);
};

const openNotificationLink = (onNotificationPress) => (notificationOpen) => {
    if (Platform.OS === "android" || !notificationOpen) {
        return;
    }

    const {notification} = notificationOpen;

    notification.data.link && onNotificationPress({
        link: notification.data.link,
        link_type: notification.data.link_type
    });
};

export {
    checkPermissionPushNotify,
    createNotificationListeners
};
