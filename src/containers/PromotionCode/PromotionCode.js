import React, {Component} from "react";
import {View} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import FontIcon from "src/components/base/FontIcon/FontIcon";
import {removeCouponCode} from "src/redux/common/commonActions";
import CouponIcon from "src/components/base/svgIcons/Coupon/Coupon";
import {calculateDiscount} from "src/utils/misc/businessLogics";
import {discountTypes} from "src/constants/enums";
import CouponItem from "src/pages/Checkout/components/CouponItem/CouponItem";
import commonStyles from "src/styles/commonStyles";
import {formatWithCurrency} from "src/utils/extensions/strings";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import screenIds from "src/constants/screenIds";
import {pushTo} from "src/utils/misc/navigator";
import Text from "src/components/platformSpecific/Text/Text";
import Button from "src/components/base/Button/Button";

class PromotionCode extends Component {
    static propTypes = {
        coupon: PropTypes.object,
        discount: PropTypes.number,
        discountType: PropTypes.string,
        subTotal: PropTypes.number,
        removeCouponCode: PropTypes.func,
    };

    static defaultProps = {
        coupon: {},
        discount: 0,
        discountType: discountTypes.PERCENT,
        subTotal: 0,
        removeCouponCode: () => {
        }
    };

    removeCoupon = value => () => {
        this.props.removeCouponCode(value);
    };

    goToApplyCoupon = () => pushTo(this.props.navigator, {screen: screenIds.APPLY_COUPON});

    renderApplyDiscountContentPart = () => {
        const {coupon, discount = 0, discountType, subTotal} = this.props;
        const couponCode = coupon && coupon.coupon_code;
        const hasCoupon = !!couponCode;
        const discountAmount = calculateDiscount(discount, discountType, subTotal);
        const showDiscountBadge = !!discount && (discountType === discountTypes.PERCENT);

        if (hasCoupon) {
            return (
                <View style={styles.promotionCodeApplyDiscountContent}>
                    <CouponItem onClose={this.removeCoupon(coupon)}>{couponCode}</CouponItem>
                    <View style={commonStyles.matchParent}/>
                    {!!showDiscountBadge && <View style={styles.promotionCodeDiscountBadge}>
                        <Text style={styles.promotionDiscountInPercent}>-{discount}%</Text>
                    </View>}
                    <Text style={styles.promotionDiscountInMoney}>
                        -{formatWithCurrency(discountAmount)}
                    </Text>
                </View>
            );
        }

        return (
            <Button
                title={tr("promotion_code_empty_text")}
                style={styles.choosePromotionCodeButton}
                textStyle={styles.choosePromotionCodeButtonText}
                onPress={this.goToApplyCoupon}
            />
        );
    };

    render() {
        return (
            <Touchable style={styles.promotionCode} onPress={this.goToApplyCoupon}>
                <CouponIcon height="30" isInPaymentButton/>
                <View style={commonStyles.matchParent}>
                    <View style={styles.couponSelect}>
                        {this.renderApplyDiscountContentPart()}
                        <FontIcon
                            fontName="FontAwesome5"
                            name="chevron-right"
                            style={styles.chevron}
                        />
                    </View>
                </View>
            </Touchable>
        );
    }
}

const mapStateToProps = state => ({
    coupon: state.common.cart.coupon,
    discount: state.common.cart.discount,
    discountType: state.common.cart.discount_type,
    subTotal: state.common.cart.sub_total,
});

export default connect(mapStateToProps, {
    removeCouponCode
})(PromotionCode);
