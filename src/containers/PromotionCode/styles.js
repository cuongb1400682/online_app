import {StyleSheet} from "react-native";

import commonStyles from "src/styles/commonStyles";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    promotionCode: {
        width: "100%",
        paddingVertical: 10,
        paddingLeft: 10,
        backgroundColor: "#fff",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        height: 48
    },
    promotionCodeTitle: {
        fontSize: 14,
        marginLeft: 8,
        marginRight: 16,
        color: "#333",
        fontWeight: "bold",
    },
    choosePromotionCodeButton: {
        backgroundColor: "#0E9544",
        borderColor: "#0E9544",
        height: 36,
        flex: 1
    },
    choosePromotionCodeButtonText: {
        color: "#fff"
    },
    promotionCodeDiscountBadge: {
        height: 18,
        backgroundColor: "#ff5b05",
        borderRadius: 9,
        paddingHorizontal: 6,
        paddingVertical: 1,
        marginRight: 6,
        ...commonStyles.centerChildren
    },
    promotionCodeApplyDiscountContent: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
    },
    promotionDiscountInPercent: {
        color: "#fff",
        fontSize: 11,
    },
    promotionDiscountInMoney: {
        fontSize: 12
    },
    chevron: {
        fontSize: 12,
        color: "#757575",
        marginLeft: 14,
    },
    couponSelect: {
        height: 24,
        paddingHorizontal: 10,
        flexDirection: "row",
        flex: 1,
        ...commonStyles.centerChildren
    }
});

export default styles;
