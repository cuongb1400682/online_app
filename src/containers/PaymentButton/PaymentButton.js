import React, {Component} from "react";
import {View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import {tr} from "src/localization/localization";
import {formatWithCurrency} from "src/utils/extensions/strings";
import commonStyles from "src/styles/commonStyles";
import Button from "src/components/base/Button/Button";
import {isUserLoggedIn} from "src/utils/userAuth/userAuth";
import {pushTo} from "src/utils/misc/navigator";
import screenIds from "src/constants/screenIds";
import {calculateDiscount} from "src/utils/misc/businessLogics";
import {discountTypes} from "src/constants/enums";
import PromotionCode from "src/containers/PromotionCode/PromotionCode";
import Text from "src/components/platformSpecific/Text/Text";

const HEIGHT_WITH_PROMOTION_CODE = 104;
const HEIGHT_WITHOUT_PROMOTION_CODE = 56;

class PaymentButton extends Component {
    static propTypes = {
        showsPromotionCodeSelect: PropTypes.bool,
        title: PropTypes.string,
        nextStep: PropTypes.string,
        navigator: PropTypes.any.isRequired,
        numberItems: PropTypes.number,
        subTotal: PropTypes.number,
        shippingFee: PropTypes.number,
        discount: PropTypes.number,
        discountType: PropTypes.string,
        onButtonPress: PropTypes.func,
    };

    static defaultProps = {
        showsPromotionCodeSelect: false,
        title: tr("payment_button_title"),
        nextStep: "",
        numberItems: 0,
        subTotal: 0,
        shippingFee: 0,
        discount: 0,
        discountType: discountTypes.PERCENT,
        onButtonPress: () => {
        },
    };

    continueNextCheckoutStep = () => {
        const {nextStep, navigator} = this.props;

        pushTo(navigator, {screen: nextStep});
    };

    handleButtonPress = () => {
        const {navigator, onButtonPress} = this.props;

        if (!isUserLoggedIn()) {
            return pushTo(navigator, {
                screen: screenIds.LOGIN,
                passProps: {
                    onLoginSucceed: this.continueNextCheckoutStep
                }
            });
        }

        onButtonPress();
    };

    renderOrderInfo = () => {
        const {numberItems, shippingFee, discount, discountType, subTotal = 0} = this.props;
        const discountAmount = calculateDiscount(discount, discountType, subTotal);

        return (
            <View style={commonStyles.matchParent}>
                <Text style={styles.totalMoneyText}>
                    {tr("payment_button_total")}
                    ({numberItems} {tr("payment_button_product")})
                </Text>
                <View style={styles.totalMoneyContainer}>
                    <Text style={styles.totalMoney}>
                        {formatWithCurrency(subTotal + shippingFee - discountAmount)}
                    </Text>
                    <Text style={styles.vatIncluded}>{tr("payment_button_vat_included")}</Text>
                </View>
            </View>
        );
    };

    render() {
        const {title, showsPromotionCodeSelect, navigator} = this.props;
        const paymentButtonHeight = {
            height: showsPromotionCodeSelect
                ? HEIGHT_WITH_PROMOTION_CODE
                : HEIGHT_WITHOUT_PROMOTION_CODE
        };

        return (
            <View style={[styles.paymentButton, paymentButtonHeight]}>
                {showsPromotionCodeSelect && <PromotionCode navigator={navigator}/>}
                <View style={styles.withoutPromotionCode}>
                    {this.renderOrderInfo()}
                    <Button
                        type="fully-colored"
                        style={styles.button}
                        title={title}
                        onPress={this.handleButtonPress}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    numberItems: state.common.cart.number_items,
    subTotal: state.common.cart.sub_total,
    shippingFee: state.common.shipping_methods.fee,
    discount: state.common.cart.discount,
    discountType: state.common.cart.discount_type
});

export default connect(mapStateToProps, {})(PaymentButton);
