import {StyleSheet} from "react-native";

import commonStyles from "src/styles/commonStyles";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    paymentButton: {
        width: "100%",
        backgroundColor: "#fff",
        flexDirection: "column",
        alignItems: "center",
        ...commonStyles.hasShadow
    },
    withoutPromotionCode: {
        flex: 1,
        paddingHorizontal: 12,
        flexDirection: "row",
        alignItems: "center",
        borderTopWidth: 1,
        borderColor: "#d7d7d7"
    },
    button: {
        height: 40,
        maxWidth: 168
    },
    totalMoneyContainer: {
        flexDirection: "row",
        marginTop: 6,
        alignItems: "flex-end"
    },
    totalMoneyText: {
        fontSize: 14,
        color: "#333",
    },
    totalMoney: {
        fontSize: 18,
        color: colors.primaryColor,
    },
    vatIncluded: {
        fontSize: 10,
        color: "#757575",
        marginBottom: 2
    }
});

export default styles;
