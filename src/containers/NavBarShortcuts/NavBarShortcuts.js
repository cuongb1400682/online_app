import React, {Component} from "react";
import {View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import styles from "./styles";
import NavBarButton from "src/components/base/NavBarButton/NavBarButton";
import Cart from "src/components/base/svgIcons/Cart/Cart";
import commonStyles from "src/styles/commonStyles";
import screenIds from "src/constants/screenIds";
import Magnifier from "src/components/base/svgIcons/Magnifier/Magnifier";
import Text from "src/components/platformSpecific/Text/Text";
import {getStats} from "src/redux/common/commonActions";
import {pushTo} from "src/utils/misc/navigator";

class NavBarShortcuts extends Component {
    static propTypes = {
        numberOfCartItems: PropTypes.number,
        showsSearch: PropTypes.bool,
        showsCart: PropTypes.bool,
        showsVerticalEllipsis: PropTypes.bool,
        navigator: PropTypes.any,
        onSearchPress: PropTypes.func,
        onCartPress: PropTypes.func,
        onVerticalEllipsisPress: PropTypes.func,
        getStats: PropTypes.func
    };

    static defaultProps = {
        numberOfCartItems: 0,
        showsSearch: false,
        showsCart: false,
        navigator: null,
        showsVerticalEllipsis: false,
        onSearchPress: null,
        onCartPress: null,
        onVerticalEllipsisPress: null,
        getStats: () => {
        }
    };

    componentDidMount() {
        const {getStats} = this.props;

        getStats();
    }

    showCart = () => {
        const {navigator, onCartPress} = this.props;

        if (typeof onCartPress === "function") {
            onCartPress();
        } else {
            pushTo(navigator, {screen: screenIds.CART});
        }
    };

    showSearch = () => {
        const {navigator, onSearchPress} = this.props;

        if (typeof onSearchPress === "function") {
            onSearchPress();
        } else {
            pushTo(navigator, {screen: screenIds.SEARCH});
        }
    };

    render() {
        const {showsSearch, showsCart, showsVerticalEllipsis} = this.props;

        return (
            <View style={commonStyles.flexRow}>
                {showsSearch && <NavBarButton onPress={this.showSearch}>
                    <Magnifier color="#fff" size={20}/>
                </NavBarButton>}
                {showsCart && <NavBarButton onPress={this.showCart}>
                    <Cart/>
                    <View style={[commonStyles.centerChildren, styles.circleNumberContainer]}>
                        <Text style={styles.circleNumber}>{this.props.numberOfCartItems}</Text>
                    </View>
                </NavBarButton>}
                {/*{showsVerticalEllipsis && <NavBarButton key={2}>*/}
                {/*<VerticalEllipsis/>*/}
                {/*</NavBarButton>}*/}
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    numberOfCartItems: state.common.cart.number_items
});

export default connect(mapStateToProps, {
    getStats
})(NavBarShortcuts);
