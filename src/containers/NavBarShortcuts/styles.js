import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    circleNumberContainer: {
        width: 14,
        height: 14,
        backgroundColor: "#fff",
        borderRadius: 7,
        position: "absolute",
        top: 8,
        left: 20
    },
    circleNumber: {
        color: colors.primaryColor,
        fontSize: 10
    },
});

export default styles;
