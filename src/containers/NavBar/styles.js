import {StyleSheet, Platform} from "react-native";
import commonStyles from "src/styles/commonStyles";
import systemFont from "src/styles/systemFont";

const styles = StyleSheet.create({
    navBar: {
        ...commonStyles.hasShadow,
        zIndex: 1000
    },
    navBarContent: {
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        overflow: "hidden"
    },
    defaultNavBar: {
        flexDirection: "row",
        alignItems: "center"
    },
    defaultBackButton: {
        fontSize: 24,
        color: "#fff"
    },
    defaultTitle: {
        color: "#fff",
        fontSize: 15,
        fontWeight: "bold"
    },
});

export default styles;
