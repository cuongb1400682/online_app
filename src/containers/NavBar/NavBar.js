import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import * as Animatable from "react-native-animatable";

import styles from "./styles";
import colors from "src/constants/colors";
import commonStyles from "src/styles/commonStyles";
import NavBarButton from "src/components/base/NavBarButton/NavBarButton";
import CustomStatusBar from "src/components/base/CustomStatusBar/CustomStatusBar";
import {popBack} from "src/utils/misc/navigator";
import Text from "src/components/platformSpecific/Text/Text";

class NavBar extends Component {
    static propTypes = {
        style: PropTypes.any,
        backgroundColor: PropTypes.string,
        children: PropTypes.any,
        title: PropTypes.string,
        navigator: PropTypes.object,
        right: PropTypes.any,
    };

    static defaultProps = {
        style: {},
        backgroundColor: colors.primaryColor,
        children: null,
        title: "",
        navigator: null,
        right: null,
    };

    handleGoBack = () => {
        const {navigator} = this.props;

        popBack(navigator, {animated: true});
    };

    renderDefaultNavBar = () => {
        return (
            <View style={[commonStyles.matchParent, styles.defaultNavBar]}>
                <NavBarButton onPress={this.handleGoBack}>
                    <MaterialCommunityIcons name="arrow-left" style={styles.defaultBackButton}/>
                </NavBarButton>
                <Text style={styles.defaultTitle}>{this.props.title}</Text>
                <View style={commonStyles.matchParent}/>
                {this.props.right}
            </View>
        );
    };

    render() {
        const {children, style, backgroundColor} = this.props;

        return (
            <Animatable.View
                style={[styles.navBar, {backgroundColor}]}
                transition="backgroundColor"
                duration={1500}
            >
                <CustomStatusBar/>
                <View style={[styles.navBarContent, style]}>
                    {children ? children : this.renderDefaultNavBar()}
                </View>
            </Animatable.View>
        );
    }
}

const mapStateToProps = () => ({});

export default connect(mapStateToProps, {})(NavBar);

