import {createAction} from "src/utils/api/createAction";
import localizationActionTypes from "src/redux/actionTypes/localizationActionTypes";

const changeSystemLanguage = (language) => createAction({
    type: localizationActionTypes.SYSTEM_LANGUAGE_CHANGE,
    payload: {
        language
    }
});

export {
    changeSystemLanguage
};
