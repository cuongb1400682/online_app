import sessionActionTypes from "src/redux/actionTypes/sessionActionTypes";
import {sendUserInfoToSentry} from "src/utils/sentry/setupSentry";
import {toRequest} from "src/utils/api/createAction";

const initialState = {
    access_token: "",
    session_key: "",
    user_info: {},
    notification: {
        fcm_token: "",
        customer_id: ""
    }
};

const sessionReducers = (state = initialState, {type, payload}) => {
    switch (type) {
        case sessionActionTypes.CLEAR_USER_INFO:
            return {
                ...state,
                access_token: "",
                user_info: {}
            };
        case sessionActionTypes.UPDATE_USER_INFO:
            sendUserInfoToSentry(payload.userInfo);

            return {
                ...state,
                access_token: payload.userInfo.access_token,
                user_info: {
                    ...payload.userInfo
                }
            };
        case sessionActionTypes.UPDATE_SESSION_KEY:
            return {
                ...state,
                session_key: payload.sessionKey
            };
        case toRequest(sessionActionTypes.ADD_DEVICE_TOKEN):
            return {
                ...state,
                notification: {
                    fcm_token: payload.body.token,
                    customer_id: payload.body.customer_id
                }
            };
        default:
            return state;
    }
};

export default sessionReducers;
