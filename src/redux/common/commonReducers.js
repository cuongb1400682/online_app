import commonActionTypes from "src/redux/actionTypes/commonActionTypes";
import {toFailure, toRequest, toSuccess} from "src/utils/api/createAction";

const defaultCartParams = {
    adding: false,
    fetching: false,
    removing: false,
    items: [],
    sub_total: 0,
    total: 0,
    number_items: 0,
    adding_coupon_code: false,
    coupon: {}
};

const defaultCustomerParams = {
    addresses: [],
    main_address_id: null
};

const initialState = {
    categories: {
        list: [],
        fetching: false
    },
    cart: {
        ...defaultCartParams
    },
    shipping_methods: {
        data: [],
        fee: 0,
        waiting: false
    },
    customer: {
        ...defaultCustomerParams
    },
    home_navigator: null
};

const mapProductOutOfStock = (addToCartItems, outOfStockProduct = {}) => {
    return addToCartItems.map(item => {
        if (item.product.id === outOfStockProduct.id) {
            return {
                ...item,
                out_of_stock: true,
                product: {...outOfStockProduct}
            };
        }

        return {...item};
    });
};

const commonReducers = (state = initialState, {type, payload = {}}) => {
    switch (type) {
        case commonActionTypes.CHANGE_SHIPPING_METHOD:
            return {
                ...state,
                shipping_methods: {
                    ...state.shipping_methods,
                    fee: payload.fee
                }
            };
        case commonActionTypes.RESET_SHIPPING_FEE:
            return {
                ...state,
                shipping_methods: {
                    ...state.shipping_methods,
                    fee: 0
                }
            };
        case toSuccess(commonActionTypes.GET_CUSTOMER_ADDRESS):
            return {
                ...state,
                customer: {
                    ...state.customer,
                    ...payload.reply
                }
            };
        case toRequest(commonActionTypes.REMOVE_PRODUCT_FROM_CART):
            return {
                ...state,
                cart: {
                    ...state.cart,
                    removing: true,
                }
            };
        case toSuccess(commonActionTypes.REMOVE_PRODUCT_FROM_CART):
            return {
                ...state,
                cart: {
                    ...payload.reply,
                    removing: false,
                }
            };
        case toFailure(commonActionTypes.REMOVE_PRODUCT_FROM_CART):
            return {
                ...state,
                cart: {
                    ...state.cart,
                    removing: false,
                }
            };
        case toRequest(commonActionTypes.FETCH_CART):
            return {
                ...state,
                cart: {
                    ...state.cart,
                    fetching: true
                }
            };
        case toSuccess(commonActionTypes.FETCH_CART):
            const {items, coupon = {}} = payload.reply;

            if (!items) {
                return {
                    ...state,
                    cart: {
                        ...defaultCartParams
                    }
                };
            }

            return {
                ...state,
                cart: {
                    ...state.cart,
                    ...payload.reply,
                    coupon,
                    fetching: false,
                }
            };
        case toFailure(commonActionTypes.FETCH_CART):
            return {
                ...state,
                fetching: false
            };
        case toSuccess(commonActionTypes.GET_STATS):
            return {
                ...state,
                cart: {
                    ...state.cart,
                    number_items: payload.reply.number_items,
                    total: payload.reply.total
                }
            };
        case toSuccess(commonActionTypes.ADD_PRODUCT_TO_CART):
            return {
                ...state,
                cart: {
                    ...payload.reply,
                    adding: false,
                }
            };
        case toFailure(commonActionTypes.ADD_PRODUCT_TO_CART): {
            const {reply = {}} = payload;

            return {
                ...state,
                cart: {
                    ...state.cart,
                    items: [...mapProductOutOfStock(state.cart.items, reply.product_detail)],
                    adding: false
                }
            };
        }
        case toRequest(commonActionTypes.FETCH_CATEGORIES):
            return {
                ...state,
                categories: {
                    ...state.categories,
                    fetching: true
                }
            };
        case toSuccess(commonActionTypes.FETCH_CATEGORIES):
            return {
                ...state,
                categories: {
                    ...state.categories,
                    list: [...payload.reply],
                    fetching: false
                }
            };
        case toFailure(commonActionTypes.FETCH_CATEGORIES):
            return {
                ...state,
                categories: {
                    ...state.categories,
                    fetching: false
                }
            };
        case toRequest(commonActionTypes.DELETE_CUSTOMER_ADDRESS):
            return {
                ...state,
                shipping_address: {
                    ...state.shipping_address,
                    id: null
                }
            };
        case toRequest(commonActionTypes.FETCH_SHIPPING_METHODS):
        case toFailure(commonActionTypes.FETCH_SHIPPING_METHODS):
            return {
                ...state,
                shipping_methods: {
                    ...state.shipping_methods,
                    data: [],
                    fee: 0
                }
            };
        case toSuccess(commonActionTypes.FETCH_SHIPPING_METHODS):
            const shippingMethods = payload.reply || [];
            const {fee: defaultShippingFee = 0} = shippingMethods[0] || {};

            return {
                ...state,
                shipping_methods: {
                    ...state.shipping_methods,
                    data: payload.reply || [],
                    fee: defaultShippingFee
                }
            };
        case toRequest(commonActionTypes.APPLY_COUPON_CODE):
            return {
                ...state,
                cart: {
                    ...state.cart,
                    adding_coupon_code: true,
                }
            };
        case toSuccess(commonActionTypes.APPLY_COUPON_CODE):
            return {
                ...state,
                cart: {
                    ...state.cart,
                    ...payload.reply,
                    adding_coupon_code: false
                }
            };
        case toFailure(commonActionTypes.APPLY_COUPON_CODE):
            return {
                ...state,
                cart: {
                    ...state.cart,
                    adding_coupon_code: false
                }
            };
        case toSuccess(commonActionTypes.REMOVE_COUPON_CODE):
            return {
                ...state,
                cart: {
                    ...state.cart,
                    ...payload.reply,
                    coupon: {}
                }
            };
        case commonActionTypes.SAVE_HOME_NAVIGATOR_OBJECT:
            return {
                ...state,
                home_navigator: payload.navigator
            };
        default:
            return state;
    }
};

export default commonReducers;
