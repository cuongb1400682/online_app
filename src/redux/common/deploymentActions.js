import deploymentActionTypes from "src/redux/actionTypes/deploymentActionTypes";
import {createAction} from "src/utils/api/createAction";

const changeUpdateMetaData = (updateMetaData ={}) => createAction({
    type: deploymentActionTypes.CHANGE_UPDATE_META_DATA,
    payload: {
        ...updateMetaData
    }
});

export {
    changeUpdateMetaData
};
