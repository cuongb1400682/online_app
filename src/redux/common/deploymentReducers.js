import {AsyncStorage} from "react-native";

import NativeConstants from "src/utils/reactNative/NativeConstants";
import deploymentActionTypes from "src/redux/actionTypes/deploymentActionTypes";
import {findDeploymentNameByKey} from "src/utils/codePush/codePush";
import {DEPLOYMENT_KEY} from "src/utils/codePush/deploymentChanging";

const initialState = {
    update_meta_data: {
        deploymentKey: NativeConstants.deploymentKey
    }
};

const deploymentReducers = (state = initialState, {payload = {}, type}) => {
    switch (type) {
        case deploymentActionTypes.CHANGE_UPDATE_META_DATA:
            AsyncStorage.setItem(DEPLOYMENT_KEY, payload.deploymentKey);

            return {
                ...state,
                update_meta_data: {
                    deploymentKey: payload.deploymentKey,
                    deploymentName: findDeploymentNameByKey(payload.deploymentKey)
                }
            };
        default:
            return state;
    }
};

export default deploymentReducers;
