import {createAction, createAsyncAction} from "src/utils/api/createAction";
import sessionActionTypes from "src/redux/actionTypes/sessionActionTypes";

const updateSessionKey = (sessionKey) => createAction({
    type: sessionActionTypes.UPDATE_SESSION_KEY,
    payload: {
        sessionKey
    }
});

const updateUserInfo = (userInfo) => createAction({
    type: sessionActionTypes.UPDATE_USER_INFO,
    payload: {
        userInfo
    }
});

const clearUserInfo = () => createAction({
    type: sessionActionTypes.CLEAR_USER_INFO,
    payload: {}
});

const addDeviceToken = ({user_id=0, token="", device_id=""}) => createAsyncAction({
    type: sessionActionTypes.ADD_DEVICE_TOKEN,
    payload: {
        request: "/notification/receive_device_token",
        method: "POST",
        body: {
            user_id,
            token,
            device_id
        }
    }
});

export {
    clearUserInfo,
    updateUserInfo,
    updateSessionKey,
    addDeviceToken
};
