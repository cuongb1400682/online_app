import {createAction, createAsyncAction} from "src/utils/api/createAction";
import commonActionTypes from "src/redux/actionTypes/commonActionTypes";
import errorCodes from "src/constants/errorCodes";
import {tr} from "src/localization/localization";
import Message from "src/components/platformSpecific/Message/Message";
import {formatWithCurrency, isEmptyString} from "src/utils/extensions/strings";

const changeShippingMethod = ({fee = 0}) => createAction({
    type: commonActionTypes.CHANGE_SHIPPING_METHOD,
    payload: {
        fee
    }
});

const resetShippingFee = () => createAction({
    type: commonActionTypes.RESET_SHIPPING_FEE,
    payload: {}
});

const fetchCustomerAddress = ({pageType, defaultAddressId} = {}) => createAsyncAction({
    type: commonActionTypes.GET_CUSTOMER_ADDRESS,
    payload: {
        request: "/customer/address/list",
        params: {
            page_type: pageType
        },
        meta: {
            default_address_id: defaultAddressId
        }
    }
});

const addToCart = ({productId, quantity}) => createAsyncAction({
    type: commonActionTypes.ADD_PRODUCT_TO_CART,
    payload: {
        request: "/cart/add_product",
        method: "POST",
        body: {
            product_id: productId,
            qty: quantity,
        }
    },
    onFailure: (dispatch, response = {}) => {
        const {reply = {}} = response;

        if (response.result === errorCodes.ERROR_CART_OUT_OF_STOCK) {
            const {qty = 0} = reply.product_detail || {};

            Message.show("", tr("error_cart_of_of_stock", qty));
            return;
        }

        Message.show("", tr(response.result));
    }
});

const fetchCategories = () => {
    return createAsyncAction({
        type: commonActionTypes.FETCH_CATEGORIES,
        payload: {
            request: "/category/list"
        }
    });
};

const getStats = () => createAsyncAction({
    type: commonActionTypes.GET_STATS,
    payload: {
        request: "/user/get_stats",
        params: {}
    }
});

const removeProductFromCart = ({productId}) => createAsyncAction({
    type: commonActionTypes.REMOVE_PRODUCT_FROM_CART,
    payload: {
        request: "/cart/add_product",
        method: "POST",
        body: {
            product_id: productId,
            qty: 0,
        }
    }
});

const deleteCustomerAddress = ({addressId}) => createAsyncAction({
    type: commonActionTypes.DELETE_CUSTOMER_ADDRESS,
    payload: {
        request: "/customer/address/delete",
        method: "POST",
        body: {
            address_id: addressId
        }
    },
    onSuccess: (dispatch) => {
        dispatch(fetchCustomerAddress());
    }
});

const fetchShippingMethods = (cart_id, page_type) => createAsyncAction({
    type: commonActionTypes.FETCH_SHIPPING_METHODS,
    payload: {
        request: "/shipping/shipping_method_list",
        params: {
            cart_id,
            page_type
        }
    }
});

const fetchCart = ({onSuccess, onFailure} = {}) => createAsyncAction({
    type: commonActionTypes.FETCH_CART,
    payload: {
        request: "/cart/get_cart",
        params: {}
    },
    onSuccess(dispatch, response) {
        if (typeof onSuccess === "function") {
            onSuccess(response);
        }
    },
    onFailure: (_, response) => {
        if (typeof onFailure === "function") {
            onFailure(response);
        }
    }
});

const applyCouponCode = (code, {onSuccess} = {}) => createAsyncAction({
    type: commonActionTypes.APPLY_COUPON_CODE,
    payload: {
        method: "POST",
        request: "/coupon/apply",
        body: {
            coupon_code: code
        }
    },
    onSuccess: () => {
        if (typeof onSuccess === "function") {
            onSuccess();
        }
    },
    onFailure: (dispatch, response) => {
        if (response.result === errorCodes.ERROR_CART_SUB_TOTAL_LESS_THAN_MIN_VALUE) {
            Message.show(
                tr("error_cart_sub_total_less_than_min_value_2"),
                tr("error_cart_sub_total_less_than_min_value_1") +
                formatWithCurrency(response.reply.min_value)
            );
        } else {
            const message = tr(
                isEmptyString(response.result)
                    ? "error_apply_coupon_code"
                    : response.result
            );

            Message.show(
                "",
                message
            );
        }
    }
});

const removeCouponCode = () => createAsyncAction({
    type: commonActionTypes.REMOVE_COUPON_CODE,
    payload: {
        method: "POST",
        request: "/coupon/remove",
        body: {}
    }
});

const saveHomeNavigatorObject = (navigator) => createAction({
    type: commonActionTypes.SAVE_HOME_NAVIGATOR_OBJECT,
    payload: {
        navigator
    }
});

export {
    changeShippingMethod,
    resetShippingFee,
    fetchShippingMethods,
    fetchCart,
    fetchCustomerAddress,
    getStats,
    fetchCategories,
    addToCart,
    removeProductFromCart,
    deleteCustomerAddress,
    applyCouponCode,
    removeCouponCode,
    saveHomeNavigatorObject
};
