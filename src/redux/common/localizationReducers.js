import {getSystemLanguage} from "src/localization/localization";
import localizationActionTypes from "src/redux/actionTypes/localizationActionTypes";

const initialState = {
    current_language: getSystemLanguage()
};

const localizationReducers = (state = initialState, {type, payload}) => {
    switch (type) {
        case localizationActionTypes.SYSTEM_LANGUAGE_CHANGE:
            return {
                ...state,
                current_language: payload.language
            };
        default:
            return state;
    }
};

export default localizationReducers;
