import {applyMiddleware, compose, createStore} from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import {persistStore} from "redux-persist";

import rootReducer from "src/redux/rootReducers";
import NativeConstants from "src/utils/reactNative/NativeConstants";

const getMiddlewares = () => {
    const middlewares = [thunk];

    if (NativeConstants.isDebug) {
        middlewares.push(logger);
    }

    return middlewares;
};

const configureStore = () => {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    return createStore(
        rootReducer,
        composeEnhancers(applyMiddleware(...getMiddlewares()))
    );
};

const store = configureStore();

const persistor = persistStore(store, null);

export {
    store as default,
    persistor
};
