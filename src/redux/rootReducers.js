import {persistCombineReducers} from "redux-persist";
import storage from "redux-persist/es/storage";

import product_detail_reducers from "src/pages/ProductDetail/productDetailReducers";
import localization_reducers from "src/redux/common/localizationReducers";
import home_reducers from "src/pages/Home/homeReducers";
import common_reducers from "src/redux/common/commonReducers";
import session_reducers from "src/redux/common/sessionReducers";
import checkout_reducers from "src/pages/Checkout/checkoutReducers";
import search_result_reducers from "src/pages/SearchResult/searchResultReducers";
import catalog_reducers from "src/pages/Catalog/catalogReducers";
import user_auth_reducers from "src/pages/UserAuth/userAuthReducers";
import profile_manager_reducers from "src/pages/ProfileManager/profileManagerReducers";
import deployment_reducers from "src/redux/common/deploymentReducers";
import bestsales_product_reducers from "src/pages/BestSaleProduct/bestSaleProductReducers";
import promoted_product_reducers from "src/pages/PromotedProduct/promotedProductReducers";
import new_product_reducers from "src/pages/NewProduct/newProductReducers";
import address_reducers from "src/pages/Address/addressReducers";
import notification_reducers from "src/pages/Notification/notificationReducers";
import deals_list_reducers from "src/pages/DealList/dealListReducers";
import landing_page_reducers from "src/pages/LandingPage/landingPageReducers";

export default persistCombineReducers({
    key: "root",
    storage
}, {
    address: address_reducers,
    localization: localization_reducers,
    home: home_reducers,
    common: common_reducers,
    product_detail: product_detail_reducers,
    session: session_reducers,
    checkout: checkout_reducers,
    search_result: search_result_reducers,
    catalog: catalog_reducers,
    user_auth: user_auth_reducers,
    profile_manager: profile_manager_reducers,
    deployment: deployment_reducers,
    bestsales: bestsales_product_reducers,
    promoted_product: promoted_product_reducers,
    new_product: new_product_reducers,
    notification: notification_reducers,
    deals: deals_list_reducers,
    landing_page: landing_page_reducers
});
