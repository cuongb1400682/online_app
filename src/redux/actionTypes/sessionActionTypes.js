const sessionActionTypes = {
    UPDATE_USER_INFO: "UPDATE_USER_INFO",
    UPDATE_SESSION_KEY: "UPDATE_SESSION_KEY",
    CLEAR_USER_INFO: "CLEAR_USER_INFO",
    ADD_DEVICE_TOKEN: "ADD_DEVICE_TOKEN"
};

export default sessionActionTypes;
