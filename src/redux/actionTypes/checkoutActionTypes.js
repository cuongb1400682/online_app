const checkoutActionTypes = {
    TRACKING_CHECKOUT: "TRACKING_CHECKOUT",
    FETCH_ORDER_DETAIL: "FETCH_ORDER_DETAIL",
    CLEAR_SHIPPING_TIME: "CLEAR_SHIPPING_TIME",
    FETCH_SHIPPING_TIME: "FETCH_SHIPPING_TIME",
    CREATE_ORDER: "CREATE_ORDER",
    CLEAR_SHIPPING_ADDRESS: "CLEAR_SHIPPING_ADDRESS",
    UPDATE_SHIPPING_ADDRESS: "UPDATE_SHIPPING_ADDRESS",
    SELECT_SHIPPING_ADDRESS: "SELECT_SHIPPING_ADDRESS",
    FETCH_CUSTOMER_ADDRESS_BY_ID: "FETCH_CUSTOMER_ADDRESS_BY_ID",
    FETCH_MY_COUPONS: "@checkout/FETCH_MY_COUPONS"
};

export default checkoutActionTypes;
