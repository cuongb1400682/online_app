const localizationActionTypes = {
    SYSTEM_LANGUAGE_CHANGE: "SYSTEM_LANGUAGE_CHANGE"
};

export default localizationActionTypes;
