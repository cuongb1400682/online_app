const searchResultActionTypes = {
    CHANGE_VIEW_MODE: "SEARCH_CHANGE_VIEW_MODE",
    SEARCH_PRODUCT_BY_KEYWORD: "SEARCH_PRODUCT_BY_KEYWORD"
};

export default searchResultActionTypes;
