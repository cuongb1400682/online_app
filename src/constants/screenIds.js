/*
 * The screen identifiers are named using the following convention:
 * <packageName>.<ScreenName>
 * Whereas:
 *    - packageName: com.giigaa.online-app
 *    - ScreenName: the name of the screen
 */

const screenIds = {
    ADDRESS_DETAIL: "com.giigaa.online-app.address.address-detail",
    HOME: "com.giigaa.online-app.home",
    CATEGORIES: "com.giigaa.online-app.categories",
    ACCOUNT: "com.giigaa.online-app.account",
    NOTIFICATION: "com.giigaa.online-app.notification",
    PRODUCT_DETAIL: "com.giigaa.online-app.product-detail",
    CART: "com.giigaa.online-app.cart",
    CHECKOUT_SHIPPING_STEP: "com.giigaa.online-app.checkout.shipping-step",
    CHECKOUT_PAYMENT_STEP: "com.giigaa.online-app.checkout.payment-step",
    CHECKOUT_SUCCESS_STEP: "com.giigaa.online-app.checkout.success-step",
    SEARCH_RESULT: "com.giigaa.online-app.search-result",
    SEARCH: "com.giigaa.online-app.search",
    CATALOG_BY_CATEGORY: "com.giigaa.online-app.catalog-by-category",
    LOGIN: "com.giigaa.online-app.user-auth.login",
    LOGIN_WITH_GIIGAA_ID: "com.giigaa.online-app.user-auth.login-with-giigaa-id",
    USER_REGISTRATION: "com.giigaa.online-app.user-auth.user-registration",
    PROFILE_MANAGER: "com.giigaa.online-app.profile-manager.user-profile",
    ORDER_MANAGER: "com.giigaa.online-app.profile-manager.order-manager",
    ORDER_DETAIL: "com.giigaa.online-app.profile-manager.order-detail",
    USER_FORGOT_PASSWORD: "com.giigaa.online-app.profile-manager.user-forgot-password",
    MY_ACCOUNT: "com.giigaa.online-app.profile-manager.my-account",
    VIEWED_PRODUCTS: "com.giigaa.online-app.profile-manager.viewed-products",
    COUPON_MANAGER: "com.giigaa.online-app.profile-manager.coupon-manager",
    UNDER_DEVELOPMENT: "com.giigaa.online-app.under-development",
    BEST_SALE_PRODUCT: "com.giigaa.online-app.best-sale-product",
    NEW_PRODUCT: "com.giigaa.online-app.new-product",
    PROMOTED_PRODUCT: "com.giigaa.online-app.promoted-product",
    APPLY_COUPON: "com.giigaa.online-app.apply-coupon",
    DEAL_LIST: "com.giigaa.online-app.deal-list",
    LANDING_PAGE: "com.giigaa.online-app.landing-page"
};

export default screenIds;
