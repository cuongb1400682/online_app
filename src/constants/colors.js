const colors = {
    primaryColor: "#ef4423",
    primaryLight: "#f56300",
    primaryDark: "#000",
    textColor: "#222",
    orderStep: {
        creation: "#dedb00",
        picking: "#e10000",
        shipping: "#0275D8",
        completed: "#5CB85C",
        cancel: "#999",
        refund: "#999",
        returned: "#999"
    },
    orderStatus: {
        active: "#5cb85c",
        inactive: "#d1d1d1"
    }
};

export default colors;
