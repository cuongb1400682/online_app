const errorCodes = {
    ERROR_ACCESS_TOKEN: "error_access_token",
    ERROR_LOGIN_FAILED: "error_login_failed",
    ERROR_LOGIN_CANCELLED: "error_login_cancelled",
    ERROR_CART_OUT_OF_STOCK: "error_cart_out_of_stock",
    ERROR_CART_SUB_TOTAL_LESS_THAN_MIN_VALUE: "error_cart_sub_total_less_than_min_value",
    ERROR_DUPLICATED_REQUEST: "error_duplicated_request",
    ASYNC_OP_IN_PROGRESS: "ASYNC_OP_IN_PROGRESS"
};

export default errorCodes;
