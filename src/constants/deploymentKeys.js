import {Platform} from "react-native";

const ios = {
    "development": "i5WrD5ELiDFbpixU5VK6MjN8OtIPSJ967S1e4",
    "production": "Ylr1XV3eql-6tIVREnaz25_eXNiZB1d3Xr1lV",
    "staging": "55xz-HbpylfC6LDbwulzS9qDRlTASJu2mS1x4"
};

const android = {
    "development": "gbJfJ0tAWbeNB24kozSGPMBqpC4zS1h8DLvw4",
    "production": "F-1cNX8kvq-V7xkl4tng_ESM7g9cByEG8IDPV",
    "staging": "TEQpAjtXyLwJ54vC6IYHWTWV_znXBy4f8UPPN"
}
export default Platform.select({
    "ios": ios,
    "android": android
});
