const formatDatePicker = "DD-MM-YYYY";
const formatDateWithTime = "DD-MM_YYYY HH:mm";
const formatTime = "HH:mm:ss";
const formatDate = "DD/MM/YYYY"

export {
    formatDatePicker,
    formatDateWithTime,
    formatTime,
    formatDate
};
