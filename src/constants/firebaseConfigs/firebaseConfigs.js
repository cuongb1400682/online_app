import iosProduction from "./ios/googleService.production.json";
import iosDevelopment from "./ios/googleService.testing.json";
import iosStaging from "./ios/googleService.staging.json";

const firebaseConfigs = {
    ios: {
        production: iosProduction,
        development: iosDevelopment,
        staging: iosStaging,
    },
    android: {

    }
};

export default firebaseConfigs;
