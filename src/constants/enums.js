const productViewModes = {
    LIST: "LIST",
    GRID: "GRID"
};

const imageTypes = {
    FULL_SIZE: "FULL_SIZE",
    PURCHASE_ORDER_DETAIL: "PURCHASE_ORDER_DETAIL",
    PROMOTED: "PROMOTED",
    BEST_SALES: "BEST_SALES",
    CART: "CART",
    VIEWED: "VIEWED",
    IMAGE_LIST: "IMAGE_LIST"
};

const paymentMethods = {
    COD: "COD",
    CASH: "CASH",
    NAPAS: "NAPAS",
    ONLINE_PAYMENT: "ONLINE_PAYMENT",
    INTERNET_BANKING: "INTERNET_BANKING",
    VISA_MASTER_CARD: "VISA_MASTER_CARD"
};

const shippingMethods = {
    STANDARD: "STANDARD",
    FAST: "FAST"
};

const loginTypes = {
    FACEBOOK: "FACEBOOK",
    GOOGLE: "GOOGLE",
    GIIGAA_ID: "GIIGAA_ID"
};

const loginSteps = {
    LOGIN_WITH_SOCIAL_ACCOUNT: "LOGIN_WITH_SOCIAL_ACCOUNT",
    PHONE_INPUT: "PHONE_INPUT",
    VALIDATION_CODE_INPUT: "VALIDATION_CODE_INPUT"
};

const registrationSteps = {
    PHONE_INPUT: "PHONE_INPUT",
    VALIDATION_CODE_INPUT: "VALIDATION_CODE_INPUT",
    USER_INFO_INPUT: "USER_INFO_INPUT"
};

const userForgotPasswordStep = {
    PHONE_INPUT: "PHONE_INPUT",
    VALIDATION_CODE_INPUT: "VALIDATION_CODE_INPUT",
    RESTORE_PASSWORD_INPUT: "RESTORE_PASSWORD_INPUT",
    RESET_PASSWORD_SUCCESSFULLY: "RESET_PASSWORD_SUCCESSFULLY",
};

const redirectLinkTypes = {
    CATEGORY: "CATEGORY",
    PRODUCT: "PRODUCT",
    SEARCH: "SEARCH",
    LANDING_PAGE: "LANDING_PAGE",
    PROMOTED: "PROMOTED",
    DEAL: "DEAL"
};

const codePushDeploymentNames = {
    DEVELOPMENT: "development",
    STAGING: "staging",
    PRODUCTION: "production",
};

const dealTimeline = {
    CURRENT: "CURRENT",
    FUTURE: "FUTURE"
};

const dealSortBy = {
    CHEAPEST: "CHEAPEST",
    BEST_SALE: "BEST_SALE",
    CLOSEST_DATE: "CLOSEST_DATE"
};

const genders = {
    MALE: "MALE",
    FEMALE: "FEMALE"
};

const SOStatus = {
    DRAFT: "DRAFT",
    PENDING: "PENDING",
    PROCESSING: "PROCESSING",
    VERIFIED: "VERIFIED",
    PICKING: "PICKING",
    PACKED: "PACKED",
    DROPSHIP: "DROPSHIP",
    SHIPPING: "SHIPPING",
    RETURNED: "RETURNED",
    REFUND: "REFUND",
    COMPLETED: "COMPLETED",
    CANCEL: "CANCEL"
};

const orderManagerSteps = {
    CREATION: "CREATION",
    PICKING: "PICKING",
    SHIPPING: "SHIPPING",
    COMPLETED: "COMPLETED",
    CANCEL: "CANCEL",
    REFUND: "REFUND",
    RETURNED: "RETURNED"
};

const discountTypes = {
    MONEY: "MONEY",
    PERCENT: "PERCENT"
};

const productTypes = {
    SIMPLE: "SIMPLE",
    ORIGIN: "ORIGIN",
    CONFIG: "CONFIG",
    COMBO: "COMBO",
    GROUP: "GROUP"
};

const navigatorEvents = {
    WILL_APPEAR: "willAppear",
    DID_APPEAR: "didAppear",
    WILL_DISAPPEAR: "willDisappear",
    DID_DISAPPEAR: "didDisappear",
    WILL_COMMIT_PREVIEW: "willCommitPreview",
};

const addressDetailScreenTypes = {
    EDIT: "edit",
    CREATE: "create"
};

const productListViewTypes = {
    HEADER: "HEADER",
    TITLE: "TITLE",
    PRODUCTS_ROW: "PRODUCTS_ROW",
    PLACEHOLDER_SECTION: "PLACEHOLDER_SECTION",
    SECTION_HEADER: "SECTION_HEADER",
    HORIZONTAL_PRODUCT: "HORIZONTAL_PRODUCT",
    EMPTY_LIST_COMPONENT: "EMPTY_LIST_COMPONENT",
    FOOTER: "FOOTER"
};

const deepLinkHandlerTypes = {
    PRODUCT_DETAIL: "PRODUCT_DETAIL"
};

const pageTrackingType = {
    SHIPPING: "SHIPPING",
    PAYMENT: "PAYMENT"
};

export {
    dealTimeline,
    dealSortBy,
    productTypes,
    orderManagerSteps,
    SOStatus,
    loginSteps,
    registrationSteps,
    userForgotPasswordStep,
    loginTypes,
    imageTypes,
    productViewModes,
    paymentMethods,
    shippingMethods,
    redirectLinkTypes,
    codePushDeploymentNames,
    navigatorEvents,
    genders,
    discountTypes,
    addressDetailScreenTypes,
    productListViewTypes,
    deepLinkHandlerTypes,
    pageTrackingType
};
