import {getStoredDeploymentName} from "src/utils/codePush/codePush";

const configures = {
    "development": require("./developmentConfigs.json"),
    "staging": require("./stagingConfigs.json"),
    "production": require("./productionConfigs.json"),
};

const getSuitableConfigures = (deploymentName) => {
    const currentDeploymentName = deploymentName || getStoredDeploymentName();

    return configures[currentDeploymentName] || {};
};

console.warn(getSuitableConfigures());

export {
    getSuitableConfigures
};
