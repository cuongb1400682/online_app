package com.giigaa.online_app.debugutils;

import android.support.annotation.StringRes;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.common.MapBuilder;
import com.giigaa.online_app.BuildConfig;
import com.giigaa.online_app.R;

import java.util.Map;

import javax.annotation.Nullable;

public final class DebugUtilsModule extends ReactContextBaseJavaModule {
    private static final String MODULE_NAME = "DebugUtils";

    private static final String CURRENT_DEPLOYMENT_KEY = "currentDeploymentKey";
    private static final String FACEBOOK_APP_ID = "facebookAppId";
    private static final String GOOGLE_CLIENT_ID = "googleClientId";
    private static final String IS_DEBUG = "isDebug";

    public DebugUtilsModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return MODULE_NAME;
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        return MapBuilder.<String, Object>builder()
                .put(CURRENT_DEPLOYMENT_KEY, BuildConfig.CODEPUSH_KEY)
                .put(FACEBOOK_APP_ID, getResourceString(R.string.facebook_app_id))
                .put(GOOGLE_CLIENT_ID, getResourceString(R.string.google_client_id))
                .put(IS_DEBUG, BuildConfig.DEBUG)
                .build();
    }

    @ReactMethod
    public void logString(@Nullable String string) {
        Log.v(getName(), string);
    }

    private String getResourceString(@StringRes Integer id) {
        final ReactApplicationContext context = getReactApplicationContext();

        return context.getResources().getString(id);
    }
}
